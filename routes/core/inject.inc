<?php

/*
 * Creating images used in image replacement.
 * THIS JUST CREATES THE IMAGES!
 */
$app->get('/inject/:data', function($data) use ($app)
{
   $app->contentType("image/png");
   
   $errorMessage = "System Error";
   $errorIcon    = "error";

   $parts = explode("-",$data);
   if(count($parts) == 2)
   {
      $contentType = strtolower($parts[0]);
      $contentID = $parts[1];

      switch($contentType)
      {
         case "faq":
            $thisFAQ = new \Mumby\WebTools\FAQ();
            $faq = $thisFAQ->getFAQInfo($contentID);
            if(empty($faq))
            {
               $imgText = $errorMessage;
               $icon = $errorIcon;
            }
            else
            {
               $imgText = $faq[0]["FAQTitle"];
               $icon = "faq";
            }
            break;
         case "people":
            if(!$person = Mumby\DB\DeptMember::getPersonFromInfo($contentID))
            {
               $imgText = $errorMessage;
               $icon = $errorIcon;
               break;
            }

            if(!empty($person["preferred_name"]))
               $name = $person["preferred_name"];
            else
               $name = $person["first_name"];
            
            $name .= " ".$person["last_name"];
            
            $name = trim($name);
            if(empty($name))
            {
               $imgText = $errorMessage;
               $icon = $errorIcon;
            }
            else
            {
               $imgText = $name;
               $icon = "people";
            }
            break;
         case "form":
            $thisForm = new \Mumby\WebTools\Form($contentID);
            if(empty($thisForm) || empty($thisForm->formName))
            {
               $imgText = $errorMessage;
               $icon = $errorIcon;
            }
            else
            {
               $imgText = trim($thisForm->formName)." Form";
               $icon = "form";
            }
            break;
         case "data":
            $data = new \Mumby\WebTools\InjectionData();
            $injection = $data->getInjection($contentID);
            if(empty($injection) || empty($injection["InjectionValue"]))
            {
               $imgText = $errorMessage;
               $icon = $errorIcon;
            }
            else
            {
               $imgText = trim($injection["InjectionName"]);
               if(strpos($injection["InjectionValue"], "people-") === 0)
               {
                  $netid = substr($injection["value"], 7);
                  $person = Mumby\DB\DeptMember::getPersonFromInfo($netid);
                  $imgText .= " (".substr($person["first_name"],0,1).". ".$person["last_name"].")";
               }
               $icon = "data";
            }
            break;
         case "course":
            $courseMgr = new \Mumby\DB\Course();
            $course = $courseMgr->getCourseByID($contentID);
            
            if(empty($course))
            {
               $imgText = $errorMessage;
               $icon = $errorIcon;
            }
            else
            {
               $course = $course[0];
               $imgText = $course["SubjectPrefix"]." ".$course["CourseNumber"];
               $icon = "course";
            }
            break;
         default:
            $imgText = $errorMessage;
            $icon = $errorIcon;
            break;
      }
   }
   else
   {
      $imgText = $errorMessage;
      $icon = $errorIcon;
   }

   $img = new Mumby\WebTools\PlaceholderImage($imgText);
   $img->setBackgroundColor("#eee");
   $img->setTextColor("#666");
   $img->setTextSize(16);
   
   switch($icon)
   {
      case "people":
         $img->addPersonIcon();      
         break;
      case "faq":
         $img->addFAQIcon();      
         break;
      case "form":
         $img->addFormIcon();      
         break;
      case "data":
         $img->addGearIcon();
         break;
      case "course":
         $img->addCourseIcon();
         break;
      case "events":
         $img->addCalendarIcon();      
         break;
      case $errorIcon:
         $img->addErrorIcon();      
         break;
      default:
         break;
   }
   
   $img->render();
});