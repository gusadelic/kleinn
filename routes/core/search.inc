<?php
$app->get('/search(/)', function() use($app)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\Page();

   // Set up the content information
   $contentObj = array();
   $contentObj["title"]        = "Search";

   $query   = $app->request()->get('q');

   $pageContent = "";

   if(empty($query))
   {
      $page->setError("You didn't submit any search terms.");
      $pageContent .= insetSearchForm("", "Please enter your search terms into the box below before pushing the 'Search' button.");
   }
   else
   {
      $thisInjection = new \Mumby\WebTools\InjectionData();
      $hasGoogleConfig = $thisInjection->getInjection("googleCustomSearchCX");

      $contentObj["title"] .= " Results";
      $pageContent .= insetSearchForm($query, "");
      $pageContent .= "<div id='searchResults'>\n";

      if($hasGoogleConfig)
      {
         $pageContent .= "<script>
           (function() {
             var cx = '<!-- _MB_: googleCustomSearchCX -->';
             var gcse = document.createElement('script');
             gcse.type = 'text/javascript';
             gcse.async = true;
             gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
                 '//www.google.com/cse/cse.js?cx=' + cx;
             var s = document.getElementsByTagName('script')[0];
             s.parentNode.insertBefore(gcse, s);
           })();
         </script>
         <gcse:searchresults-only></gcse:searchresults-only>";
      }
      else
      {
         $page->setError("System Error: Missing Google Custom Search config information.");
         $pageContent .= "<em>We were unable to complete your search due to a system error.</em>";
      }

      $pageContent .= "</div>\n";

      $pageContent .= insetSearchForm($query, "");
   }
   
   $contentObj["content"] = $pageContent;
   
   $page->displayContent($contentObj);

});

if(!function_exists("insetSearchForm"))
{
   function insetSearchForm($query="", $text="You can also search our website using the form below:")
   {
      $thisInjection = new \Mumby\WebTools\InjectionData();
      $hasGoogleConfig = $thisInjection->getInjection("googleCustomSearchCX");
      
      // This should be defined as an injection variable.
      if($hasGoogleConfig)
      {
         $searchContent  = "";
         
         if(!empty($text))
            $searchContent  = "<p>\n".$text."\n</p>\n";
         
         $searchContent .= "<form class='insetSearch' action='/search' method='get'>\n";
         $searchContent .= "   <fieldset>\n";
         $searchContent .= "      <label for='q'><span>Search</span><input type='search' name='q' id='q' placeholder='Search' title='Search' value='".clean($query)."' /></label><input type='submit' class='submit fa fa-search' value='&#xf002;' />\n";
         $searchContent .= "<input type='hidden' name='cx' value='<!-- _MB_: googleCustomSearchCX -->' />\n";
         $searchContent .= "<input type='hidden' name='cof' value='FORID:9' />\n";
         $searchContent .= "<input type='hidden' name='ie' value='UTF-8' />\n";
         $searchContent .= "   </fieldset>\n";
         $searchContent .= "</form>\n";
      }
      else
         $searchContent = "<p class='errorMessage'>System Error: Missing Google Custom Search config information.</p>\n";

      return $searchContent;
   }
}