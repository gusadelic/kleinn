<?php

$app->get('(/people(/))', function() use($app)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\Page();
   
   $contentObj = array();
   $contentObj["title"]        = "People";
   $contentObj["parentLink"]   = "People";
   
   $page->addCSS("/themes/"._MB_THEME_."/css/profiles.css");

   $pageContent  = "";
   
   $peopleData = new Mumby\DB\DeptMember();
   $people = $peopleData->getDeptMembers(true, true);
   
   if(empty($people))
      $pageContent .= "<em>It appears that the people directory is not currently available. We apolgize for this inconvenience.</em>";
   else
   {
      $peopleList = makePeopleList($people);
      $pageContent .= $peopleList;
   }

   $contentObj["content"] = $pageContent;
   $page->displayContent($contentObj);
});

$app->get('(/people/directory(/))', function() use($app)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\Page();
   $page->addCSS("/themes/"._MB_THEME_."/css/profiles.css");

   $contentObj = array();
   $contentObj["title"]        = "Directory";
   $contentObj["parentLink"]   = "Directory";
   
   $pageContent  = "";

   $peopleData = new Mumby\DB\DeptMember();
   $people = $peopleData->getDeptMembers(true, true);

   if(empty($people))
      $pageContent .= "<em>It appears that the building directory is not currently available. We apolgize for this inconvenience.</em>";
   else
   {
      $peopleData = array();
      foreach($people as $p)
      {
         $thisPerson = array();
         
         $thisName = $p["LastName"].", ";
         if(!empty($p["PreferredName"]))
            $thisName .= $p["PreferredName"];
         else
            $thisName .= $p["FirstName"];

         $thisPerson["Name"] = "<span class='hidden'>".$p["LastName"]."</span><a href='/people/".strtolower($p["NetID"])."'>".$thisName."</a>";
         
         $category = trim($p["DeptRoleCategoryID"]);
         $thisCategory = "";
         switch($category)
         {
            case 1:
               $thisCategory .= "Faculty";
               break;
            case 2:
               $thisCategory .= $p["DeptRoleName"];
               break;
            case 3:
               $thisCategory .= "Staff";
               break;
            default:
               $thisCategory .= "";
               break;
         }
         
         $phone = array();
         $office = array();
         $email = "";
         
         if(!empty($p["UserData"]["emailAddresses"]))
         {
            foreach($p["UserData"]["emailAddresses"] as $e)
            {
               if($e["IsPrimaryAddress"])
                  $email = "<a href='mailto:".$e["EmailAddress"]."'>".$e["EmailAddress"]."</a>";
            }
         }
         
         if(!empty($p["UserData"]["phones"]))
         {
            foreach($p["UserData"]["phones"] as $ph)
            {
               if($ph["IsPublic"])
                  $phone[] = \Mumby\DB\Phone::formatPhoneNumber($ph["FullPhoneNumber"]);
            }
         }
         
         if(!empty($p["UserData"]["rooms"]))
         {
            foreach($p["UserData"]["rooms"] as $r)
            {
               $phone[] = \Mumby\DB\Phone::formatPhoneNumber($r["PhoneNumber"]);
               $thisOffice = "";
               if(!empty($r["BuildingAbbr"]))
                  $thisOffice .= $r["BuildingAbbr"];
               else
                  $thisOffice .= $r["BuildingName"];
               
               $thisOffice .= " ".$r["RoomNumber"];
               $office[] = $thisOffice;
            }
         }
         
         $thisPerson["Position"] = $thisCategory;
         $thisPerson["Phone"]    = implode("<br />", $phone);
         $thisPerson["Office"]   = implode("<br />", $office);
         $thisPerson["Email"]    = $email;
         
         $peopleData[] = $thisPerson;
      }
      
      $dataTable = new \Mumby\WebTools\Table();
      $isSortable = true;
      $showFilter = true;
      $pageContent .= $dataTable->getTable($peopleData, "peopleDirectory", "", $isSortable, $showFilter);
   }
   
   $contentObj["content"] = $pageContent;
   $page->displayContent($contentObj);
});

$app->get('(/people/:userID(/))', function($userID) use($app)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\Page();
   $page->addCSS("/themes/"._MB_THEME_."/css/profiles.css");

   $contentObj = array();
   $contentObj["title"]        = "";
   $contentObj["parentLink"]   = "People";
   $contentObj["contentClass"] = "profile";
   
      $person = Mumby\DB\DeptMember::getPersonFromInfo($userID);
      dump($person);
      die();
   
      $personCategory = ((string) $person->category);

      if($personCategory == "Graduate Student")
         $personCategory .= "s";

      if(!empty($personCategory))
         $contentObj["parentLink"]   = $personCategory;

      $pageContent  = "";

      $isActive = ((bool) trim($person->active));
      if(!$isActive)
         $page->setWarning("This person is no longer active within the department.");

      $personsName = trim(((string) $person->name));

      if(!$person || empty($personsName))
      {
         $app->pass();
      }
      else
      {
         $pageContent .= "<div class='row clearfix'>\n";
            $pageContent .= "<div class='col-md-2 text-center'>\n";
               $pageContent .= "<img src='".$person->img."' alt='".$person->name."' />\n";
            $pageContent .= "</div>\n\n";
         
         $pageContent .= "<div class='col-md-10'>\n";
         $pageContent .= "<div class='profileHeading'>\n";
         $pageContent .= "<h1>".$person->name."</h1>\n";

         if($person->category != "Graduate Student")
         {
            $titles = $person->titles;
            if(!empty($titles))
            {
               $titleStr = "";
               foreach($titles->title as $t)
               {
                  $thisTitle = trim($t);

                  // Skip empty titles
                  if(empty($thisTitle))
                     continue;

                  $titleStr .= $t."<br />";
               }
               if(empty($titleStr))
                  $titleStr = $person->category;
            
               $pageContent .= "<h2 class='position'>".$titleStr."</h2>\n";
            }
         }
         else
         {
            $pageContent .= "<h2 class='position'>".$person->category." - ".$person->subcategory."</h2>\n";
         }
         $pageContent .= "</div>\n";

         if(!empty($person->email))
            $pageContent .= "<div class='email'><strong>Email:</strong> <a href='mailto:".$person->email."'>".$person->email."</a></div>\n";

         $phone = ((string) $person->phone);
         if(!empty($phone))
         {
            $pageContent .= "<div class='phone'><strong>Telephone:</strong> ";
            $numsOnly = preg_replace('/[^0-9]/', '', $phone);
            if(strlen($numsOnly) == 7)
               $phone = "520-".$phone;
            $pageContent .= $phone;
            $pageContent .= "</div>\n";
         }
            
         $office = trim($person->office);
         if(!empty($office))
            $pageContent .= "<div class='office'><strong>Office:</strong> ".$office."</div>\n";

         $url = trim($person->url);
         if(!empty($url))
            $pageContent .= "<div class='homepage'><strong>Homepage:</strong> <a href='http:".$url."' class='external'>http:".$url."</a></div>\n";

         $pageContent .= "</div>\n";
         $pageContent .= "</div>\n\n";

         $bio = trim($person->bio);
         
         if(!empty($bio))
         {
            $pageContent .= "<div class='row'>\n";
            $pageContent .= "<div class='col-md-10 col-md-offset-2'>\n";
            $pageContent .= $bio;
            $pageContent .= "</div>\n";
            $pageContent .= "</div>\n\n";
         }

         $research = $person->research;
         if(!empty($research))
         {
            $pageContent .= "<div class='row'>\n";
            $pageContent .= "<div class='col-md-10 col-md-offset-2'>\n";
            $pageContent .= "<h2>Areas of Research</h2>\n";
            $pageContent .= "<ul>\n";

            foreach($research->topic as $t)
            {
               $pageContent .= "<li><a href='/research/".$t->code."'>".$t->name."</a></li>\n";
            }
            $pageContent .= "</ul>\n";
            $pageContent .= "</div>\n";
            $pageContent .= "</div>\n\n";
         }
      }

      $contentObj["content"] = $pageContent;
      $page->displayContent($contentObj);
});

if(!function_exists("makePeopleList"))
{
   function makePeopleList($people, $showAllGrads=true, $includeNav=true)
   {
      $letterLinksFlags = array();
      for($a=97; $a < (97+26); $a++)
         $letterLinksFlags[$a] = 0;

      $lastLetter = "";

      $peopleContent  = "";
      $peopleContent .= "<div class='peopleList'>";
         $peopleContent .= "<div class='row'>";
         $peopleCounter = 0;
         foreach($people as $p)
         {
            $peopleCounter++;
            $letter = substr(strtolower($p["LastName"]),0,1);
            $letterLinksFlags[ord($letter)] = 1;

            if($letter != $lastLetter)
            {
               $bulletID = "people_".$letter;
               $lastLetter = $letter;
            }
            else
               $bulletID = "";

            $peopleContent .= getProfile($p, $bulletID);

            if($peopleCounter%3 ==0)
               $peopleContent .= "</div><div class='row'>";
         }
         $peopleContent .= "</div>";
      $peopleContent .= "</div>";

      $letterLinks  = "<nav class='text-center letterLinks'>\n";
      $letterLinks .= "<ul class='pagination'>\n";
   
      foreach($letterLinksFlags as $chr=>$l)
      {
         $letterLinks .= "<li>";
         $letter = chr($chr);
         if($l)
            $letterLinks .= "<a href='#people_".$letter."'>".$letter."</a>\n";
         else
            $letterLinks .= "<span>".$letter."</span>\n";
         $letterLinks .= "</li>";
      }
      
      $letterLinks .= "</ul>\n";
      $letterLinks .= "</nav>\n";

      if($includeNav)
         $peopleContent = $letterLinks . $peopleContent;

      return $peopleContent;
   }
}

if(!function_exists("getProfile"))
{
   function getProfile($p, $containerID="", $inline=false)
   {
      if(!empty($p["PreferredName"]))
         $personsName = $p["PreferredName"];
      else
         $personsName = $p["FirstName"];
      $personsName .= " ".$p["LastName"];

         $peopleContent = "<li";
         if(!empty($containerID))
            $peopleContent .= " id='".$containerID."'";

         $peopleContent .= ">";
         $peopleContent .= "<div class='nameAndTitle'>";
            $peopleContent .= "<h2><a href='/people/".$p["NetID"]."'>".$personsName."</a></h2>";
            $peopleContent .= "</div>";
            $peopleContent .= "<div class='contactInfo'>";

//            $peopleContent .= "<span class='mail'><a href='mailto:".$p->email."'>".$p->email."</a></span>";
//            $phone = $p->phone;
//            if(!empty($phone) && strlen($phone) > 5)
//               $peopleContent .= "<span class='phone'>".$p->phone."</span>";
//            if(!empty($p->office))
//               $peopleContent .= "<span class='office'>".$p->office."</span>";
//            if(!empty($p->url))
//               $peopleContent .= "<span class='homepage'><a href='".$p->url."' class='external'>Homepage</a></span>";

         $peopleContent .= "</div>";
         $peopleContent .= "</li>";

         if($inline)
         {
            $peopleContent = str_replace('<li', '<div', $peopleContent);
            $peopleContent = str_replace('/li>', '/div>', $peopleContent);
         }

         return $peopleContent;
   }
}