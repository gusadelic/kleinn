<?php
/*
 * Show page-related functions.
 */
$app->get('(/admin/pages(/))', function() use($app, $user)
{
   // Create the Page instance
   $pageTitle = "Web Pages";
   $page  = new Mumby\WebTools\AdminPage($pageTitle, $user, _MB_ROLE_EDIT_WEB_PAGES_);
   
   if($page->isAuthorized())
   {
      $pageContent = array();
      $pageContent["title"] = $pageTitle;
      
      $userOptions = array();
      $userOptions[] = array(
          "label" => "View/Edit Web Pages",
          "link"  => "/admin/pages/view",
          "desc"  => "View a list of all existing web pages. From this page, you can view current information about each web page, edit web page content, and revert to previous versions of a given web page."
      );

      $userOptions[] = array(
          "label" => "Add Page",
          "link"  => "/admin/pages/add",
          "desc"  => "Create a new web page."
      );
      
      if($user->hasPermission(_MB_ROLE_DELETE_WEB_PAGES_))
         $userOptions[] = array(
             "label" => "Delete Web Pages",
             "link"  => "/admin/pages/delete",
             "desc"  => "Delete (or undelete) web pages."
         );
      
      $pageContent["content"] = "<div class='adminOptionWrapper'>\n";
      $pageContent["content"] .= buildLandingPage($userOptions);
      $pageContent["content"] .= "</div>\n";
      
//      $pageContent["content"] .= "<ul>\n";
//      $pageContent["content"] .= "<li>TODO:</li>\n";
//      $pageContent["content"] .= "<li>Figure out what drafts look like</li>\n";
//      $pageContent["content"] .= "<li>Figure out what published/unpublished looks like</li>\n";
//      $pageContent["content"] .= "<li>Figure out where page permissions would go</li>\n";
//      $pageContent["content"] .= "</ul>\n";
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * Add route for displaying pages by id.
 */
$app->get('(/admin/pages/id/:id(/))', function($pageID) use($app, $user)
{
   $page  = new Mumby\WebTools\AdminPage("Web Pages", $user, _MB_USER_AUTHENTICATED_);
   
   if($page->isAuthorized())
   {
      $pageID = ((int) $pageID);
      $content = new Mumby\WebTools\Content();
      $thisPage = $content->getContent($pageID);

      if(!empty($thisPage))
         $app->redirect($thisPage["currentURL"]);
      else
         $app->redirect('/404', 404);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * List all undeleted pages in the system (including unpublished pages).
 */
$app->get('(/admin/pages/view(/))', function() use($app, $user)
{
   // Create the Page instance
   $pageTitle = "View Web Pages";
   $page  = new Mumby\WebTools\AdminPage("Web Pages", $user, _MB_ROLE_EDIT_WEB_PAGES_);
   
   if($page->isAuthorized())
   {
      $pageContent = array();
      $pageContent["title"] = "View Web Pages";
      
      $pageContent["content"]  = "<p>Below is a list of all of the pages available on this website. You can edit the conent of any page by clicking on the title of the page. Additional operations can be found in the last column of the table.</p>\n";
      $pageContent["content"] .= "<p>If you would like to create a new page, please visit <a href='/admin/pages/add'>Add New Page</a>.</p>\n";
 
      $contentMgmt = new Mumby\WebTools\Content();
      $allPages = $contentMgmt->getAllContent();
      //$allUsers = Mumby\WebTools\ACL\LocalUser::getAllUsers();
      
      if(!empty($allPages))
      {
         
         $pageData = array();
         foreach($allPages as $p)
         {
            $pageTitle  = "<a href='/admin/pages/edit/".$p["pageID"]."' class='editableItemLink'><i class='glyphicon glyphicon-pencil'></i> ".$p["title"]."</a>";
            $author     = str_replace(" ","&nbsp;", $p["authorUsername"]);
            $time       = "<span class='hidden'>".strtotime($p["lastUpdated"])."</span> ".str_replace(' ','&nbsp;', date('F j, Y g:ia', strtotime($p["lastUpdated"])));
            $status     = ($p["published"] ? "Published" : "Draft");
            $operations = "";
               $operations .= "<a href='".$p["url"]."' class='newWindow'>[view]</a>&nbsp;";
               $operations .= "<a href='/admin/pages/copy/".$p["pageID"]."'>[copy]</a>&nbsp;";
               $operations .= "<a href='/admin/pages/view/history/".$p["pageID"]."'>[history]</a>&nbsp;";
               $type = strtok($contentMgmt->PageTypes[$p['PageTypeID']],"-");

            $thisPerson = Mumby\WebTools\ACL\LocalUser::getUsersName($p["authorUsername"]);
            
            if(empty($thisPerson))
               $author = "Unknown";
            else
            {         
               $author = $thisPerson;
            }
            
            $pageData[] = array(
               "Title"        => $pageTitle,
               "Author"       => $author,
               "Last Updated" => $time,
               "Status"       => $status,
               "Operations"   => $operations
            );
         }
         
         $dataTable = new \Mumby\WebTools\Table();
         $pageContent["content"] .= $dataTable->getTable($pageData, "", "small table-striped", true, true);

      }
      else
         $pageContent["content"] .= "<p><em>No web pages found!</em></p>";
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * View page history
 */
$app->get('(/admin/pages/view/history/:pageID(/))', function($pageID) use($app, $user)
{
   /*
    * Display basic page details (title, current url, last modified date, created date.
    * 
    * Show all revisions, including author, date, notes, and two links:
    *    - view the given revision (opens in a new window). (/admin/pages/view/:pageID/:revID)
    *    - roll back to the given revision.
    */
   
   if(!ctype_digit($pageID))
      $app->pass();
   
   // Create the Page instance
   $pageTitle = "Web Pages";
   $page  = new Mumby\WebTools\AdminPage("Web Pages", $user, _MB_ROLE_EDIT_WEB_PAGES_);
   
   if($page->isAuthorized())
   {
      $contentMgmt = new Mumby\WebTools\Content();
      
      $pageHistory = $contentMgmt->getAllRevisions($pageID);
      if(empty($pageHistory))
         $app->pass();
      
      $pageContent = array();
      $pageContent["title"] = $pageHistory[0]["title"]." - Page History";
      
      $pageContent["content"]  = "<p>Below is a list of all revisions that have been made to this page. Click on the revision link to view the revision.</p>\n";

      $revCounter = count($pageHistory);

      $revHistory = array();
      foreach($pageHistory as $k=>$p)
      {
         $revision = "";
         $revision .= "<a href='/admin/pages/view/".((int) $p["pageID"])."/";

         if($k == 0)
            $revision .= ((int) $k);
         else
            $revision .= ((int) $p["revID"]);
         $revision .= "'> <span class='hidden'>";
         $revision .= str_repeat("0", 10-strlen($k));
         $revision .= $k;
         $revision .= "</span>";
         if($k == 0)
            $revision .= "<span class='fa fa-star'></span>";
         $revision .= "Revision&nbsp;".$revCounter."</a>";

         $thisPerson = Mumby\WebTools\ACL\LocalUser::getUsersName($p["authorUsername"]);
         dump($thisPerson);die();
         if(empty($thisPerson))
            $author = "Unknown";
         else
         {
            if(!empty($thisPerson["preferred_name"]))
               $author = $thisPerson["preferred_name"];
            else
               $author = $thisPerson["first_name"];

            $author .= " ".$thisPerson["last_name"];
         }
            
         $time      = "<span class='hidden'>".strtotime($p["lastUpdated"])."</span> ".str_replace(' ','&nbsp;', date('F j, Y g:ia', strtotime($p["lastUpdated"])));

         $revHistory[] = array(
            "Revision"      => $revision,
            "Author"        => $author,
            "Revision Date" => $time,
            "Notes"         => nl2br($p["notes"])
         );
         
         $revCounter--;
      }
      
      $dataTable = new \Mumby\WebTools\Table();
      $pageContent["content"] .= $dataTable->getTable($revHistory, "", "", true);

      $pageContent["content"] .= "<ul class='list-unstyled'>";
      $pageContent["content"] .= "<li><span class='fa fa-star'></span> Current Published revision</li>";
      $pageContent["content"] .= "</ul>";
      
      $page->displayContent($pageContent);
   }
   else {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
   
});

/*
 * Display a specific revision
 */
$app->map('(/admin/pages/view/:pageID/:revID(/))', function($pageID, $revID="-1") use($app, $user)
{
   if(!ctype_digit($pageID) || !ctype_digit($revID))
      $app->pass();
   
   $pageID = (int) $pageID;
   $revID  = (int) $revID;
   
   // Create the Page instance
   $pageTitle = "Web Pages";
   $page  = new Mumby\WebTools\AdminPage("Web Pages", $user, _MB_ROLE_EDIT_WEB_PAGES_);
   
   if($page->isAuthorized())
   {
      $page->hideAdminMenu();
      
      $contentMgr = new Mumby\WebTools\Content();
      if(empty($revID))
         $content = $contentMgr->getContent($pageID);
      else
         $content = $contentMgr->getContent($pageID, $revID);
      if(empty($content))
         $app->pass();
      
      $revisionForm = new Mumby\WebTools\SystemForm();
      $pageField = $revisionForm->addHiddenField("Page ID", array("value"=>$pageID));
      $revField  = $revisionForm->addHiddenField("Revision ID", array("value"=>$revID));
      
      if($revisionForm->submitted() && !empty($revID))
      {
         $revisionForm->processFormData();

         if($revisionForm->processedData)
         {
            $submittedPageID = $pageField->getValue();
            $submittedRevID  = $revField->getValue();
            
            if(!$contentMgr->revertToRevision($submittedPageID, $submittedRevID, trim($user->username)))
            {
               $errorMessages = $contentMgr->getErrors();
               if(!empty($errorMessages))
               {
                  foreach($errorMessages as $e)
                  {
                     $app->flash("error", $e);
                  }
               }
               else
               {
                  $app->flash("error", "<div class='errorMessage'>There was an error reverting to the revision.</div>");
               }
            }
            else
            {
               $app->flash("success", "Reverted to previous reversion successfully.");
            }
            
            $app->redirect("/admin/pages/view/history/".((int) $pageID));
         }
         else
         {
            $errorMessages = $addPageForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $page->setError($e);
               }
            }
            else
            {
               $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
            }
         }
      }
      
      if(!empty($revID))
         $page->setWarning("You are viewing an older revision published on <strong>".date("F j, Y", strtotime($content["created"]))."</strong>.", true);
      else
         $page->setSuccess("You are viewing the current published version of this page.", true);

      $page->setInfo("<strong>Revision Notes:</strong> <em>".nl2br(clean($content["notes"]))."</em>", true);
      
      $content["content"] = "<div class='row'>".$content["content"]."</div>";
              
      if(!empty($revID))
      {
         $content["content"] .= "<div class='well text-center'>\n";
         $revisionForm->submitClass = "btn btn-success";
         $content["content"] .= $revisionForm->generateHiddenForm("Revert to this revision");
         $content["content"] .= "<a class='center-block' href='/admin/pages/view/history/".((int) $pageID)."'>Return to the page history.</a>";
         $content["content"] .= "</div>\n";
      }
      else
      {
         $content["content"] .= "<div class='alert alert-success alert-dismissible' role='alert'>";
         $content["content"] .= "   <button type='button' class='close' data-dismiss='alert' aria-label='Close'>\n";
         $content["content"] .= "      <span aria-hidden='true'>&times;</span>\n";
         $content["content"] .= "   </button>\n";
         $content["content"] .= "You are viewing the current published version of this page.</div>";
         $content["content"] .= "<div class='well text-center'>\n";
         $content["content"] .= "<a class='center-block' href='/admin/pages/view/history/".((int) $pageID)."'>Return to the page history.</a>";
         $content["content"] .= "</div>\n";
      }
      
      $page->displayContent($content);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Add a new page
 */
$app->map('(/admin/pages/add(/))', function() use($app, $user)
{
   $showPage = true;

   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Web Pages", $user, _MB_ROLE_EDIT_WEB_PAGES_);
   
   if($page->isAuthorized())
   {

      $contentMgr = new Mumby\WebTools\Content();
      $addPageForm = new Mumby\WebTools\SystemForm();

      //$pageType = $addPageForm->addSelectField("Page Type", $contentMgr->PageTypes, true);

      $pageTitle   = $addPageForm->addTextField("Page Title", true);
      $pageURL     = $addPageForm->addTextField("Page URL", true, array("placeholder"=>"/example/page-address"));
      
      $navMgr = new \Mumby\WebTools\Menu(2);
      $allNav = $navMgr->getNavigationAsArray();
      
      $navOptions = array();
      foreach($allNav as $n)
      {
         if($n["MenuItemDepth"] == 0)
            continue;
         
         $navOptions[$n["MenuItemID"]] = str_repeat("|--", $n["MenuItemDepth"])." ".$n["MenuItemText"];
      }

      
      $parentLink = $addPageForm->addSelectField("Menu Item", $navOptions, true, array("moreLabel"=>"Which menu item should the page be associated with?"));

      $configMgr = new Mumby\WebTools\WYSIWYG_Config();
      $configMgr->fullFeatured(true);
      
      if($user->hasPermission(_MB_ROLE_EDIT_WEB_PAGE_SOURCE_))
         $configMgr->allowViewSource(true);
      else
         $configMgr->allowViewSource(false);
      
      $wysiwygConfig = array("config"=>$configMgr->getJSONConfig());
      $wysiwygConfig["moreLabel"] = "For best results, we recommend editing using the 'Full Screen' view (press 'Ctrl+Alt+F' while in the editor).";
              
      $pageContent = $addPageForm->addHTMLField("Page Content", true, $wysiwygConfig);
      $pagePreview = $addPageForm->addHiddenField("Previewed?");
      
      

      if($addPageForm->submitted())
      {
         $addPageForm->processFormData();

         if($addPageForm->processedData)
         {
            $submittedPageTitle  = $pageTitle->getValue();
            $submittedPageURL    = $pageURL->getValue();
            $submittedParentLink = $parentLink->getValueKey();
            //$submittedPageType   = $pageType->getValueKey();
            $submittedContent    = $pageContent->getValue();

            $existingURL = $contentMgr->urlExists($submittedPageURL);
            if($existingURL === false)
            {
               $previewedAlready = $pagePreview->getValue();
               if($previewedAlready == false)
               {
                  $pagePreview->setValue("1", true);

                  // Show preview page.
                  $showPage = false;
                  $page->addJS("/common/js/prevPageLinks.js");
                  $page->hideAdminMenu();

                  $pageContent = array();
                  $pageContent["title"] = $submittedPageTitle;               
                  $pageContent["parentLink"] = $submittedParentLink;

                  $page->setWarning("Below is the preview of your new webpage. Please note that the page has not been created yet! In order to save the page, please click the button at the bottom of the page that says 'Save Page'.");

                  // We manually set image replacements here to accurately preview the page content.
                  $pageContent["content"]  = "<div class='row'>";
                  $pageContent["content"] .= $page->setImageReplacements($submittedContent);
                  $pageContent["content"] .= "</div>\n";
                  $pageContent["content"] .= "<div class='well text-center'>\n";
                  $pageContent["content"] .= $addPageForm->generateHiddenForm("Save Page");
                  $pageContent["content"] .= "<a class='prevPageLink' href='#'>Return to editing the page.</a>";
                  $pageContent["content"] .= "</div>\n";

                  // We disable image replacements here so that we don't
                  // actually remove it from the submitted data.
                  $page->displayContent($pageContent, 200, true);
               }
               else
               {
                  $data = array(
                      "pageTitle"  => $submittedPageTitle,
                      "username"   => $user->username,
                      "parentLink" => $submittedParentLink,
                      "PageTypeID" => "1",
                      "url"        => $submittedPageURL,
                      "content"    => $submittedContent,
                      "notes"      => "Initial page creation."
                  );

                  if(!$contentMgr->addContent($data))
                  {
                     $app->flash("error", "We were unable to add the new webpage.");
                  }
                  else
                  {
                     $app->flash("success", "Added <a href='".clean($submittedPageURL)."'>".clean($submittedPageTitle)."</a> to the website.");
                  }
                  $app->redirect("/admin/pages/view");
               }
            }
            else
            {
               // Set page URL back to the original
               $page->setError("The URL specified, '<a href='".clean($submittedPageURL)."'>".clean($submittedPageURL)."</a>', is already in use by another webpage.");
            }
         }
         else
         {
            $errorMessages = $addPageForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $page->setError($e);
               }
            }
            else
            {
               $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
            }
         }
      }

      if($showPage)
      {
         $pageContent = array();
         $pageContent["title"] = "Add New Webpage";      
         $pageContent["content"]  = "<p>Use the form below to add a new webpage to the site.</p>\n";
         $pageContent["content"] .= $addPageForm->generateForm();//"<img src='/inject/sysform-3' />";

         // Add injection options.
         $page->addJS("/js/injectionData.js");

         $page->displayContent($pageContent, 200, true);
      }
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Edit an existing page
 */
$app->map('(/admin/pages/edit/:pageID(/))', function($pageID) use($app, $user)
{
   $showPage = true;

   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Web Pages", $user, _MB_ROLE_EDIT_WEB_PAGES_);
   
   if($page->isAuthorized())
   {
      $pageID = (int) $pageID;

      $contentMgr = new Mumby\WebTools\Content();
      $existingPage = $contentMgr->getContent($pageID);

      $editPageForm = new Mumby\WebTools\SystemForm();

      //$pageType = $editPageForm->addSelectField("Page Type", $contentMgr->PageTypes, true);

      $pageTitle    = $editPageForm->addTextField("Page Title", true);
      $pageURL      = $editPageForm->addTextField("Page URL", true, array("placeholder"=>"/example/page-address"));
      
      $navMgr = new \Mumby\WebTools\Menu(2);
      $allNav = $navMgr->getNavigationAsArray();
      
      $navOptions = array();
      foreach($allNav as $n)
      {
         if($n["MenuItemDepth"] == 0)
            continue;
         
         $navOptions[$n["MenuItemID"]] = str_repeat("|--", $n["MenuItemDepth"])." ".$n["MenuItemText"];
      }
      
      $parentLink = $editPageForm->addSelectField("Menu Item", $navOptions, true, array("moreLabel"=>"Which menu item should the page be associated with?"));

      $configMgr = new Mumby\WebTools\WYSIWYG_Config();
      $configMgr->fullFeatured(true);
      
      if($user->hasPermission(_MB_ROLE_EDIT_WEB_PAGE_SOURCE_))
         $configMgr->allowViewSource(true);
      else
         $configMgr->allowViewSource(false);
      
      $wysiwygConfig = array("config"=>$configMgr->getJSONConfig());
      $wysiwygConfig["moreLabel"] = "For best results, we recommend editing using the 'Full Screen' view (press 'Ctrl+Alt+F' while in the editor).";
              
      $pageContent = $editPageForm->addHTMLField("Page Content", true, $wysiwygConfig);
      $pagePreview = $editPageForm->addHiddenField("Previewed?");
      
      $pageTitle->setValue($existingPage["title"]);
      $pageURL->setValue($existingPage["currentURL"]);
      $parentLink->setValue($existingPage["parentLink"]);
      $pageContent->setValue($existingPage["content"]);
      //$pageType->setValue($existingPage['PageTypeID']);
      
      $pageNotes = $editPageForm->addTextAreaField("Notes", true, array("rows"=>4));

      if($editPageForm->submitted())
      {
         $editPageForm->processFormData();

         if($editPageForm->processedData)
         {
            $submittedPageTitle  = $pageTitle->getValue();
            $submittedPageURL    = $pageURL->getValue();
            //$submittedPageType   = $pageType->getValueKey();
            $submittedParentLink = $parentLink->getValueKey();
            $submittedContent    = $pageContent->getValue();
            $submittedNotes      = $pageNotes->getValue();
            
            $existingURL = $contentMgr->urlExists($submittedPageURL);
            if($existingURL === false || $existingURL == $pageID)
            {
               $previewedAlready = $pagePreview->getValue();

               if($previewedAlready == false)
               {
                  $pagePreview->setValue("1", true);
                  
                  // Not sure if this is a good idea...
                  header("X-XSS-Protection: 0");

                  // Show preview page.
                  $showPage = false;
                  $page->addJS("/common/js/prevPageLinks.js");
                  $page->hideAdminMenu();

                  $pageContent = array();
                  $pageContent["title"] = $submittedPageTitle;               
                  $pageContent["parentLink"] = $submittedParentLink;

                  $page->setWarning("Below is the preview of your webpage update. Please note that the update has NOT been saved yet! In order to save the update, please click the button at the bottom of the page that says 'Save Page'.");

                  $pageContent["content"]  = "<div class='row'>";
                  $pageContent["content"] .= $page->setImageReplacements($submittedContent);
                  $pageContent["content"] .= "</div>\n";
                  $pageContent["content"] .= "<div class='well text-center'>\n";
                  $pageContent["content"] .= $editPageForm->generateHiddenForm("Save Page");
                  $pageContent["content"] .= "<a class='prevPageLink' href='#'>Return to editing the page.</a>";
                  $pageContent["content"] .= "</div>\n";

                  $page->displayContent($pageContent, 200, true);
               }
               else
               {
                  $data = array(
                      "username"   => $user->username,
                      "content"    => $submittedContent,
                      "notes"      => $submittedNotes,
                      "PageTypeID" => "1",
                      "pageTitle"  => $submittedPageTitle,
                      "url"        => $submittedPageURL,
                      "parentLink" => $submittedParentLink
                  );

                  if(!$contentMgr->updateContent($pageID, $data))
                  {
                     $errorMessages = $contentMgr->getErrors();
                     if(!empty($errorMessages))
                     {
                        foreach($errorMessages as $e)
                        {
                           $app->flash("error", $e);
                        }
                     }
                     else
                     {
                        $app->flash("error", "<div class='errorMessage'>There was an error updating the webiste</div>");
                     }
                  }
                  else
                  {
                     $app->flash("success", "Updated <a href='".clean($submittedPageURL)."'>".clean($submittedPageTitle)."</a>.");
                  }

                  $app->redirect("/admin/pages/view");
               }
            }
            else
            {
               // Set page URL back to the original
               $pageURL->setValue($existingPage["currentURL"], true);
               $page->setError("The URL specified, '<a href='".clean($submittedPageURL)."'>".clean($submittedPageURL)."</a>', is already in use by another webpage.");
            }
         }
         else
         {
            $errorMessages = $editPageForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $page->setError($e);
               }
            }
            else
            {
               $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
            }
         }
      }

      if($showPage)
      {
         $pageContent = array();
         $pageContent["title"] = "Update Webpage";      
         $pageContent["content"]  = "<p>Use the form below to update this webpage's content.</p>\n";
         $pageContent["content"] .= $editPageForm->generateForm();

         // Add injection options.
         $page->addJS("/js/injectionData.js");

         $page->displayContent($pageContent, 200, true);
      }
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Create a copy of a page
 */
$app->map('(/admin/pages/copy/:pageID(/))', function($pageID) use($app, $user)
{
   // Get User
   $showPage = true;

   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Web Pages", $user, _MB_ROLE_EDIT_WEB_PAGES_);
   
   if($page->isAuthorized())
   {
      $contentMgr = new Mumby\WebTools\Content();
      $oldPage = $contentMgr->getContent($pageID);
      if(empty($oldPage))
         $app->pass();

      $copyPageForm = new Mumby\WebTools\SystemForm();
      $pageTitle    = $copyPageForm->addTextField("Page Title", true);
      $pageTitle->setValue($oldPage["title"]." [copy]");
      
      $pageURL      = $copyPageForm->addTextField("Page URL", true);
      
      $copyPageForm->submitValue = "Copy Page";
      
      if($copyPageForm->submitted())
      {
         $copyPageForm->processFormData();

         if($copyPageForm->processedData)
         {            
            $submittedPageTitle  = $pageTitle->getValue();
            $submittedPageURL    = $pageURL->getValue();
            
            $existingURL = $contentMgr->urlExists($submittedPageURL);
            if($existingURL === false)
            {
               if(!$contentMgr->copyContent($pageID, $submittedPageURL, trim($user->username), $submittedPageTitle))
               {
                  $app->flash("error", "We were unable to copy the webpage.");
               }
               else
               {
                  $app->flash("success", "Copied <a href='".clean($submittedPageURL)."'>".clean($oldPage["title"])."</a> successfully.");
               }

               $app->redirect("/admin/pages/view");
            }
            else
            {
               // Set page URL back to the original
               $page->setError("The URL specified, '<a href='".clean($submittedPageURL)."'>".clean($submittedPageURL)."</a>', is already in use by another webpage.");
            }
         }
         else
         {
            $errorMessages = $copyPageForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $page->setError($e);
               }
            }
            else
            {
               $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
            }
         }
      }

      if($showPage)
      {
         $pageContent = array();
         $pageContent["title"] = "Copy Webpage";      
         $pageContent["content"]  = "<p>Please use the form to provide a URL and (optionally) a new title for the new copy.</p>\n";
         $pageContent["content"] .= $copyPageForm->generateForm();//"<img src='/inject/sysform-3' />";
         $page->displayContent($pageContent);
      }
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Delete pages.
 */
$app->map('(/admin/pages/delete(/))', function() use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Web Pages", $user, _MB_ROLE_EDIT_WEB_PAGES_);
   
   if($page->isAuthorized())
   {
      $contentMgmt = new Mumby\WebTools\Content();
      $allPagesFullData = $contentMgmt->getAllContent(1, true);

      $tmp = array();
      foreach($allPagesFullData as $k=>$v)
         $tmp[$k] = $v["title"];
      
      array_multisort($tmp, SORT_ASC, $allPagesFullData);

      $previouslyDeletedPages   = array();
      $previouslyUndeletedPages = array();
      $allPages = array();
      foreach($allPagesFullData as $k=>$p)
      {
         if($p["deleted"])
            $previouslyDeletedPages[] = $p["pageID"];
         else
            $previouslyUndeletedPages[] = $p["pageID"];

         $allPages[$p["pageID"]] = "<a href='".$p["url"]."' class='newWindow'>".$p["title"]."</a> (".date("F j, Y", strtotime($p["lastUpdated"])).")";
      }
         
      $deletePagesForm = new Mumby\WebTools\SystemForm();
      $pagesStatus = $deletePagesForm->addCheckboxField("Pages", $allPages, false, array("hiddenLabel"=>true, "checked"=>$previouslyDeletedPages));
      
      if($deletePagesForm->submitted())
      {
         $deletePagesForm->processFormData();

         if($deletePagesForm->processedData)
         {
            $success = true;

            $deletedPages = $pagesStatus->getValueKey();
            
            // Update any newly deleted pages.
            foreach($deletedPages as $dp)
            {
               if(!in_array($dp, $previouslyDeletedPages))
                  $success &= $contentMgmt->deleteContent($dp);
            }
            
            // Update any newly 'undeleted' pages.
            foreach($previouslyDeletedPages as $pdp)
            {
               if(!in_array($pdp, $deletedPages))
                  $success &= $contentMgmt->undeleteContent($pdp);               
            }

            if($success)
               $app->flash("success","Updated deleted pages.");
            else
               $app->flash("error","Unable to updated deleted pages.");
            
            $app->redirect("/admin/pages/delete");
         }
         else
         {
            $errorMessages = $deletePagesForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $page->setError($e);
               }
            }
            else
            {
               $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
            }
         }
      }

      $page->setWarning("Please note that any pages that are checked on the form below will not be viewable by any users <strong>or website editors</strong>!");

      $pageContent = array();
      $pageContent["title"] = "Delete/Undelete Pages";
      $pageContent["content"]  = "<p>Use the form below to delete/undelete pages.</p>\n";
      $pageContent["content"] .= $deletePagesForm->generateForm();

      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');