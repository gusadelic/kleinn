<?php

/*
 * Show admin properties.
 */
$app->get('(/admin/properties(/))', function() use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Application Properties", $user, _MB_USER_ADMIN_);
   
   if($page->isAuthorized())
   {
      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"] = "Application Properties";
      
      $userOptions = array();
      $userOptions[] = array(
          "label" => "Set Application ID",
          "link"  => "/admin/appID",
          "desc"  => "Set the unique ID for this application."
      );
      
      $userOptions[] = array(
          "label" => "View/Edit Application Properties",
          "link"  => "/admin/properties/view",
          "desc"  => "View a list of all application properties."
      );

      $userOptions[] = array(
          "label" => "Add Application Property",
          "link"  => "/admin/properties/add",
          "desc"  => "Create a new application properties."
      );
      
      $pageContent["content"] = "<div class='adminOptionWrapper'>\n";
      $pageContent["content"] .= buildLandingPage($userOptions);
      $pageContent["content"] .= "</div>\n";
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * Show admin properties.
 */
$app->get('(/admin/properties/view(/))', function() use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Application Properties", $user, _MB_USER_ADMIN_);
   
   if($page->isAuthorized())
   {
      $app = new \Mumby\DB\Application(_MB_WEB_APPLICATION_ID_);
      $properties = $app->getApplicationProperties();

      $tableData = array();
      foreach($properties as $p)
      {
         $thisProp = array(
            "Property Name" => "<span class='hidden'>".($p["IsRequiredBySystem"]*-1)."</span><a href='/admin/properties/edit/".$p["ApplicationPropertyID"]."'>".$p["PropertyName"]."</a>",
            "Description" => $p["PropertyDescription"],
            "Constant" => $p["PropertyConstant"],
            "Value" => $p["PropertyValue"]
         );
         
         $thisProp["Operations"] = "";
         $thisProp["Operations"] .= "<a href='/admin/properties/edit/".$p["ApplicationPropertyID"]."'>[edit]</a> ";
         if(!$p["IsRequiredBySystem"])
            $thisProp["Operations"] .= "<a href='/admin/properties/delete/".$p["ApplicationPropertyID"]."'>[delete]</a>";
         
         $tableData[] = $thisProp;
      }
      
      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"] = _MB_APP_NAME_." - Application Properties";
      $pageContent["content"] = "<p>Below is a list of properties for this application. You can edit any of the properties listed below. However, you can only delete those properties that are not required by the system.</p>";
      $pageContent["content"] .= "<p>You can also add a new application property by visiting the <a href='/admin/properties/add'>Add Application Property page</a>.</p>";

      $tblMgr = new \Mumby\WebTools\Table();
      $pageContent["content"] .= $tblMgr->getTable($tableData, "", "small", true);
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * Add new admin property.
 */
$app->map('(/admin/properties/add(/))', function() use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Application Properties", $user, _MB_USER_ADMIN_);
   
   if($page->isAuthorized())
   {
      $propMgr = new \Mumby\DB\Application(_MB_WEB_APPLICATION_ID_);

      $propForm  = new Mumby\WebTools\SystemForm();
      $propForm->submitValue = "Add Property";
      $propName  = $propForm->addTextField("Property Name", true);
      $propDesc  = $propForm->addTextAreaField("Description", false);
      $propConst = $propForm->addTextField("Constant", true, array("moreLabel"=>"This allows the property to be used within the source code/theme of the application.", "placeholder"=>"_MB_CONSTANT_NAME_"));
      $propValue = $propForm->addTextField("Property Value", true, array("moreLabel"=>"If specifying a boolean value, use 'boolean:true' or 'boolean:false' rather than 0 or 1."));

      if($propForm->submitted())
      {
         $propForm->processFormData();

         if($propForm->processedData)
         {
            $n = $propName->getValue();
            $d = $propDesc->getValue();
            $c = $propConst->getValue();
            $v = $propValue->getValue();
            
            if(defined($c))
            {
               $page->setError("The constant you defined is already in use within the system.");
            }
            else
            {
               if($propMgr->addApplicationProperty($n, $d, $c, $v) === false)
                  $app->flash("error", "We were unable to add '".clean($n)."' to the application properties.");
               else
                  $app->flash("success", "Added '".clean($n)."' to the application properties.");

               $app->redirect("/admin/properties/view");

            }
         }
         else
         {
            $errorMessages = $propForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $page->setError($e);
               }
            }
            else
            {
               $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
            }
         }
      }
      
      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"] = clean(_MB_APP_NAME_)." - Add Application Property";
      $pageContent["content"] = "<p>Use the form below to add a new property to the ".clean(_MB_APP_NAME_)." application.</p>";

      $pageContent["content"] .= $propForm->generateForm();
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Update admin property.
 */
$app->map('(/admin/properties/edit/:id(/))', function($id) use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Application Properties", $user, _MB_USER_ADMIN_);
   
   if($page->isAuthorized())
   {
      $propMgr = new \Mumby\DB\Application(_MB_WEB_APPLICATION_ID_);

      $thisProperty = $propMgr->getApplicationProperty($id);
      if(empty($thisProperty))
      {
         $app->flash('error','We were unable to find the requested application property for editing.');
         $app->redirect("/admin/properties/view");
      }
      else
         $thisProperty = $thisProperty[0];
      
      $propForm  = new Mumby\WebTools\SystemForm();
      $propForm->submitValue = "Update Property";
      $propName  = $propForm->addTextField("Property Name", true);
      $propDesc  = $propForm->addTextAreaField("Description", false);
      $propConst = $propForm->addTextField("Constant", false, array("disabled"=>true, "moreLabel"=>"Note: Because of the potential risk, changing the constant for an application property is not allowed."));

      $propValue = $propForm->addTextField("Property Value", true, array("moreLabel"=>"If specifying a boolean value, use 'boolean:true' or 'boolean:false' rather than 0 or 1."));

      if($propForm->submitted())
      {
         $propForm->processFormData();

         if($propForm->processedData)
         {
            $n = $propName->getValue();
            $d = $propDesc->getValue();
            $v = $propValue->getValue();
            
            if($propMgr->updateApplicationProperty($id, $n, $d, $v) === false)
               $app->flash("error", "We were unable to update '".clean($n)."'.");
            else
               $app->flash("success", "Updated '".clean($n)."'.");

            $app->redirect("/admin/properties/view");
         }
         else
         {
            $errorMessages = $propForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $page->setError($e);
               }
            }
            else
            {
               $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
            }
         }
      }
      else
      {
         $propName->setValue($thisProperty["PropertyName"]);
         $propDesc->setValue($thisProperty["PropertyDescription"]);
         $propConst->setValue($thisProperty["PropertyConstant"]);
         $propValue->setValue($thisProperty["PropertyValue"]);
      }
      
      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"] = clean(_MB_APP_NAME_)." - Update Application Property";
      $pageContent["content"] = "<p>Use the form below to update this property for the ".clean(_MB_APP_NAME_)." application.</p>";

      $pageContent["content"] .= $propForm->generateForm();
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Delete an application property.
 */
$app->map('(/admin/properties/delete/:id(/))', function($id) use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Application Properties", $user, _MB_USER_ADMIN_);
   
   if($page->isAuthorized())
   {      
      $propMgr = new \Mumby\DB\Application(_MB_WEB_APPLICATION_ID_);

      $thisProperty = $propMgr->getApplicationProperty($id);
      if(empty($thisProperty))
      {
         $app->flash('error','We were unable to find the requested application property for editing.');
         $app->redirect("/admin/properties/view");
      }
      else
         $thisProperty = $thisProperty[0];

      
      $deleteForm = new Mumby\WebTools\SystemForm();
      $deleteForm->inlineForm = true;
      
      $propIDField = $deleteForm->addHiddenField("Property ID");
      $propIDField->setValue($id);

      if($deleteForm->submitted())
      {
         $deleteForm->processFormData();
         if($deleteForm->processedData)
         {
            if(($propMgr->deleteProperty($id)) === false)
            {
               $app->flash("error", "We were unable to delete the application property.");
            }
            else
               $app->flash("success", "Deleted '".clean($thisProperty["PropertyName"])."' from the application properties.");

            $app->redirect("/admin/properties/view");
         }
         else
         {
            $errorMessages = $deleteForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $page->setError($e);
               }
            }
            else
            {
               $page->setError("There was an error submitting the form.");
            }
         }
      }

      $pageContent = $page->getEmptyContentObj();

      $pageContent["title"] = "Delete Application Property";
      $pageContent["content"]  = "<p class='alert alert-warning'>Are you sure that you want to delete '".  clean($thisProperty["PropertyName"])." from the application properties for ".clean(_MB_APP_NAME_)."? Note that this action cannot be undone!</p>\n";
      $pageContent["content"] .= "<div style='text-align: center;'>";
      $deleteForm->submitValue = "Yes, delete the application property.";
      $pageContent["content"] .= $deleteForm->generateForm();
      $pageContent["content"] .= "<a href='/admin/properties/view' class='panel panel-body center-block'>No, don't delete this application property.</a>";
      $pageContent["content"] .= "</div>";
         
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Set application ID.
 */
$app->map('(/admin/appID(/))', function() use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Application Properties", $user, _MB_USER_ADMIN_);
   
   if($page->isAuthorized())
   {      
      $propMgr = new \Mumby\DB\Application();
      $allApps = $propMgr->getAllApplications();
      
      if(empty($allApps))
      {
         $app->flash('error','There are no applications in the central data store!!!');
         $app->redirect("/admin/properties");
      }
      else
      {
         $options = array();
         foreach($allApps as $a)
         {
            $options[$a["ApplicationID"]] = $a["ApplicationName"];
         }
      }

      $appIDForm = new Mumby\WebTools\SystemForm();

      
      $appIDField = $appIDForm->addSelectField("This Application", $options, true);
      if(defined("_MB_WEB_APPLICATION_ID_"))
         $appIDField->setValue(_MB_WEB_APPLICATION_ID_);

      if($appIDForm->submitted())
      {
         $appIDForm->processFormData();
         if($appIDForm->processedData)
         {
            $thisAppID = $appIDField->getValueKey();

            if(($page->update(array("ConfigValue"=>$thisAppID), array("ConfigName"=>"_MB_WEB_APPLICATION_ID_"), "Configs")) === false)
            {
               $app->flash("error", "We were unable to update the application identification.");
            }
            else
               $app->flash("success", "Application identification updated successfully.");

            $app->redirect("/admin/properties/view");
         }
         else
         {
            $errorMessages = $appIDForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $page->setError($e);
               }
            }
            else
            {
               $page->setError("There was an error submitting the form.");
            }
         }
      }

      $pageContent = $page->getEmptyContentObj();

      $pageContent["title"] = "Application Identification";
      $pageContent["content"]  = "<p>Use the form below to specify which application this is.</p>\n";
      $appIDForm->submitValue = "Set Application ID";
      $pageContent["content"] .= $appIDForm->generateForm();
         
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');