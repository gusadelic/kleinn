<?php

/*
 * Show admin properties.
*/
$app->get('(/admin/headlines(/))', function() use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Headlines", $user, _MB_ROLE_EDIT_HEADLINES_);
   
   if($page->isAuthorized())
   {
      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"] = "Headlines";
      
      $userOptions = array();
      
      $userOptions[] = array(
          "label" => "View/Edit Headlines",
          "link"  => "/admin/headlines/view",
          "desc"  => "View a list of all application headlines."
      );

      $userOptions[] = array(
          "label" => "Add Headlines",
          "link"  => "/admin/headlines/add",
          "desc"  => "Create a new application headline."
      );
      
      $pageContent["content"] = "<div class='adminOptionWrapper'>\n";
      $pageContent["content"] .= buildLandingPage($userOptions);
      $pageContent["content"] .= "</div>\n";
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * Show headlines.
*/
$app->get('(/admin/headlines/view(/))', function() use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("View/Edit Headlines", $user, _MB_ROLE_EDIT_HEADLINES_);
   
   if($page->isAuthorized())
   {
      $headlineMgr = new Mumby\WebTools\Headline();
      $headlines = $headlineMgr->getAllHeadlines();

      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"] = "View/Edit Headlines";
      $pageContent["content"] = "<p>Below is a list of headlines for this application.  To edit a headline, click on its title.</p>";

      if(!empty($headlines))
      {
         $pageContent["content"] .= "<ul class='list-group'>";
         foreach($headlines as $h)
         {
            $pageContent["content"] .= "<li class='list-group-item'>";
            $pageContent["content"] .= "<a class='pull-right btn btn-danger' href='/admin/headlines/delete/".$h["HeadlineID"]."'>Delete</a>";
            if ( !$h["IsApproved"] )
                $pageContent['content'] .= "<div class='label label-warning'>Disabled!</div> ";
            $pageContent["content"] .= "<a class='lead' href='/admin/headlines/edit/".$h["HeadlineID"]."'>".$h["HeadlineTitle"]."</a>";
            $pageContent["content"] .= "<p>".$h["HeadlineDesc"]."</p>";
            $pageContent["content"] .= "<p><a href='".$h["HeadlineLink"]."'>".$h["HeadlineLink"]."</a></p>";
            $pageContent['content'] .= "<p>";
            if ( $h["IsImportant"] )
                $pageContent['content'] .= "<div class='label label-success'>Important!</div> ";
            else if ( strtotime($h["EndDate"]) < time() )
                $pageContent['content'] .= "<div class='label label-danger'>Expired!</div> ";
            else 
                $pageContent["content"] .= "<em>Runs from ".date("F j,Y", strtotime($h["StartDate"]))." until ".date("F j,Y", strtotime($h["EndDate"]))."</a></em></p>";
            
            $userName = Mumby\WebTools\ACL\LocalUser::getUsersName('gusadelic@gmail.com');

            if ( !empty($userName) ) {
                $pageContent["content"] .= '<span style="float:right;">Created by: ' . $userName . '</span><br />';
            }
            
         }
         $pageContent["content"] .= "</ul>";
      }
      else
      {
         $pageContent["content"] .= "<p class='alert alert-warning'>There are not any headlines for this application.</p>";
      }
      
      $pageContent["content"] .= "<p>You can also add a new Headline by visiting the <a href='/admin/headlines/add'>Add Headline page</a>.</p>";
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * Show headlines.
 */ 
$app->map('(/admin/headlines/add(/))', function() use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Add Headline", $user, _MB_ROLE_EDIT_HEADLINES_);
   

   if($page->isAuthorized())
   {
      $headlineMgr = new Mumby\WebTools\Headline();

      $headlineForm  = new Mumby\WebTools\SystemForm();
      $headlineForm->submitValue = "Add Headline";
      $headlineName  = $headlineForm->addTextField("Headline Title", true);
      $headlineDesc  = $headlineForm->addTextAreaField("Description", false);
      $headlineLink = $headlineForm->addURLField("More Info Link", false);
      $headlineStart = $headlineForm->addHTMLDateField("Start Date", true);
      $headlineEnd = $headlineForm->addHTMLDateField("End Date", true);
      $headlineForm->addMarkup("<div class='row'><p class='alert alert-info col-sm-offset-2 col-sm-8'>Only \"Approved\" headlines will be displayed on the home page. Check the box below to approve the headline to be displayed.</p></div>");     
      $headlineApproved = $headlineForm->addCheckboxField("Approved?", array("Approved Headline"), false, array('hiddenLabel'=>true));
      $headlineForm->addMarkup("<div class='row'><p class='alert alert-info col-sm-offset-2 col-sm-8'>\"Important\" headlines will continue to be displayed indefinately. Check the box below to override the End date for this headline and force it to remain on the homepage.</p></div>");     
      $headlineImportant = $headlineForm->addCheckboxField("Important?", array("Important Headline"), false, array('hiddenLabel'=>true));
      
      if($headlineForm->submitted())
      {
         $headlineForm->processFormData();

         if($headlineForm->processedData)
         {
            $n = $headlineName->getValue();
            $d = $headlineDesc->getValue();
            $l = $headlineLink->getValue();
            $s = $headlineStart->getValue();
            $e = $headlineEnd->getValue();

            if ( !empty($headlineApproved->getValue()) )
                $a = "1";
            else
                $a = "0";
            
            if ( !empty($headlineImportant->getValue()) )
                $i = "1";
            else
                $i = "0";
            
            if($headlineMgr->addHeadline($user->userID, $n, $d, $l, $s, $e, $a, $i) === false) {
               $app->flash("error", "We were unable to add '".clean($n)."' to the headlines.");
               
            }
            else
               $app->flash("success", "Added '".clean($n)."' to the Headlines.");

            $app->redirect("/admin/headlines/view");


         }
         else
         {
            $errorMessages = $headlineForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $page->setError($e);
               }
            }
            else
            {
               $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
            }
         }
      }
      
      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"] = "Add Headline";
      $pageContent["content"] = "<p>Use the form below to add a new headline to the ".clean(_MB_APP_NAME_)." application.</p>";

      $pageContent["content"] .= $headlineForm->generateForm();
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');

$app->map('(/admin/headlines/edit/:HeadlineID(/))', function($HeadlineID) use($app, $user)
{
   // Create the Page instance
   $page = new Mumby\WebTools\AdminPage("Edit Headline", $user, _MB_ROLE_EDIT_HEADLINES_);


   if($page->isAuthorized())
   {
      $headlineMgr = new Mumby\WebTools\Headline();
      $headline = $headlineMgr->getHeadline($HeadlineID);
      $headline = $headline[0];
      $headlineForm  = new Mumby\WebTools\SystemForm();
      $headlineForm->submitValue = "Save Changes";
      $headlineName = $headlineForm->addTextField("Headline Title", true);
      $headlineDesc = $headlineForm->addTextAreaField("Description", false);
      $headlineLink = $headlineForm->addURLField("More Info Link", false);
      $headlineStart = $headlineForm->addHTMLDateField("Start Date", true);
      $headlineEnd = $headlineForm->addHTMLDateField("End Date", true);
      $headlineForm->addMarkup("<div class='row'><p class='alert alert-info col-sm-offset-2 col-sm-8'>Only \"Approved\" headlines will be displayed on the home page. Check the box below to approve the headline to be displayed.</p></div>");     
      $headlineApproved = $headlineForm->addCheckboxField("Approved?", array("Approved Headline"), false, array('hiddenLabel'=>true));
      $headlineForm->addMarkup("<div class='row'><p class='alert alert-info col-sm-offset-2 col-sm-8'>\"Important\" headlines will continue to be displayed indefinately. Check the box below to override the End date for this headline and force it to remain on the homepage.</p></div>");     
      $headlineImportant = $headlineForm->addCheckboxField("Important?", array("Important Headline"), false, array('hiddenLabel'=>true));

      $headlineName->setValue($headline['HeadlineTitle']);
      $headlineDesc->setValue($headline['HeadlineDesc']);
      $headlineLink->setValue($headline['HeadlineLink']);
      $headlineStart->setValue( date('Y-m-d', strtotime($headline['StartDate'])));
      $headlineEnd->setValue( date('Y-m-d', strtotime($headline['EndDate'])));
      
      if($headlineForm->submitted())
      {
         $headlineForm->processFormData();

         if($headlineForm->processedData)
         {
            $n = $headlineName->getValue();
            $d = $headlineDesc->getValue();
            $l = $headlineLink->getValue();
            $s = $headlineStart->getValue();
            $e = $headlineEnd->getValue();

            if ( !empty($headlineApproved->getValue()) )
                $a = "1";
            else
                $a = "0";
            
            if ( !empty($headlineImportant->getValue()) )
                $i = "1";
            else
                $i = "0";
            
            if($headlineMgr->updateHeadline($HeadlineID, $n, $d, $l, $s, $e, $a, $i) === false) 
               $app->flash("error", "We were unable to add '".clean($n)."' to the headlines.");
            else
               $app->flash("success", "Added '".clean($n)."' to the Headlines.");

            $app->redirect("/admin/headlines/view");
         }
         else
         {
            $errorMessages = $headlineForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $page->setError($e);
               }
            }
            else
            {
               $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
            }
         }
      }
      
      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"] = "Edit Headline";
      $pageContent["content"] = "<p>Use the form below to edit this headline for the ".clean(_MB_APP_NAME_)." application.</p>";

      $pageContent["content"] .= $headlineForm->generateForm();
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');

$app->get('(/admin/headlines/delete/:HeadlineID)', function($HeadlineID) use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Headlines", $user, _MB_ROLE_EDIT_HEADLINES_);
   
   if($page->isAuthorized())
   {
       $headlineMgr = new Mumby\WebTools\Headline();
       if ( $headlineMgr->deleteHeadline($HeadlineID) === false ) 
           $app->flash("error", "We were unable to delete this headline.");
       else 
           $app->flash("success", "Headline deleted!");
       
       $app->redirect("/admin/headlines/view");
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});