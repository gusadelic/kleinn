<?php
/*
 * Show page-related functions.
 */
$app->get('(/admin/slideshows(/))', function() use($app, $user) // Done
{
   // Create the Page instance
   $pageTitle = "Slideshows";
   $page  = new Mumby\WebTools\AdminPage($pageTitle, $user, _MB_ROLE_EDIT_SLIDESHOWS_);
   
   if($page->isAuthorized())
   {
      $pageContent = array();
      $pageContent["title"] = $pageTitle;
      
      $userOptions = array();
      $userOptions[] = array(
          "label" => "View/Edit Slideshows",
          "link"  => "/admin/slideshows/view",
          "desc"  => "View a list of all existing slideshows. From this page, you can view current information about each slideshow and edit slides."
      );

      $userOptions[] = array(
          "label" => "Add Slideshow",
          "link"  => "/admin/slideshows/add",
          "desc"  => "Create a new slideshow."
      );
     
      $pageContent["content"] = "<div class='adminOptionWrapper'>\n";
      $pageContent["content"] .= buildLandingPage($userOptions);
      $pageContent["content"] .= "</div>\n";

      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * List all slideshows.
 */
$app->get('(/admin/slideshows/view(/))', function() use($app, $user) // Done
{
   // Create the Page instance
   $pageTitle = "View Web Pages";
   $page  = new Mumby\WebTools\AdminPage("Slideshows", $user, _MB_ROLE_EDIT_SLIDESHOWS_);
   
   if($page->isAuthorized())
   {
      $pageContent = array();
      $pageContent["title"] = "View Slideshows";
      
      $pageContent["content"]  = "<p>Below is a list of all of the slideshows available on this website. You can edit individual slides by clicking on the title of the slideshow. Additional operations can be found in the last column of the table.</p>\n";
      $pageContent["content"] .= "<p>If you would like to create a new slideshow, please visit <a href='/admin/slideshows/add'>Add New Slideshow</a>.</p>\n";
      
      $slideshowMgmt = new Mumby\WebTools\Slideshow();
      $allShows = $slideshowMgmt->getAllSlideshows();
      
      if(!empty($allShows))
      {
         $showData = array();
         foreach($allShows as $s)
         {
            $pageTitle  = "<a href='/admin/slideshows/edit/".$s["SlideshowID"]."' class='editableItemLink'><i class='glyphicon glyphicon-pencil'></i> ".$s["SlideshowTitle"]."</a>";
            $operations = "";
            $operations .= "<a href='/admin/slideshows/viewShow/".$s["SlideshowID"]."'>[View Slideshow]</a><br />";
            $operations .= "<a href='/admin/slideshows/editSlides/".$s["SlideshowID"]."'>[Edit/Delete Slides]</a><br />";
            $operations .= "<a href='/admin/slideshows/addSlide/".$s["SlideshowID"]."'>[Add New Slide]</a><br />";

            $showData[] = array(
               "Title"        => $pageTitle,
               "Operations"   => $operations
            );
         }
         
         $dataTable = new \Mumby\WebTools\Table();
         $pageContent["content"] .= $dataTable->getTable($showData, "", "", true);
      }
      else
         $pageContent["content"] .= "<p><em>No slideshows found!</em></p>";
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * View slideshows.
 */
$app->get('(/admin/slideshows/viewShow/:id(/))', function($showID) use($app, $user) // Done - FYI, the CSS on this doesn't match the CSS on the website...
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Slideshows", $user, _MB_ROLE_EDIT_SLIDESHOWS_);

   $page->addCSS("/themes/uamath/css/slideshows.css");
   $page->hideAdminMenu();
   
   if($page->isAuthorized())
   {
      $pageContent = $page->getEmptyContentObj();
      $thisSlideshow = new Mumby\WebTools\Slideshow($showID);
   
      if(!empty($thisSlideshow))
      {
         $pageContent["title"]    = "Viewing ".$thisSlideshow->SlideshowTitle;
         $pageContent["content"]  = $thisSlideshow->generateSlideShow(true, false);
         $pageContent["content"] .= "<div class='row'><div class='text-center well well-default'><a href='/admin/slideshows/view'>Return to viewing all slideshows.</a></div></div>";
      }
      else
         $app->redirect('/admin/slideshows/view', 404);

      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * Add slideshow.
 */
$app->get('(/admin/slideshows/add(/))', function() use($app, $user) // Done
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Slideshows", $user, _MB_ROLE_EDIT_SLIDESHOWS_);

   if($page->isAuthorized())
   {
      $pageContent = $page->getEmptyContentObj();
      
      $thisSlideshow = new Mumby\WebTools\Slideshow();

      $slideshowForm = new \Mumby\WebTools\SystemForm();
      
      $showName      = $slideshowForm->addTextField("Slideshow Name", true);
      $includeNav    = $slideshowForm->addCheckboxField("Slideshow Nav", array("Include navigational buttons for your slideshow?"), false, array("hiddenLabel"=>true));
      $includeArrows = $slideshowForm->addCheckboxField("Slideshow Arrows", array("Include left/right arrows for your slideshow?"), false, array("hiddenLabel"=>true));
      
      if($slideshowForm->submitted())
      {
         $slideshowForm->processFormData();
         if($slideshowForm->processedData)
         {
            $newShowName = $showName->getValue();
            $addNav      = $includeNav->getValue();
            $addArrows   = $includeArrows->getValue();

            $newMetadata = array();
            if(!empty($addNav))
               $newMetadata["includeNav"] = true;
            else
               $newMetadata["includeNav"] = false;

            if(!empty($addArrows))
               $newMetadata["includeArrows"] = true;
            else
               $newMetadata["includeArrows"] = false;

            if($thisSlideshow->addSlideshow($newShowName, json_encode($newMetadata)) === false)
            {
               $app->flash("error", "We were unable to add the slideshow.");
            }
            else
            {
               $app->flash("success", "Added '".clean($newShowName)."</a>' successfully.");
            }
         }
         else
            $app->flash("error", "We were unable to add the slideshow.");
         
         $app->redirect("/admin/slideshows/view");
      }

      $pageContent["title"]   = "Add New Slideshow";
      $pageContent["content"] = $slideshowForm->generateForm();
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Add slides.
 */
$app->get('(/admin/slideshows/addSlide/:id(/))', function($showID) use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Slideshows", $user, _MB_ROLE_EDIT_SLIDESHOWS_);

   if($page->isAuthorized())
   {
      $pageContent = $page->getEmptyContentObj();
      
      $thisSlideshow = new Mumby\WebTools\Slideshow($showID);
      if(empty($thisSlideshow))
      {
         $app->flash("error", "We were unable to locate the slideshow.");
         $app->redirect("/admin/slideshows/view", 404);
      }

      $slideshowForm = new \Mumby\WebTools\SystemForm();
      
      $slideHeading = $slideshowForm->addTextField("Slide Heading", true);
      $slideCaption = $slideshowForm->addHTMLField("Slide Caption", true);
      $slideLink    = $slideshowForm->addTextField("Slide Link");
      $slideImage   = $slideshowForm->addFileField("Image File", true, array("allowedFileTypes"=>array("jpg","png", "gif"), "fileDestinationPath"=>"./imgs/front/", "overwriteFiles"=>true));
      $slideshowForm->setFileStoreResponse();
      
      if($slideshowForm->submitted())
      {
         $slideshowForm->processFormData();
         if($slideshowForm->processedData)
         {
            $sHeader  = $slideHeading->getValue();
            $sCaption = $slideCaption->getValue();
            $sLink    = $slideLink->getValue();
            $sImg     = $slideImage->getValue();
            $sImg = $sImg["name"];
            
            if(!$thisSlideshow->addSlide($sHeader, $sCaption, $sLink, $sImg))
            {
               dump($thisSlideshow->getLastQuery());
               die();
               $app->flash("error", "We were unable to update the slideshow.");
            }
            else
            {
               $app->flash("success", "Updated '".clean($thisSlideshow->SlideshowTitle)."</a>' successfully.");
            }
            $app->redirect("/admin/slideshows/editSlides/".$showID);
         }
         else
         {
            $page->setError(implode("<br />", $slideshowForm->getErrors()));
         }
      }

      $pageContent["title"]   = $thisSlideshow->SlideshowTitle." - Add New Slide";
      $pageContent["content"] = $slideshowForm->generateForm();
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Edit slideshows.
 */
$app->get('(/admin/slideshows/edit/:id(/))', function($showID) use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Slideshows", $user, _MB_ROLE_EDIT_SLIDESHOWS_);

   if($page->isAuthorized())
   {
      $pageContent = $page->getEmptyContentObj();
      
      $thisSlideshow = new Mumby\WebTools\Slideshow($showID);

      $slideshowForm = new \Mumby\WebTools\SystemForm();
      
      $showName      = $slideshowForm->addTextField("Slideshow Name", true);
      $includeNav    = $slideshowForm->addCheckboxField("Slideshow Nav", array("Include navigational buttons for your slideshow?"), false, array("hiddenLabel"=>true));
      $includeArrows = $slideshowForm->addCheckboxField("Slideshow Arrows", array("Include left/right arrows for your slideshow?"), false, array("hiddenLabel"=>true));
      
      if($slideshowForm->submitted())
      {
         $slideshowForm->processFormData();
         if($slideshowForm->processedData)
         {
            $newShowName = $showName->getValue();
            $addNav      = $includeNav->getValue();
            $addArrows   = $includeArrows->getValue();

            $newMetadata = array();
            if(!empty($addNav))
               $newMetadata["includeNav"] = true;
            else
               $newMetadata["includeNav"] = false;

            if(!empty($addArrows))
               $newMetadata["includeArrows"] = true;
            else
               $newMetadata["includeArrows"] = false;

            if($thisSlideshow->updateSlideshow($showID, $newShowName, json_encode($newMetadata)) === false)
            {
               $app->flash("error", "We were unable to update the slideshow.");
            }
            else
            {
               $app->flash("success", "Updated '".clean($newShowName)."</a>' successfully.");
            }
         }
         else
            $app->flash("error", "We were unable to update the slideshow.");
         
         $app->redirect("/admin/slideshows/view");
      }
      else
      {
         $showName->setValue($thisSlideshow->SlideshowTitle);
         $showMetadata = $thisSlideshow->SlideshowMetadata;
         if(!empty($showMetadata))
         {
            $metadata = json_decode($showMetadata, true);
            if(isset($metadata["includeNav"]) && $metadata["includeNav"])
               $includeNav->checked = 0;
            if(isset($metadata["includeArrows"]) && $metadata["includeArrows"])
               $includeArrows->checked = 0;
         }
      }
      $pageContent["title"]   = "Edit Slideshow Information";
      $pageContent["content"] = $slideshowForm->generateForm();
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Display form for updating slideshows.
 */
$app->get('(/admin/slideshows/editSlides/:id(/))', function($showID) use($app, $user)
{
   // Create the Page instance
   $page = new Mumby\WebTools\AdminPage("Slideshows", $user, _MB_ROLE_EDIT_SLIDESHOWS_);
   
   if($page->isAuthorized())
   {
      $thisSlideshow = new Mumby\WebTools\Slideshow($showID);

      if(empty($thisSlideshow))
      {
         $app->flash('error','We were unable to find the requested slideshow for editing.');
         $app->redirect("/admin/slideshows/view");
      }

      $allSlides = $thisSlideshow->getSlides();

      $page->addJS( array(
         '/common/bower/jquery-nested-sortable/jquery.ui.nestedSortable.js',
         '/common/admin/js/slideshows.js'
      ));
      
      $page->addCSS( array(
         '/common/admin/css/slideshows.css'
      ));
      
      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"] = "Edit ".$thisSlideshow->SlideshowTitle;
      
      $output  = "<div class='row'><a href='/admin/slideshows/addSlide/".((int) $showID)."' class='btn btn-success pull-right'><span class='fa fa-plus'></span> Add New Slide</a></div>";

      $form = new \Mumby\WebTools\SystemForm();
      $form->inlineForm = true;
      
      $currentItem = 0;
      
      if(!empty($allSlides))
      {
         $output .= "<form action='/admin/slideshows/editSlides/".((int) $showID)."' method='post'>";
         $output .= "<ol id='editFAQ' class='sortable'>\n";

         foreach($allSlides as $q)
         {
            $output .= "<li id='faq_".$currentItem."'>\n";
            $output .= "<div class='clearfix'>\n";
            $output .= "<i class='fa fa-sort-amount-asc'></i>\n";
            $output .= "<i class='fa fa-pencil-square-o toggleFields active' title='Expand this slide for editing'></i>\n";
            $output .= "<span class='deleteMe fa fa-ban' onclick='javascript: deleteItem(".$currentItem."); return false;'></span>\n";
            $output .= "<div class='faqEditFields'>\n";

            $currentRowID = "faq_".$currentItem."_";

            $idField       = $form->addHiddenField("ID", array("hiddenLabel"=>true, "fieldName"=>$currentRowID."id"));
            $questionField = $form->addTextField("Slide Heading", false, array("hiddenLabel"=>true, "fieldName"=>$currentRowID."text", "fieldClass"=>"faqQuestion"));

            $configMgr = new Mumby\WebTools\WYSIWYG_Config();
            $configMgr->fullFeatured(false);
            $configMgr->allowBold(true);
            $configMgr->allowItalics(true);
            $configMgr->allowUnderline(true);
            $configMgr->allowLinks(true);
            $configMgr->allowPeopleInjections(true);
            $configMgr->allowDataInjections(true);
            $configMgr->allowMenubar(false);

            if($user->hasPermission(_MB_ROLE_EDIT_WEB_PAGE_SOURCE_))
               $configMgr->allowViewSource(true);
            else
               $configMgr->allowViewSource(false);

            $wysiwygConfig = array(
               "config"      => $configMgr->getJSONConfig(),
               "hiddenLabel" => true,
               "fieldName"   => $currentRowID."answer"
            );
            $answerField  = $form->addHTMLField("Slide Caption", false, $wysiwygConfig);
            $linkField  = $form->addTextField("Slide Link", false, array("hiddenLabel"=>true, "fieldName"=>$currentRowID."link"));

            $idField->setValue($q->SlideshowSlideID);
            $questionField->setValue(clean($q->SlideHeading));
            $answerField->setValue(clean($q->SlideCaption));
            $linkField->setValue(clean($q->SlideLink));

            $output .= $idField->render();
            $output .= $questionField->render();
            $output .= $answerField->render();
            $output .= $linkField->render();

            $output .= "</div>\n";
            $output .= "</div>\n";

            $output .= "</li>\n";
            $currentItem++;
         }
         
         $output .= "</ol>\n";

         $output .= "<hr />\n";
         $output .= "<div class='submitDiv'>\n";
         $output .= "<input type='hidden' name='serialized' id='serialized' value='' />\n";
         $output .= "<input type='submit' name='submit' id='submit' value='Save Slideshow Information' />\n";
         $output .= "</div>\n";
         $output .= "</form>\n";
      }
      else
      {
         $page->setWarning("This slideshow does not currently have any slides associated with it.");
         $output .= "<p><em>There are no slides to display.</em></p>";
      }
      
      $pageContent["content"] = $output;

      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * Process submitted FAQ information.
 */
$app->post('(/admin/slideshows/editSlides/:id(/))', function($showID) use($app, $user)
{
   // Create the Page instance
   $page = new Mumby\WebTools\AdminPage("Slideshows", $user, _MB_ROLE_EDIT_SLIDESHOWS_);
   
   if($page->isAuthorized())
   {
      $thisSlideshow = new Mumby\WebTools\Slideshow($showID);

      if(empty($thisSlideshow))
      {
         $app->flash('error','We were unable to find the requested slideshow for editing.');
         $app->redirect("/admin/slideshows/view");
      }
      else

      $submittedData = $app->request->post();
      
      $numberOfSlides = (count($submittedData)-2)/4;

      //$nestedOrder = $submittedData["serialized"];

//      if(!empty($nestedOrder) && !preg_match_all("/faq\[(\d+)\]\=root/", $nestedOrder, $submittedData))
//      {
//         $app->flash('error','We were unable to process your updated slideshow submission. Error Code: 001');
//         $app->redirect("/admin/slideshows/view");
//      }

      
      $slides = array();
      
//      foreach($submittedData as $k=>$v)
//      {
//         $slides[] = array(
//            "slideID"=>$submittedData["faq_".$v."_id"],
//            "heading"=>$submittedData["faq_".$v."_text"],
//            "caption"=>$submittedData["faq_".$v."_answer"],
//            "link"=>$submittedData["faq_".$v."_link"]
//         );
//      }
      
      for ( $i = 0 ; $i < $numberOfSlides ; $i++ ) {
         $slides[] = array(
            "slideID"=>$submittedData["faq_".$i."_id"],
            "heading"=>$submittedData["faq_".$i."_text"],
            "caption"=>$submittedData["faq_".$i."_answer"],
            "link"=>$submittedData["faq_".$i."_link"]
         );
      }
      
      $result = $thisSlideshow->updateSlides($showID, $slides);
      
      if( !$result )
      {
         echo $thisSlideshow->getLastQuery();
         die();
         $app->flash("error", "We were unable to update ".$thisSlideshow->SlideshowTitle.".");
      }
      else
      {
         $app->flash("success", "We updated ".$thisSlideshow->SlideshowTitle." successfully.");
      }
      
      $app->redirect("/admin/slideshows/view");
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});