<?php

/*
 * Show user-related functions.
 */
$app->get('(/admin/users(/))', function() use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("User Management", $user, _MB_ROLE_MANAGE_USERS_);
   
   if($page->isAuthorized())
   {
      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"] = "User Management";
      
      $userOptions = array();
      $userOptions[] = array(
          "label" => "View/Edit User Accounts",
          "link"  => "/admin/users/view",
          "desc"  => "View a list of all users (including de-activated users). From this page, you can change user information, give/revoke user permissions, or re-activate any disabled accounts."
      );

      $userOptions[] = array(
          "label" => "Add Users",
          "link"  => "/admin/users/add",
          "desc"  => "Create a new user and give them specific permissions."
      );
      
      $userOptions[] = array(
          "label" => "View Permission Levels",
          "link"  => "/admin/users/permissions",
          "desc"  => "View or add permission levels which can then be granted to individuals.",
      );      
      
      if($user->hasPermission(_MB_ROLE_MANAGE_PERMISSIONS_))
         $userOptions[] = array(
          "label" => "Add Permission Levels",
          "link"  => "/admin/users/permissions/add",
          "desc"  => "Add permission levels which can then be granted to individuals.",
         );
      
      if($user->hasPermission(_MB_ROLE_SWITCH_USERS_))
         $userOptions[] = array(
             "label" => "Switch User",
             "link"  => "/admin/users/switch",
             "desc"  => "Log in as a different user.",
         );
      
      if(_MB_AUTH_TYPE_ != "cas")
      {
         $userOptions[] = array(
             "label" => "Reset User Passwords",
             "link"  => "/admin/users/reset",
             "desc"  => "Reset passwords for a given user.",
             "class" => "todo"
         );
      }
      
      $pageContent["content"] = "<div class='adminOptionWrapper'>\n";
      $pageContent["content"] .= buildLandingPage($userOptions);
      $pageContent["content"] .= "</div>\n";
      
      $page->displayContent($pageContent);
   }
   else
   {
      $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
      //$app->redirect('/403', 403);
   }
});

/*
 * Show all users in the system.
 */
$app->get('(/admin/users/view(/))', function() use($app, $user)
{
   // Create the Page instance
   $page  = new \Mumby\WebTools\AdminPage("User Management", $user, _MB_ROLE_MANAGE_USERS_);
   
   if($page->isAuthorized())
   {
      $table = new \Mumby\WebTools\Table();
      
      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"] = "User Accounts";
      
      $pageContent["content"]  = "<p>Click on any user below to edit their profile.</p>\n";
      $pageContent["content"] .= "<p>You can also add a new user by visiting the <a href='/admin/users/add'>Add User page</a>.</p>\n";
      
      $userMgmt = new \Mumby\WebTools\UserManagement();
      $allUsers = $userMgmt->getAllUsers();
      
      if(!empty($allUsers))
      {  
         $userData = array();
         foreach($allUsers as $a)
         {
            // Make sure users can't edit their own permissions.
            if($user->username == $a["username"])
               $name = $a["firstName"]." ".$a["lastName"];
            else
               $name = "<a href='/admin/users/edit/".$a["username"]."'>".$a["firstName"]." ".$a["lastName"]."</a>";

            if(!$a["active"])
               $name = "<span class='fa fa-ban'></span>&nbsp;".$name;
            else if(in_array(_MB_USER_ADMIN_, $a["permissions"]) || in_array(_MB_USER_SUPER_ADMIN_, $a["permissions"]))
               $name = "<span class='fa fa-star'></span>&nbsp;".$name;

            if(!$a["active"])
            {
               $permissions = "<em>Blocked</em>";
            }
            else if(empty($a["permissions"]))
            {
               $permissions = "<em>None</em>";
            }
            else
            {
               $permissions = array();
               $allPerms = $user->getAllPermissions();
               if(!empty($a["permissions"]))
               {
                  foreach($a["permissions"] as $p)
                  {
                     if(isset($allPerms[$p]))
                        $permissions[] = _cleanPerms($allPerms[$p]["PermissionName"]);
                  }
                  sort($permissions);
                  $permissions = implode("<br />", $permissions);
               }
            }
            
            $thisUser = array(
               "Name"        => $name,
               "Username"    => $a["username"],
               "Email"       => $a["email"],
               "Permissions" => $permissions
            );
            
            if(!$a["active"])
               $thisUser["_tr_className_"] = "bg-danger-table-row";
            
            $userData[] = $thisUser;
         }
         
         $pageContent["content"] .= $table->getTable($userData, "", "small", false, true);
         
         $pageContent["content"] .= "<ul class='list-unstyled'>";
         $pageContent["content"] .= "<li><span class='fa fa-star'></span> Administrators</li>";
         $pageContent["content"] .= "<li><span class='fa fa-ban'></span> Blocked Users</li>";
         $pageContent["content"] .= "</ul>";
      }
      else
         $pageContent["content"] .= "<p><em>No user accounts found!</em></p>";
      
      $page->displayContent($pageContent);
   }
   else
   {
      $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
      //$app->redirect('/403', 403);
   }
});

/*
 * Add a new user to the system.
 */
$app->map('(/admin/users/add(/))', function() use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("User Management", $user, _MB_ROLE_MANAGE_USERS_);
   
   if($page->isAuthorized())
   {
      $addUserForm  = new Mumby\WebTools\SystemForm();
      $addUserForm->validation = 'rules: {
				field_firstname: "required",
				field_lastname: "required",
				field_username: {
					required: true,
					minlength: 3
				},
				field_password: {
					required: true,
					minlength: 6
				},
				field_password_confirm: {
					required: true,
					minlength: 6,
					equalTo: "#field_password"
				},
				field_email: {
					required: true,
					email: true
				}
			},
			messages: {
				field_firstname: "Enter your firstname",
				field_lastname: "Enter your lastname",
				field_username: {
					required: "Enter a username",
					minlength: jQuery.validator.format("Enter at least {0} characters"),
					remote: jQuery.validator.format("{0} is already in use")
				},
				field_password: {
					required: "Provide a password",
					minlength: jQuery.validator.format("Enter at least {0} characters")
				},
				field_password_confirm: {
					required: "Repeat your password",
					minlength: jQuery.validator.format("Enter at least {0} characters"),
					equalTo: "Enter the same password as above"
				},
				field_email: {
					required: "Please enter a valid email address",
					minlength: "Please enter a valid email address",
					remote: jQuery.validator.format("{0} is already in use")
				}
			}';
   

      $userMgmt = new \Mumby\WebTools\UserManagement();


      $newUserName = $addUserForm->addTextField("User Name", true, array("id" => "username"));
      $addUserForm->addMarkup('<br >');
      
      $newPassword = $addUserForm->addPasswordField("Enter Password", true, array("id" => "password"));
      $newPassword2 = $addUserForm->addPasswordField("Validate Password", true, array("id" => "password_confirm"));
      
      
      $newFirstName = $addUserForm->addTextField("First Name", true, array("id" => "firstname"));
      $newLastName = $addUserForm->addTextField("Last Name", true, array("id" => "lastname"));
      $newEmail = $addUserForm->addEmailField("Email", true, array("id" => "email"));
      
      
      $perms = $user->getAllPermissions(_MB_WEB_APPLICATION_ID_);

      $permissionFields = array();

      $permissionCounter = 0;
      $currentCategory = "";
      foreach($perms as $p)
      {
         $thisConst = constant($p["PermissionConstant"]);
         
         // Users can't give out permissions for things
         // they themselves don't have.
         if(
            $thisConst == _MB_USER_AUTHENTICATED_ ||
            $thisConst == _MB_USER_NOBODY_ ||
            $thisConst == _MB_USER_ANONYMOUS_ ||
            $thisConst == _MB_USER_SUPER_ADMIN_ ||
            !$user->hasPermission($thisConst)
         )
            continue;
         
         // Check permission category
         if($currentCategory != $p["PermissionCategoryName"])
         {
            $addUserForm->addMarkup("<div class='row'><h2 class='col-md-offset-2 col-md-8'>Permissions: ".$p["PermissionCategoryName"]."</h2></div>");
            $currentCategory = $p["PermissionCategoryName"];
            $permissionCounter = 0;
         }
         
         $fieldMetadata = array();
         $thisLabel = "";
         $fieldMetadata["hiddenLabel"] = true;
         
         $permissionsCheckboxLabel  = "<strong>";
         $permissionsCheckboxLabel .= _cleanPerms($p["PermissionName"]);
         $permissionsCheckboxLabel .= "</strong><br /><em class='checkboxDesc'>".$p["PermissionDesc"]."</em>";

         $permissionFields[] = $addUserForm->addCheckboxField($thisLabel, array($p["PermissionID"] => $permissionsCheckboxLabel), false, $fieldMetadata);
         $permissionCounter++;
      }
      
      if($addUserForm->submitted())
      {
         $addUserForm->processFormData();

         if($addUserForm->processedData)
         {
            $userData = array();
            $userData['netID'] = $newUserName->getValue();
            $userData['firstName'] = $newFirstName->getValue();
            $userData['lastName'] = $newLastName->getValue();
            $userData['emailAddress'] = $newEmail->getValue();
            $userData['password'] = $newPassword->getValue();
            
            $newPermissions = array();
            foreach($permissionFields as $p)
            {
               $checked = $p->getValueKey();
               if(!empty($checked))
               {
                  $newPermissions[] = $checked[0];
               }
            }

            $usrMgmt = new Mumby\WebTools\UserManagement();
           
            if($usrMgmt->addUser($userData, $newPermissions) === false)
               $app->flash("error", "We were unable to add the new user.<br />" . $usrMgmt->getErrors());
            else
               $app->flash("success", "Added new user to the system successfully.");
            
            $app->redirect("/admin/users/view");
         }
         else
         {
            $errorMessages = $addUserForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $page->setError($e);
               }
            }
            else
            {
               $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
            }
         }
      }

      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"] = "Add New User";      
      $pageContent["content"]  = "<p>Use the form below to add a new user to the system.</p>\n";
      $pageContent["content"] .= $addUserForm->generateForm();

      $page->displayContent($pageContent);
   }
   else
   {
      $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
      //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Edit an existing users information (including permissions).
 */
$app->map('(/admin/users/edit/:username(/))', function($username) use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("User Management", $user, _MB_ROLE_MANAGE_USERS_);
   
   if($page->isAuthorized())
   {
      $userMgmt = new \Mumby\WebTools\UserManagement();
      $thisUser = $userMgmt->getUserInfo($username);
      if(empty($thisUser))
      {
         $app->flash('error','We were unable to find the requested user for editing.');
         $app->redirect("/admin/users/view");
      }
      else
      {
         if(!empty($thisUser["PreferredName"]))
            $fullName = $thisUser["PreferredName"];
         else
            $fullName = $thisUser["FirstName"];

         $fullName .= " ".$thisUser["LastName"];
         
         if(in_array(_MB_USER_SUPER_ADMIN_, $thisUser["permissions"]))
         {
            $app->flash('error', "Because ".$fullName." is a \"super admin\", they have access to all permissions in every application. If you would like to limit this person's permission levels, you will need to remove them from the super admin list.");
            $app->redirect('/admin/users/view');
         }
      }

      $editUserForm = new Mumby\WebTools\SystemForm();
         
      $perms = $user->getAllPermissions();
      $permissionFields = array();

      $permissionCounter = 0;
      $currentCategory = "";
      foreach($perms as $p)
      {
         $thisConst = constant($p["PermissionConstant"]);
         
         // Users can't give out permissions for things
         // they themselves don't have.
         if(
            $thisConst == _MB_USER_AUTHENTICATED_ ||
            $thisConst == _MB_USER_NOBODY_ ||
            $thisConst == _MB_USER_ANONYMOUS_ ||
            $thisConst == _MB_USER_SUPER_ADMIN_ ||
            !$user->hasPermission($thisConst) ||
            ($thisConst == _MB_USER_ADMIN_ && $user->username == $username)
         )
            continue;
         
         // Check permission category
         if($currentCategory != $p["PermissionCategoryName"])
         {
            $editUserForm->addMarkup("<div class='row'><h2 class='col-md-offset-2 col-md-8'>Permissions: ".$p["PermissionCategoryName"]."</h2></div>");
            $currentCategory = $p["PermissionCategoryName"];
            $permissionCounter = 0;
         }
         
         $fieldMetadata = array();
         $thisLabel = "";
         $fieldMetadata["hiddenLabel"] = true;
         
         if(in_array($thisConst, $thisUser["permissions"]))
            $fieldMetadata["checked"] = $p["PermissionID"];

         $permissionsCheckboxLabel  = "<strong>";
         $permissionsCheckboxLabel .= _cleanPerms($p["PermissionName"]);
         $permissionsCheckboxLabel .= "</strong><br /><em class='checkboxDesc'>".$p["PermissionDesc"]."</em>";

         $permissionFields[] = $editUserForm->addCheckboxField($thisLabel, array($p["PermissionID"] => $permissionsCheckboxLabel), false, $fieldMetadata);
         $permissionCounter++;
      }
            
      if($editUserForm->submitted())
      {
         $editUserForm->processFormData();

         if($editUserForm->processedData)
         {
            $newPermissions = array();
            foreach($permissionFields as $p)
            {
               $checked = $p->getValueKey();
               if(!empty($checked))
               {
                  $newPermissions[] = $checked[0];
               }
            }
            
            if(($userMgmt->setPermissions($thisUser["id"], $newPermissions)) === false)
            {
               $app->flash("error", "We were unable to update the user's permission settings.");
            }
            else
               $app->flash("success", $fullName."'s permissions have been successfully updated.");
            
            $app->redirect("/admin/users/view");
         }
         else
         {
            $errorMessages = $editUserForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $page->setError($e);
               }
            }
            else
            {
               $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
            }
         }
      }

      $pageContent = $page->getEmptyContentObj();
      
      $pageContent["title"] = $fullName . " (" . $username . ") - Profile";

   
      $pageContent["content"]  = "<p>Use the form below to edit this user's profile.</p>\n";
      $pageContent["content"] .= $editUserForm->generateForm();

      $page->displayContent($pageContent);
   }
   else
   {
      $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
      //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * View all permission levels (including who has what permissions).
 */
$app->map('(/admin/users/permissions(/))', function() use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("User Management", $user, _MB_ROLE_MANAGE_USERS_);
   
   if($page->isAuthorized())
   {
      // Create the Page instance
  
      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"] = "Manage Permission Levels";
      $pageContent["content"]  = "<p>";
      $pageContent["content"] .= "Below is a list of all of the permissions used within this application, including a list of those who have said permissions. ";
      $pageContent["content"] .= "You can filter data within the table using the search box to the right.";
      $pageContent["content"] .= "</p>";
      $pageContent["content"] .= "<p>";
      $pageContent["content"] .= "To revoke/set user permissions, please select individual users from the <a href='/admin/users/view'>User Management page</a>.";
      $pageContent["content"] .= "</p>";
      
      if($user->hasPermission(_MB_ROLE_MANAGE_PERMISSIONS_))
      {
         $pageContent["content"] .= "<p>";
         $pageContent["content"] .= "<a href='/admin/users/permissions/add'>You can add a new permission to the list by clicking here.</a>";
         $pageContent["content"] .= "</p>";
      }

      $userMgmt = new \Mumby\WebTools\UserManagement();
      $allUsers = $userMgmt->getAllUsers();
      $allPerms = $user->getAllPermissions();
      if(!empty($allPerms))
      {  
         $currentCategory = "";
         
         $permissionData = array();
         foreach($allPerms as $p)
         {
            $thisConst = constant($p["PermissionConstant"]);
            if(
               $thisConst == _MB_USER_AUTHENTICATED_ ||
               $thisConst == _MB_USER_NOBODY_ ||
               $thisConst == _MB_USER_ANONYMOUS_ ||
               $thisConst == _MB_USER_SUPER_ADMIN_
            )
               continue;
       
            $pConst = constant($p["PermissionConstant"]);
            $thisUsers = array();
            foreach($allUsers as $u)
            {
               if(!empty($u["permissions"]) && in_array($pConst, $u["permissions"]))
                  $thisUsers[] = str_replace(" ", "&nbsp;", $u["firstName"]." ".$u["lastName"]);
            }
            
            $thisPerm = array(
               "Permission"  => "<a href='/admin/users/permissions/edit/".$p["PermissionID"]."'>"._cleanPerms($p["PermissionName"])."</a>",
               "Description" => $p["PermissionDesc"],
               "Category"    => $p["PermissionCategoryName"],
               "Constant"    => $p["PermissionConstant"],
               "Users"       => implode("<br />\n",$thisUsers)
            );
            
            $permissionData[] = $thisPerm;
         }
         
         $table = new \Mumby\WebTools\Table();
         $pageContent["content"] .= $table->getTable($permissionData, "", "small", true, true);
         $pageContent["content"] .= "<p class='text-smaller'>* Constant values can be used used within the CMS source code to enforce permissions at the code level.</p>";
      }

      $page->displayContent($pageContent);
   }
   else
   {
      $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
      //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Add a new permission level to the system.
 */
$app->map('(/admin/users/permissions/add(/))', function() use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("User Management", $user, _MB_ROLE_MANAGE_PERMISSIONS_);
   
   if($page->isAuthorized())
   {
      $usrMgmt = new Mumby\WebTools\UserManagement();

      $addPermForm = new Mumby\WebTools\SystemForm();
      $permName  = $addPermForm->addTextField("Name", true);
      $permDescription = $addPermForm->addTextAreaField("Description", true);
      $permCategories = $usrMgmt->getPermissionCategories();
      $categoryOptions = array();
      foreach($permCategories as $pc)
      {
         $categoryOptions[$pc["PermissionCategoryID"]] = $pc["PermissionCategoryName"];
      }
      asort($categoryOptions);
      $permCategory = $addPermForm->addSelectField("Category", $categoryOptions, true);
      $permConst = $addPermForm->addTextField("Constant", true, array("moreLabel"=>"This allows the permission to be used within the source code for the application.", "placeholder"=>"_MB_CONSTANT_NAME_"));

      if($addPermForm->submitted())
      {
         $addPermForm->processFormData();

         if($addPermForm->processedData)
         {
            $pName     = $permName->getValue();
            $pDesc     = $permDescription->getValue();
            $pConstant = $permConst->getValue();
            $pCat      = $permCategory->getValueKey();

            if(defined($pConstant))
            {
               $page->setError("The constant you defined is already in use within the system.");
            }
            else
            {
               if(!$usrMgmt->addPermission($pName, $pDesc, $pConstant, $pCat))
                  $app->flash("error", "We were unable to add '".clean($pName)."' to the permissions list.");
               else
                  $app->flash("success", "Added '".clean($pName)."' to the permissions list.");

               $app->redirect("/admin/users/permissions");
            }
         }
         else
         {
            $errorMessages = $addPermForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $page->setError($e);
               }
            }
            else
            {
               $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
            }
         }
      }

      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"] = "Add New Permission Level";
      $pageContent["content"]  = "<p>Use the form below to add a new permission level to the system.</p>\n";
      $pageContent["content"] .= $addPermForm->generateForm();//"<img src='/inject/sysform-3' />";

      $page->displayContent($pageContent);

   }
   else
   {
      $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
      //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Add a new permission level to the system.
 */
$app->map('(/admin/users/permissions/edit/:permID(/))', function($permID) use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("User Management", $user, _MB_ROLE_MANAGE_PERMISSIONS_);
   
   if($page->isAuthorized())
   {
      $usrMgmt = new Mumby\WebTools\UserManagement();
      $thisPerm = $usrMgmt->getPermission($permID);
      
      $updatePermForm = new Mumby\WebTools\SystemForm();
      $permName  = $updatePermForm->addTextField("Name", true);
      $permDescription = $updatePermForm->addTextAreaField("Description", true);
      
      $permCategories = $usrMgmt->getPermissionCategories();
      $categoryOptions = array();
      foreach($permCategories as $pc)
      {
         $categoryOptions[$pc["PermissionCategoryID"]] = $pc["PermissionCategoryName"];
      }
      asort($categoryOptions);
      $permCategory = $updatePermForm->addSelectField("Category", $categoryOptions, true, array("checked"=>$thisPerm["PermissionCategoryID"]));
      $permConst = $updatePermForm->addTextField("Constant", false, array("disabled"=>true, "moreLabel"=>"Note: Because of the potential risk, changing the constant for a permission level is not allowed."));

      $permName->setValue($thisPerm["PermissionName"]);
      $permDescription->setValue($thisPerm["PermissionDesc"]);
      $permConst->setValue($thisPerm["PermissionConstant"]);
      
      if($updatePermForm->submitted())
      {
         $updatePermForm->processFormData();

         if($updatePermForm->processedData)
         {
            $pName     = $permName->getValue();
            $pDesc     = $permDescription->getValue();
            $pCat      = $permCategory->getValueKey();
            
            if(!$usrMgmt->updatePermission($permID, $pName, $pDesc, $pCat))
               $app->flash("error", "We were unable to update '".clean($pName)."' in the permissions list.");
            else
               $app->flash("success", "Updated '".clean($pName)."' in the permissions list.");
            
            $app->redirect("/admin/users/permissions");
         }
         else
         {
            $errorMessages = $updatePermForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $page->setError($e);
               }
            }
            else
            {
               $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
            }
         }
      }

      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"] = "Update Permission Level";
      $pageContent["content"]  = "<p>Use the form below to update details about the permission level.</p>\n";
      $pageContent["content"] .= $updatePermForm->generateForm();

      $page->displayContent($pageContent);

   }
   else
   {
      $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
      //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Switch from one user to another (useful for debugging).
 */
$app->map('(/admin/users/switch(/))', function() use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("User Management", $user, _MB_ROLE_SWITCH_USERS_);
   
   if($page->isAuthorized())
   {
      $userList = array();
      $userMgmt = new \Mumby\WebTools\UserManagement();
      $allUsers = $userMgmt->getAllUsers();
      
      if(!empty($allUsers))
      {
         foreach($allUsers as $u)
         {
            // Can't switch to yourself.
            if($u["username"] == $user->username)
               continue;
            
            // Can't become a super admin (no need to)
            if(in_array(_MB_USER_SUPER_ADMIN_, $u["permissions"]))
               continue;
            
            // Can't become an admin user if you're not already an admin
            if(in_array(_MB_USER_ADMIN_, $u["permissions"]) && !$user->hasPermission(_MB_USER_ADMIN_))
               continue;

            $userList[$u["username"]] = $u["firstName"]." ".$u["lastName"];
         }
      }
      
      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"] = "Switch Users";      
      $pageContent["content"] = "";
      
      if(empty($userList))
      {
         $page->setWarning("There are no users available for you to switch with!");
         
         $pageContent["content"] .= "<p>Hey there, lone wolf &mdash; It looks like you're the only user in the system!</p>";
         if($user->hasPermission(_MB_ROLE_MANAGE_USERS_))
            $pageContent["content"] .= "<p>If you would like to add additional users, please visit the <a href='/admin/users/add'>Add Users page</a>.</p>";
      }
      else
      {
         $switchUserForm = new Mumby\WebTools\SystemForm();
         $switchUserForm->submitValue = "Switch Users";

         $userField = $switchUserForm->addSelectField("User Name", $userList, true);

         if($switchUserForm->submitted())
         {
            $switchUserForm->processFormData();

            if($switchUserForm->processedData)
            {
               $username = $userField->getValueKey();

               if(!$user->switchUser($username))
                  $app->flash("error", "We were unable to change your identity.");
               else
                  $app->flash("success", "You are now logged in as '".$user->firstName." ".$user->lastName."'.<br /><br/> <strong>Please note that any changes you make will be recorded under that person's name!</strong> To log out as this user, simply click on the 'Sign Out' link at the top of the page.");

               $app->redirect("/admin");
            }
            else
            {
               $errorMessages = $addPermForm->getErrors();
               if(!empty($errorMessages))
               {
                  foreach($errorMessages as $e)
                  {
                     $page->setError($e);
                  }
               }
               else
               {
                  $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
               }
            }
         }

         $pageContent["content"]  = "<p>Use the form below to log in as a different user.</p>\n";
         $pageContent["content"] .= "<strong>Please note that any changes you make will be recorded under that person's name!</strong> To log out as this user, simply click on the 'Sign Out' link at the top of the page.</p>\n";
         $pageContent["content"] .= $switchUserForm->generateForm();//"<img src='/inject/sysform-3' />";
      }
      
      $page->displayContent($pageContent);

   }
   else
   {
      $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
      //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Simple function for returning a cleaned-up
 * permission name for output.
 */
function _cleanPerms($permStr)
{
  return trim(preg_replace('/[^a-zA-Z0-9\ ]/','',$permStr)); 
}