<?php

/*
 * Show menu-related functions.
 */
$app->get('(/admin/injections(/))', function() use($app, $user)
{
   // Create the Page instance
   $page = new Mumby\WebTools\AdminPage("Injections", $user, _MB_ROLE_EDIT_INJECTIONS_);
   
   if($page->isAuthorized())
   {
      $pageContent = array();
      $pageContent["title"] = "Injections";
      
      $menuOptions = array();
      $menuOptions[] = array(
          "label" => "View/Edit Injections",
          "link"  => "/admin/injections/view",
          "desc"  => "View a list of all available injections, including what pages they're used on within this website."
      );

      $menuOptions[] = array(
          "label" => "Add New Injection",
          "link"  => "/admin/injections/add",
          "desc"  => "Create a new injection for use within the website."
      );
      
      $pageContent["content"] = "<div class='adminOptionWrapper'>\n";
      $pageContent["content"] .= buildLandingPage($menuOptions);
      $pageContent["content"] .= "</div>\n";
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * Show all menus in the system.
 */
$app->get('(/admin/injections/view(/))', function() use($app, $user)
{
   // Create the Page instance
   $page = new Mumby\WebTools\AdminPage("Injections", $user, _MB_ROLE_EDIT_INJECTIONS_);
   
   if($page->isAuthorized())
   {
      $userIsAdmin = $user->hasPermission(_MB_USER_ADMIN_);
      $contentMgr = new Mumby\WebTools\Content();

      $pageContent = $contentMgr->getEmptyContentObj();
      $pageContent["title"] = "Injections";
      
      $pageContent["content"]  = "<p>Click on any injection title below to edit.</p>\n";
      $pageContent["content"] .= "<p>You can also add a new injection by visiting the <a href='/admin/injections/add'>Add Injection page</a>.</p>\n";
      
      $injMgr = new \Mumby\WebTools\InjectionData();
      $allInjections = $injMgr->getAllInjections(_MB_WEB_APPLICATION_ID_);
      
      if(!empty($allInjections))
      {
         $currentCategory = -1;
         //$peopleData = Mumby\DB\DeptMember::getPeopleFromInfo();
         
         $injectionOutput = "";
         $categories = array();

         $injectionData = array();
         $currentInjectionSet = array();
         foreach($allInjections as $i)
         {
            $name = "";
            $desc = "";
//            if(strpos($i["InjectionValue"], "people-") === 0)
//            {
//               $thisPerson = Mumby\DB\DeptMember::getPersonFromInfo(substr($i["InjectionValue"],7));
//               if(!empty($thisPerson["preferred_name"]))
//                  $value = $thisPerson["preferred_name"];
//               else
//                  $value = $thisPerson["first_name"];
//               $value .= " ".$thisPerson["last_name"];
//            }
//            else
               $value = clean($i["InjectionValue"]);
            
            $variable = "";
            $thisInjection = array(
               "Name"        => "<a href='/admin/injections/edit/".$i["InjectionVariableName"]."'>".clean($i["InjectionName"])."</a>",
               "Description" => clean($i["InjectionDesc"]),
               "Value"       => $value,
               "Category"    => $i["category_name"]
            );
            
            if($userIsAdmin)
               $thisInjection["Variable"] = $i["InjectionVariableName"];
            
            $injectionData[] = $thisInjection;
         }
         
         $table = new \Mumby\WebTools\Table();
         $pageContent["content"] .= $table->getTable($injectionData, "", "small", true, true);
      }
      else
         $pageContent["content"] .= "<p><em>No injections found!</em></p>";
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * Add a new injection to the system.
 */
$app->map('(/admin/injections/add(/))', function() use($app, $user)
{
   // Create the Page instance
   $page = new Mumby\WebTools\AdminPage("Injections", $user, _MB_ROLE_EDIT_INJECTIONS_);
   
   if($page->isAuthorized())
   {
      $userIsAdmin = $user->hasPermission(_MB_USER_ADMIN_);

      $injMgmt = new \Mumby\WebTools\InjectionData();

      $injectionForm = new Mumby\WebTools\SystemForm();
      $injectionName = $injectionForm->addTextField("Injection Name", true);

      $injCategories = $injMgmt->getAllInjectionCategories();
      $catOptions = array();
      foreach($injCategories as $c)
      {
         $catOptions[$c["InjectionCategoryID"]] = $c["InjectionCategoryName"];
      }

      $injectionCategory = $injectionForm->addSelectField("Injection Category", $catOptions, false);
      
      if(!$userIsAdmin)
         $injectionVariableName = $injectionForm->addHiddenField("Variable Name");
      else
         $injectionVariableName = $injectionForm->addTextField("Variable Name", true, array("moreLabel"=>"Variable names can be used by programmers to incorporate this information into system-generated web pages."));
      
      $contentType = $injectionForm->addRadioField("Content Type", array(
          _MB_WEB_APPLICATION_ID_=>"Local Data - Only used on this website",
          "-1"=>"Shared Data - Used on this and other websites"
      ), true, array("checked"=>_MB_WEB_APPLICATION_ID_));
      
      $injectionValue = $injectionForm->addTextField("Injection Value");
//      $people = Mumby\DB\DeptMember::getPeopleFromInfo();
//      $allPeopleOptions = array();
//      foreach($people as $p)
//      {
//         $allPeopleOptions[trim($p["netid"])] = trim($p["last_name"]).", ".trim($p["first_name"]);
//      }
//      $peopleValue    = $injectionForm->addSelectField("Injection Value", $allPeopleOptions);
//      $peopleValue->wrapperClass = "hidden";
      
      $injectionDesc  = $injectionForm->addTextAreaField("Injection Description", true, array("moreLabel"=>"Brief description of how this can be used."));

      /*
       * Add toggle functionality for injection value. When the user
       * selects people data, they should get a dropdown list of
       * people rather than an input box.
       */
//      $inputFieldID  = "fieldWrapper_".$injectionValue->getFieldID();
//      $selectFieldID = "fieldWrapper_".$peopleValue->getFieldID();
//      $toggleValueFields  = "if($(this).val() == -2) {\n";
//      $toggleValueFields .= "   \$('#".$inputFieldID."').addClass('hidden');\n   $('#".$selectFieldID."').removeClass('hidden');\n";
//      $toggleValueFields .= "} else {\n";
//      $toggleValueFields .= "\$('#".$selectFieldID."').addClass('hidden');\n   $('#".$inputFieldID."').removeClass('hidden');\n";
//      $toggleValueFields .= "}\n";
//      $contentType->onchange = $toggleValueFields;

      if($injectionForm->submitted())
      {
         // We'll need to manually check these since only
         // one is actually required.
         //$peopleValue->required = false;
         $injectionValue->required = false;
         
         $injectionForm->processFormData();

         if($injectionForm->processedData)
         {
            $submittedInjName = $injectionName->getValue();
            
            if($userIsAdmin)
               $submittedInjVar  = $injectionVariableName->getValue();
            else
            {
               $newVariableName = preg_replace("/[^a-zA-Z0-9]/", "", $submittedInjName);
               while(($alreadyExists = $injMgmt->getInjection($newVariableName)))
               {
                  $newVariableName .= rand(0,10);
               }
               $submittedInjVar = $newVariableName;
            }
            
            $alreadyExists = $injMgmt->getInjection($submittedInjVar);
            if($alreadyExists && $alreadyExists["InjectionID"] != $thisInjection["InjectionID"])
            {
               $page->setError("The variable name specified is already in use by another injection.");
            }
            else
            {
               $submittedInjDesc = $injectionDesc->getValue();
               $submittedInjCat  = $injectionCategory->getValueKey();

               $submittedInjType = $contentType->getValueKey();

               if($submittedInjType == -2)
                  $submittedInjVal = $peopleValue->getValueKey();
               else
                  $submittedInjVal = $injectionValue->getValue();
               
               if(empty($submittedInjVal))
                  $page->setError("Missing injection value!");
               else
               {
                  if($submittedInjType == -2)
                     $submittedInjVal = "people-".$submittedInjVal;

                  if($submittedInjType != _MB_WEB_APPLICATION_ID_)
                     $submittedInjType = null;

                  if(!$injMgmt->addInjection($submittedInjName, $submittedInjDesc, $submittedInjVar, $submittedInjVal, $submittedInjCat))
                  {
                     $errorMessages = $injMgmt->getErrors();
                     if(!empty($errorMessages))
                     {
                        foreach($errorMessages as $e)
                        {
                           $app->flash("error", $e);
                        }
                     }
                     else
                     {
                        $app->flash("error", "<div class='errorMessage'>There was an error adding the injection.</div>");
                     }
                  }
                  else
                  {
                     $app->flash("success", "The injection was added successfully.</a>.");
                  }

                  $app->redirect("/admin/injections/view");
               }
            }
         }
         else
         {
            $errorMessages = $injectionForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $page->setError($e);
               }
            }
            else
            {
               $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
            }
         }
      }
      else
      {
         $page->addJS('/common/admin/js/injections.js');
      }
      
      $pageContent = array();
      $pageContent["title"] = "Add Injection";      
      $pageContent["content"]  = "<p>Use the form below to add a new injection to the system.</p>\n";
      $pageContent["content"] .= $injectionForm->generateForm();
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Display form for updating injections.
 */
$app->map('(/admin/injections/edit/:injID(/))', function($injID) use($app, $user)
{
   // Create the Page instance
   $page = new Mumby\WebTools\AdminPage("Injections", $user, _MB_ROLE_EDIT_INJECTIONS_);
   
   if($page->isAuthorized())
   {
      $userIsAdmin = $user->hasPermission(_MB_USER_ADMIN_);

      $injMgmt = new \Mumby\WebTools\InjectionData();
      $thisInjection = $injMgmt->getInjection($injID);

      if(empty($thisInjection))
      {
         $app->flash('error','We were unable to find the requested injection for editing.');
         $app->redirect("/admin/injections/view");
      }

      if($thisInjection["ApplicationID"] == _MB_WEB_APPLICATION_ID_)
         $injectionScope = _MB_WEB_APPLICATION_ID_;
      else
      {
         if(strpos($thisInjection["InjectionValue"], "people-") !== false)
            $injectionScope = "-2";
         else
            $injectionScope = "-1";
      }
      
      $injectionForm = new Mumby\WebTools\SystemForm();

      $injectionName = $injectionForm->addTextField("Injection Name", true);

      $injCategories = $injMgmt->getAllInjectionCategories();
      $catOptions = array();
      foreach($injCategories as $c)
      {
         $catOptions[$c["InjectionCategoryID"]] = $c["InjectionCategoryName"];
      }

      $injectionCategory = $injectionForm->addSelectField("Injection Category", $catOptions, false);
      
      if(!$userIsAdmin)
         $injectionVariableName = $injectionForm->addHiddenField("Variable Name");
      else
         $injectionVariableName = $injectionForm->addTextField("Variable Name", true, array("moreLabel"=>"Variable names can be used by programmers to incorporate this information into system-generated web pages."));
      
      $contentType = $injectionForm->addRadioField("Content Type", array(
          _MB_WEB_APPLICATION_ID_=>"Local Data - Only used on this website",
          "-1"=>"Shared Data - Used on this and other websites",
          "-2"=>"People Data"
      ), true);
      
      $injectionValue = $injectionForm->addTextField("Injection Value", true);
//      $people = Mumby\DB\DeptMember::getPeopleFromInfo();
//      $allPeopleOptions = array();
//      foreach($people as $p)
//      {
//         $allPeopleOptions[trim($p["netid"])] = trim($p["last_name"]).", ".trim($p["first_name"]);
//      }
//      $peopleValue    = $injectionForm->addSelectField("Injection Value", $allPeopleOptions, true);
      if($injectionScope != -2)
      {
         //$peopleValue->wrapperClass = "hidden";
         $injectionValue->setValue($thisInjection["InjectionValue"]);
      }
      else
      {
         $injectionValue->wrapperClass = "hidden";
         $peopleValue->setValue(substr($thisInjection["InjectionValue"], 7));
      }
      
      $injectionDesc  = $injectionForm->addTextAreaField("Injection Description", true, array("moreLabel"=>"Brief description of how this can be used."));

      /*
       * Add toggle functionality for injection value. When the user
       * selects people data, they should get a dropdown list of
       * people rather than an input box.
       */
//      $inputFieldID  = "fieldWrapper_".$injectionValue->getFieldID();
//      $selectFieldID = "fieldWrapper_".$peopleValue->getFieldID();
//      $toggleValueFields  = "if($(this).val() == -2) {\n";
//      $toggleValueFields .= "   \$('#".$inputFieldID."').addClass('hidden');\n   $('#".$selectFieldID."').removeClass('hidden');\n";
//      $toggleValueFields .= "   \$('#".$inputFieldID." input').removeAttr('required');\n   $('#".$selectFieldID." select').attr('required','required');\n";
//      $toggleValueFields .= "} else {\n";
//      $toggleValueFields .= "   \$('#".$selectFieldID."').addClass('hidden');\n   $('#".$inputFieldID."').removeClass('hidden');\n";
//      $toggleValueFields .= "   \$('#".$selectFieldID." select').removeAttr('required');\n   $('#".$inputFieldID." input').attr('required','required');\n";
//      $toggleValueFields .= "}\n";
//      $contentType->onchange = $toggleValueFields;
      
      if($injectionForm->submitted())
      {
         // We'll need to manually check these since only
         // one is actually required.
         //$peopleValue->required = false;
         $injectionValue->required = false;
         
         $injectionForm->processFormData();

         if($injectionForm->processedData)
         {
            $submittedInjVar  = $injectionVariableName->getValue();
            
            $alreadyExists = $injMgmt->getInjection($submittedInjVar);
            if($alreadyExists && $alreadyExists["InjectionID"] != $thisInjection["InjectionID"])
            {
               $page->setError("The variable name specified is already in use by another injection.");
            }
            else
            {
               $submittedInjName = $injectionName->getValue();
               $submittedInjDesc = $injectionDesc->getValue();
               $submittedInjCat  = $injectionCategory->getValueKey();
               $submittedInjType = $contentType->getValueKey();

               if($submittedInjType == -2)
                  $submittedInjVal = $peopleValue->getValueKey();
               else
                  $submittedInjVal = $injectionValue->getValue();
               
               if(empty($submittedInjVal))
                  $page->setError("Missing injection value!");
               else
               {
                  if($submittedInjType == -2)
                     $submittedInjVal = "people-".$submittedInjVal;
                  
                  if($submittedInjType != _MB_WEB_APPLICATION_ID_)
                     $submittedInjType = null;

                  if(!$injMgmt->updateInjection($thisInjection["InjectionID"], $submittedInjName, $submittedInjDesc, $submittedInjVar, $submittedInjVal, $submittedInjCat, $submittedInjType))
                  {
                     $errorMessages = $injMgmt->getErrors();
                     if(!empty($errorMessages))
                     {
                        foreach($errorMessages as $e)
                        {
                           $app->flash("error", $e);
                        }
                     }
                     else
                     {
                        $app->flash("error", "<div class='errorMessage'>There was an error updating the injection.</div>");
                     }
                  }
                  else
                  {
                     $app->flash("success", "The injection was updated successfully.</a>.");
                  }

                  $app->redirect("/admin/injections/view");
               }
            }
         }
         else
         {
            $errorMessages = $injectionForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $page->setError($e);
               }
            }
            else
            {
               $page->setError("<div class='alert alert-danger'>There was an error submitting the form.</div>");
            }
         }
      }
      else
      {
         $injectionName->setValue($thisInjection["InjectionName"]);
         $injectionDesc->setValue($thisInjection["InjectionDesc"]);
         $injectionVariableName->setValue($thisInjection["InjectionVariableName"]);
         $injectionCategory->setValue($thisInjection["InjectionCategoryID"]);
         $contentType->setValue($injectionScope);
         
         $page->addJS('/common/admin/js/injections.js');
      }
      
      $pageContent = array();
      $pageContent["title"] = "Injection Information";      
      $pageContent["content"]  = "<p>Use the form below to edit this injection's information.</p>\n";
      $pageContent["content"] .= $injectionForm->generateForm();
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');


/*
 * Javascript file listing all injections
 */
$app->get('/js/injectionData.js', function() use($app)
{
   //Injections are not yet implemented
   die();

   // Create the Page instance
   $page  = new Mumby\WebTools\JavascriptPage($app);

//   $people = \Mumby\DB\DeptMember::getPeopleFromInfo();
//   
//   $jsonPeople = array();
//   foreach($people as $p)
//   {
//      $thisPerson = trim($p["last_name"]).", ".trim($p["first_name"]);
//      $jsonPeople[] = array("text"=>$thisPerson, "value"=>trim($p["netid"]));
//   }
   
   $formData = new Mumby\WebTools\Form();
   $forms = $formData->getAllForms();
   $jsonForms = array();
   foreach($forms as $f)
   {
      $jsonForms[] = array("text"=>trim($f["FormName"]), "value"=>((int) $f["FormID"]));
   }

   // Get all FAQs
   $faqData = new Mumby\WebTools\FAQ();
   $faqs = $faqData->getAllFAQs();
   $jsonFAQs = array();
   foreach($faqs as $f)
   {
      $jsonFAQs[] = array("text"=>trim($f["FAQTitle"]), "value"=>((int) $f["FAQID"]));
   }

   // Get all Injections
   $injData = new Mumby\WebTools\InjectionData();
   $injections = $injData->getAllInjections();
   $jsonInjections = array();
   foreach($injections as $i)
   {
      // Don't include system variables.
      if($i["InjectionCategoryID"] == 10)
         continue;

      $iLabel = trim($i["InjectionName"]);
//      if(strpos($i["InjectionValue"], "people-") === 0)
//      {
//         // Append the person's name to the label
//         $thisPerson = \Mumby\DB\DeptMember::getPersonFromInfo(substr($i["InjectionValue"],7));
//         $iLabel .= " (".trim($thisPerson["first_name"])." ".trim($thisPerson["last_name"]).")";
//      }
//      else
//      {
         // Append the value of the injection.
         $iLabel .= " (".$i["InjectionValue"].")";
      //}

      $jsonInjections[] = array("text"=>$iLabel, "value"=>trim($i["InjectionVariableName"]));
   }

   // Get all Courses
   $courseData = new \Mumby\DB\Course();
   $allCourses = $courseData->getAllCourses();
   $jsonCourses = array();
   foreach($allCourses as $c)
   {
      $cLabel = trim($c["SubjectPrefix"]." ".$c["CourseNumber"]." - ".$c["CourseName"]);
      $cValue = $c["CourseID"];

      $jsonCourses[] = array("text"=>$cLabel, "value"=>$cValue);
   }

   $contentMgmt = new Mumby\WebTools\Content();
   $allPages = $contentMgmt->getAllContent();
   if(!empty($allPages))
   {
      $linkList = array();
      foreach($allPages as $p)
      {
         $linkList[$p["title"].$p["pageID"]] = array("title"=>$p["title"], "value"=>$p["url"]);
      }
      ksort($linkList);

      $jsonLinks = json_encode($linkList);
   }
   else
      $jsonLinks = "!1";

   // Add JavaScript variables for the editor to use
   $jsData  = "";
   $jsData .= "var peopleList = ".json_encode($jsonPeople).";\n\n";
   $jsData .= "var formList = ".json_encode($jsonForms).";\n\n";
   $jsData .= "var faqList = ".json_encode($jsonFAQs).";\n\n";
   $jsData .= "var dataList = ".json_encode($jsonInjections).";\n\n";
   $jsData .= "var courseList = ".json_encode($jsonCourses).";\n\n";
   $jsData .= "var linksList = ".$jsonLinks.";\n\n";
   
   $contentObj = $page->getEmptyContentObj();
   $contentObj["content"] = $jsData;

   $page->displayContent($contentObj);
});