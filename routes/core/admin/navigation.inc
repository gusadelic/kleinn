<?php

/*
 * Show menu-related functions.
 */
$app->get('(/admin/menus(/))', function() use($app, $user) // DONE
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Menus", $user, _MB_ROLE_EDIT_WEBSITE_NAVIGATION_);

   if($page->isAuthorized())
   {
      $pageContent = array();
      $pageContent["title"] = "Website Navigation";
      
      $menuOptions = array();
      $menuOptions[] = array(
          "label" => "View/Edit Menus",
          "link"  => "/admin/menus/view",
          "desc"  => "View a list of all available menus within the website."
      );

      $menuOptions[] = array(
          "label" => "Add New Menu",
          "link"  => "/admin/menus/add",
          "desc"  => "Create a new menu for the system.",
          "class" => "todo"
      );
      
      $pageContent["content"] = "<div class='adminOptionWrapper'>\n";
      $pageContent["content"] .= buildLandingPage($menuOptions);
      $pageContent["content"] .= "</div>\n";
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * Show all menus in the system.
 */
$app->get('(/admin/menus/view(/))', function() use($app, $user) 
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Menus", $user, _MB_USER_AUTHENTICATED_);

   if($page->isAuthorized())
   {
      $pageContent = array();
      $pageContent["title"] = "Website Menus";
      
      $pageContent["content"]  = "<p>Click on any menu title below to edit.</p>\n";
      $pageContent["content"] .= "<p>You can also add a new menu by visiting the <a href='/admin/menus/add'>Add Menu page</a>.</p>\n";
      
      $menuMgr = new \Mumby\WebTools\Menu();
      $allMenus = $menuMgr->getAllMenus();
      
      if(!empty($allMenus))
      {  
         $menuOptions = array();
         $hasSystemMenuAccess = $user->hasPermission(_MB_ROLE_EDIT_SYSTEM_NAVIGATION_);

         foreach($allMenus as $m)
         {
            if($m["IsSystemMenu"] && !$hasSystemMenuAccess)
               continue;
            
            $thisMenu = array(
                "id"    => $m['MenuID'],
                "label" => $m["MenuName"],
                "link"  => "/admin/menus/editNav/".$m["MenuID"],
                "desc"  => $m["MenuDesc"],
                "ops"  => ""
            );

            if($m["IsSystemMenu"])
               $thisMenu["desc"] .= "<br /><span class='label label-danger'>Danger</span> <em class='text-danger'>This is a system menu - edit at your own risk!</em>";

            $thisMenu['desc'] = "<p>".$thisMenu["desc"]."</p>";
            
            $thisMenu["ops"]  = "<p>".$thisMenu["ops"];
            $thisMenu["ops"] .= "<br /><em><a href='/admin/menus/edit/".$m["MenuID"]."'>Edit menu details</a></em>";
            $thisMenu["ops"] .= "<br /><em><a href='/admin/menus/editNav/".$m["MenuID"]."'>Edit navigation links</a></em>";
            $thisMenu["ops"] .= "</p>";
            
            $menuOptions[] = $thisMenu;
         }

         $pageContent["content"] = "<div class='adminOptionWrapper'>\n";
         $pageContent["content"] .= buildLandingPage($menuOptions);
         $pageContent["content"] .= "</div>\n";
      }
      else
         $pageContent["content"] .= "<p><em>No menus found!</em></p>";
      
      
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * Add a new menu to the system.
 */
$app->map('(/admin/menus/add(/))', function() use($app, $user) // TODO
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Menus", $user, _MB_ROLE_EDIT_WEBSITE_NAVIGATION_);
   
   if($page->isAuthorized())
   {
      $pageContent = array();
      $pageContent["title"] = "Website Navigation";
      $pageContent["content"] = "<div class='adminOptionWrapper'>\n";
      
      $form = new \Mumby\WebTools\SystemForm();
      $name = $form->addTextField("Menu Name", true);
      $desc = $form->addTextAreaField("Description", true, array("rows"=>2));
      $injection = $form->addTextField("Menu Identifier", true, array("moreLabel"=>"This is used to create injection point within your HTML templates."));

      $form->addFieldBreak();

      $form->addMarkup("<div class='row'><p class='alert alert-info col-sm-offset-2 col-sm-8'>Some menus are used within the application itself (administration menus, etc.). These menus are not visible to anyone other than system administrators. If this is a menu that you'd like protected from non-admins, be sure to check the box below.</p></div>");
      $system = $form->addCheckboxField("System Form", array("This is a system form"), false, array('hiddenLabel'=>true) );

      $form->addFieldBreak();

      $formatOptions = array(
         "1"=>"Horizontal Dropdown",
         "2"=>"Sidebar",
         "3"=>"Inline"
      );
      $menuFormat = $form->addRadioField("Menu Format", $formatOptions);
      
      $form->addFieldBreak();
      
      $onClick = "if(\$(this).is(':checked')) \$('#mobileOptions').removeClass('hidden'); else \$('#mobileOptions').addClass('hidden');";
      $mobileFriendly = $form->addCheckboxField("Mobile Friendly?", array("Is this menu mobile friendly?"), false, array("hiddenLabel"=>true, "onclick"=>$onClick));
      $mobileFieldset = $form->addFieldBreak("mobileOptions");
      
      $brandingText   = $form->addTextField("Branding Text", false, array("moreLabel"=>"This is the text displayed when the menu is viewed on a mobile device.", "placeholder"=>"Menu"));
      $brandingLink   = $form->addTextField("Branding Link", false, array("moreLabel"=>"This is the URL users will be taken to when clicking on the branding text.", "placeholder"=>"/"));
      $hideBranding   = $form->addCheckboxField("Hide branding?", array("Hide branding link/text?"), false, array("hiddenLabel"=>true));

      $form->submitValue = "Create Menu";
      $form->submitClass = "btn-success";
      
      if($form->submitted())
      {
         $form->processFormData();
         
         $isSystemMenu = $system->getValue();
         if(empty($isSystemMenu))
            $isSystemMenu = false;
         else
            $isSystemMenu = true;

         $metadata = array();
         $metadata["menuInjectionID"] = $injection->getValue();
         
         $thisFormat = $menuFormat->getValueKey();
         switch($thisFormat)
         {
            case 1:
               $metadata["isSidebar"] = false;
               $metadata["isInline"] = false;
               break;
            case 2:
               $metadata["isSidebar"] = true;
               $metadata["isInline"] = false;
               break;
            case 3:
               $metadata["isSidebar"] = false;
               $metadata["isInline"] = true;
               break;
            default:
               break;
         }
         
         $metadata["hideRoot"] = true;
         
         $isMobileFriendlyValue = $mobileFriendly->getValue();
         if(!empty($isMobileFriendlyValue))
         {
            $metadata["isMobileFriendly"] = true;
            $bText = $brandingText->getValue();            
            $bLink = $brandingLink->getValue();
            $bHide = $hideBranding->getValue();
            
            if(empty($bText))
               $metadata["brandText"] = "";
            else
               $metadata["brandText"] = $bText;
            
            if(empty($bLink))
               $metadata["brandLink"] = "#";
            else
               $metadata["brandLink"] = $bLink;
            
            if(!empty($bHide))
               $metadata["hideBrand"] = true;
            else
               $metadata["hideBrand"] = false;
         }
         else
         {
            $metadata["isMobileFriendly"] = false;
            $metadata["brandText"] = "";
            $metadata["brandLink"] = "#";
            $metadata["hideBrand"] = true;     
         }
         
         $data = array(
            "MenuName" => $name->getValue(),
            "MenuDesc" => $desc->getValue(),
            "MenuMetadata" => json_encode($metadata),
            "IsSystemMenu" => $isSystemMenu
         );
         
         
         if(($newMenuID = $form->insert($data, "Menus")))
         {
            $rootLinkData = array(
               "MenuID"=>$newMenuID,
               "MenuItemText"=>"Home",
               "MenuItemLinkAttr"=>"",
               "MenuItemLink"=>"/",
               "MenuItemParentID"=>0,
               "MenuItemRestricted"=>"_MB_USER_ANONYMOUS_",
               "MenuItemLHS"=>1,
               "MenuItemRHS"=>2,
               "MenuItemDepth"=>0,
               "MenuItemMetadata"=>"{'noPageAttachment':true}",
               "MenuItemIsUpdated"=>1,
            );
            
            if($form->insert($rootLinkData, "MenuItems"))
            {
               $app->flash("success", "Your menu was created successfully!");
               $app->redirect("/admin/menus/editNav/".$newMenuID);               
            }
            else
            {
               $app->flash("error", "We were unable to create the menu.");
               $app->redirect("/admin/menus");
            }
         }
         else
         {
            $app->flash("error", "We were unable to create the menu.");
            $app->redirect("/admin/menus");
         }
      }
      else
      {
         $mobileFriendly->checked = 0;
         $mainContent = "<p>Use the form below to create a new menu. After you create a menu, you will be taken to the next page where you will be able to add links to your new menu.</p>";
         
         $mainContent  = "<p>";
         $mainContent .= "Use the form below to create a new menu. After you ";
         $mainContent .= "create a menu, you will be taken to the next page ";
         $mainContent .= "where you will be able to add links to your new menu.";
         $mainContent .= "</p>";

         $mainContent .= $form->generateForm();
         $pageContent["content"] = $mainContent;
      }
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Edit menu details.
 */
$app->map('(/admin/menus/edit/:menuID(/))', function($menuID) use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Menus", $user, _MB_ROLE_EDIT_WEBSITE_NAVIGATION_);
   
   if($page->isAuthorized())
   {
      $thisMenu = new Mumby\WebTools\Menu($menuID);
      if($thisMenu->MenuID === false)
      {
         $app->flash("error", "We were unable to find the requested menu.");
         $app->redirect("/admin/menus", 404);
      }
      
      $pageContent = array();
      $pageContent["title"] = "Website Navigation";
      
      $pageContent["content"] = "<div class='adminOptionWrapper'>\n";
      
      $form = new \Mumby\WebTools\SystemForm();
      $name = $form->addTextField("Menu Name", true);
      $desc = $form->addTextAreaField("Description", true, array("rows"=>2));
      $injection = $form->addTextField("Menu Identifier", true, array("moreLabel"=>"This is used to create injection point within your HTML templates."));
      $rootText = $form->addTextField("Root Link Text", true);
      $rootLink = $form->addTextField("Root Link URL", true);
      $form->addMarkup("<div class='row'><p class='alert alert-info col-sm-offset-2 col-sm-8'>While you are required to specify a root node, it is oftentimes helpful to have a root menu item (i.e., Home). That said, you may not want to display said menu item in your navigation. You can use the box below to toggle this functionality.</p></div>");
      $ignoreRoot = $form->addCheckboxField("Ignore Root Element", array("Ignore Root Element"), false, array('hiddenLabel'=>true) );
      $form->addFieldBreak();

      $form->addMarkup("<div class='row'><p class='alert alert-info col-sm-offset-2 col-sm-8'>Some menus are used within the application itself (administration menus, etc.). These menus are not visible to anyone other than system administrators. If this is a menu that you'd like protected from non-admins, be sure to check the box below.</p></div>");
      $system = $form->addCheckboxField("System Form", array("This is a system form"), false, array('hiddenLabel'=>true) );

      $form->addFieldBreak();

      $formatOptions = array(
         "1"=>"Horizontal Dropdown",
         "2"=>"Sidebar",
         "3"=>"Inline"
      );
      $menuFormat = $form->addRadioField("Menu Format", $formatOptions);
      
      $form->addFieldBreak();
      
      $onClick = "if(\$(this).is(':checked')) \$('#mobileOptions').removeClass('hidden'); else \$('#mobileOptions').addClass('hidden');";
      $mobileFriendly = $form->addCheckboxField("Mobile Friendly?", array("Is this menu mobile friendly?"), false, array("hiddenLabel"=>true, "onclick"=>$onClick));
      $mobileFieldset = $form->addFieldBreak("mobileOptions");
      
      $brandingText   = $form->addTextField("Branding Text", false, array("moreLabel"=>"This is the text displayed when the menu is viewed on a mobile device.", "placeholder"=>"Menu"));
      $brandingLink   = $form->addTextField("Branding Link", false, array("moreLabel"=>"This is the URL users will be taken to when clicking on the branding text.", "placeholder"=>"/"));
      $hideBranding   = $form->addCheckboxField("Hide branding?", array("Hide branding link/text?"), false, array("hiddenLabel"=>true));
      
      $thisMenuID = $form->addHiddenField();

      $rootNode = $thisMenu->getRootNode($thisMenu->MenuID);
      
      if($form->submitted())
      {
         $form->processFormData();
         
         // Something fishy is going on with the form...
         if($thisMenuID->getValue() != $thisMenu->MenuID)
         {
            $app->flash("error", "The menu information provided was invalid!");
            $app->redirect("/admin/menus/view", 404);
         }

         $isSystemMenu = $system->getValue();
         if(empty($isSystemMenu))
            $isSystemMenu = false;
         else
            $isSystemMenu = true;

         $metadata = array();
         $metadata["menuInjectionID"] = $injection->getValue();
         
         $thisFormat = $menuFormat->getValueKey();
         switch($thisFormat)
         {
            case 1:
               $metadata["isSidebar"] = false;
               $metadata["isInline"] = false;
               break;
            case 2:
               $metadata["isSidebar"] = true;
               $metadata["isInline"] = false;
               break;
            case 3:
               $metadata["isSidebar"] = false;
               $metadata["isInline"] = true;
               break;
            default:
               break;
         }
         
         $ignoreRootValue = $ignoreRoot->getValue();
         if(!empty($ignoreRootValue))
            $metadata["hideRoot"] = true;
         
         $isMobileFriendlyValue = $mobileFriendly->getValue();
         if(!empty($isMobileFriendlyValue))
         {
            $metadata["isMobileFriendly"] = true;
            $bText = $brandingText->getValue();            
            $bLink = $brandingLink->getValue();
            $bHide = $hideBranding->getValue();
            
            if(empty($bText))
               $metadata["brandText"] = "";
            else
               $metadata["brandText"] = $bText;
            
            if(empty($bLink))
               $metadata["brandLink"] = "#";
            else
               $metadata["brandLink"] = $bLink;
            
            if(!empty($bHide))
               $metadata["hideBrand"] = true;
            else
               $metadata["hideBrand"] = false;
         }
         else
         {
            $metadata["isMobileFriendly"] = false;
            $metadata["brandText"] = "";
            $metadata["brandLink"] = "#";
            $metadata["hideBrand"] = true;     
         }
         
         $data = array(
            "MenuName" => $name->getValue(),
            "MenuDesc" => $desc->getValue(),
            "MenuMetadata" => json_encode($metadata),
            "IsSystemMenu" => $isSystemMenu
         );
         
         if($form->update($data, array("MenuID"=>$menuID), "Menus") !== false)
         {
            $rootLinkData = array(
               "MenuID"=>$menuID,
               "MenuItemText"=>$rootText->getValue(),
               "MenuItemLink"=>$rootLink->getValue()
            );
            
            if($form->update($rootLinkData, array("MenuItemID"=>$rootNode["MenuItemID"]), "MenuItems") !== false)
            {
               $app->flash("success", "Your menu was updated successfully!");
               $app->redirect("/admin/menus/view");               
            }
            else
            {
               $app->flash("error", "We were unable to update your menu.");
               $app->redirect("/admin/menus/view");
            }
         }
         else
         {
            $app->flash("error", "We were unable to create the menu.");
            $app->redirect("/admin/menus");
         }
      }
      else
      {
         $name->setValue($thisMenu->MenuName);
         $desc->setValue($thisMenu->MenuDesc);

         $originalMetadata = json_decode($thisMenu->MenuMetadata, true);
         if($originalMetadata["hideRoot"])
            $ignoreRoot->checked = 0;
         if($thisMenu->IsSystemMenu)
            $system->checked = 0;

         $injection->setValue($originalMetadata["menuInjectionID"]);
         $rootText->setValue($rootNode["MenuItemText"]);
         $rootLink->setValue($rootNode["MenuItemLink"]);

         if(isset($originalMetadata["isSidebar"]) && $originalMetadata["isSidebar"])
            $menuFormat->setValue(2);
         else if(isset($originalMetadata["isInline"]) && $originalMetadata["isInline"])
            $menuFormat->setValue(3);
         else
            $menuFormat->setValue(1);

         if(isset($originalMetadata["isMobileFriendly"]) && $originalMetadata["isMobileFriendly"])
         {
            $mobileFriendly->checked = 0;
            $mobileFieldset->FormFieldClass = "";
         }
         else
         {
            $mobileFieldset->FormFieldClass = "hidden";            
         }

         if(isset($originalMetadata["brandText"]))
            $brandingText->setValue($originalMetadata["brandText"]);
         else
            $brandingText->setValue("");

         if(isset($originalMetadata["brandLink"]))
            $brandingLink->setValue($originalMetadata["brandLink"]);
         else
            $brandingLink->setValue("");

         if(isset($originalMetadata["hideBrand"]) && $originalMetadata["hideBrand"])
            $hideBranding->checked = 0;

         $thisMenuID->setValue($menuID);

         $mainContent  = "<p>";
         $mainContent .= "Use the form below to create a new menu. After you ";
         $mainContent .= "create a menu, you will be taken to the next page ";
         $mainContent .= "where you will be able to add links to your new menu.";
         $mainContent .= "</p>";

         $mainContent .= $form->generateForm();
         $pageContent["content"] = $mainContent;
      }

      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');

/*
 * Display form for updating menus.
 */
$app->get('(/admin/menus/editNav/:menuid(/))', function($menuID) use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Menus", $user, _MB_ROLE_EDIT_WEBSITE_NAVIGATION_);

   if($page->isAuthorized())
   {
      $menu = new Mumby\WebTools\Menu($menuID);
      $navArray = $menu->getNavigationAsArray();

      if($navArray === false)
      {
         $app->flash('error','We were unable to find the requested menu for editing.');
         $app->redirect("/admin/menus/view");
      }
   
      $page->addJS( array(
         '/common/bower/jquery-nested-sortable/jquery.ui.nestedSortable.js',
         '/common/admin/js/nav.js'
      ));
      
      $page->addCSS( array(
         '/common/admin/css/navigation.css'
      ));
      
      $pageContent = array();
      $pageContent["title"]      = "Edit Menu";
      $pageContent["parentLink"] = null;
      $pageContent["id"]         = null;

      
      $currentDepth = -1;
      $flag = false;
      
      $output  = "<span onclick='javascript:addNavItem();return false' class='addButton btn btn-success'><i class='glyphicon glyphicon-plus'></i> Add New Menu Item</span>";
      $output .= "<form action='/admin/menus/editNav/".((int) $menuID)."' method='post'>";
      
      $indentLevel = 4;
      $startIndentLevel = $indentLevel;
      $indent = "   ";
      
      $linkCounter = 0;

      foreach($navArray as $link)
      {
         if(isset($link["MenuItemLinkAttr"]) && !empty($link["MenuItemLinkAttr"]))
            $linkAttr = str_replace("'","\"",$link["MenuItemLinkAttr"]);
         else
            $linkAttr = "";

         if($link["MenuItemDepth"] > $currentDepth)
         {
            while($link["MenuItemDepth"] != $currentDepth)
            {
               $output .= "\n".str_repeat($indent, $indentLevel)."<ol";
               if($linkCounter == 0)
                  $output .= " id='editNav' class='sortable'";
               $output .= ">\n";
               $indentLevel++;
               $output .= str_repeat($indent, $indentLevel)."<li";
               $output .= " id='menu_".$linkCounter."'";
               $output .= ">\n";
               $flag = false;
               $currentDepth++;
            }
         }
         
         if($link["MenuItemDepth"] < $currentDepth)
         {
            while($link["MenuItemDepth"] < $currentDepth)
            {
               $output .= str_repeat($indent, $indentLevel)."</li>\n";
               $indentLevel--;
               $output .= str_repeat($indent, $indentLevel)."</ol>\n";
               $currentDepth--;
            }
         }
         
         if($flag)
         {
            $output .= str_repeat($indent, $indentLevel)."</li>\n";
            $output .= str_repeat($indent, $indentLevel)."<li";
            $output .= " id='menu_".$linkCounter."'";
            $output .= ">\n";
            $flag = false;
         }
         
         $output .= str_repeat($indent, $indentLevel)."<div>\n";
         $indentLevel++;
         if($link["MenuItemDepth"] != 0)
         {
            $output .= str_repeat($indent, $indentLevel)."<i class='fa fa-sort-amount-asc glyphicon glyphicon-sort'></i>\n";
            $output .= str_repeat($indent, $indentLevel)."<span class='deleteMe fa fa-ban glyphicon glyphicon-remove-circle' onclick='javascript: deleteItem(".$linkCounter."); return false;'></span>\n";
            $output .= str_repeat($indent, $indentLevel)."<input type='hidden' name='menu_".$linkCounter."_id' value='".($link["MenuItemID"])."' />\n";
            $output .= str_repeat($indent, $indentLevel)."<input type='hidden' name='menu_".$linkCounter."_attr' value='".htmlentities($linkAttr)."' />\n";
            $output .= str_repeat($indent, $indentLevel)."<input type='text' name='menu_".$linkCounter."_text' value='".htmlentities($link["MenuItemText"])."' />\n";
            $output .= str_repeat($indent, $indentLevel)."<input type='text' name='menu_".$linkCounter."_link' value='".htmlentities($link["MenuItemLink"])."' />\n";
         }
         else
         {
            $output .= str_repeat($indent, $indentLevel)."<input type='hidden' name='menu_".$linkCounter."_id' value='".($link["MenuItemID"])."' />\n";
            $output .= str_repeat($indent, $indentLevel)."<input type='hidden' name='menu_".$linkCounter."_text' value='".htmlentities($link["MenuItemText"])."' />\n";
            $output .= str_repeat($indent, $indentLevel)."<input type='hidden' name='menu_".$linkCounter."_link' value='".htmlentities($link["MenuItemLink"])."' />\n";
            $output .= str_repeat($indent, $indentLevel)."<input type='hidden' name='menu_".$linkCounter."_attr' value='".htmlentities($linkAttr)."' />\n";
            $output .= str_repeat($indent, $indentLevel).htmlentities($link["MenuItemText"])."\n";
         }
         $indentLevel--;
         $output .= str_repeat($indent, $indentLevel)."</div>\n";
         $flag = true;
         
         $linkCounter++;
      }
      
      // Make sure we close the list.
      while($startIndentLevel < $indentLevel)
      {
         $output .= str_repeat($indent, $indentLevel)."</li>\n";
         $indentLevel--;
         $output .= str_repeat($indent, $indentLevel)."</ol>\n";
      }

      $pageContent["content"] = $output;
      
      $pageContent["content"] .= "<div class='submitDiv'>\n";
      $pageContent["content"] .= "<input type='hidden' name='serialized' id='serialized' value='' />\n";
      $pageContent["content"] .= "<input type='submit' name='submit' id='submit' value='Save Menu Information' />\n";
      $pageContent["content"] .= "</div>\n";
      $pageContent["content"] .= "</form>\n";

      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * Process submitted menu information.
 */
$app->post('(/admin/menus/editNav/:menuid(/))', function($menuID) use($app)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Menus", $user, _MB_ROLE_EDIT_WEBSITE_NAVIGATION_);
   
   if($page->isAuthorized())
   {
      $menu = new Mumby\WebTools\Menu($menuID);
      $navArray = $menu->getNavigationAsArray();
      
      // Specify the number of fields we are checking for each
      // navigation item.
      $fieldCount = 4;
      
      // Specify the number of fields that were submitted that aren't
      // associated with a specific navigation item (submit button, etc.).
      $fieldOffset = 6;
      
      if($navArray === false)
      {
         $app->flash('error','We were unable to find the requested menu for editing.');
         $app->redirect("/admin/menus/view");
      }

      $submittedData = $app->request->post();
      $navData = array();

      foreach($submittedData as $fieldName=>$fieldValue)
      {
         $menuPattern = "/menu_([0-9]+)/";
         if(preg_match($menuPattern, $fieldName, $fieldParts))
         {
            $a = $fieldParts[1];
            $navData[$a] = array(
                "MenuItemID"   => $submittedData["menu_".$a."_id"],
                "MenuItemText" => $submittedData["menu_".$a."_text"],
                "MenuItemLink" => $submittedData["menu_".$a."_link"],
                "MenuItemLinkAttr" => $submittedData["menu_".$a."_attr"]
            );
         }
      }

      // Make sure we set the root's parent to null.
      $navData[0]["MenuItemParentID"] = null;

      // Pull in information about which nodes are in what order.
      // Pulls both order as well as parent node ID.
      $submittedTree = explode("&",$submittedData["serialized"]);
      $nodeOrder = array();
      foreach($submittedTree as $node)
      {
         if(preg_match("/menu\[([0-9]+)\]\=([0-9]+)/", $node, $parts))
         {
            $nodeKey   = $parts[1];
            $parentKey = $parts[2];
            $parentID = $navData[$parentKey]["MenuItemID"];
            if($parentID == "")
               $parentID = $navData[$parentKey]["MenuItemText"];
            $navData[$nodeKey]["MenuItemParentID"] = $parentID;
            $nodeOrder[] = $nodeKey;
         }
      }

      // Make sure that we have parent/order information for every node!
      // Note: We add one to nodeOrder to account for the root node.
      if( (count($nodeOrder)+1) !== count($navData))
      {
         $app->flash("error", "We were unable to update your menu. Error code: 001.");
         $app->redirect("/admin/menus/view");
      }
      
      // Fix this...
      $responses = $menu->updateNavigation($navData, $menuID, $nodeOrder);
      if(empty($responses))
      {
         $app->flash("error", "We were unable to update your menu. Error code: 002.");
         $app->redirect("/admin/menus/view");
      }
      else
      {
         $app->flash("success", "We updated your navigation items successfully.");
      }
      
      $app->redirect("/admin/menus/view");
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});