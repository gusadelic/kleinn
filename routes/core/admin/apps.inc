<?php

$app->map('/admin/apps(/)', function() use($app, $user)
{
    
    $page = new Mumby\WebTools\AdminPage("App Integration", $user, _MB_EDIT_CONNECTED_APPS_);
    $page->addJS("/common/admin/js/appIntegration.js");
    
    if($page->isAuthorized())
    {
        
        $apps = new \Mumby\DB\ConnectedApps();
        $allApps = $apps->getAllApps();
        
        $pageContent = $page->getEmptyContentObj();
        $pageContent["title"] = "App Integration";
        $pageContent['content'] = "";
        
        if ( empty($allApps) || !is_array($allApps) ) {
            $pageContent["content"] .= '<div class="alert alert-info">No Apps are available.</div>';
        }
        else {
            $app_data = array();
            foreach ( $allApps as $a ) {
                $deleteButton = '<button class="btn-danger btn-xs btn-block" onclick="deleteApp(' . $a['AppID'] . ', $(this))">Delete</button>';
                $app_data[] = array(  
                  "Constant" => $a['Constant'],
                  "Name" => $a['AppName'],
                  "AppClientID" => $a['AppClientID'],
                  "Authentication Token" => $apps->getAuthCell($a),
                  "Delete" => $deleteButton
                );
            }
         
         $appTable = new Mumby\WebTools\Table();
         $columns = array("Constant", "Name", "Authentication Token", "Delete");
         $pageContent["content"] .= $appTable->getTable($app_data, "apptable", "small", false, false, 10, false, $columns);
        }
        
       $pageContent['content'] .= "<h2>Add Connected App</h2>";
       $pageContent['content'] .= '<div class="panel panel-info"><div class="panel-heading">remote_uri: </div><div class="panel-body">' . 'http://' . _MB_APP_DOMAIN_ . '/app/apps/auth</div></div>';
       $addForm  = new Mumby\WebTools\SystemForm();
       $nameField = $addForm->addTextField("Name", true, array());
       $clientField = $addForm->addTextField("Client ID", true, array());
       $secretField = $addForm->addTextField("App Secret", true, array());
       $grantTypes = array (
           "0" => "authorization_code",
           "1" => "client_credentials"
       );
       $grantTypeField = $addForm->addSelectField("Grant Type", $grantTypes, true, array());
       $stateField = $addForm->addTextField("State", false, array());
       $codeURIField = $addForm->addURLField("Code Request URI", true, array());
       $tokenURIField = $addForm->addURLField("Token Request URI", true, array());
       $configConstField = $addForm->addTextField("System Constant", true, array("topLabel" => "System Constant which will refer to the AppID.", "placeHolder" => "_MB_EXT_APPID_"));
       
       
       $pageContent["content"] .= $addForm->generateForm();
        
        if($addForm->submitted())
        {
            $addForm->processFormData();

            if($addForm->processedData)
            {
               $data = array(
                "AppName" => $nameField->getValue(),
                "AppClientID" => $clientField->getValue(),
                "AppSecret" => $secretField->getValue(),
                "AppGrantType" => $grantTypeField->getValue(),
                "AppState" => $stateField->getValue(),
                "AppCodeRequestURI" => $codeURIField->getValue(),
                "AppTokenRequestURI" => $tokenURIField->getValue(),
                "AppPropertyConstant" => str_replace(' ', '_', $configConstField->getValue())  
               );

               if ( $apps->addApp($data) )
                 $app->flash("success", $nameField->getValue() . " Added");
               else 
                 $app->flash("error", $nameField->getValue() . " failed to be added.");

               $app->redirect('/admin/apps/', 302);
            }
            else
            {
               $errorMessages = $addForm->getErrors();
               if(!empty($errorMessages))
               {
                  foreach($errorMessages as $e)
                  {
                     $page->setError($e);
                  }
               }
               else
               {
                  $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
               }
            }
        }
        
      
        $page->displayContent($pageContent);
    }
    else
    {
      $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
      //$app->redirect('/403', 403);
    }
    
    

})->via('GET','POST');


$app->map('/admin/apps/:appID(/)', function($appID) use($app, $user)
{
    $page = new Mumby\WebTools\AdminPage("App Integration", $user, _MB_EDIT_CONNECTED_APPS_);

    if($page->isAuthorized())
    {
        $apps = new \Mumby\DB\ConnectedApps();
        
        $thisApp = $apps->getAppData($appID);
        dump($thisApp);
        
        
        
    //$tokens = $apps->getTokens($appID);
    }
    else
    {
      $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
      //$app->redirect('/403', 403);
    }
    

    
})->via('GET','POST');


// Asynchronus routes

$app->map('(/app/apps/delete(/))', function() use($app, $user)
{
   
   $page = new Mumby\WebTools\AdminPage("App Integration", $user, _MB_EDIT_CONNECTED_APPS_);

   if($page->isAuthorized())
   {

        $apps = new Mumby\DB\ConnectedApps();

        try {
          if ( $apps->deleteApp($_POST['id']) )
          $app->flash('success', "App deleted.");
        }
        catch ( Exception $e ) {
          $app->flash('error', "App deletion failed.<br />" . $e->getMessage() );
        }

   } 
   else
   {
      //$app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
      $app->redirect('/403', 403);
   }

})->via('POST');

$app->map('(/app/apps/refresh(/))', function() use($app, $user)
{
   
   $page = new Mumby\WebTools\AdminPage("App Integration", $user, _MB_EDIT_CONNECTED_APPS_);

   if($page->isAuthorized())
   {

        $apps = new Mumby\DB\ConnectedApps();

        $result = $apps->refreshToken($_POST['id']);

        $app->redirect('/admin/apps', 302);
        
   } 
   else
   {
      //$app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
      $app->redirect('/403', 403);
   }

})->via('POST');

$app->map('/app/apps/auth', function() use ($app)
{
   $page = new Mumby\WebTools\AdminPage("App Integration", $user, _MB_EDIT_CONNECTED_APPS_);

   if($page->isAuthorized())
   {
        $apps = new Mumby\DB\ConnectedApps();
        
        if ( !empty($_GET) ) {
            
            $result = $apps->requestToken($_COOKIE['_OAUTH_APP_ID_'], $_GET); 
            //dump($result);
            $app->redirect('/admin/apps', 302);
        }
        else {
            
            if ( empty($_POST['access_token']) ) {
                $app->flash('error', $result['error'].": ".$result['error_description']);
            }
            else {
                $result = $apps->updateToken($_POST);
            }

            $app->redirect('/admin/apps', 302);
        }   
   }
    
})->via('GET','POST');

$app->map('/app/apps/auth/define', function() use ($app) 
{
    $page = new Mumby\WebTools\AdminPage("App Integration", $user, _MB_EDIT_CONNECTED_APPS_);

    if($page->isAuthorized())
    {
        setcookie("_OAUTH_APP_ID_", $_POST['AppID']);
                
    }  
    
})->via('GET','POST');