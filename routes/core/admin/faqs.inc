<?php

/*
 * Show menu-related functions.
 */
$app->get('(/admin/faqs(/))', function() use($app, $user) // Done
{
   // Create the Page instance
   $page = new Mumby\WebTools\AdminPage("FAQs", $user, _MB_ROLE_EDIT_FAQS_);
   
   if($page->isAuthorized())
   {
      $pageContent = array();
      $pageContent["title"] = "FAQs";
      
      $menuOptions = array();
      $menuOptions[] = array(
          "label" => "View/Edit FAQs",
          "link"  => "/admin/faqs/view",
          "desc"  => "View a list of all existing FAQs."
      );

      $menuOptions[] = array(
          "label" => "Add New FAQ",
          "link"  => "/admin/faqs/add",
          "desc"  => "Create a new FAQ."
      );

      $pageContent["content"] = "<div class='adminOptionWrapper'>\n";
      $pageContent["content"] .= buildLandingPage($menuOptions);
      $pageContent["content"] .= "</div>\n";
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * Show all FAQs in the system.
 */
$app->get('(/admin/faqs/view(/))', function() use($app, $user)
{
   // Create the Page instance
   $page = new Mumby\WebTools\AdminPage("FAQs", $user, _MB_ROLE_EDIT_FAQS_);
   
   if($page->isAuthorized())
   {
      $pageContent = array();
      $pageContent["title"] = "FAQs";
      
      $faqMgr = new \Mumby\WebTools\FAQ();
      $allFAQs = $faqMgr->getAllFAQs();
      
      if(!empty($allFAQs))
      {
         $pageContent["content"] = "<p>Click on any FAQ title below to edit.</p>\n";
         $pageContent["content"] .= "<ul class='list-group'>\n";

         foreach($allFAQs as $f)
         {            
            $pageContent["content"] .= "<li class='list-group-item'>\n";
            $pageContent["content"] .= "<a href='/admin/faqs/edit/".$f["FAQID"]."'>".$f["FAQTitle"]."</a>";
            $pageContent["content"] .= "</li>\n";
         }
         $pageContent["content"] .= "</ul>\n";
      }
      else
      {
         $pageContent["content"] = "<p class='alert alert-warning'><em>No FAQs found!</em></p>";
         $pageContent["content"] .= "<p>You can add a new FAQ by visiting the <a href='/admin/faqs/add'>Add FAQ page</a>.</p>\n";
      }
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * Display form for updating FAQs.
 */
$app->get('(/admin/faqs/edit/:faqid(/))', function($faqID) use($app, $user)
{
   // Create the Page instance
   $page = new Mumby\WebTools\AdminPage("FAQs", $user, _MB_ROLE_EDIT_FAQS_);
   
   if($page->isAuthorized())
   {
      $faqMgr = new Mumby\WebTools\FAQ();
      $thisFAQ = $faqMgr->getContent($faqID);

      if(empty($thisFAQ))
      {
         $app->flash('error','We were unable to find the requested FAQ for editing.');
         $app->redirect("/admin/faqs/view");
      }
      
      $thisFAQInfo = $faqMgr->getFAQInfo($faqID);
      $thisFAQInfo = $thisFAQInfo[0];

      $page->addJS( array(
         '/common/bower/jquery-nested-sortable/jquery.ui.nestedSortable.js',
         '/common/admin/js/faqs.js'
      ));
      
      $page->addCSS( array(
         '/common/admin/css/faqs.css'
      ));
      
      $pageContent = array();
      $pageContent["title"]      = "Edit FAQ";
      $pageContent["parentLink"] = null;
      $pageContent["id"]         = null;
      
      $output  = "<button onclick='javascript:addFAQItem();return false' class='btn btn-success pull-right'><span class='fa fa-plus'></span> Add New FAQ Question</button>";

      $form = new \Mumby\WebTools\SystemForm();
      $form->inlineForm = true;
      
      $output .= "<form action='/admin/faqs/edit/".((int) $faqID)."' method='post'>";
      $formTitle = $form->addTextField("FAQ Title", true, array("fieldName"=>"faqTitle"));
      $formTitle->setValue($thisFAQInfo["FAQTitle"]);
      $output .= "<div class='well well-lg' style='clear:both;'>".$formTitle->render()."</div>";
      $output .= "<ol id='editFAQ' class='sortable'>\n";

      $currentItem = 0;
      $output .= "<li id='faq_".$currentItem."'><span>";

      $output .= "Categories &amp; Questions";
      $output .= "</span><ol>\n";
      $currentItem++;

      $currentCategory = -1;
      foreach($thisFAQ as $q)
      {
         if($currentCategory != $q["category"])
         {
            if($currentCategory != -1)
            {
               $output .= "</ol>\n";
               $output .= "</li>\n\n";
            }
            $output .= "<li id='faq_".$currentItem."' class='categoryHeading'>\n";
            $output .= "<div>\n";
            $output .= "<i class='fa fa-sort-amount-asc'></i>\n";
            $output .= "<i class='fa fa-pencil-square-o toggleFields active' title='Expand this question for editing'></i>\n";
            $output .= "<span class='deleteMe fa fa-ban' onclick='javascript: deleteItem(".$currentItem."); return false;'></span>\n";
            $output .= "<div class='faqEditFields'>\n";
            $output .= "<input type='text' name='faq_".$currentItem."_text' class='faqQuestion' id='faq_category_".$currentItem."_text' value=\"".clean($q["category"])."\" />\n";
//            $output .= "<input type='hidden' name='category_val' value='".$q["category"]."' />\n";
            $output .= "</div>\n";
            $output .= "</div>\n";
            $output .= "<ol>\n";
            $currentItem++;
            $currentCategory = $q["category"];
         }
         
         $output .= "<li id='faq_".$currentItem."'>\n";
         $output .= "<div class='clearfix'>\n";
         $output .= "<i class='fa fa-sort-amount-asc'></i>\n";
         $output .= "<i class='fa fa-pencil-square-o toggleFields active' title='Expand this question for editing'></i>\n";
         $output .= "<span class='deleteMe fa fa-ban' onclick='javascript: deleteItem(".$currentItem."); return false;'></span>\n";
         $output .= "<div class='faqEditFields'>\n";
         
         $currentRowID = "faq_".$currentItem."_";
         
         $idField       = $form->addHiddenField("ID", array("hiddenLabel"=>true, "fieldName"=>$currentRowID."id"));
         $questionField = $form->addTextField("Question", false, array("hiddenLabel"=>true, "fieldName"=>$currentRowID."text", "fieldClass"=>"faqQuestion"));
         
         $configMgr = new Mumby\WebTools\WYSIWYG_Config();
         $configMgr->fullFeatured(false);
         $configMgr->allowBold(true);
         $configMgr->allowItalics(true);
         $configMgr->allowUnderline(true);
         $configMgr->allowImages(true);
         $configMgr->allowLists(true);
         $configMgr->allowLinks(true);
         $configMgr->allowPeopleInjections(true);
         $configMgr->allowDataInjections(true);
         $configMgr->allowMenubar(false);

         if($user->hasPermission(_MB_ROLE_EDIT_WEB_PAGE_SOURCE_))
            $configMgr->allowViewSource(true);
         else
            $configMgr->allowViewSource(false);

         $wysiwygConfig = array(
            "config"      => $configMgr->getJSONConfig(),
            "hiddenLabel" => true,
            "fieldName"   => $currentRowID."answer"
         );
         $answerField  = $form->addHTMLField("Answer", false, $wysiwygConfig);

         $idField->setValue($q["id"]);
         $questionField->setValue(clean($q["question"]));
         $answerField->setValue(clean($q["answer"]));
         
         $output .= $idField->render();
         $output .= $questionField->render();
         $output .= $answerField->render();

         $output .= "</div>\n";
         $output .= "</div>\n";
         
         $output .= "</li>\n";
         $currentItem++;
      }
      
      // Make sure we close the list.
      $output .= "</li>\n";
      $output .= "</ol>\n";
      $output .= "</li>\n";
      $output .= "</ol>\n";

      $pageContent["content"] = $output;
      
      $pageContent["content"] .= "<hr />\n";
      $pageContent["content"] .= "<div class='submitDiv'>\n";
      $pageContent["content"] .= "<input type='hidden' name='serialized' id='serialized' value='' />\n";
      $pageContent["content"] .= "<input type='submit' name='submit' id='submit' value='Save FAQ Information' />\n";
      $pageContent["content"] .= "</div>\n";
      $pageContent["content"] .= "</form>\n";

      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * Process submitted FAQ information.
 */
$app->post('(/admin/faqs/edit/:faqid(/))', function($faqID) use($app, $user)
{
   // Create the Page instance
   $page = new Mumby\WebTools\AdminPage("FAQs", $user, _MB_ROLE_EDIT_FAQS_);
   
   if($page->isAuthorized())
   {
      $faqMgmt = new Mumby\WebTools\FAQ();
      $thisFAQ = $faqMgmt->getFAQInfo($faqID);

      if(empty($thisFAQ))
      {
         $app->flash('error','We were unable to find the requested FAQ for editing.');
         $app->redirect("/admin/faqs/view");
      }
      else
         $thisFAQ = $thisFAQ[0];

      $submittedData = $app->request->post();
      
      if(isset($submittedData["faqTitle"]) && $submittedData["faqTitle"] != $thisFAQ["FAQTitle"])
      {
         if(!$faqMgmt->updateFAQTitle($faqID, $submittedData["faqTitle"]))
         {
            $app->flash('error','We were unable to process your updated FAQ submission. Error Code: 001');
            $app->redirect("/admin/faqs/view");         
         }
         else
         {
            $app->flash('success','We successfully updated your FAQ title.');
            $thisFAQ["FAQTitle"] = $submittedData["faqTitle"];
         }
      }

      $nestedOrder = $submittedData["serialized"];
      if(!preg_match_all("/faq\[(\d+)\]\=(\d+)/", $nestedOrder, $serializedPieces))
      {
         $app->flash('error','We were unable to process your updated FAQ submission. Error Code: 001');
         $app->redirect("/admin/faqs/view");         
      }
      
      $categories = array();
      $questions  = array();
      
      foreach($serializedPieces[1] as $k=>$v)
      {
         // Categories
         if($serializedPieces[2][$k] == 0)
         {
            $categories[] = array(
               "fauxID"=>$v,
               "categoryName"=>$submittedData["faq_".$v."_text"]
            );
         }
         else
         {
            $questions[] = array(
               "fauxID"=>$v,
               "categoryID"=>$serializedPieces[2][$k],
               "question"=>$submittedData["faq_".$v."_text"],
               "answer"=>$submittedData["faq_".$v."_answer"]
            );            
         }
      }
      
      if(!$faqMgmt->updateFAQ($faqID, $categories, $questions))
      {
         $app->flash("error", "We were unable to update ".$thisFAQ["FAQTitle"].".");
      }
      else
      {
         $app->flash("success", "We updated ".$thisFAQ["FAQTitle"]." successfully.");
      }
      
      $app->redirect("/admin/faqs/view");
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

$app->map('(/admin/faqs/add(/))', function() use ($app, $user)
{
   // Create the Page instance
   $page = new Mumby\WebTools\AdminPage("FAQs", $user, _MB_ROLE_EDIT_FAQS_);
   
   if($page->isAuthorized())
   {
      $faqMgr = new Mumby\WebTools\FAQ();

      $faqForm = new \Mumby\WebTools\SystemForm();
      $faqTitle = $faqForm->addTextField("FAQ Name", true);
      
      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"] = "Add FAQ";
      $pageContent["content"] = "Use the form below to add a new FAQ to the system.";
      
      if($faqForm->submitted())
      {
         $thisTitle = $faqTitle->getValue();
         if(!empty($thisTitle) && $faqMgr->addFAQ($thisTitle))
         {
            $app->flash("success", "We were able to add '".$thisTitle."' to the list of FAQs.");
         }
         else
         {
            $app->flash("error", "We were unable to add '".$thisTitle."' to the list of FAQs.");
         }
         $app->redirect("/admin/faqs/view");
      }
      else
      {
         $pageContent["content"] .= $faqForm->generateForm();
      }

      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
})->via('GET','POST');