<?php
$app->get('/admin(/)', function() use($app, $user)
{
   // Create the Page instance
   $pageTitle = "Website Administration Dashboard";
   $page = new Mumby\WebTools\AdminPage($pageTitle, $user);
   if($page->isAuthorized())
   {
      $pageContent = array();
      $pageContent["title"] = "Website Administration Dashboard";
      
      $pageContent["content"]  = "<p>";
      $pageContent["content"] .= "Hello, ".$user->firstName."! Use the links on the left to navigate ";
      $pageContent["content"] .= "through the available administrative functions.";
      $pageContent["content"] .= "</p>";
      $page->displayContent($pageContent);
   }
   else
   {      
      $app->redirect('admin/login?url='.$_SERVER["REQUEST_URI"],302);
      //$app->redirect('/403', 403);

   }
});


$app->map('/admin/login(/)', function() use($app, $user)
{
   $page = new Mumby\WebTools\Page();
   $form = new Mumby\WebTools\SystemForm();
   $username = $form->addTextField("Username", true);
   $password = $form->addPasswordField("Password", true);
   
   $pageContent = $page->getEmptyContentObj();
   
   if ( $form->submitted() ) {
       $form->processFormData();
       
        if($form->processedData)
        {
           $uName     = $username->getValue();
           $pWord     = $password->getValue();
           
           $newUser = \Mumby\WebTools\ACL\User::create(_MB_AUTH_TYPE_, $uName, $pWord);
           
           $user = $newUser;
           
           if ( !$user->isAuthenticated() ) {
                $pageContent["title"] = "Login";
                $page->setError("<div class='errorMessage'>Login failed. Please try again.</div>");
                $pageContent["content"] = $form->generateForm();
           }
           else {
                if ( $app->request->get('url') )
                   $app->redirect($app->request->get('url'), 302);
                else 
                    $app->redirect('/admin', 302);
           }

        }
        else
        {
           $errorMessages = $form->getErrors();
           if(!empty($errorMessages))
           {
              foreach($errorMessages as $e)
              {
                 $page->setError($e);
              }
           }
           else
           {
              $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
           }
        }

   }
   else {
      $user = \Mumby\WebTools\ACL\User::create();
      $pageContent["title"] = "Login";
      $pageContent["content"] = $form->generateForm();

      
   }
   
   $page->displayContent($pageContent);
   
})->via('GET','POST')->name('admin.login');



function buildLandingPage($options)
{
   $content = "<ul class='list-group'>";
   foreach($options as $o)
   {  
      if ( empty($o["data-toggle"]) ) {
          $dataToggle = "";
      }
      else {
          $dataToggle = $o["data-toggle"];
      }
      
      if ( empty($o["data-target"]) ) {
          $dataTarget = "";
      }
      else {
          $dataTarget = $o["data-target"];
      }
      
      if ( empty($o['id']) ) {
          $dataId = '';
      }
      else {
          $dataId = $o['id'];
      }
      
      if ( !isset($o['ops']) )  $o['ops'] = "";
      
      $content .= "<li class='list-group-item row'>\n";
      $content .= "<span class='col-sm-3'>\n";
      $content .= "   <strong><a class='adminOptionLabel' id='".$dataId."' href='".$o["link"]."' data-toggle='". $dataToggle . "' data-target='" . $dataTarget ."'>".$o["label"]."</a></strong>\n";
      $content .= "</span>\n";
      if ( $o['ops'] == "" ) {
        $content .= "<span class='col-sm-9'>\n";
        $content .= $o["desc"]."\n";
        $content .= "</span>\n";
      }
      else {
        $content .= "<span class='col-sm-6'>\n";
        $content .= $o["desc"]."\n";
        $content .= "</span>\n";
        $content .= "<span class='col-sm-3'>\n";
        $content .= $o['ops'];
        $content .= "</span>\n";
      }
      $content .= "</li>\n";
   }
   $content .= "</ul>\n";
   
   return $content;
}