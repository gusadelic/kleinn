<?php

$app->get('/async/vehicles/:year', function($year) use($app)
{
    if ( !is_numeric($year) ) die("Error: Years can only be numeric!");
    
    $vehicleDB = new \Mumby\DB\Vehicle();
    $vehicles = $vehicleDB->getVehiclesByYear($year);
    echo(json_encode($vehicles));
});

$app->get('/async/vehicles/makes/:year', function($year) use($app)
{
    if ( !is_numeric($year) ) die("Error: Years can only be numeric!");
    
    $vehicleDB = new \Mumby\DB\Vehicle();
    $vehicles = $vehicleDB->getMakes($year);
    echo(json_encode($vehicles));
});

$app->get('/async/vehicles/models/:year/:make', function($year, $make) use($app)
{
    if ( !is_numeric($year) ) die("Error: Years can only be numeric!");
    
    $vehicleDB = new \Mumby\DB\Vehicle();
    $vehicles = $vehicleDB->getModels($year, $make);
    echo(json_encode($vehicles));
});