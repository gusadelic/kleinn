<?php

// Catchall - Anything that hasn't been found at this point
// is assumed to be in the local DB. We'll try to fetch it
// and redirect if necessary.
$app->map('(/:url+)', function($url="") use($app, $user)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\Page();

   // Create the Content instance.
   $content = new Mumby\WebTools\Content();
   
   // Get the full URL that was requested to
   // try and find a match in the DB.
   $fullURL = "/".implode("/", $url);
   $getParams = $app->request->get();
   if(!empty($getParams))
   {
      $queryString = implode('&', array_map(function ($v, $k) { return $k . '=' . $v; }, $getParams, array_keys($getParams)));
   }
   else
      $queryString = "";

   if(substr($fullURL,-1) == '/')
      $fullURL = substr($fullURL,0,-1);

   // Try to pull the requested content
   $contentObj = $content->getContent($fullURL);
      
   // Throw an error if nothing was found!
   if(!$contentObj)
   {
      // We should check to see if there are any redirects
      // in place for the given URL.
      if(($redirectURL = $content->getRedirect($fullURL)))
      {
         if(!empty($queryString))
            $redirectURL .= "?".$queryString;
         $app->redirect($redirectURL, 302);
      }
      else
         $app->notFound();
   }
   // Redirect old URLs to the new one.
   else if($contentObj["currentURL"] != $fullURL)
   {
      $redirectURL = $contentObj["currentURL"];
      
      // This usually only happens when the URL(s) for the page
      // aren't marked as current.
      if(empty($redirectURL))
      {
         $page->setError("The page you're looking for doesn't have a valid URL associated with it. System Error 500");
         $contentObj = $content->getContent("/500");
      }
      else
      {
         if(!empty($queryString))
            $redirectURL .= "?".$queryString;

         $app->redirect($redirectURL, 302);
      }
   }
   // Manage permissions
   else if($contentObj["hasRestrictions"])
   {
      $restrictions = $content->getRestrictions($contentObj["id"]);
      
      // Give the user the benefit of the doubt.
      $hasPerms = true;

      if(!empty($restrictions))
      {
         if(!empty($restrictions["roles"]) || !empty($restrictions["users"]))
         {
            if(!empty($restrictions["users"]) && !in_array($user->username, $restrictions["users"]))
               $hasPerms = false;

            // Check roles after verifying users since it's more
            // processor-intensive.
            if($hasPerms && !empty($restrictions["roles"]))
            {
               foreach($restrictions["roles"] as $r)
               {
                  if(!$user->hasPermission($r))
                     $hasPerms = false;
               }
            }
         }

         if($hasPerms && !empty($restrictions["password"]))
         {
            $username = $app->request()->headers('PHP_AUTH_USER');
            $password = $app->request()->headers('PHP_AUTH_PW');

            if (isset($username) && isset($password))
            {
               $userId = password_verify($username.$password, $restrictions["password"]);

               if ($userId === false)
               {
                  $app->response()->header('WWW-Authenticate', 'Basic realm="Username/Password Required"');
                  $contentObj = $content->getContent("/403");
                  $hasPerms = false;
               }
            }
            else
            {
               $app->response()->header('WWW-Authenticate', 'Basic realm="Username/Password Required"');
               $app->response()->status(401);
               $hasPerms = false;
            }
         }
      }

      // If they don't have permission to view the page, then we shut them down.
      if(!$hasPerms)
         $contentObj = $content->getContent("/403");
   }
   
   // If the user's logged in, let them know that they
   // can edit the page.
//   if(\Mumby\WebTools\ACL\User::isAuthenticated() && !$contentObj["systemPage"])
//   {
//      $user = \Mumby\WebTools\ACL\User::create(_MB_AUTH_TYPE_);
//      if($user->hasPermission(_MB_ROLE_EDIT_WEB_PAGES_))
//         $page->addInjectable("<ul class='list-unstyled list-inline'><li><a class='editThisPageLink' href='/admin/pages/edit/".((int) $contentObj["id"])."'>Edit this page</a></li></ul>", "moreAdminMenu");
      //else
      //   $page->addInjectable("<span class='fauxEditThisPageLink'>Edit this page</span>", "moreAdminMenu");
   //}
   
   // Make sure integers are actually integers.
   $contentObj["parentLink"] = ((int) $contentObj["parentLink"]);
   $contentObj["id"] = ((int) $contentObj["id"]);

   // Add useful debugging information
   $contentObj["content"] .= "<!-- ".date('d/m/y g:i:').$contentObj["id"]." -->\n";
   $contentObj["content"] .= "<!-- Requested URL: ".$fullURL." -->\n";
   
   if(isset($contentObj["parentLink"]) && !empty($contentObj["parentLink"]))
      $page->menuNode = $contentObj["parentLink"];
   
   $page->displayContent($contentObj);
})->via('GET','POST');