<?php

// Set the 404 handler
$app->notFound(function () use ($app)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\Page();
   $page->logError($app, 404);
   
   // Create the Content instance.
   $content = new Mumby\WebTools\Content();
   
   $pageContent = $content->getContent("/404");
   if(empty($pageContent))
   {
      $pageContent = $page->getEmptyContentObj();
      $pageContent["title"]   = "HTTP 404 Error: Page Not Found";
      $pageContent["content"] = "<p>The page you requested could not be found.</p>";
   }
   
   $searchForm = new Mumby\WebTools\SystemForm();
   $searchForm->addSearchField("Search", false, array("hiddenLabel"=>true));
   $searchForm->action = "/search/";
   $searchForm->method = "get";
   $searchForm->submitValue = "Search";
   $searchForm->submitClass = "btn btn-primary";
   $searchForm->inlineForm = true;
   
   $pageContent["content"] .= "<div class='well text-center'>";
   $pageContent["content"] .= $searchForm->generateForm();
   $pageContent["content"] .= "</div>";
  
   $page->displayContent($pageContent);
});

// Set the 403 handler
$app->get('/403', function() use ($app)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\Page();
   $page->logError($app, 403);

   // Create the Content instance.
   $content = new Mumby\WebTools\Content();
   $pageContent = $content->getContent("/403");
   if(empty($pageContent))
   {
      $pageContent = $content->getEmptyContentObj();
      $pageContent["title"]   = "HTTP 403 Error: Forbidden";
      $pageContent["content"] = "<p>You are not authorized to view the requested page.</p>";
   }
   
   $searchForm = new Mumby\WebTools\SystemForm();
   $searchForm->addSearchField("Search", false, array("hiddenLabel"=>true));
   $searchForm->action = "/search/";
   $searchForm->method = "get";
   $searchForm->submitValue = "Search";
   $searchForm->submitClass = "btn btn-primary";
   $searchForm->inlineForm = true;
   
   $pageContent["content"] .= "<div class='well text-center'>";
   $pageContent["content"] .= $searchForm->generateForm();
   $pageContent["content"] .= "</div>";
   
   $page->displayContent($pageContent);
});

// Set logout functionality
$app->get('/logout', function() use ($app)
{
   $redirect = $app->request->get("u");
   $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
   $thisHost = $_SERVER['HTTP_HOST'];
   
   $redirectURL = $protocol.$thisHost; 

   if(!empty($redirect))
   {
      if(strpos($redirect, "/") !== 0)
         $redirect = "/".$redirect;

      $redirectURL .= $redirect;
   }

   if(Mumby\WebTools\ACL\User::isAuthenticated())
   {
      $user = Mumby\WebTools\ACL\User::getAuthenticatedUser(_MB_AUTH_TYPE_);
      if(!empty($redirect))
         $redirect = $protocol.$thisHost.$redirect;
      else
         $redirect = $protocol.$thisHost;

      $user->logout();
      
      if(Mumby\WebTools\ACL\User::isAuthenticated())
      {
         $app->flash("success", "Hi, ".$user->firstName."! Glad to see you're back to your old self again.");
         $redirectURL = "/admin";
      }
      
      $app->redirect($redirectURL);
   }
   else
      $app->redirect($redirectURL);
});

$app->error(function (\Exception $e=null) use ($app)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\Page();
   $page->logError($app, 505);
   
   // Create the Content instance.
   $content = new Mumby\WebTools\Content();
   
   // Get the content for 500-level errors
   $sysError = $content->getContent("/500");

   $page->displayContent($sysError, 500);
});