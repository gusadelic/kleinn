<?php

$app->get('/', function() use($app)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\Page("front.html", "header.html"); 

   
   $app->redirect('/tipsheet/form');
   /*
    * Build the front page slideshow
    */
   //$page->addCSS("/themes/"._MB_THEME_."/css/slideshows.css");
   //$slideshow = new Mumby\WebTools\Slideshow(1);
   //$fb->addGroupSlides('422720557911468', $slideshow, date("Y-m-d", strtotime('-1 week')));

   
   //$page->addInjectable($slideshow->generateSlideShow(), "frontSlides");

   /*
    * Pull headlines from the Database.
    */
   
   
   $headlinesContent = "";
   
   $headlineMgr = new \Mumby\WebTools\Headline();
   $headlines   = $headlineMgr->getCurrentHeadlines();

   if(!empty($headlines))
   {
      foreach($headlines as $h)
      {
         $headlinesContent .= "<article>\n";
         
         $title = str_replace('$', '\$', $h["HeadlineTitle"]);
         $desc = str_replace('$', '\$', $h["HeadlineDesc"]);
         
         $headlinesContent .= "<h1>".htmlspecialchars($title)."</h1>\n";
         $headlinesContent .= "<p>";
         $headlinesContent .= htmlspecialchars($desc);
         $url = $h["HeadlineLink"];
         if(!empty($url))
            $headlinesContent .= " <a class='moreLink' href='".$url."'>Read more...</a>\n";
         $headlinesContent .= "</p>\n";
         $headlinesContent .= "</article>\n\n";
      }
   }
   else
   {
         $headlinesContent .= "<article>\n";
         $headlinesContent .= "<h1>Welcome!</h1>\n";
         $headlinesContent .= "</article>\n\n";
   }
   $page->addInjectable($headlinesContent, "frontHeadlines");
   

//   $eventData = new Mumby\DB\Event();
//   $events = $eventData->getUpcomingEvents();
   
/*
   $events = $fb->getEvents('200527489974139');
   $eventsContent = "<h1>Upcoming Events</h1>";

   $maxFrontPageEvents = 6;
   
   if(!empty($events['data']))
   {
      $stneve = array_reverse($events['data']);
     $frontEvents = array_slice($stneve, 0, $maxFrontPageEvents);
            
      $eventsContent .= makeFBEventsList($stneve, false, true, 2);
   
      if(count($events) > $maxFrontPageEvents)
         $eventsContent .= "<a class='moreLink' href='/events'>View more events...</a>\n";
   }
   else
      $eventsContent .= "<p><em>There are no upcoming events.</em></p>\n";
   $page->addInjectable($eventsContent, "frontEvents");
  */ 

});

$app->get('/tipsheet/form', function() use($app)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\Page(); 

   $content = "";
   
    // Order Number instructions modal
   $content .= '<div class="modal fade in" id="orderNumberModal" role="dialog" style="display: none;">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title">Where do I find my Order Number?</h4>
         </div>
         <div class="modal-body">
            <img class="img-responsive" src="http://remotestart.mypushcart.com/assets/images/find_order_num.png" style="max-height:350px;">
         </div>
         <div class="modal-footer">
            <a href="http://mypushcart.com/helpme" class="btn btn-default pull-left" style="background-color:orange; font-weight:bold;"><i class="fa fa-desk"></i>&nbsp;Visit Our Help Desk Online</a>
            <button type="button" class="btn btn-default btn-success" data-dismiss="modal">Found It!</button>
         </div>
      </div>
   </div>
</div>';
   
   // Sku Modal
   $content .= '<div class="modal fade in" id="productSKUModal" role="dialog" style="display:none; padding-left: 0px;">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title">Where do I find my Product SKU?</h4>
         </div>
         <div class="modal-body">
            <img class="img-responsive" src="http://remotestart.mypushcart.com/assets/images/find_sku.jpg" style="max-height:350px;">
         </div>
         <div class="modal-footer">
            <a href="http://mypushcart.com/helpme" class="btn btn-default pull-left" style="background-color:orange; font-weight:bold;"><i class="fa fa-desk"></i>&nbsp;Visit Our Help Desk Online</a>
            <button type="button" class="btn btn-default btn-success" data-dismiss="modal">Found It!</button>
         </div>
      </div>
   </div>
</div>';
   
   $form = new Mumby\WebTools\SystemForm();
   $content .= "<div class='well well-lg'><h2>Get Your Tipsheet</h2>";
   $content .= "<strong><h3>Congratulations! You have purchased the finest Remote Start product on the market today. Now you are ready to get your installation instructions:</h3></strong>";
   $content .= "<h5>Once you complete the following information, the link to download your installation document(s) will be available to you immediately.
This form must be completed in full to receive your tip sheet download link.</h5></div>\n";
   
   $emailField = $form->addEmailField("Email", true, array("fieldName"=>"EmailAddress","placeholder"=>"Please enter your Email."));
   $firstnameField = $form->addTextField("First Name", true, array("fieldName"=>"FirstName","placeholder"=>"Please enter your First Name."));
   $lastnameField = $form->addTextField("Last Name", true, array("fieldName"=>"LastName","placeholder"=>"Please enter your Last Name."));
   $phoneField = $form->addPhoneField("Telephone", false, array("fieldName"=>"Phone"));
   $zipField = $form->zipFIeld("Zip/Postal Code", true, array("fieldName"=>"Zip","placeholder"=>"Please enter your Zip/Postal Code."));
   
   $locationOptions = array(
        "mypushcart.com"    => "MyPushCart.com",
        "amazon"            => "Amazon (Standard)",
        "amazon_prime"      => "Amazon Prime",
        "ebay"              => "eBay",
        "jet"               => "Jet",
        "newegg"            => "Newegg",
        "sears"             => "Sears",
        "walmart"           => "WalMart",
        "pricefalls"        => "Pricefalls"  
        );
   $locationField = $form->addSelectField("Purchase Location", $locationOptions, true, array("fieldName"=>"PurchaseLocation","placeholder"=>"Select a Store."));
   
   $orderField = $form->addTextField("Order Number", false, array("fieldName"=>"OrderNumber","placeholder"=>"Please enter your Order Number."));
   
   $form->addMarkup('<button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#orderNumberModal">Where do I find my Order Number?</button><br />');
   
   $skuField = $form->addTextField("Product SKU", true, array("fieldName"=>"SKU","placeholder"=>"Please enter your Product SKU."));

   $form->addMarkup('<button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#productSKUModal">Where do I find my Product SKU?</button><br />');
   
    $vehiclesDB = new \Mumby\DB\Vehicle();
    $vehicles = $vehiclesDB->getAllVehicles(true, true);

    $content .= "<script>var vehicleString = '".str_replace("'", "\'", json_encode($vehicles))."';</script>";
    $content .= "<script>var vehicles = JSON.parse(vehicleString);</script>";
    $makes = array_keys($vehicles);  
    asort($makes);
    
    $selectNew = "";
    // Construct Vehicle select boxes for Javascript
    if ( is_array($vehicles) ) {
        $selectNew .= '<div class="form-group">
<label class="col-sm-3 control-label select_field required" for="makeSelect"><span class="labelText">Vehicle Make</span></label>
   <div class="col-sm-9">';
        $selectNew .= '<select class="form-control makeSelect vehicle">';
        $selectNew .= '<option value=""></option>';
        foreach( $makes as $i=>$m ) {
            $selectNew .= '<option value="'.$m.'" >'.$m.'</option>';
        }
        $selectNew .= '</select></div></div>';

        $selectNew .= '<div class="form-group">
<label class="col-sm-3 control-label select_field hidden required" for="modelSelect"><span class="labelText">Vehicle Model</span></label>
   <div class="col-sm-9">';
        $selectNew .= '<select class="form-control modelSelect vehicle hidden">';
        $selectNew .= '<option value=""></option>';
        $selectNew .= '</select></div></div>';

        $selectNew .= '<div class="form-group">
<label class="col-sm-3 control-label select_field hidden required" for="yearSelect"><span class="labelText">Vehicle Year</span></label>
   <div class="col-sm-9">';
        $selectNew .= '<select class="form-control yearSelect vehicle hidden">';
        $selectNew .= '<option value=""></option>';
        $selectNew .= '</select></div></div>';
    }

    $page->addJS( array(
        '/themes/kleinn/js/vehicles.js'
    ));

    $page->addCSS( array(
       '/common/admin/css/tipsheet_vehicles.css'
    ));
    
    $form->addMarkup($selectNew);
    
    $vehicleForm = $form->addHiddenField("vehicle_data", array('fieldName'=>"vehicle_data"));
   
    $form->submitClass = "btn-success";
    $content .= $form->generateForm('/tipsheet/submission');
    //$page->addInjectable($content, "tipsheetForm");
   
    $contentObject = $page->getEmptyContentObj();
    $contentObject['title'] = "";
    $contentObject['content'] = $content;
    
   // Display the page
   $page->displayContent($contentObject);
});

$app->post('/tipsheet/submission', function() use($app) 
{
    $submittedData = $app->request->post();
    
    if ( !isset($submittedData['FirstName']) 
      || !isset($submittedData['LastName'])    
      || !isset($submittedData['Phone'])  
      || !isset($submittedData['EmailAddress'])  
      || !isset($submittedData['PurchaseLocation']) 
      || !isset($submittedData['OrderNumber']) 
      || !isset($submittedData['SKU']) 
      || ( !isset($submittedData['vehicle_data']) || !json_decode($submittedData['vehicle_data']) )      
            ) {
        $app->flash("error", "Your submission was invalid.  Please try again.");
        $app->redirect("/tipsheet/form");
    }
            
    $customerData = array(
        "FirstName"     => $submittedData['FirstName'],
        "LastName"      => $submittedData['LastName'],
        "Phone"         => $submittedData['Phone'],
        "EmailAddress"  => $submittedData['EmailAddress']
    );
    
    $custDB = new \Mumby\DB\CustomerData();
    $thisCustomer = $custDB->updateCustomerData($customerData);
    if ( is_array($thisCustomer) ) {
	$CustomerID = $thisCustomer['CustomerID'];
    }
    else if ( is_numeric($thisCustomer) ) {
	$CustomerID = $thisCustomer;
    }
    else {
        $app->flash("error", "There was a problem submitting your information. Please try again or contact us for assistance by visiting <a href='http://mypushcart.com/helpme/' target='_blank'>http://mypushcart.com/helpme</a>.");
        $app->redirect("/tipsheet/form");   
    }  
    
    $vehicleData = json_decode($submittedData['vehicle_data'], true);
    $vehicleDB = new Mumby\DB\Vehicle();
    
    $VehicleID = $vehicleDB->findVehicle($vehicleData[0]['Year'], $vehicleData[0]['Make'], $vehicleData[0]['Model']);

    if ( !is_numeric($VehicleID) ) {
        $app->flash("error", "There was a problem finding your vehicle. Please try again or contact us for assistance by visiting <a href='http://mypushcart.com/helpme/' target='_blank'>http://mypushcart.com/helpme</a>.");
        $app->redirect("/tipsheet/form");   
    }  
    
    $customerOrder = array(
        "CustomerID"            => $CustomerID,
        "PurchaseLocation"      => $submittedData['PurchaseLocation'],
        "OrderNumber"           => $submittedData['OrderNumber'],
        "SKU"                   => $submittedData['SKU'],
        "VehicleID"               => $VehicleID
    );
    
    $thisOrder = $custDB->updateCustomerOrder($customerOrder);

    if ( is_array($thisOrder) ) {
        $OrderID = $thisOrder['CustomerOrderID'];
    }
    else if ( is_numeric($thisOrder) ) {
        $OrderID = $thisOrder;;
    }
    else {
        $app->flash("error", "There was a problem submitting your order. Please try again or contact us for assistance by visiting <a href='http://mypushcart.com/helpme/' target='_blank'>http://mypushcart.com/helpme</a>.");
        $app->redirect("/tipsheet/form");   
    }
    
    $app->redirect("/tipsheets/".$submittedData['SKU']."/".$VehicleID);
});
