<?php

/*
 * Show Customer-related functions.
 */
$app->get('(/admin/customers(/))', function() use($app, $user)
{
   // Create the Page instance
   $pageTitle = "Customer Data";
   $page  = new Mumby\WebTools\AdminPage($pageTitle, $user, _MB_ROLE_MANAGE_CUSTOMER_DATA_);
   
   if($page->isAuthorized())
   {
      $pageContent = array();
      $pageContent["title"] = $pageTitle;
      
      $userOptions = array();
      $userOptions[] = array(
          "label" => "View/Export Customer Data",
          "link"  => "/admin/customers/view",
          "desc"  => "View a list of all existing Customer Data."
      );
      
      $pageContent["content"] = "<div class='adminOptionWrapper'>\n";
      $pageContent["content"] .= buildLandingPage($userOptions);
      $pageContent["content"] .= "</div>\n";
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * Show all Customer Data in the system.
 */
$app->get('(/admin/customers/view(/))', function() use($app, $user) 
{
    // Create the Page instance
    $page  = new Mumby\WebTools\AdminPage("Menus", $user, _MB_ROLE_MANAGE_CUSTOMER_DATA_);

    if($page->isAuthorized())
    {
        $pageContent = $page->getEmptyContentObj();
        $pageContent["title"] = "Customer Data";
        $pageContent["content"] = "";
        
        $custMgr = new \Mumby\DB\CustomerData();
        $custs = $custMgr->getAllCustomerData();

        if(!empty($custs))
        {  
            $pageContent["content"] .= "<p>Below is a sortable list of Customer Data. You can enable/disable columns at the top.\n";
            $pageContent["content"] .= "<p>You can download a CSV export of the data by clicking <a href='/admin/customers/export'>Export Customer Data</a>.</p>\n";
      
            $tipsheetOptions = array();
          
            $table = new \Mumby\WebTools\Table();      
            
            $visible = array( 
                "Order Number", "First Name", "Last Name", "Email Address", "Zip", "Phone", "SKU", "Purchase Location"
            );
            
            $paginate = false;
            if ( count($custs) > 10 ) $paginate = true;
            $pageContent["content"] .= $table->getTable($custs, "", "", true, true, $paginate, true, $visible);
            
        }
        else
            $pageContent["content"] .= "<div class='alert alert-warning'>No Customer Data found!</div>";
      
      
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

/*
 * Export Customer Data in the system.
 */
$app->get('(/admin/customers/export(/))', function() use($app, $user) 
{
    // Create the Page instance
    $page  = new Mumby\WebTools\AuthPage("Menus", $user, _MB_ROLE_MANAGE_CUSTOMER_DATA_);

    if($page->isAuthorized())
    {
        $custMgr = new \Mumby\DB\CustomerData();
        $custs = $custMgr->getAllCustomerData();
        
        if ( !empty($custs) ) {
            $heading = array_keys($custs[0]);           
            ob_clean();
            header("Content-type: application/csv");
            header('Content-disposition: attachment; filename="CustomerData.csv"');
            $fp = fopen('php://output', 'w');
            fputcsv($fp, $heading, ',', '"');
            foreach ($custs as $row) {
                if ( !empty($row) )
                    fputcsv($fp, $row, ',', '"');
            }
            fclose($fp);
        }
    }
    else
    {
         $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
         //$app->redirect('/403', 403);
    }
    
});