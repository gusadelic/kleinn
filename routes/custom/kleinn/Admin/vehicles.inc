<?php
/*
 * Show vehicle-related functions.
 */
$app->get('(/admin/vehicles(/))', function() use($app, $user)
{
    // Create the Page instance
    $pageTitle = "Vehicles";
    $page  = new Mumby\WebTools\AdminPage($pageTitle, $user, _MB_ROLE_MANAGE_VEHICLES_);

    if($page->isAuthorized())
    {
        $pageContent = array();
        $pageContent["title"] = $pageTitle;

        $userOptions = array();
        $userOptions[] = array(
            "label" => "View/Edit Vehicles",
            "link"  => "/admin/vehicles/view",
            "desc"  => "View a list of all existing Vehicles."
        );

        $userOptions[] = array(
            "label" => "Add Vehicle",
            "link"  => "/admin/vehicles/add",
            "desc"  => "Add a new Vehicle."
        );

        $pageContent["content"] = "<div class='adminOptionWrapper'>\n";
        $pageContent["content"] .= buildLandingPage($userOptions);
        $pageContent["content"] .= "</div>\n";


        $page->displayContent($pageContent);
    }
    else
    {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
    }
});

$app->map('(/admin/vehicles/view(/))', function() use($app, $user)
{
    // Create the Page instance
    $pageTitle = "View/Edit Vehicles";
    $page  = new Mumby\WebTools\AdminPage($pageTitle, $user, _MB_ROLE_MANAGE_VEHICLES_);

    if($page->isAuthorized())
    {
        $pageContent = $page->getEmptyContentObj();
        $pageContent["title"] = $pageTitle;
        $pageContent['content'] = "";
        $pageContent['content'] .= "<p>Select a year to view the vehicles from that year.</p>\n";
        $pageContent["content"] .= "<p>If you would like to add a nre Vehicle, please visit <a href='/admin/vehicles/add'>Add New Vehicle</a>.</p>\n";
 
        $vehicleDB  = new \Mumby\DB\Vehicle();
        $yearsData = $vehicleDB->getYears();
        $years = array();
        foreach ( $yearsData as $y ) {
            $years[$y] = $y;
        }
        
        $form = new Mumby\WebTools\SystemForm(null, true);
        $yearSelect = $form->addSelectField("Year", $years, true, array("fieldName"=>"year"));
        
        $resultContent = "";
                
        if($form->submitted() || $app->request->isGet() && is_numeric($app->request->get('year')) )
        {
            $form->processFormData();

            if($form->processedData)
            {
                if ( $app->request->isGet() && is_numeric($app->request->get('year')) ) { 
                    $year = $app->request->get('year');
                }
                else {
                    $year = $yearSelect->value;
                }
            
                $vehicles = $vehicleDB->getVehiclesByYear($year);

                if ( $vehicles === false ) {
                    $pageContent['content'] .= '<div class="alert alert-warning">There are currently no vehicles stored in the system for the selected Year. You can add one by visiting the <a href=""/admin/vehicles/add">Add Vehicle Page</a> of try selecting a different Year.</div><br />';
                }
                else {

                    $table = new \Mumby\WebTools\Table();
                    $vehicleData = array();
                    foreach ($vehicles as $v){
                        $vehicleData[] = array(
                          "Year"  => $v['Year'],
                          "Make"  => $v['Make'],
                          "Model" => $v['Model'],
                          "Edit"    => '<a href="/admin/vehicles/view/'.$v['VehicleID'].'">[details]</a>' . " " . '<a href="/admin/vehicles/wiring/'.$v['VehicleID'].'">[wiring]</a>'
                        );
                    }
                    
                    $resultContent = $table->getTable($vehicleData, "", "", true, true);
                    
                } 
            }
            else
            {
                $errorMessages = $form->getErrors();
                if(!empty($errorMessages))
                {
                    foreach($errorMessages as $e)
                    {
                       $page->setError($e);
                    }
                }
                else
                {
                   $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
                }
            }      
        }
        
        $pageContent["content"] .= $form->generateForm() . $resultContent;
        $page->displayContent($pageContent);
    }
    else
    {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
    }
})->via('GET','POST');

$app->map('(/admin/vehicles/view/:VehicleID(/))', function($VehicleID) use($app, $user)
{
    // Create the Page instance
    $pageTitle = "Edit Vehicle";
    $page  = new Mumby\WebTools\AdminPage($pageTitle, $user, _MB_ROLE_MANAGE_VEHICLES_);

    if($page->isAuthorized())
    {
        $vehicleDB  = new \Mumby\DB\Vehicle();
        $vehicle = $vehicleDB->getVehicle($VehicleID);
        
        $pageContent = $page->getEmptyContentObj();
        $pageContent["title"] = $pageTitle . " - " . $vehicle['Year'] . " " . $vehicle['Make'] . " " . $vehicle['Model'];
        $pageContent['content'] = "";

        
        //$years = $vehicleDB->getYears();
        $makes = $vehicleDB->getMakes();
        $models = $vehicleDB->getModels();

        $form = new Mumby\WebTools\SystemForm(null, false);
        
        $yearField = $form->addNumberField("Year", true, array("moreAttributes"=>array("min"=>"1900", "max"=>"9999", "step"=>"1", "autocomplete"=>"off")));
        $yearField->setValue($vehicle['Year'], false);
        $makeField = $form->addTextField("Make", true, array("options"=>$makes, "moreAttributes"=>array("autocomplete"=>"off")));
        $makeField->setValue($vehicle['Make'], false);
        $modelField = $form->addTextField("Model", true, array("options"=>$models, "moreAttributes"=>array("autocomplete"=>"off")));
        $modelField->setValue($vehicle['Model'], false);

        $pageContent["content"] .= $form->generateForm();
        
        if($form->submitted())
        {
            $form->processFormData();
            if($form->processedData)
            {
                
                if(!$vehicleDB->updateVehicle($yearField->getValue(), $makeField->getValue(), $modelField->getValue(), $VehicleID) )
                {
                   $errorMessages = $vehicleDB->getErrors();
                   if(!empty($errorMessages))
                   {
                      foreach($errorMessages as $e)
                      {
                         $app->flash("error", $e);
                      }
                   }
                   else
                   {
                      $app->flash("error", "<div class='errorMessage'>There was an error modifying the Vehicle.</div>");
                   }
                }
                else
                {
                   $app->flash("success", "The Vehicle was modified successfully.</a>.");
                }

                $app->redirect("/admin/vehicles/view?year=".$yearField->getValue());
            }
            else
            {
                $errorMessages = $form->getErrors();
                if(!empty($errorMessages))
                {
                    foreach($errorMessages as $e)
                    {
                       $page->setError($e);
                    }
                }
                else
                {
                   $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
                }
            }      
        }

        $page->displayContent($pageContent);
    }
    else
    {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
    }
})->via('GET','POST');

$app->map('(/admin/vehicles/add(/))', function() use($app, $user)
{
    // Create the Page instance
    $pageTitle = "Add Vehicle";
    $page  = new Mumby\WebTools\AdminPage($pageTitle, $user, _MB_ROLE_MANAGE_VEHICLES_);

    if($page->isAuthorized())
    {
        $vehicleDB  = new \Mumby\DB\Vehicle();
        
        $pageContent = $page->getEmptyContentObj();
        $pageContent["title"] = $pageTitle;
        $pageContent['content'] = "";

        
        //$years = $vehicleDB->getYears();
        $makes = $vehicleDB->getMakes();
        $models = $vehicleDB->getModels();

        $form = new Mumby\WebTools\SystemForm(null, false);
        
        $yearField = $form->addNumberField("Year", true, array("moreAttributes"=>array("min"=>"1900", "max"=>"9999", "step"=>"1", "autocomplete"=>"off")));
        $yearField->setValue(date("Y)"), false);
        $makeField = $form->addTextField("Make", true, array("options"=>$makes, "moreAttributes"=>array("autocomplete"=>"off")));
        $modelField = $form->addTextField("Model", true, array("options"=>$models, "moreAttributes"=>array("autocomplete"=>"off")));

        $pageContent["content"] .= $form->generateForm();
        
        if($form->submitted())
        {
            $form->processFormData();
            if($form->processedData)
            {
                
                if(!$vehicleDB->updateVehicle($yearField->getValue(), $makeField->getValue(), $modelField->getValue()) )
                {
                   $errorMessages = $vehicleDB->getErrors();
                   if(!empty($errorMessages))
                   {
                      foreach($errorMessages as $e)
                      {
                         $app->flash("error", $e);
                      }
                   }
                   else
                   {
                      $app->flash("error", "<div class='errorMessage'>There was an error adding the Vehicle.</div>");
                   }
                }
                else
                {
                   $app->flash("success", "The Vehicle was added successfully.</a>.");
                }

                $app->redirect("/admin/vehicles/view?year=".$yearField->getValue());

            }
            else
            {
                $errorMessages = $form->getErrors();
                if(!empty($errorMessages))
                {
                    foreach($errorMessages as $e)
                    {
                       $page->setError($e);
                    }
                }
                else
                {
                   $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
                }
            }      
        }

        $page->displayContent($pageContent);
    }
    else
    {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
    }
})->via('GET','POST');

$app->map('(/admin/vehicles/wiring/:VehicleID(/))', function($VehicleID) use($app, $user)
{
    // Create the Page instance
    $pageTitle = "Vehicle Wiring";
    $page  = new Mumby\WebTools\AdminPage($pageTitle, $user, _MB_ROLE_MANAGE_VEHICLES_);

    if($page->isAuthorized())
    {
        $form = new Mumby\WebTools\SystemForm();
        $form->submitValue = "Save Vehicle Wiring";
        $vehicleDB  = new \Mumby\DB\Vehicle();
        $vehicle = $vehicleDB->getVehicle($VehicleID);
        $wiring = $vehicleDB->getVehicleWiring($VehicleID);
        
        $compDB  = new \Mumby\DB\Component();
        $wires = $compDB->getDefaultWiring();
        
        if ( empty($vehicle) ) {
            $app->flash("error", "<div class='errorMessage'>The Vehicle must exist in the system in order to edit wiring for it!</div>");
            $app->redirect("/admin/vehicles/view");
        }    
            
        $wireSelect = "<select class='wireName form-control'>";
        foreach ( $wires as $k=>$wi )
        {
            $wireSelect .= "<option value='".$k."' >".$wi."</option>";
        }
        $wireSelect .='</select>';
        
        $default = false;
        if ( empty($wiring) ) {
            $default = true;
            $wiring = array();
            foreach($vehicleDB->getDefaultWiring() as $w) {
                $wiring[] = array(
                    "VehicleWireName" => $w,
                    "VehicleWireColor" => "",
                    "VehicleWirePolarity" => "",
                    "VehicleWireLocation" => ""
                );
            }
        }

        $page->addJS( array(
            '/common/admin/js/vehicle_wires.js'
        ));
        
        $page->addCSS( array(
           '/common/css/loader.css',
           '/common/admin/css/vehicle_wires.css'
        ));
        
        $pageContent = $page->getEmptyContentObj();
        $pageContent["title"] = $pageTitle . " for " . $vehicle['Year'] . " " . $vehicle['Make'] . " " . $vehicle['Model'];
        $pageContent['content'] = "";
        
        if ( $default ) 
            $pageContent["content"] .= "<div class='alert alert-warning defaultMessage'>This is an unsaved default layout.</div>";
        
        $output = "";
        $output .= '<script>wireSelect = "'.$wireSelect.'"</script>';
        $output .= "<table id='vehicleWires'>";
        $output .= "<thead><tr>";
        $output .= "<th id='colName'>Wire</th><th id='colColor'>Color</th><th id='colPolarity'>Polarity</th><th id='colLocation'>Location</th><th></th>";
        $output .= "</tr></thead><tbody>";
        $count = 0;
        foreach ( $wiring as $k=>$w ) {
            $output .= '<tr lindex="'.$count.'" class="" id="wire_'.$count.'">';
            //$output .= '<td><input type="text" class="wireName form-control" value="'.$w["VehicleWireName"].'"></input></td>';
            
            $wireSelect = '<select class="wireName form-control">';
            foreach ( $wires as $k=>$wi )
            {
                if ( $w["VehicleWireName"] === $wi )
                    $wireSelect .= "<option value='".$k."' selected='selected'>".$wi."</option>";
                else
                    $wireSelect .= "<option value='".$k."' >".$wi."</option>";
            }
            $wireSelect .='</select>';
            $output .= '<td>'.$wireSelect."</td>";
            
            $output .= '<td><input type="text" class="wireColor form-control" value="'.$w["VehicleWireColor"].'"></input></td>';
            $output .= '<td><input type="text" class="wirePolarity form-control" value="'.$w["VehicleWirePolarity"].'"></input></td>';
            $output .= '<td><input type="text" class="wireLocation form-control" value="'.$w["VehicleWireLocation"].'"></input></td>';
            $output .= '<td><span class="deleteMe fa fa-ban glyphicon glyphicon-remove-circle" onclick="javascript:deleteWire(\''.$count.'\'); return false;"></span></td>';
            $output .= '</tr>';

            $count++;
        }
        $output .= "</tbody></table>";
        $output .= "<script>currentIndex = ".$count.";</script>"."\n"; 
        $output .= "<script>vehicleID = ".$VehicleID.";</script>"."\n"; 
        $output .= '<br /><a href="#" class="addButton btn btn-success btn-sm" onclick="javascript:addWire();return false">Add Wire</a>';

        $wireData = $form->addHiddenField("Wire Data", array("fieldName" => "wireData"));
        
        $pageContent['content'] .= $output;
        if($form->submitted())
        {
            $form->processFormData();
            if($form->processedData)
            {

                if( !$vehicleDB->updateVehicleWiring(json_decode($wireData->getValue(), true)) )
                {
                   $errorMessages = $vehicleDB->getErrors();
                   if(!empty($errorMessages))
                   {
                      foreach($errorMessages as $e)
                      {
                         $app->flash("error", $e);
                      }
                   }
                   else
                   {
                      $app->flash("error", "<div class='errorMessage'>There was an error updating the Vehicle Wiring.</div>");
                   }
                }
                else
                {
                   $app->flash("success", "The Vehicle Wiring was updated successfully.</a>.");
                }

                $app->redirect("/admin/vehicles/view?year=".$vehicle['Year']);

            }
            else
            {
                $errorMessages = $form->getErrors();
                if(!empty($errorMessages))
                {
                    foreach($errorMessages as $e)
                    {
                       $page->setError($e);
                    }
                }
                else
                {
                   $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
                }
            }      
        }

        $pageContent["content"] .= $form->generateForm();
        $page->displayContent($pageContent);
    }
    else
    {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
    }
})->via('GET','POST');
