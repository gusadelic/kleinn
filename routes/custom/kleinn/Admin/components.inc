<?php
/*
 * Show component-related functions.
 */
$app->get('(/admin/components(/))', function() use($app, $user)
{
    // Create the Page instance
    $pageTitle = "Components";
    $page  = new Mumby\WebTools\AdminPage($pageTitle, $user, _MB_ROLE_MANAGE_COMPONENTS_);

    if($page->isAuthorized())
    {
        $pageContent = array();
        $pageContent["title"] = $pageTitle;

        $userOptions = array();
        $userOptions[] = array(
            "label" => "View/Edit Components",
            "link"  => "/admin/components/view",
            "desc"  => "View a list of all existing Components."
        );

        $userOptions[] = array(
            "label" => "Add Component",
            "link"  => "/admin/components/add",
            "desc"  => "Add a new Component."
        );

        $pageContent["content"] = "<div class='adminOptionWrapper'>\n";
        $pageContent["content"] .= buildLandingPage($userOptions);
        $pageContent["content"] .= "</div>\n";


        $page->displayContent($pageContent);
    }
    else
    {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
    }
});

$app->map('(/admin/components/view(/))', function() use($app, $user)
{
    // Create the Page instance
    $pageTitle = "View/Edit Components";
    $page  = new Mumby\WebTools\AdminPage($pageTitle, $user, _MB_ROLE_MANAGE_VEHICLES_);

    if($page->isAuthorized())
    {
        $pageContent = $page->getEmptyContentObj();
        $pageContent["title"] = $pageTitle;
        $pageContent['content'] = "";
        $pageContent['content'] .= "<p></p>\n";
        $pageContent["content"] .= "<p>If you would like to add a new Component, please visit <a href='/admin/components/add'>Add New Component</a>.</p>\n";
 
        $compDB  = new \Mumby\DB\Component();
        $compData = $compDB->getAllComponents();
        
        if ( !is_array($compData) ) {
            
            $pageContent["content"] .= "<div class='alert alert-warning'>There are currntly no Components defined in the system.</div>";
        }
        else {
            $data = array();
            foreach ( $compData as $c ) {
                $thisData = array(
                    "Name"      => $c['ComponentName'],
                    "Type"      => $c['ComponentType'],
                    "RSType"    => $c['RSType'],
                    "Brand"     => $c['ComponentBrand'],
                    "Edit"      => "<a href='/admin/components/edit/".$c['ComponentID']."'>[details]</a> "
                 );
                
                if ( $c['ComponentType'] == "rs" ) {
                    $thisData['Edit'] .= " <a href='/admin/components/wiring/".$c['ComponentID']."'>[wiring]</a>";
                }
                
                $data[] = $thisData;
                
            }
            $table = new \Mumby\WebTools\Table();

            $pageContent["content"] .= $table->getTable($data, "", "", true, true, false, false);
        }
        $page->displayContent($pageContent);
    }
    else
    {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
    }
})->via('GET','POST');

$app->map('(/admin/components/edit/:ComponentID(/))', function($ComponentID) use($app, $user)
{
    // Create the Page instance
    $pageTitle = "Edit Component";
    $page  = new Mumby\WebTools\AdminPage($pageTitle, $user, _MB_ROLE_MANAGE_VEHICLES_);

    if($page->isAuthorized())
    {
        $compDB  = new \Mumby\DB\Component();
        $thisComp = $compDB->getComponent($ComponentID);
        
        $compTypes = $compDB->getComponentTypes();
        
        $pageContent = $page->getEmptyContentObj();
        $pageContent["title"] = $pageTitle . " - " . $thisComp['ComponentName'];
        $pageContent['content'] = "";

        $form = new Mumby\WebTools\SystemForm(null, false);
        
        $nameField = $form->addTextField("Name", true, array());
        $nameField->setValue($thisComp['ComponentName'], false);
        
        if ( !is_array($compTypes) ) {
            $typeField = $form->addTextField("Type", true);
        }
        else {
            $compTypeData = array();
            foreach ( $compTypes as $t ) {
                $compTypeData[] = $t['ComponentType'];
            }
            $typeField = $form->addSelectField("Type", $compTypeData, true);
            $typeField->setValue($thisComp['ComponentType'], false);
        }
        
        $rstypeField = $form->addTextField("RS Type");
        $rstypeField->setValue($thisComp['RSType'], false);
        
        $brandField = $form->addTextField("Brand");
        $brandField->setValue($thisComp['ComponentBrand'], false);

        $pageContent["content"] .= $form->generateForm();
        
        if($form->submitted())
        {
            $form->processFormData();
            if($form->processedData)
            {
                $params = array(
                    "ComponentID"       => $ComponentID,
                    "ComponentName"     => $nameField->getValue(),
                    "ComponentType"     => $typeField->getValue(),
                    "RSType"            => $rstypeField->getValue(),
                    "ComponentBrand"    => $brandField->getValue()
                );
                
                if( !$compDB->updateComponent($params) )
                {
                   $errorMessages = $compDB->getErrors();
                   if(!empty($errorMessages))
                   {
                      foreach($errorMessages as $e)
                      {
                         $app->flash("error", $e);
                      }
                   }
                   else
                   {
                      $app->flash("error", "<div class='errorMessage'>There was an error modifying the Component.</div>");
                   }
                }
                else
                {
                   $app->flash("success", "The Component was modified successfully.</a>.");
                }

                $app->redirect("/admin/components/view");

            }
            else
            {
                $errorMessages = $form->getErrors();
                if(!empty($errorMessages))
                {
                    foreach($errorMessages as $e)
                    {
                       $page->setError($e);
                    }
                }
                else
                {
                   $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
                }
            }      
        }

        $page->displayContent($pageContent);
    }
    else
    {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
    }
})->via('GET','POST');

$app->map('(/admin/components/add(/))', function() use($app, $user)
{
    // Create the Page instance
    $pageTitle = "Add Component";
    $page  = new Mumby\WebTools\AdminPage($pageTitle, $user, _MB_ROLE_MANAGE_VEHICLES_);

    if($page->isAuthorized())
    {
        $compDB  = new \Mumby\DB\Component();
        
        $compTypes = $compDB->getComponentTypes();
        
        $pageContent = $page->getEmptyContentObj();
        $pageContent["title"] = $pageTitle;
        $pageContent['content'] = "";

        $form = new Mumby\WebTools\SystemForm(null, false);
        
        $nameField = $form->addTextField("Name", true, array());
        
        if ( !is_array($compTypes) ) {
            $typeField = $form->addTextField("Type", true);
        }
        else {
            $compTypeData = array();
            foreach ( $compTypes as $t ) {
                $compTypeData[] = $t['ComponentType'];
            }
            $typeField = $form->addSelectField("Type", $compTypeData, true);
        }
        
        $rstypeField = $form->addTextField("RS Type");
        
        $brandField = $form->addTextField("Brand");

        $pageContent["content"] .= $form->generateForm();
        
        if($form->submitted())
        {
            $form->processFormData();
            if($form->processedData)
            {
                $params = array(
                    "ComponentName"     => $nameField->getValue(),
                    "ComponentType"     => $typeField->getValue(),
                    "RSType"            => $rstypeField->getValue(),
                    "ComponentBrand"    => $brandField->getValue()
                );
                
                if( !$compDB->updateComponent($params) )
                {
                   $errorMessages = $compDB->getErrors();
                   if(!empty($errorMessages))
                   {
                      foreach($errorMessages as $e)
                      {
                         $app->flash("error", $e);
                      }
                   }
                   else
                   {
                      $app->flash("error", "<div class='errorMessage'>There was an error adding the Component.</div>");
                   }
                }
                else
                {
                   $app->flash("success", "The Component was added successfully.</a>.");
                }

                $app->redirect("/admin/components/view");

            }
            else
            {
                $errorMessages = $form->getErrors();
                if(!empty($errorMessages))
                {
                    foreach($errorMessages as $e)
                    {
                       $page->setError($e);
                    }
                }
                else
                {
                   $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
                }
            }      
        }

        $page->displayContent($pageContent);
    }
    else
    {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
    }
})->via('GET','POST');

$app->map('(/admin/components/wiring/:ComponentID(/))', function($ComponentID) use($app, $user)
{
    // Create the Page instance
    $pageTitle = "Component Wiring";
    $page  = new Mumby\WebTools\AdminPage($pageTitle, $user, _MB_ROLE_MANAGE_VEHICLES_);

    if($page->isAuthorized())
    {
        $form = new Mumby\WebTools\SystemForm();
        $form->submitValue = "Save Component Wiring";
        $compDB  = new \Mumby\DB\Component();
        $comp = $compDB->getComponent($ComponentID);
        $wiring = $compDB->getComponentWiring($ComponentID);
        
        $wires = $compDB->getDefaultWiring();
        
        if ( empty($comp) ) {
            $app->flash("error", "<div class='errorMessage'>The Component must exist in the system in order to edit wiring for it!</div>");
            $app->redirect("/admin/components/view");
        }    
        
        $wireSelect = "<select class='wireName form-control'>";
        foreach ( $wires as $k=>$wi )
        {
            $wireSelect .= "<option value='".$k."' >".$wi."</option>";
        }
        $wireSelect .='</select>';
            
        $default = false;
        if ( empty($wiring) ) {
            $default = true;
            $wiring = array();
            foreach($wires as $w) {
                $wiring[] = array(
                    "Name" => $w,
                    "Color" => "",
                    "Harness" => "",
                    "Notes" => ""
                );
            }
        }

        $page->addJS( array(
            '/common/admin/js/component_wires.js'
        ));
        
        $page->addCSS( array(
           '/common/css/loader.css',
           '/common/admin/css/component_wires.css'
        ));
        
        $pageContent = $page->getEmptyContentObj();
        $pageContent["title"] = $pageTitle . " for " . $comp['ComponentName'];
        $pageContent['content'] = "";

        if ( $default ) 
            $pageContent["content"] .= "<div class='alert alert-warning defaultMessage'>This is an unsaved default layout.</div>";
        
        $output = "";
        $output .= '<script>wireSelect = "'.$wireSelect.'"</script>';
        $output .= "<table id='componentWires'>";
        $output .= "<thead><tr>";
        $output .= "<th id='colName'>Wire</th><th id='colColor'>Color</th><th id='colHarness'>Harness</th><th id='colNotes'>Notes</th><th></th>";
        $output .= "</tr></thead><tbody>";
        $count = 0;
        foreach ( $wiring as $k=>$w ) {
            $output .= '<tr lindex="'.$count.'" class="" id="wire_'.$count.'">';
            //$output .= '<td><input type="text" class="wireName form-control" value="'.$w["Name"].'"></input></td>';
            
            $wireSelect = '<select class="wireName form-control">';
            foreach ( $wires as $k=>$wi )
            {
                if ( $w["Name"] === $wi )
                    $wireSelect .= "<option value='".$k."' selected='selected'>".$wi."</option>";
                else
                    $wireSelect .= "<option value='".$k."' >".$wi."</option>";
            }
            $wireSelect .='</select>';
            $output .= '<td>'.$wireSelect."</td>";
            
            $output .= '<td><input type="text" class="wireColor form-control" value="'.$w["Color"].'"></input></td>';
            $output .= '<td><input type="text" class="wireHarness form-control" value="'.$w["Harness"].'"></input></td>';
            $output .= '<td><input type="text" class="wireNotes form-control" value="'.$w["Notes"].'"></input></td>';
            $output .= '<td><span class="deleteMe fa fa-ban glyphicon glyphicon-remove-circle" onclick="javascript:deleteWire(\''.$count.'\'); return false;"></span></td>';
            $output .= '</tr>';

            $count++;
        }
        $output .= "</tbody></table>";
        $output .= "<script>currentIndex = ".$count.";</script>"."\n"; 
        $output .= "<script>componentID = ".$ComponentID.";</script>"."\n"; 
        $output .= '<br /><a href="#" class="addButton btn btn-success btn-sm" onclick="javascript:addWire();return false">Add Wire</a>';

        $wireData = $form->addHiddenField("Wire Data", array("fieldName" => "wireData"));
        
        $pageContent['content'] .= $output;
        if($form->submitted())
        {
            $form->processFormData();
            if($form->processedData)
            {

                if( !$compDB->updateComponentWiring(json_decode($wireData->getValue(), true)) )
                {
                   $errorMessages = $compDB->getErrors();
                   if(!empty($errorMessages))
                   {
                      foreach($errorMessages as $e)
                      {
                         $app->flash("error", $e);
                      }
                   }
                   else
                   {
                      $app->flash("error", "<div class='errorMessage'>There was an error updating the Component Wiring.</div>");
                   }
                }
                else
                {
                   $app->flash("success", "The Component Wiring was updated successfully.</a>.");
                }

                $app->redirect("/admin/components/view");

            }
            else
            {
                $errorMessages = $form->getErrors();
                if(!empty($errorMessages))
                {
                    foreach($errorMessages as $e)
                    {
                       $page->setError($e);
                    }
                }
                else
                {
                   $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
                }
            }      
        }

        $pageContent["content"] .= $form->generateForm();
        $page->displayContent($pageContent);
    }
    else
    {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
    }
})->via('GET','POST');
