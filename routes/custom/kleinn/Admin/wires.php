<?php

/*
 * Show wire-related functions.
 */
$app->get('(/admin/wires(/))', function() use($app, $user)
{
   // Create the Page instance
   $pageTitle = "Tipsheets";
   $page  = new Mumby\WebTools\AdminPage($pageTitle, $user, _MB_ROLE_MANAGE_WIRES_);
   
   if($page->isAuthorized())
   {
      $pageContent = array();
      $pageContent["title"] = $pageTitle;
      
      $userOptions = array();
      $userOptions[] = array(
          "label" => "View/Edit Wires",
          "link"  => "/admin/wires/edit",
          "desc"  => "View an editable list of Wires."
      );

      $userOptions[] = array(
          "label" => "Set Default Wire Order",
          "link"  => "/admin/wires/default",
          "desc"  => "Change the default wire layout."
      );
      
      $pageContent["content"] = "<div class='adminOptionWrapper'>\n";
      $pageContent["content"] .= buildLandingPage($userOptions);
      $pageContent["content"] .= "</div>\n";
      
      $page->displayContent($pageContent);
   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

$app->get('(/admin/wires/edit)', function() use($app, $user)
{
    // Create the Page instance
    $pageTitle = "Wires";
    $page  = new Mumby\WebTools\AdminPage($pageTitle, $user, _MB_ROLE_MANAGE_WIRES_);

    if($page->isAuthorized())
    {
        //$form = new Mumby\WebTools\SystemForm();
        //$form->submitValue = "Save Wires";

        $wireDB  = new \Mumby\DB\Wire();
        $wires = $wireDB->getWires(); 

        $page->addJS( array(
            '/common/admin/js/wires.js'
        ));
        
        $page->addCSS( array(
           '/common/admin/css/wires.css',
           '/common/css/loader.css'
        ));
        
        $pageContent = $page->getEmptyContentObj();
        $pageContent["title"] = "Wires";
        $pageContent['content'] = "";
        
        $output = "";
        
        $output .= "<input type='hidden' name='wireOrder' id='wireOrder'></input>";
        
        $output .= "<span onclick='javascript:addWire();return false' class='js-add-item-button addButton btn btn-success btn-sm'><i class='glyphicon glyphicon-plus'></i> Add Wire</span>";
        $output .= "<span onclick='javascript:deleteAllWires();return false' class='js-add-item-button addButton btn btn-default btn-sm'><i class='glyphicon glyphicon-minus'></i> Clear All Wires</span><br />";
        $output .= "\n<ol id='editWires' class='js-sortable'>\n";
        $count = 0;

        if ( is_array($wires ) ) {
            foreach ( $wires as $k=>$w ) {
                $output .= '<li wireid="'.$w['WireID'].'" lindex="'.$count.'" class="" id="wire_'.$count.'"><div class="row">';
                $output .= '<span class="deleteMe fa fa-ban glyphicon glyphicon-remove-circle" onclick="javascript:deleteWire('.$count.'); return false;"></span>';
                $output .= '<label class="control-label text_field required" for="wire_name_'.$count.'"><span class="labelText">Wire Name</span></label>';
                $output .= '<input type="text" name="wire_name_'.$count.'" id="wire_name_'.$count.'" class="form-control" required="required" value="'.$w['WireName'].'" autocomplete="off">';
                $output .= '<label class="control-label text_field required" for="wire_default_'.$count.'"><span class="labelText">Default Wire</span></label>';                
                if ( $w['IsDefault'] == 1 ) 
                    $output .= '<input type="checkbox" name="wire_default_'.$count.'" id="wire_default_'.$count.'" checked="checked">';
                else 
                    $output .= '<input type="checkbox" name="wire_default_'.$count.'" id="wire_default_'.$count.'">';

                $output .= '</div>';
                $output .= '</li>';

                $count++;
            }
        }
        $output .= "</ol>";
        $output .= "<script>currentIndex = ".$count.";</script>"."\n"; 

        $output .= "<form id='wireForm' action='/admin/wires/edit' method='post'>";       
        $output .= "<input type='hidden' name='wireData' id='wireData'></input>";
        //$wireData = $form->addHiddenField("Wire Data", array("fieldName" => "wireData"));
        
        $pageContent['content'] .= $output;

        //$pageContent["content"] .= $form->generateForm();
        $pageContent["content"] .= "<br /><div class='submitDiv'>";
        $pageContent["content"] .= "<input class='btn btn-success' type='submit' name='submit' id='submit' value='Save Wires' />";
        $pageContent["content"] .= "</div>";
        $pageContent["content"] .= "</form>";
        $page->displayContent($pageContent);
    }
    else
    {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
    }
});

/*
 * Process submitted section information.
 */
$app->post('/admin/wires/edit', function() use($app)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\AdminPage("Menus", $user, _MB_ROLE_MANAGE_TIPSHEETS_);
   
   if($page->isAuthorized())
   {
        $wireDB = new \Mumby\DB\Wire();
        $submittedData = $app->request->post();

        $submittedWires = json_decode($submittedData['wireData'], true);
        $duplicate = false;
        $blank = false;

        foreach ($submittedWires as $i=>$s) {
            if ( $s['WireName'] == "" ) {
                $blank = true;
                break;
            }
            foreach ($submittedWires as $k=>$w ) {
                if ( $s['WireName'] == $w['WireName'] && $i != $k ) {
                    $duplicate = $s['WireName'];
                    break;
                }
            }
        }
        if ( $duplicate ) {
            $result = false;
            $wireDB->errorMessages[] = "Duplicate wire name ".$duplicate." detected. Wire names must be Unique.";
        }
        else if ( $blank ) {
            $result = false;
            $wireDB->errorMessages[] = "Wire names cannot be blank.";   
        }
        else {
            $result = $wireDB->updateWires($submittedWires);
        }

        if( !is_array($result) )
        {
           $errorMessages = $wireDB->getErrors();
           if(!empty($errorMessages))
           {
               $app->flash("error", $errorMessages);
           }
           else
           {
              $app->flash("error", "<div class='errorMessage'>There was an error updating the Wires.</div>");
           }
        }
        else
        {
           $app->flash("success", "The Wires were updated successfully.</a>.");
        }

        $app->redirect("/admin/wires/edit");

   }
   else
   {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
   }
});

$app->map('(/admin/wires/default)', function() use($app, $user)
{
    // Create the Page instance
    $pageTitle = "Wires";
    $page  = new Mumby\WebTools\AdminPage($pageTitle, $user, _MB_ROLE_MANAGE_WIRES_);

    if($page->isAuthorized())
    {
        $form = new Mumby\WebTools\SystemForm();
        $form->submitValue = "Save Default Wire Layout";

        $wireDB  = new \Mumby\DB\Wire();
        $wires = $wireDB->getDefaultWires(); 

        $page->addJS( array(
            '/common/bower/html5sortable/dist/html.sortable.min.js',
            '/common/admin/js/wires.js'
        ));
        
        $page->addCSS( array(
           '/common/admin/css/wires.css'
        ));
        
        $pageContent = $page->getEmptyContentObj();
        $pageContent["title"] = "Default Wire Layout";
        $pageContent['content'] = "";
        
        $output = "";
        
        $output .= "\n<ol id='orderWires' class='js-sortable'>\n";
        $count = 0;

        if ( is_array($wires ) ) {
            foreach ( $wires as $k=>$w ) {
                $output .= '<li wireid="'.$w['WireID'].'" lindex="'.$count.'" class="wire" id="wire_'.$count.'"><div class="row">';
                $output .= "<i class='fa fa-sort-amount-asc glyphicon glyphicon-sort'></i>\n";
                $output .= '<strong>'.$w['WireName'].'</strong>';
                $output .= '</div>';
                $output .= '</li>';

                $count++;
            }
        }
        $output .= "</ol>";
        
        $wireOrder = $form->addHiddenField("Wire Order", array("fieldName" => "wireOrder"));
        
        $pageContent['content'] .= $output;
        if($form->submitted())
        {
            $form->processFormData();
            if($form->processedData)
            {
                $submittedOrder = json_decode($wireOrder->getValue(), true);

                $result = $wireDB->updateWireOrder($submittedOrder);

                if( !$result )
                {
                   $errorMessages = $wireDB->getErrors();
                   if(!empty($errorMessages))
                   {
                       $app->flash("error", $errorMessages);
                   }
                   else
                   {
                      $app->flash("error", "<div class='errorMessage'>There was an error updating the default Wire layout.</div>");
                   }
                }
                else
                {
                   $app->flash("success", "The default Wire layout was updated successfully.</a>.");
                }

                $app->redirect("/admin/wires");

            }
            else
            {
                $errorMessages = $form->getErrors();
                if(!empty($errorMessages))
                {
                    foreach($errorMessages as $e)
                    {
                       $page->setError($e);
                    }
                }
                else
                {
                   $page->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
                }
            }      
        }

        $pageContent["content"] .= $form->generateForm();
        $page->displayContent($pageContent);
    }
    else
    {
        $app->redirect('/admin/login?url='.$app->request->getPathInfo(),302);
        //$app->redirect('/403', 403);
    }
})->via('GET','POST');