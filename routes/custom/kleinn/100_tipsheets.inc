<?php

$app->get('/tipsheets/:SKU/:VehicleID', function($SKU, $VehicleID) use($app)
{
    // Create the Page instance
    $page  = new Mumby\WebTools\Page("index.html", "tipsheet_header.html", "tipsheet_footer.html");
    
    $contentObj = $page->getEmptyContentObj();
    $contentObj['title'] = "";
    $contentObj['content'] = "";
    
    $contentMgr = new Mumby\WebTools\Section();
    $tipsheetMgr = new Mumby\DB\Tipsheet($SKU);
    
    $tipsheet = $tipsheetMgr->tipsheet;

    if (!is_array($tipsheet) || !isset($tipsheet['Sections']) || !is_array($tipsheet['Sections'])  ) {
        $app->flash("error", "A Tipsheet for this SKU doesn't exist!");
        $app->redirect("/tipsheet/form");
    }
    
    $page->addInjectable($tipsheet['Name'], "tipsheetName");

    $appendWirechart = true;
    
    foreach ($tipsheet['Sections'] as $s) {
        $content = $contentMgr->getContent($s["PageID"]);
        $category = $contentMgr->getCategory($s["PageID"]);

        if ( $s["ShowTitle"] )
            $contentObj['content'] .= "<h1>".$content["title"]."</h1>";

        if ( $content['title'] === "Wirechart" && $category['Name'] === "Wirechart" ) {
            $appendWirechart = false;
            $wirechart = $tipsheetMgr->getWireChartData($VehicleID, $SKU, true);
            if ( empty($wirechart) )
                $wirechart = $tipsheetMgr->getWireChartData($VehicleID, $SKU, false);
            $compDB = new Mumby\DB\Component();
            $component = $compDB->getComponent($tipsheet['RSID']);
            $vehicleDB = new Mumby\DB\Vehicle();
            $vehicle = $vehicleDB->getVehicle($VehicleID);
            if ( !empty($wirechart) && !empty($component) ) {
                $page->addCSS( array(
                    '/common/admin/css/wirechart.css'
                ));

                $wirechartData = array(
                    "Component" => $component,
                    "Vehicle" => $vehicle,
                    "Wirechart" => $wirechart
                );

                $contentObj['content'] .= "<h1>Wirechart</h1>";
                $contentObj['content'] .= "<p>".$tipsheetMgr->buildWirechart($wirechartData)."</p>";
            } 
        }
        else {
            $contentObj['content'] .= "<p>".$content['content']."</p>";
        }
    }

    // Get wirechart and add it to the Tipsheet if the Bypass isn't an Evo Bypass.
    if ( !$tipsheetMgr->isEvo() && $VehicleID != null && $appendWirechart ) {
        $wirechart = $tipsheetMgr->getWireChartData($VehicleID, $SKU, true);
        if ( empty($wirechart) )
            $wirechart = $tipsheetMgr->getWireChartData($VehicleID, $SKU, false);
        $compDB = new Mumby\DB\Component();
        $component = $compDB->getComponent($tipsheet['RSID']);
        $vehicleDB = new Mumby\DB\Vehicle();
        $vehicle = $vehicleDB->getVehicle($VehicleID);
        if ( !empty($wirechart) && !empty($component) ) {
            $page->addCSS( array(
                '/common/admin/css/wirechart.css'
            ));

            $wirechartData = array(
                "Component" => $component,
                "Vehicle" => $vehicle,
                "Wirechart" => $wirechart
            );

            $contentObj['content'] .= "<h1>Wirechart</h1>";
            $contentObj['content'] .= "<p>".$tipsheetMgr->buildWirechart($wirechartData)."</p>";
        }
    }
    


    $contentObj['content'] .= '<hr><a href="/pdf/'.$SKU.'/'.$VehicleID.'" class="btn btn-info" style="float:right;margin-bottom:2em;">Download PDF</a>';
    
    $page->displayContent($contentObj);
});

// Exports a PDF file for a given SKU
// Requires installing wkhtmltopdf:
//  wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
//  tar xvf wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
//  mv wkhtmltox/bin/wkhtmlto* /usr/bin/
//  ln -nfs /usr/bin/wkhtmltopdf /usr/local/bin/wkhtmltopdf
//  
$app->get('/pdf/:SKU/:VehicleID', function($SKU, $VehicleID) use($app)
{
    // Create the Page instance
    $page  = new Mumby\WebTools\Page("index.html", "tipsheet_header.html", "tipsheet_footer.html");
    
    $contentObj = $page->getEmptyContentObj();
    $contentObj['title'] = "";
    $contentObj['content'] = "";
    
    // Get and assemble the Content
    $contentMgr = new Mumby\WebTools\Section();
    $tipsheetMgr = new Mumby\DB\Tipsheet($SKU);
    $tipsheet = $tipsheetMgr->tipsheet;

    if ( !is_array($tipsheet) ) {
        $app->flash("error", "A Tipsheet for this SKU doesn't exist!");
        $app->redirect("/tipsheet/form");
    }
    
    $appendWirechart = true;
    
    if ( isset($tipsheet['Sections']) && is_array($tipsheet['Sections']) ) {
        $page->addInjectable($tipsheet['Name'], "tipsheetName");
        foreach ($tipsheet['Sections'] as $s) {
            $content = $contentMgr->getContent($s["PageID"]);
            $category = $contentMgr->getCategory($s["PageID"]);
            if ( $s["ShowTitle"] )
                $contentObj['content'] .= "<h1>".$content["title"]."</h1>";

            if ( $content['title'] === "Wirechart" && $category['Name'] === "Wirechart" ) {
                $appendWirechart = false;
                $wirechart = $tipsheetMgr->getWireChartData($VehicleID, $SKU, true);
                if ( empty($wirechart) )
                    $wirechart = $tipsheetMgr->getWireChartData($VehicleID, $SKU, false);
                $compDB = new Mumby\DB\Component();
                $component = $compDB->getComponent($tipsheet['RSID']);
                $vehicleDB = new Mumby\DB\Vehicle();
                $vehicle = $vehicleDB->getVehicle($VehicleID);
                if ( !empty($wirechart) && !empty($component) ) {
                    $page->addCSS( array(
                        '/common/admin/css/wirechart.css'
                    ));

                    $wirechartData = array(
                        "Component" => $component,
                        "Vehicle" => $vehicle,
                        "Wirechart" => $wirechart
                    );

                    $contentObj['content'] .= "<h1>Wirechart</h1>";
                    $contentObj['content'] .= "<p>".$tipsheetMgr->buildWirechart($wirechartData)."</p>";
                } 
            }
            else {
                $contentObj['content'] .= "<p>".$content['content']."</p>";
            }
        }
        
        // Get wirechart and add it to the Tipsheet if the Bypass isn't an Evo Bypass.
        if ( !$tipsheetMgr->isEvo() && $VehicleID != null && $appendWirechart ) {
            $wirechart = $tipsheetMgr->getWireChartData($VehicleID, $SKU);
            $compDB = new Mumby\DB\Component();
            $component = $compDB->getComponent($tipsheet['RSID']);
            $vehicleDB = new Mumby\DB\Vehicle();
            $vehicle = $vehicleDB->getVehicle($VehicleID);
            if ( !empty($wirechart) && !empty($component) ) {
                $page->addCSS( array(
                    '/common/admin/css/wirechart.css'
                ));

                $wirechartData = array(
                    "Component" => $component,
                    "Vehicle" => $vehicle,
                    "Wirechart" => $wirechart
                );

                $contentObj['content'] .= "<h1 class='page-break'>Wirechart</h1>";
                $contentObj['content'] .= "<p>".$tipsheetMgr->buildWirechart($wirechartData)."</p>";
            }
        }
    }
    else {
        $app->flash("error", "The Tipsheet for this SKU/Vehicle combination is not available.");
        $app->redirect("/tipsheet/form");
    }
    
    // Run wkhtmltopdf
    $descriptorspec = array(
        0 => array('pipe', 'r'), // stdin
        1 => array('pipe', 'w'), // stdout
        2 => array('pipe', 'w'), // stderr
    );
    $process = proc_open('wkhtmltopdf -q --no-background -s Letter --footer-font-size 8 --footer-line --footer-right "Page [page] of [topage]" - -', $descriptorspec, $pipes);
    $html = $page->displayContent($contentObj,200,false,true,$app->request->getUrl());
    
    // Send the HTML on stdin
    fwrite($pipes[0], $html);
    fclose($pipes[0]);

    // Read the outputs
    $pdf = stream_get_contents($pipes[1]);
    $errors = stream_get_contents($pipes[2]);
       
    // Close the process
    fclose($pipes[1]);
    $return_value = proc_close($process);

    // Output the results
    if ($errors) {
        throw new Exception('PDF generation failed: ' . $errors);
    } else {
        $filename = str_replace(" ", "_",$tipsheet["Name"]);

        header('Content-Type: application/pdf');
        header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
        header('Pragma: public');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s').' GMT');
        header('Content-Length: ' . strlen($pdf));
        header('Content-Disposition: attachemnt; filename="' . $filename . '-' . $SKU . '.PDF";');
        echo $pdf;
    }
         
});