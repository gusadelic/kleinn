<?php

$app->error(function (\Exception $e=null) use ($app)
{
   // Create the Page instance
   $page  = new Mumby\WebTools\Page();
   
   // Create the Content instance.
   $content = new Mumby\WebTools\Content();
   
   $sysError = $content->getContent("/500");

   $mathJokes = array();
   
   $mathJokes[] = "<p>A physicist, a biologist, and a mathematician are sitting on a bench across from a house. They watch as two people go into the house, and then a little later, three people walk out.</p>
                   <p>The physicist says, \"The initial measurement was incorrect.\"</p>
                   <p>The biologist says, \"They must have reproduced.\"</p>
                   <p>And the mathematician says, \"If exactly one person enters that house, it will be empty.\"</p>";
   
   $mathJokes[] = "<p>
      Infinitely many mathematicians walk into a bar. The first says, \"I'll have a beer.\"
      The second says, \"I'll have half a beer.\" The third says, \"I'll have a quarter of a beer.\"
      The barman pulls out just two beers. The mathematicians are all like, \"That's all you're giving us? How do you expect us to all have a drink with that?\"
      The bartender says, \"Come on guys. Know your limits.\"</p>";
   
   $mathJokes[] = "<p>What do you get when you cross a mosquito with a mountain climber?</p>
                  <p>Nothing. You can't cross a vector and a scalar.</p>";

   $jokeContent  = "<article class='joke'>\n";
   $jokeContent .= $mathJokes[array_rand($mathJokes)];
   $jokeContent .= "</article>\n";
   
   $sysError["content"] .= $jokeContent;

   $page->displayContent($sysError, 500);
});

$app->get("/phpinfo", function() use ($app)
{
	dump(phpinfo());

});