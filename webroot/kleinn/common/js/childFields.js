var requiredFields;

$(document).ready( function()
{
   requiredFields = $(".hiddenFieldWrapper").find('input[required], select[required]');
   requiredFields.prop('required',false);

});

/* Toggles child fields
 *     - Only available on _MB_FORM_FIELD_RADIO_, _MB_FORM_FIELD_SELECT_, and _MB_FORM_FIELD_CHECKBOX_
 * To set up a parent field, add the following to the FormFieldMetadata, where {x} is the id of the parent element:
 *            "fieldName": {x} 
 *            "onchange": "toggleFields($(this))"
 *            
 * To set up a child field give the field the following classes:
 *            child_of_{x} where {x} is the id of the parent element.
 *            parent_option_{y} where {y} is the option value that will trigger this child element.
 * You will also need to add the following to FormFieldMetadata for each child field:
 *            "visibleByDefault": "false"
 */
function toggleFields(parent)
{
    requiredFields.prop('required',false);

    value = parent.val();
    parentName = parent.attr('name');

    targets = ".child_of_"+parentName+".parent_option_"+value;
    offWrappers = $(".child_of_"+parentName).not(targets).parents(".form-group");
    onWrappers = $(targets).parents(".form-group");

    offWrappers.addClass("hiddenFieldWrapper");
    onWrappers.removeClass("hiddenFieldWrapper");

    $(targets).each( function(i,e) {
        if ( $(this).is(requiredFields) )
                $(this).attr('required',true);
    });
    
    $(".child_of_"+parentName).not(targets).uReset();

    //$(targets).fadeTo(1000, 0.5, function() { $('.element').fadeTo(800, 1); });

}