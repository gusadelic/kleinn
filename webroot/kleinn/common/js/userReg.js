$(document).ready( function() {
    
    if ( $('#field_email').val() !== "" ) {
        $('#field_email').attr("disabled", true);
    }
    
    $('#field_copyphysical_option_0').click(function() {
       if ( $(this).prop('checked') ) {
           copyAddressFields();
       }
       else {
           clearAddressFields();
       }
    });
    
    $('.postAddress').change(function() {
        $('#field_copyphysical_option_0').prop("checked", false);
    });

    function copyAddressFields() {
        $('#field_postal_address1').val($('#field_physical_address1').val());

        $('#field_postal_address2').val($('#field_physical_address2').val());

        $('#field_postal_city').val($('#field_physical_city').val());

        $('#field_postal_state').val($('#field_physical_state').val());

        $('#field_postal_postcode').val($('#field_physical_postcode').val());

    }

    function clearAddressFields() {
        $('#field_postal_address1').val("");

        $('#field_postal_address2').val("");
 
        $('#field_postal_city').val("");

        $('#field_postal_state').val("");

        $('#field_postal_postcode').val("");

    }
   
});