var currentIndex = 0;

$(document).ready( function() { 
    
    if ( $('#orderWires.js-sortable').length > 0 ) {
        sortable('#orderWires.js-sortable', {
            forcePlaceholderSize: true,
            placeholderClass: 'placeholder'
        });

        sortable('#orderWires.js-sortable')[0].addEventListener('sortupdate', function(e) {
            updateOrder();
            console.log(e.detail);
        });
    }

    // Setup the loading indicator
    $("body").prepend('<div id="fade"></div>');
    $("body").prepend('<div id="loading"><img id="loader" src="/common/imgs/loading.gif" /></div>');


    $('#submit').click( function(e) {
        $.when(  getWireData(), $("#fade").show(), $("#loading").show() ).then( $(this).submit() );  
    });
    
});

function addWire()
{

    newNode = '<li lindex="'+currentIndex+'" class="" id="wire_'+currentIndex+'">'+
              '<div class="row">'+
              '<span class="deleteMe fa fa-ban glyphicon glyphicon-remove-circle" onclick="javascript:deleteWire('+currentIndex+'); return false;"></span>'+
              '<label class="control-label text_field required" for="wire_name_'+currentIndex+'"><span class="labelText">Wire Name</span></label>'+
              '<input type="text" name="wire_name_'+currentIndex+'" id="wire_name_'+currentIndex+'" class="form-control" required="required" autocomplete="off">'+
              '<label class="control-label text_field required" for="wire_default_'+currentIndex+'"><span class="labelText">Default Wire</span></label>'+
              '<input type="checkbox" name="wire_default_'+currentIndex+'" id="wire_default_'+currentIndex+'">'+
              '</div>'+
              '</li>'; 
      
    $('ol#editWires').append(newNode);
    
    var targetOffset = $('ol#editWires li').last().offset().top;
    $('html, body').animate({scrollTop: targetOffset}, 1000);
    
    currentIndex++;
};

function deleteWire(item)
{
    $('#wire_'+item).remove();
}

function deleteAllWires()
{
    $('ol#editWires li').remove();
}

function getWireData()
{
    wireData = [];
    $("#editWires li").each( function() {
        index = $(this).attr("lindex");
        name = $(this).find('input[type="text"]').val();
        id = $(this).attr('wireid');
        isdef = 0;
        if ( $(this).find('input[type="checkbox"]').prop('checked') )
            isdef = 1;
        wireData.push({WireID:id,WireName:name,IsDefault:isdef});
    });
    
    $("#wireData").val(JSON.stringify(wireData));
}

function getWireOrder()
{
    wireData = [];
    $("#editWires li").each( function() {
        index = $(this).attr("lindex");
        name = $(this).find('input[type="text"]').val();
        id = $(this).attr('wireid');
        isdef = 0;
        if ( $(this).find('input[type="checkbox"]').prop('checked') )
            isdef = 1;
        wireData.push({WireID:id,WireName:name,IsDefault:isdef});
    });
    
    $("#wireData").val(JSON.stringify(wireData));
}

function checkNames()
{
    status = true;
    index1 = 0;
    $('input[type="text"]').each( function() {
        if ( $(this).val() == "" ) {
            error = 'Names cannot be blank.';
            this.setCustomValidity(error);
            this.reportValidity();
            status = false;
        }
        current = $(this).val();
        index2 = 0;
        $('input[type="text"]').each( function() {
            if ( index1 !== index2 && current === $(this).val() ) {
                error = "Names must be unique.";
                this.setCustomValidity(error);
                this.reportValidity();
                status = false;
            }
            index2++;
        });
        index1++;
        
    });
    return status;
}

function updateOrder()
{
    wireOrder = [];
    $("li.wire").each( function(k) { wireOrder[k] = $(this).attr("wireid"); });
    $('#wireOrder').val(JSON.stringify(wireOrder));
}