var currentIndex = -1;

// If they press enter, collapse the field but don't submit the form.
var catchEnter = function(e)
{
   if(e.which == 13)
   {
      e.preventDefault();
      updateFieldHeadings($(this).parents('div.faqEditFields').siblings('i.toggleFields'));
   }      
};

$(document).ready( function() {
	$('ol.sortable').nestedSortable( {
		disableNesting: 'no-nest',
		forcePlaceholderSize: true,
		handle: 'i',
		helper: 'clone',
		items: 'li',
      minLevel:1,
      maxLevels:1,
		opacity: .6,
		placeholder: 'placeholder',
		revert: 250,
		tabSize: 25,
		tolerance: 'pointer',
		toleranceElement: '> div',
      protectRoot: true,
      start: function (e, ui) {
        $(ui.item).find('textarea').each(function () {
           tinymce.execCommand('mceRemoveEditor', false, $(this).attr('id'));
        });
      },
      stop: function (e, ui) {
        $(ui.item).find('textarea').each(function () {
           tinymce.execCommand('mceAddEditor', true, $(this).attr('id'));
        });
      }
	});

   faqFields = $('div.faqEditFields');
   faqFields.each( function()
   {
      $(this).before("<span class='questionHeading'>"+$(this).find('.faqQuestion').val()+"</span>");
   });
   
   $('div.faqEditFields').hide();
   
   $('i.toggleFields').click( function()
   {
      updateFieldHeadings($(this));
   });
   
   $('span.questionHeading').click( function()
   {
      updateFieldHeadings($(this).siblings('i.toggleFields'));
   });
   
   $('div.faqEditFields input.faqQuestion').keypress(catchEnter);
   
	$('#submit').click( function(e) {
		arraied = $('ol.sortable').nestedSortable('serialize');
		$('#serialized').val(arraied);
		$(this).parents("form").first().submit();
	})
});

function updateFieldHeadings(el)
{
   thisFields = el.siblings('div.faqEditFields');
   thisSpan   = el.siblings('span.questionHeading');

   thisFields.toggle();
   thisSpan.toggle();

   el.toggleClass('fa-pencil-square-o');
   el.toggleClass('fa-pencil-square');
   
   if(el.hasClass('fa-pencil-square-o'))
   {
      el.attr('title', 'Expand this question for editing');

      // Update heading
      val = thisFields.children('.faqQuestion').val();
      thisSpan.text(val);
   }
   else
      el.attr('title', 'Collapse this question');      
}

function deleteItem(item)
{
	$('#faq_'+item).remove();
	$('ol.sortable').sortable('refresh');
}