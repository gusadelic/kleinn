var main = "http://" + location.host;

function authApp(app, button) {
    //button.html("____");
    button.prop('disabled', 'true');
    button.addClass('facebook-small');
    var a = JSON.parse(app);
    $.post(main + "/app/apps/auth/define", {'AppID': a.AppID}, function() {
        window.location.assign(a.URL);
    });
    
}


function deleteApp(id, button) {
    //button.html("____");
    button.prop('disabled', 'true');
    button.addClass('facebook-small');
    var url = main + "/app/apps/delete";
    var data = { 'id': id };
    $.post(url, data, function() {
        window.location.reload(true);
    });
}

function refreshToken(id, button) {

    //button.html("____");
    button.prop('disabled', 'true');
    button.addClass('facebook-small');
    var url = main + "/app/apps/refresh";
    var data = { 'id': id };
    $.post(url, data, function() {
        window.location.reload(true);
    });
    
}
