var currentIndex = 0;
var wires = [];
var componentID = 0;

$(document).ready( function() {
    
    $('form button[type="submit"]').click( function() {
        $.when( updateWireData() );
    });
    
});

function addWire()
{

    newWire = '<tr lindex="'+currentIndex+'" class="" id="wire_'+currentIndex+'">'+
              '<td>'+
              wireSelect+
              '</td>'+
              '<td>'+
              '<input type="text" class="wireColor form-control"></input>'+
              '</td>'+
              '<td>'+
              '<input type="text" class="wireHarness form-control"></input>'+
              '</td>'+
              '<td>' +
              '<input type="text" class="wireNotes form-control"></input>' +
              '</td>' +
              '<td>' +
              '<span class="deleteMe fa fa-ban glyphicon glyphicon-remove-circle" onclick="javascript:deleteWire('+currentIndex+'); return false;"></span>'+
              '</td>'+
              '</tr>'; 
      
    $('table#componentWires tbody').append(newWire);
    
    currentIndex++;
};

function deleteWire(item)
{
    $('#wire_'+item).remove();
}

function updateWireData()
{
    wires = [];
    $('table#componentWires tbody tr').each( function() {
        data =   
            {   
                ComponentID:componentID,
                WireID:$(this).find('.wireName option:selected').val(),
                Color:$(this).find('input.wireColor').val(),
                Harness:$(this).find('input.wireHarness').val(),
                Notes:$(this).find('input.wireNotes').val()
            };

        wires.push(data);
    });

    $('input#wireData').val( JSON.stringify(wires));
}


