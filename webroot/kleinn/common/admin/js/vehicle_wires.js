var currentIndex = 0;
var wires = [];
var vehicleID = 0;

$(document).ready( function() {
    
    $('form button[type="submit"]').click( function() {
        $.when( updateWireData() );
    });
    
});

function addWire()
{

    newWire = '<tr lindex="'+currentIndex+'" class="" id="wire_'+currentIndex+'">'+
              '<td>'+
              wireSelect+
              '</td>'+
              '<td>'+
              '<input type="text" class="wireColor form-control"></input>'+
              '</td>'+
              '<td>'+
              '<input type="text" class="wirePolarity form-control"></input>'+
              '</td>'+
              '<td>' +
              '<input type="text" class="wireLocation form-control"></input>' +
              '</td>' +
              '<td>' +
              '<span class="deleteMe fa fa-ban glyphicon glyphicon-remove-circle" onclick="javascript:deleteWire('+currentIndex+'); return false;"></span>'+
              '</td>'+
              '</tr>'; 
      
    $('table#vehicleWires tbody').append(newWire);
    
    currentIndex++;
};

function deleteWire(item)
{
    $('#wire_'+item).remove();
}

function updateWireData()
{
    wires = [];
    $('table#vehicleWires tbody tr').each( function() {
        data =   
            {   
                VehicleID:vehicleID,
                WireID:$(this).find('.wireName option:selected').val(),
                VehicleWireColor:$(this).find('input.wireColor').val(),
                VehicleWirePolarity:$(this).find('input.wirePolarity').val(),
                VehicleWireLocation:$(this).find('input.wireLocation').val()
            };

        wires.push(data);   
    });

    $('input#wireData').val( JSON.stringify(wires));
}


