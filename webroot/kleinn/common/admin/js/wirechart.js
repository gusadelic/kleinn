$(document).ready( function() {
    
    $("select#sku").change( function() {
        sku = $(this).find('option:selected').val();
        vehicles = makeVehicleOptions(tipsheets[sku]["Vehicles"]);
        $("select#vid").html(vehicles);
    });

    sku = $("select#sku").find('option:selected').val();
    vehicles = makeVehicleOptions(tipsheets[sku]["Vehicles"]);
    $("select#vid").html(vehicles);
});


function makeVehicleOptions(obj)
{
    var output = [];
    for (var key in obj) {
        output.push('<option value="'+obj[key]['VehicleID']+'">'+obj[key]['Year']+" "+obj[key]["Make"]+" "+obj[key]["Model"]+'</option>');
    }
    output.push('<option value=""></option>');
    return output.reverse().join("\n");
}