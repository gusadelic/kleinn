var currentIndex = 0;
var sectionOrder = [];

$(document).ready( function() {

    // Setup the loading indicator
    $("body").prepend('<div id="fade"></div>');
    $("body").prepend('<div id="loading"><img id="loader" src="/common/imgs/loading.gif" /></div>');

    $('#submit').click( function(e) {
            $.when( $("#fade").show(), $("#loading").show() ).then( $(this).submit() );
    });
    
    readyCheck();
});

function addVehicle()
{

    newNode = '<li lindex="'+currentIndex+'" class="" id="vehicle_'+currentIndex+'">'+
              '<div>'+
              '<span class="deleteMe fa fa-ban glyphicon glyphicon-remove-circle" onclick="javascript:deleteVehicle('+currentIndex+'); return false;"></span>'+
              newvehicle +
              '</div>'+
              '</li>'; 
      
    $('ol#editNav').append(newNode);
    
    setupSelectChangeEvents();
    currentIndex++;
};

function deleteVehicle(item)
{
    $('#vehicle_'+item).remove();
    readyCheck();
}

function deleteAllVehicles()
{
    $('ol#editNav li').remove();
    readyCheck();
}

function setupSelectChangeEvents()
{
    $('select').change(function(e) {
        if ( $(this).hasClass("makeSelect") ) {
            make = $(this).val();
            modelSelect = $(this).parent().find("select.modelSelect");
            options = '<option value=""></option><option value="*">All Models and Years</option>'+makeOptions(vehicles[make]);
            $.when(  modelSelect.removeClass("hidden") ).done( modelSelect.html(options) );

        }
        else if ( $(this).hasClass("modelSelect") ) {
            model = $(this).val();
            if ( model === "*") {
                readyCheck();
            }
            else {
                make = $(this).parent().find("select.makeSelect").val();
                yearSelect = $(this).parent().find("select.yearSelect");
                options = '<option value=""></option><option value="*">All Years</option>'+makeYearOptions(vehicles[make][model]['Year']);
                $.when(  yearSelect.removeClass("hidden") ).done( yearSelect.html(options) ); 
            }
        }
        else {
            readyCheck();
        }
    });
        
    $('select').mousedown(function() {
        $(this).val("");
        
        if ( $(this).parent().parent().hasClass("ready") ) {
            if ( $(this).hasClass("makeSelect") ) {
                 yearSelect = $(this).parent().find("select.yearSelect");
                 modelSelect = $(this).parent().find("select.modelSelect");
                 yearSelect.addClass("hidden");
                 yearSelect.html("");
                 modelSelect.addClass("hidden");
                 modelSelect.html("");
            }
            else if ( $(this).hasClass("modelSelect") ) {
                yearSelect = $(this).parent().find("select.yearSelect");
                yearSelect.html("");
                yearSelect.addClass("hidden");
            }
        }
        readyCheck();
    });
        
}

function readyCheck()
{
    vehicleData = [];
    $("#editNav li").each( function() {
        index = $(this).attr("lindex");
        selected_count = 0;
        yearHasStar = false;
        modelHasStar = false;
        selected = $(this).find('select option:selected');
        selected.each( function() { 
            if ($(this).val() !== "" ) 
                selected_count++;
            if ( $(this).val() === "*" ) {
                modelHasStar = $(this).parent().hasClass("modelSelect");
                yearHasStar = $(this).parent().hasClass("yearSelect");
            }

        });
        if ( selected_count === 3 
             || (modelHasStar  && selected_count === 2 ) 
             || yearHasStar 
           ) {
            year = $(this).find('select.yearSelect option:selected').val();
            make = $(this).find('select.makeSelect option:selected').val();
            model = $(this).find('select.modelSelect option:selected').val();
            vehicleData.push({Year:year,Make:make,Model:model});

            $(this).addClass('ready');
        }
        else {
            $(this).removeClass('ready');
        }
    });
    
    $("#vehicle_data").val(JSON.stringify(vehicleData));
}

function setVehicle(index, year, make, model) {
    $('#vehicle_'+index).find('select.yearSelect').val(year);
    $('#vehicle_'+index).find('select.makeSelect').val(make);
    $('#vehicle_'+index).find('select.modelSelect').val(model);  
}

function makeOptions(obj)
{
    var output = [];
    for (var key in obj) {
        if ( Number.isInteger(key) )
            output.push('<option value="'+obj[key]+'">'+obj[key]+'</option>');
        else
            output.push('<option value="'+key+'">'+key+'</option>');
    }
    output.push('<option value=""></option>');
    return output.reverse().join("\n");
}

function makeYearOptions(obj)
{
    var output = [];
    for (var key in obj) {
        output.push('<option value="'+obj[key]+'">'+obj[key]+'</option>');
    }
    output.push('<option value=""></option>');
    return output.reverse().join("\n");
}