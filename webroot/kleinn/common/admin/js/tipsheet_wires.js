var currentIndex = 0;
var wireSelect = "";

$(document).ready( function() { 
    $.when( makeWireSelect() ).then ( updateSelects() );
    updateOrder();
    
    if ( $('#orderWires.js-sortable').length > 0 ) {
        sortable('#orderWires.js-sortable', {
            forcePlaceholderSize: true,
            placeholderClass: 'placeholder'
        });

        sortable('#orderWires.js-sortable')[0].addEventListener('sortupdate', function(e) {
            updateOrder();
        });
    }

    // Setup the loading indicator
    $("body").prepend('<div id="fade"></div>');
    $("body").prepend('<div id="loading"><img id="loader" src="/common/imgs/loading.gif" /></div>');


    $('#submit').click( function(e) {
        $.when( updateOrder(), $("#fade").show(), $("#loading").show() ).then( $(this).submit() );  
    });

    setupSelectChangeEvents();
});

function setupSelectChangeEvents()
{
    $('select.wireSelect').change(function() {
        $.when( makeWireSelect() ).then ( updateSelects() );
        updateOrder();
    });
    
    $('select.wireSelect').mousedown( function() {
        $(this).val("");
    });
}

function addWire()
{

    newNode = '<li lindex="'+currentIndex+'" class="wire" id="wire_'+currentIndex+'">'+
              '<div class="row">'+
              '<i class="fa fa-sort-amount-asc glyphicon glyphicon-sort"></i>'+
              '<span class="deleteMe fa fa-ban glyphicon glyphicon-remove-circle" onclick="javascript:deleteWire('+currentIndex+'); return false;"></span>'+
                wireSelect +
              '</div>'+
              '</li>'; 
      
    $('ol#orderWires').append(newNode);
    
    sortable('#orderWires.js-sortable');
    
    var targetOffset = $('ol#orderWires li').last().offset().top;
    $('html, body').animate({scrollTop: targetOffset}, 1000);
    
    setupSelectChangeEvents();
    
    updateOrder();
    
    currentIndex++;
   
};

function deleteWire(item)
{
    $('#wire_'+item).remove();
    $.when( makeWireSelect() ).then ( updateSelects() );
}

function deleteAllWires()
{
    $('ol#orderWires li').remove();
    $.when( makeWireSelect() ).then ( updateSelects() );
}

function updateOrder()
{
    wireOrder = [];
    $("li.wire").each( function(k) { wireOrder[k] = $(this).find('select.wireSelect').val(); });
    $('#wireOrder').val(JSON.stringify(wireOrder));
}

function makeWireSelect()
{   
    wireSelect = '<select class="form-control wireSelect">';
    wireSelect += '<option></option>';
    for ( var i = 0; i < wires.length ; i++ ) {
        if ( notSelected(wires[i].WireID) === true )
            wireSelect += '<option value="'+wires[i].WireID+'" >'+wires[i].WireName+'</option>';
    }
    wireSelect += '</select>';

}

function notSelected(id)
{
    matched = true;
    $('select.wireSelect option:checked').each( function() {
        if ( $(this).val() === id ) {
            matched = false;
            return false;
        }
    });
    return matched;
}

function updateSelects()
{
    $('select.wireSelect').each( function() {
        current = $(this).find('option:checked').prop('outerHTML');
        currentVal = $(this).val();
        $(this).html(wireSelect);
        $(this).append(current);
        $(this).val(currentVal);
        
    });
}
