var currentIndex = -1;

// If they press enter, collapse the field but don't submit the form.
var catchEnter = function(e)
{
   if(e.which == 13)
   {
      e.preventDefault();
      updateFieldHeadings($(this).parents('div.faqEditFields').siblings('i.toggleFields'));
   }      
};

$(document).ready( function() {
	$('ol.sortable>li>ol').nestedSortable( {
		disableNesting: 'no-nest',
		forcePlaceholderSize: true,
		handle: 'i',
		helper: 'clone',
		items: 'li',
      minLevel:1,
      maxLevels:2,
		opacity: .6,
		placeholder: 'placeholder',
		revert: 250,
		tabSize: 25,
		tolerance: 'pointer',
		toleranceElement: '> div',
      protectRoot: true,
      start: function (e, ui) {
        $(ui.item).find('textarea').each(function () {
           tinymce.execCommand('mceRemoveEditor', false, $(this).attr('id'));
        });
      },
      stop: function (e, ui) {
        $(ui.item).find('textarea').each(function () {
           tinymce.execCommand('mceAddEditor', true, $(this).attr('id'));
        });
      }
	});

   faqFields = $('div.faqEditFields');
   faqFields.each( function()
   {
      $(this).before("<span class='questionHeading'>"+$(this).find('.faqQuestion').val()+"</span>");
   });
   
   $('div.faqEditFields').hide();
   
   $('i.toggleFields').click( function()
   {
      updateFieldHeadings($(this));
   });
   
   $('span.questionHeading').click( function()
   {
      updateFieldHeadings($(this).siblings('i.toggleFields'));
   });
   
   $('div.faqEditFields input.faqQuestion').keypress(catchEnter);
   
	$('#submit').click( function(e) {
		arraied = $('ol.sortable>li>ol').nestedSortable('serialize');
		$('#serialized').val(arraied);
		$(this).parents("form").first().submit();
	})
});

function updateFieldHeadings(el)
{
   thisFields = el.siblings('div.faqEditFields');
   thisSpan   = el.siblings('span.questionHeading');

   thisFields.toggle();
   thisSpan.toggle();

   el.toggleClass('fa-pencil-square-o');
   el.toggleClass('fa-pencil-square');
   
   if(el.hasClass('fa-pencil-square-o'))
   {
      el.attr('title', 'Expand this question for editing');

      // Update heading
      val = thisFields.children('.faqQuestion').val();
      thisSpan.text(val);
   }
   else
      el.attr('title', 'Collapse this question');      
}

function addFAQItem()
{
	currentIndex = $('ol.sortable>li>ol').find('li').length;
   currentIndex++;
   
   newNode = '<li id="faq_'+currentIndex+'">'+
             '<div class="topDiv">'+
             '<i class="fa fa-sort-amount-asc"></i>'+
             '<i class="fa fa-pencil-square-o toggleFields active" title="Expand this question for editing"></i>'+
             '<span class="deleteMe fa fa-ban" onclick="javascript:deleteItem('+currentIndex+'); return false;"></span>'+
             '<span class="questionHeading">New Question</span>'+
             '<div class="faqEditFields" style="display:none">'+
             '<input name="faq_'+currentIndex+'_id" value="" type="hidden" /> '+
             '<input name="faq_'+currentIndex+'_text" class="faqQuestion" placeholder="New question" type="text" /> '+
             '<textarea class="wysiwyg" name="faq_'+currentIndex+'_answer" id="faq_'+currentIndex+'_answer">New Answer</textarea>'+
             '</div>'+
             '</div>'+
             '</li>';

	$('ol.sortable>li>ol>li>ol').first().prepend(newNode);
   $('#faq_'+currentIndex+' i.toggleFields').click( function()
   {
      updateFieldHeadings($(this));
   });
   
   $('#faq_'+currentIndex+' span.questionHeading').click( function()
   {
      updateFieldHeadings($(this).siblings('i.toggleFields'));
   });

   $('#faq_'+currentIndex+' input.faqQuestion').keypress(catchEnter);

   // Highlight the new node
   $('#faq_'+currentIndex+ ' div.topDiv').animate({ backgroundColor: "rgb(190,230,190)" }, 'medium').animate({ backgroundColor: "rgb(238,238,238)" }, 'slow');

	$('#emptyFAQ').hide();

   if((typeof(tinymce) != 'undefined'))
   {
      tinymce.init({
         selector: 'textarea#faq_'+currentIndex+'_answer',
         plugins: ['image link lists responsivefilemanager injectPerson injectData'],
         content_css: '/common/admin/css/wysiwyg.css',
         target_list:!1,
         link_class_list:[{title: '', value: ''},{title: 'New Window', value: 'newWindow'}],
         browser_spellcheck:true,
         relative_urls:false,
         remove_script_host:true,
         paste_auto_cleanup_on_paste:true,
         paste_remove_styles:true,
         paste_remove_styles_if_webkit:true,
         paste_strip_class_attributes:true,
         paste_word_valid_elements:'b,strong,i,em,a',
         external_filemanager_path:'/filemanager/',
         filemanager_title:'File Manager',
         external_plugins:{ 'filemanager' : '/filemanager/plugin.min.js'},
         menubar: '',
         link_list: linksList,
         toolbar1: 'bold italic underline | bullist numlist',
         toolbar2: 'image link | injectData injectPerson'
      });
   }
}

function deleteItem(item)
{
	$('#faq_'+item).remove();
	$('ol.sortable').sortable('refresh');
}