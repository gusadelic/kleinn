var currentIndex = -1;
var sectionOrder = [];

$(document).ready( function() {
    
    sortable('.js-sortable', {
        forcePlaceholderSize: true,
        placeholderClass: 'placeholder'
    });
    
    sortable('.js-sortable')[0].addEventListener('sortupdate', function(e) {
        updateOrder();
    });
    
    setupSelectChangeEvents();
    
    // Setup the loading indicator
    $("body").prepend('<div id="fade"></div>');
    $("body").prepend('<div id="loading"><img id="loader" src="/common/imgs/loading.gif" /></div>');
    
    $('#previewButton').click( function(e) {
        $.when(  updateOrder() ).then( 
            function() {
                e.stopPropagation();
                var href = '/tipsheets/preview?SKU='+$('input#SKU').val()+"&Sections="+btoa(unescape(encodeURIComponent($('input#groupOrder').val())));
                var link = $('<a href="' + href + '" />');
                link.attr('target', '_blank');
                //console.log(href);
                window.open(link.attr('href'));   
            }
        );
        
    });

    $('#submit').click( function(e) {
            updateOrder();
            $.when( $("#fade").show(), $("#loading").show() ).then( $(this).submit() );
            //$('#sectionForm').submit();
    });
    
});

function addGroup()
{
    currentIndex++;
    newNode = '<li lindex="'+currentIndex+'" class="group" id="group_'+currentIndex+'">'+
              '<div>'+
              '<i class="fa fa-sort-amount-asc glyphicon glyphicon-sort"></i>'+
              '<span class="deleteMe fa fa-ban glyphicon glyphicon-remove-circle" onclick="javascript:deleteGroup('+currentIndex+'); return false;"></span>'+
              "<span onclick='javascript:addSection("+currentIndex+");return false' class='js-add-item-button addButton btn btn-success btn-sm'><i class='glyphicon glyphicon-plus'></i> Add New Section</span><br />"+
              '<div class="row">'+
              '<div class="col-lg-4">'+
              "<input type='text' class='form-control group_name' name='group_name_"+currentIndex+"' id='group_name_"+currentIndex+"' value='' placeholder='Name' >"+
              '</div>'+
              '<div class="col-lg-8">'+
              "<input type='text' class='form-control group_description' name='group_description_"+currentIndex+"' id='group_description_"+currentIndex+"' value='' placeholder='Description' >"+
              '</div></div>'+
              '<div class="sublist"><ol class="js-sortable editSection">'+     
              '</div>'+
              '</li>';    
    
    $('ol#editGroup').append(newNode);
    sortable('.js-sortable');
    
    updateOrder();
      
}

function addSection(targetIndex)
{
    currentIndex++;
    newNode = '<li lindex="'+currentIndex+'" class="section" id="section_'+currentIndex+'">'+
              '<div class="row">'+
              '<div class="col-lg-1">'+
              '<i class="fa fa-sort-amount-asc glyphicon glyphicon-sort"></i>'+
              '</div>'+
              '<div class="col-lg-8">'+
              ' Category: <span class="categorySelectWrapper">'+categories+'</span>'+
              ' Section: <span class="sectionSelectWrapper">'+sections[0]+'</span>   '+
              '</div>'+
              '<div class="col-lg-2">'+
              '<a href="#" data-toggle="modal" id="preview_button_'+currentIndex+'" data-target="#section_modal_'+currentIndex+'">'+
              '<span class="fa fa-search glyphicon glyphicon-search"></span> Preview Section</a>'+
              '&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" id="edit_button_'+currentIndex+'" onClick="editSection($(this))">'+
              '<span class="fa fa-pencil glyphicon glyphicon-pencil"></span> Edit Section</a>'+
              '<br /><span class="showTitleWrapper"><input type="checkbox" style="width:9px;" class="showTitle"> Show Title </span>'+
              '</div>'+
              '<div class="col-lg-1">'+
              '<span class="deleteMe fa fa-ban glyphicon glyphicon-remove-circle" onclick="javascript:deleteSection('+currentIndex+'); return false;"></span>'+
              '</div></div>'+
              '</li>'; 

    $('li#group_'+targetIndex+' ol.editSection').append(newNode);
    sortable('.js-sortable');
    
    newModal = '<div class="modal fade" style="display:none;" id="section_modal_'+currentIndex+'" tabindex="-1" role="dialog" aria-labelledby="section_modal_'+currentIndex+'" aria-hidden="true">'+
              '<div class="modal-dialog">'+
              '<div class="modal-content">'+
              '<div class="modal-header">'+
              '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><br />'+
              '</div>'+
              '<div class="modal-body">'+
              '<div class="wrap"><iframe src="#" id="content" class="iframe frame" name="content" seamless="" height="100%" width="100%"></iframe></div>'+
              '</div></div></div></div>';
    
    $("span#modals").append(newModal);
    
    setupSelectChangeEvents();
    
    reloadModals();
    
    updateOrder();
};

function setupSelectChangeEvents()
{
    $('select.categorySelect').change(function() {
        $.when(
            $(this).parent().parent().find("span.sectionSelectWrapper").html(sections[$(this).val()])
        ).done( reloadModals(), updateOrder() );
    });
    
    $('select.sectionSelect').change(function() {
        $.when( reloadModals(), updateOrder() ).done(  );
    });
    
    $('select.sectionSelect').mousedown( function() {
        $(this).val("");
    });
}

function deleteSection(item)
{
	$('#section_'+item).remove();
	$('ol.sortable').sortable('refresh');
        updateOrder();
}

function deleteGroup(item)
{
	$('#group_'+item).remove();
	$('ol.sortable').sortable('refresh');
        updateOrder();
}

function updateOrder()
{
    groupOrder = [];
    $('li.group').each( function(c) {
        sectionOrder = [];
        
        $(this).find('select.sectionSelect').each( function(k) {
            id = $(this).val();
            show = $(this).parent().parent().parent().find('input.showTitle').prop('checked');
            sectionOrder[k] = [id, show ? 1 : 0] ; 
        });

        if ( $(this).attr('groupid') === undefined )
            groupOrder[c] = {"Name":$(this).find('input.group_name').val(), "Description":$(this).find('input.group_description').val(), "Order":c, "Sections": sectionOrder };
        else
            groupOrder[c] = {"SectionGroupID":$(this).attr('groupid'), "Name":$(this).find('input.group_name').val(), "Description":$(this).find('input.group_description').val(), "Order":c,  "Sections": sectionOrder };
    });
    
    $('#groupOrder').val(JSON.stringify(groupOrder));
}

function reloadModals()
{
    $('select.sectionSelect').each(function()  { 
        var id = $(this).val(); 
        var target = $(this).parent().parent().parent().parent().attr("lindex");
        $("#section_modal_"+target).find("iframe#content").attr("src", "/async/content/"+id);
    });
}

function editSection(target)
{
    select = target.parent().parent().find('select.sectionSelect');
    window.open("/admin/sections/edit/"+select.val());
    
}