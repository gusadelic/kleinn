$(document).ready( function() {
    
    $('#SKU_field').parent().append('<span class="help-block hidden" id="SKU_error"></span>');
  
    $('#SKU_field').blur( function() {
        checkSKU($('#SKU_field').val());
    });
    


});

function checkSKU(sku) {
    if ( sku !== original_sku ) {
        $.ajax({
            type: "GET",
            url: "/async/tipsheets/uniquesku/"+sku
        }).done( function(result) { 
            if ( result == "false" ) {
                if ( !$('button[type=submit]').prop("disabled") ) {
                    $('#SKU_error').removeClass('hidden');
                    $('button[type=submit]').prop('disabled', true);
                    $('#SKU_field').parent().addClass("has-error");
                    $('#SKU_error').html('SKU already exists!');
                }
            }
            else {
                if ( $('button[type=submit]').prop("disabled") ) {
                    $('#SKU_field').parent().removeClass("has-error");
                    $('#SKU_error').html("This SKU is unique. Thank you.");
                    $('#SKU_field').parent().addClass("has-success");
                    $('button[type=submit]').prop('disabled', false);
                }
            }
        });
    }
    else {
        $('#SKU_error').addClass('hidden');
        $('#SKU_field').parent().removeClass("has-error");
        $('button[type=submit]').prop('disabled', false);
    }
}