<?php

/* Main Index Page 
 * This file sets up all the constants, functions, and variables necessary to run the application.
 */

$pathToCore = "/var/www/public/";

/* Load config file first */
require_once($pathToCore."config.inc");

if(defined("_MB_MAX_EXECUTION_TIME_"))
{
   set_time_limit(_MB_MAX_EXECUTION_TIME_);
}

//header('Access-Control-Allow-Origin: http://localhost:8180');

/* Load third-party classes */
require_once($pathToCore."vendor/autoload.php");

/* Disable PHP's session cache limiter */
session_cache_limiter(false);
session_start();

// Create the Slim instance
$app = new \Slim\Slim();

// Allow users to specify the content type
// for the site (useful for APIs, etc.)
//if(defined("_MB_SITE_CONTENT_TYPE_"))
//    $app->contentType(_MB_SITE_CONTENT_TYPE_);

// Pull in useful utilities
require_once($pathToCore."utils/functions.inc");

// Pull in constants from the DB
require_once($pathToCore."utils/constants.inc");

// Set debug to true/false
if(_MB_DEBUG) $app->config('debug', true);
else          $app->config('debug', false);

// Allow users to specify a generic username/password (think .htaccess)
// required for looking at any page on the site.
//if(defined("_MB_SITE_REQUIRED_USER_") && defined("_MB_SITE_REQUIRED_PASS_") && $app->request()->getResourceUri() != "/403")
//   $app->add(new \Slim\Extras\Middleware\HttpBasicAuth(_MB_SITE_REQUIRED_USER_, _MB_SITE_REQUIRED_PASS_));

// Allow users to create an HTTP Digest authentication model
if(defined("_MB_AUTH_TYPE_") && _MB_AUTH_TYPE_ == "digest")
{
   $currentURL = $app->request()->getResourceUri();
   if($currentURL != "/auth" && $currentURL != "/403")
   {
      if(!_MB_DEBUG)
      {
         $digestKey = $app->request()->get('apikey');
         if(!$digestKey)
         {
            $app->redirect("/403", 403);
            $user = false;
         }
         else
            $user = Mumby\WebTools\ACL\User::create("digest", "", $digestKey);
      }
      else if(defined("_MB_DEBUG_USER_") && defined("_MB_DEBUG_PASS_"))
      {
         $u = _MB_DEBUG_USER_;
         $p = md5(_MB_DEBUG_USER_._MB_DIGEST_HASH_DELIMITER_._MB_DEBUG_PASS_);
         $user = Mumby\WebTools\ACL\User::create("debug", _MB_DEBUG_USER_, $p);
      }
      else
         $user = Mumby\WebTools\ACL\User::create("anon");
   }
   else
      $user = Mumby\WebTools\ACL\User::create("anon"); // Required to prevent errors on 403/auth pages
}
else
   $user = Mumby\WebTools\ACL\User::create("anon");


// Pull in basic system functionality first
require_once($pathToCore."routes/core/system.inc");

// Pull in custom routes first (in case the user wants to overwrite something)
$customRoutesPath = $pathToCore."routes/custom/";
$customRoutes = fetchRoutes($customRoutesPath);
foreach($customRoutes as $custom)
   require_once($custom);

// Pull in standard routes afterwards
$exclude = array(
    $pathToCore."routes/core/system.inc",
    $pathToCore."routes/core/catchall.inc",
   );

$coreRoutes = fetchRoutes($pathToCore."routes/core/", "", $exclude);

foreach($coreRoutes as $core)
   require_once($core);


// Last, but not least, add the catchall route
require_once($pathToCore."routes/core/catchall.inc");

$app->run();
