$(document).ready(function() {

   $('#tutoringCalendar').fullCalendar({
      header: {
       left: '',
       center: '',
       right: ''},
      defaultDate: defaultCalDate, 
      events: existingEvents,
      editable: false,
      droppable: false,
      defaultView: 'agendaWeek',
      allDaySlot: false,
      slotDuration: '00:'+scheduleIncrements+':00',
      minTime: calStartTime,
      maxTime: calEndTime,
      defaultTimedEventDuration: '01:00:00',
      columnFormat: 'dddd',
      eventDurationEditable: false, 
      weekends: false,
      timeFormat: 'dddd,H',
      eventRender: function(event, element) {
         element.find('.fc-title').html(event.title);
      },
      windowResize: function(view) { setPrintable(); }
   });
   
   setPrintable();
});

function setPrintable()
{
   var headingsAdded = new Array();
   
   $('.fc-event').each( function() {
      time = $(this).find(".fc-event-time").text();
      thisDay = time.split(",");
      $(this).addClass('dayHeader');

      thisHeading = eval(headingsAdded[thisDay[0]]);
      console.debug(thisHeading);
      if(thisHeading == undefined)
      {
         $(this).addClass('day'+thisDay[0]);
         headingsAdded[thisDay[0]] = true;
      }
      
      startHour = parseInt(thisDay[1]);
      endHour = startHour + 1;
      
      startMeridian = "";
        endMeridian = "";
        
      if(startHour >= 12)
      {
         if(startHour > 12)
            startHour = startHour-12;

         startMeridian = "p";
      }
      else
         startMeridian = "a";
      
      if(endHour >= 12)
      {
         if(endHour > 12)
            endHour = endHour-12;

         endMeridian = "p";
      }
      else
         endMeridian = "a";
      
      if(startMeridian == endMeridian)
         startMeridian = "";

      $(this).find(".fc-event-time").text(startHour+":00"+startMeridian+"-"+endHour+":00"+endMeridian);
    });   
}