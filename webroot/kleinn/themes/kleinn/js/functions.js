var host = "http://nexus.gusadelic.com/isleofgames/app/";


function openSetModal(code) {
    $('#set_modal').find('.modal-header').html("");
    $('#set_modal').find('#modal_content').html('<div class="ellipsis"></div>');
    
   drawSetList(code, $('#set_modal') );
   $("#set_modal").modal();
   $("#set_modal").find('.modal-body').html('<table id="resulttable" class="table table-striped table-hover display nowrap compact"></table>');
        
}

function openGroupModal(id, form) {
    
    
     var parseData = function(callback) { 
         $('#group_modal').find('#modal_content').html(form);
         return $.post( host + "admin.php", { 'function': "group", cmd: "get", id: id, javascript: "yes" }, callback); 
     };
    
        
    parseData(function(data) {  
        var group = JSON.parse(data)[0];
        form.find('input#group_cmd').attr('value', 'modify');
        form.find('input#group_name').attr('value', group.name);
        form.find('input#group_code').attr('value', group.code);
        form.find('input#group_percent').attr('value', group.percent);
        form.find('input#group_description').attr('value', group.description);
        form.append('<input type="hidden" name="id" value="' + id + '" />');
        $("#group_modal").modal();    
    });
   
}

function openCondModal(id, form) {
    
    
     var parseData = function(callback) { 
         $('#cond_modal').find('#modal_content').html(form);
         return $.post( host + "admin.php", { 'function': "condition", cmd: "get", id: id, javascript: "yes" }, callback); 
     };
    
        
    parseData(function(data) {  
        var cond = JSON.parse(data)[0];
        form.find('input#cond_cmd').attr('value', 'modify');
        form.find('input#cond_name').attr('value', cond.name);
        form.find('input#cond_code').attr('value', cond.code);
        form.find('input#cond_percent').attr('value', cond.percent);
        form.find('input#cond_description').attr('value', cond.description);
        form.append('<input type="hidden" name="id" value="' + id + '" />');
        $("#cond_modal").modal();    
    });
   
}

function openLangModal(id, form) {
    
    
     var parseData = function(callback) { 
         $('#lang_modal').find('#modal_content').html(form);
         return $.post( host + "admin.php", { 'function': "language", cmd: "get", id: id, javascript: "yes" }, callback); 
     };
    
        
    parseData(function(data) {  
        var lang = JSON.parse(data)[0];
        form.find('input#lang_cmd').attr('value', 'modify');
        form.find('input#lang_name').attr('value', lang.name);
        form.find('input#lang_code').attr('value', lang.code);
        form.find('input#lang_percent').attr('value', lang.percent);
        form.append('<input type="hidden" name="id" value="' + id + '" />');
        $("#lang_modal").modal();    
    });
   
}

function togglePrice(target, id) {
    var price_form = target.closest('tr').find('input.price_input');
    price_form.val("");
    price_form.addClass('facebook-small');
    
    var price_data;

    var lang = target.closest('tr').find('select.lang_select').val();
    var cond = target.closest('tr').find('select.cond_select').val();

    if ( target.closest('tr').find('input.new_foil').is(':checked') ) {
        getPrice( id, 1, function(data) { 
            price_data = JSON.parse(data);           
            price_form.removeClass('facebook-small');
            price_form.val( accounting.formatMoney( parseFloat(price_data.fair_price).toFixed(2) * lang * cond, { format: "%v" }) );
        });
    }
    else {
        getPrice( id, 0, function(data) { 
            price_data = JSON.parse(data);
            price_form.removeClass('facebook-small');
            price_form.val( accounting.formatMoney( parseFloat(price_data.fair_price).toFixed(2) * lang * cond, { format: "%v" }) );
        });
    }
    
}

function pToMarkup(percent) {
    return (percent)/100 + 1; 
}

function pToReduction(percent) {
    return (100 - percent)/100;
}

// Only for local files
function UrlExists(url) {
  var http = new XMLHttpRequest();
  http.open('HEAD', url, false);
  http.send();
  return http.status != 404;
}



function getAllCards() {

$.get('results.php', "javascript=yes&canary=" + encodeURIComponent($('#canary').val()), function(result) {      
  
  		var json = JSON.parse(result);
                        
    	getCardList(json);

	});

}

function getCardResult(id, cardname) {

	$("#result").html("");

	getCard(id);

	$.get('results.php', "card=" + cardname + "&javascript=yes&canary=" + encodeURIComponent($('#canary').val()), function(result) {      
  
  		var json = JSON.parse(result);
                        
    	getCardList(json);

	});
  
}




function getSearchResult(cardname) {

        $.get('results.php', "card=" + cardname + "&javascript=yes&canary=" + encodeURIComponent($('#canary').val()), function(result) {
            	//console.log(result);
        		var json = JSON.parse(result);        		

			//$("#result").html("");

			getCard(json[json.length - 1].id);
                        
    		getCardList(json);
		
        });
}



            

function getCard(id) {
	$.ajax({
		url: 'results.php?id=' + id + '&javascript=yes&canary=' + encodeURIComponent($('#canary').val()),
		type: 'get',
		async: false,
		success:  function(card_result) {
			var json = JSON.parse(card_result);

//			$("#dialog").html("<h1>" + json[0].name + "</h1>");
			$('#dialog').attr('title',json[0].name);
			$("#dialog").dialog({ modal: true, height: 534, width: 744});

			var cardname = unescape(decodeURIComponent(json[0].name));
			cardname = cardname.replace(/\u00c6/g, "AE");
			cardname = cardname.replace("`", "'");
			cardname = cardname.replace(" // ", "");

			$.ajax({
			    url:'pics/' + json[0].set + '/' + cardname + '.jpg',
			    async: false,
			    type:'HEAD',
			    error: function()
			    {
			        $("#dialog").html('<img class="cardimage" src="pics/MagicCardBack.jpg">');
			    },
			    success: function()
			    {
			        if ( json[0].variation == 0 ) {
						$("#dialog").html('<img class="cardimage" src="pics/' + json[0].set + '/' + cardname + '.jpg">');
					}
					else { 
						$("#dialog").html('<img class="cardimage" src="pics/' + json[0].set + '/' + cardname + json[0].variation + '.jpg">');
					}
			    }
			});


	$("#dialog").append('<span class="ruling"><B>RULES CLARIFICATIONS:</B><br />' + json[0].ruling.replace(/\u00a3/g, '<br /><hr id="rules"><br />') );

		}
	});
}

function getCardAlphaList(letter) {
	$.get('browse.php', "function=cards&letter=" + letter + '&canary=' + encodeURIComponent($('#canary').val()), function(result) {
		$('#result').html(result);
	});
}

function makeDefault(id) {
    $.post( host + "admin.php", { 'function': "group", cmd: "default", id: id, javascript: "yes" }, function(data) {
        location.reload();
    });
}


function updateCardStock(card_id) {
    getGroups(function(d) {
        var group_select = buildGroupBox(JSON.parse(d));
        $(".card-" + card_id).remove();
        getStock(card_id, function(data) {
            var stock_object = JSON.parse(data);
            console.log(stock_object);
            console.log(stock_object.length);
            $("#" + card_id).find("td.cardname").attr("rowspan", stock_object.length+1);
              

            $.each(stock_object, function(j) {
                drawCardStock(stock_object[j], group_select);
            });3
        });
    });
}
    


function addStock(card_id, target, callback) {
    var button = $(target);
    button.html("___");
    button.prop('disabled', 'true');
    button.addClass('facebook-small');
    
    var row = target.closest('tr');
    
    var condition = row.find('.cond_select').val();
    var language = row.find('.lang_select').val();
    var group = row.find('.group_select').val();
    var stock = row.find('.stock_input').val();
    var price = row.find('.price_input').val();
    var foil = 0;
    
    if ( row.find('.new_foil').is(':checked') ) {
        foil = 1;
    }
       
    var url = host + "admin.php";
    var data = { 'function': "inventory", 'cmd': "add", 'card_id': card_id, 'condition': condition, 'language': language, 'group': group, 'stock': stock, 'foil': foil, 'price': price, 'javascript': "yes" };
    $.post(url, data, function(response) { 
        if (response && !response.error) {
            callback();
            button.removeClass('facebook-small');
            button.prop('disabled', 'false');
            button.html("Add");
        }
    });
}

function modStock(item_id, target, callback) {
    var button = $(target);
    button.html("_____");
    button.prop('disabled', 'true');
    button.addClass('facebook-small');
    
    var row = target.closest('tr')
    var group = row.find('.group_select').val();
    var stock = row.find('input.stock').val();
    var price = row.find('input.price_input').val();
       
    var url = host + "admin.php";
    var data = { 'function': "inventory", 'cmd': "modify", 'item': item_id, 'group': group, 'qty': stock, 'price': price, 'javascript': "yes" };
    $.post(url, data, function(response) { 
        if (response && !response.error) {
            callback();
            button.removeClass('facebook-small');
            button.prop('disabled', 'false');
            button.html("Modify");
        }
    });
}





function getSets(type, callback) {
    var url = host + "browse.php?function=sets&javascript=yes&type=" + type;
    return $.get(url, {}, callback );
}

function getSetList(code, callback) {
    var url = host + "browse.php?function=setlist&javascript=yes&code=" + code;
    return $.get(url, {}, callback );
}

function getSetName(code, callback) {
    var url = host + 'app.php?function=set&cmd=name&code=' + code + '&javascript=yes';
    return $.get(url, {}, callback ); 
}

function getSetData(code, callback) {
    var url = host + 'app.php?function=set&cmd=data&code=' + code + '&javascript=yes';
    return $.get(url, {}, callback ); 
}

function getPriceList(code, foil, callback) {
    var url = host + 'app.php?function=cards&cmd=pricelist&code=' + code + '&foil=' + foil + '&javascript=yes';
    return $.get(url, {}, callback ); 
}

function getPrice(id, foil, callback) {
    var url = host + 'app.php?function=cards&cmd=price&card_id=' + id + '&foil=' + foil + '&javascript=yes';
    return $.get(url, {}, callback ); 
}

function getStockList(code, callback) {
    var url = host + 'app.php?function=inventory&cmd=set&code=' + code + '&javascript=yes';
    return $.get(url, {}, callback ); 
}

function getConditions(callback) {
    var url = host + 'admin.php';
    return $.post(url, { 'function': "condition", javascript: "yes" }, callback ); 
}

function getLanguages(callback) {
    var url = host + 'admin.php';
    return $.post(url, { 'function': "language", javascript: "yes" }, callback ); 
}

function getGroups(callback) {
    var url = host + 'admin.php';
    return $.post(url, { 'function': "group", javascript: "yes" }, callback ); 
}

function getStock(card_id, callback) {
    var url = host + 'app.php?function=inventory&cmd=get&card_id=' + card_id + '&javascript=yes';
    return $.get(url, {}, callback ); 
}

function getSetListData(set, callback) {
    var url = host + 'app.php?function=table&cmd=set&code=' + set + '&javascript=no';
    return $.get(url, {}, callback);
}



function drawSetList(set, target) {
    
    var stockTable = function (d) {
        return d.inventory;
    };
         
    getSetData(set, function(setdata) {
        setdata = JSON.parse(setdata);
        target.find('.modal-header').html('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h2 class="modal-title">' + setdata.name + '</h2>');
        target.find('.modal-header').append('<h4>Released ' + moment(setdata.releaseDate).fromNow() + ' </h4>');

        /* Create an array with the values of all the input boxes in a column */
        $.fn.dataTable.ext.order['dom-text'] = function  ( settings, col )
        {
            return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
                return $('input', td).val();
            } );
        };

        /* Create an array with the values of all the input boxes in a column, parsed as numbers */
        $.fn.dataTable.ext.order['dom-text-numeric'] = function  ( settings, col )
        {
            return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
                return $('input', td).val() * 1;
            } );
        };

        /* Create an array with the values of all the select options in a column */
        $.fn.dataTable.ext.order['dom-select'] = function  ( settings, col )
        {
            return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
                return $('select', td).val();
            } );
        };

        /* Create an array with the values of all the checkboxes in a column */
        $.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col )
        {
            return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
                return $('input', td).prop('checked') ? '1' : '0';
            } );
        };


        var table = $('#resulttable').DataTable( {
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

                if ( aData.instock.substring(23,24) === "1" ) {
                    $(nRow).find('td.stock').addClass('in_stock');

                }
            },
            "ajax": host + 'app.php?function=table&cmd=set&code=' + set + '&javascript=no',
            "columns": [
                {
                "title": "In Stock",
                "className":      "stock",
                "orderable":      true,
                "data":           "instock",
                "defaultContent": ''
                },
                { "title": "Card Name", "data": "name", "className": "cardname" },
                { "title": "Rarity", "data": "rarity", "className": "rarity" },
                { "title": "Condition", "data": "conditions", "className": "condition" },
                { "title": "Language", "data": "languages", "className": "language" },
                { "title": "Group", "data": "groups", "className": "group" },
                { "title": "Market Price", "data": "price", "className": "price", "orderDataType": "dom-text", "type": "numeric-comma" },
                { "title": "Qty.", "data": "stock", "className": "quantity", "orderable": false },
                { "title": "", "data": "addbutton", "orderable": false }
            ],
            "order": [[6, 'desc']]
        } );



        table.$('tr').each( function(i,d) { 
            console.log(d);
        });


        // Add event listener for opening and closing details
        $('#resulttable tbody').on('click', 'td.in_stock', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
         }
        else {
            // Open this row
                row.child( stockTable(row.data()) ).show();
                tr.addClass('shown');

            }
        } );



    });
   
}


function reloadSetList() {
    var table = $('#resulttable').DataTable({
            retrieve: true
        });
    
    table.ajax.reload( null, false);    
}




function drawSets(type, target) {
    var sets;

    target.html("");
    
    if ( type === 'expansion') {
        var count_promo = 0;
        var promo_group = 0;
        getSets(type, function(data) { 
            sets = JSON.parse(data); 
            var blocks = [];
            for ( var i = 0 ; i < sets.length ; i++  ) {
                var thisBlock = sets[i].block.replace("'", "").replace(/ /g, "_");
                 if (blocks.indexOf(thisBlock) === -1 && thisBlock !== '')
                    blocks.push(thisBlock);
             }

            for ( var i = blocks.length-1 ; i > -1 ; i--  ) {
                target.append('<div id="' + blocks[i] + '" class="btn-group btn-group-justified"></div>');
            }
            for ( var i = 0 ; i < sets.length ; i++  ) {
                var name = sets[i].name;
                var code = sets[i].code;
                var logo = sets[i].logo;
                var icon = '<img src="' + sets[i].icon + '">';

                if ( logo === "" ) logo = '<h5>' + name + '</h5>';
                else logo = '<img src="' + logo + '">';
                    
                if ( sets[i].block === "Promo" ) {
                    
                    if ( count_promo !== 0 ) {
                        if ( count_promo % 3 === 0 ) {
                            promo_group ++;
                            target.append('<div id="Promo_' + promo_group + '" class="btn-group btn-group-justified"></div>');
                        }
                        if ( promo_group === 0 ) {
                            target.find('#Promo').append('<a id="' + sets[i].code + '" href="#" class="btn btn-default btn-lg btn-block" onclick="openSetModal(\'' + sets[i].code + '\');">' + icon + logo + '</a>');          
                        }
                        else {
                            target.find('#Promo_' + promo_group).append('<a id="' + sets[i].code + '" href="#" class="btn btn-default btn-lg btn-block" onclick="openSetModal(\'' + sets[i].code + '\' );">' + icon + logo + '</a>');        
                        }
                    }
                    else {
                        target.find('#Promo').append('<a id="' + sets[i].code + '" href="#" class="btn btn-default btn-lg btn-block" onclick="openSetModal(\'' + sets[i].code + '\');">' + icon + logo + '</a>');        
                    }
                    count_promo++;
                }
                else {
                    target.find('#' + sets[i].block.replace("'", "").replace(/ /g, "_")).append('<a id="' + sets[i].code + '" href="#" class="btn btn-default btn-lg btn-block" onclick="openSetModal(\'' + sets[i].code + '\');">' + icon + logo + '</a>');        
                }
            }
        });
    }
    else if ( type === 'core' ) {
        getSets(type, function(data) { 
            sets = JSON.parse(data); 
            var group = 1;
            target.html('<div id="group_' + group + '" class="btn-group btn-group-justified"></div>');
            for ( var i = sets.length - 1 ; i > -1  ; i--  ) {
                var name = sets[i].name;
                var code = sets[i].code;
                var logo = sets[i].logo;
                var icon = '<img src="' + sets[i].icon + '">';

                if ( logo === "" ) logo = '<h5>' + name + '</h5>';
                else logo = '<img src="' + logo + '">';
                
                if ( i !== 0 && i % 3 === 0) { 
                    group++; 
                    target.append('<div id="group_' + group + '" class="btn-group btn-group-justified"></div>'); 
                }
                
                target.find("#group_" + group).append('<a id="' + code + '" href="#" class="btn btn-default btn-lg btn-block" onclick="openSetModal(\'' + sets[i].code + '\')">' + icon + logo + '</a>');        
            }
        });
    }
    else if ( type === 'duel deck') {
        getSets(type, function(data) { 
            sets = JSON.parse(data); 
            var group = 1;
            target.html('<div id="dual_group_' + group + '" class="btn-group btn-group-justified"></div>');
            for ( var i = sets.length - 1 ; i > -1  ; i--  ) {
                var name = sets[i].name;
                var code = sets[i].code;
                var logo = sets[i].logo;
                var icon = '<img src="' + sets[i].icon + '">';

                if ( logo === "" ) logo = '<h5>' + name + '</h5>';
                else logo = '<img src="' + logo + '">';
                
                if ( i % 3 === 0) { 
                    group++; 
                    target.append('<div id="dual_group_' + group + '" class="btn-group btn-group-justified"></div>'); 
                }
 
                target.find("#dual_group_" + group).append('<a id="' + sets[i].code + '" href="#" class="btn btn-default btn-lg btn-block"  onclick="openSetModal(\'' + sets[i].code + '\')">\n' + icon + logo + '</a>');        
            }
        });        
    }
    else if ( type === 'from the vault') {
        getSets(type, function(data) { 
            sets = JSON.parse(data); 
            var group = 1;
            target.html('<div id="vault_group_' + group + '" class="btn-group btn-group-justified"></div>');
                
            for ( var i = sets.length - 1 ; i > -1  ; i--  ) {
                var name = sets[i].name;
                var code = sets[i].code;
                var logo = sets[i].logo;
                var icon = '<img src="' + sets[i].icon + '">';

                if ( logo === "" ) logo = '<h5>' + name + '</h5>';
                else logo = '<img src="' + logo + '">';                
                
                if ( i % 3 === 0) { 
                    group++; 
                    target.append('<div id="vault_group_' + group + '" class="btn-group btn-group-justified"></div>'); 
                }
 
                target.find("#vault_group_" + group).append('<a id="' + sets[i].code + '" href="#" class="btn btn-default btn-lg btn-block"  onclick="openSetModal(\'' + sets[i].code + '\')">' + icon + logo + '</a>');        
            }
        });        
    }
    else if ( type === 'multiplayer') {
        getSets(type, function(data) { 
            sets = JSON.parse(data); 
            var group = 1;
            target.html('<div id="multi_group_' + group + '" class="btn-group btn-group-justified"></div>');
            for ( var i = sets.length - 1 ; i > -1  ; i--  ) {
                var name = sets[i].name;
                var code = sets[i].code;
                var logo = sets[i].logo;
                var icon = '<img src="' + sets[i].icon + '">';

                if ( logo === "" ) logo = '<h5>' + name + '</h5>';
                else logo = '<img src="' + logo + '">';
                
                if ( i % 3 === 0) { 
                    group++; 
                    target.append('<div id="multi_group_' + group + '" class="btn-group btn-group-justified"></div>'); 
                }
 
                target.find("#multi_group_" + group).append('<a id="' + sets[i].code + '" href="#" class="btn btn-default btn-lg btn-block"  onclick="openSetModal(\'' + sets[i].code + '\')">' + icon + logo + '</a>');        
            }
        });        
    }
    else if ( type === 'special') {
        getSets(type, function(data) { 
            sets = JSON.parse(data); 
            var group = 1;
            target.html('<div id="special_group_' + group + '" class="btn-group btn-group-justified"></div>');
            for ( var i = sets.length - 1 ; i > -1  ; i--  ) {
                var name = sets[i].name;
                var code = sets[i].code;
                var logo = sets[i].logo;
                var icon = '<img src="' + sets[i].icon + '">';

                if ( logo === "" ) logo = '<h5>' + name + '</h5>';
                else logo = '<img src="' + logo + '">';
                
                if ( i % 3 === 0) { 
                    group++; 
                    target.append('<div id="special_group_' + group + '" class="btn-group btn-group-justified"></div>'); 
                }
                
                target.find("#special_group_" + group).append('<a id="' + sets[i].code + '" href="#" class="btn btn-default btn-lg btn-block"  onclick="openSetModal(\'' + sets[i].code + '\')">' + icon + logo + '</a>');        
            }
        });        
    }

    
}




    




