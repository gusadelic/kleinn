$(document).ready(function() {
    
    
   
    $("input#field_password2").on("blur", function() {
        if ( $("input#field_password2").val() !== $("input#field_password").val() ) {
            $('span#password_alert').attr('class', '');
            $('span#password_alert').addClass('label label-danger');
            $('span#password_alert').html("Passwords don't match!");
        }
        else {
            $('span#password_alert').attr('class', '');
            $('span#password_alert').addClass('label label-success');
            $('span#password_alert').html("Password Validated!");
        }
    });
    
});


