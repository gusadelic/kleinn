var currentIndex = 0;
var sectionOrder = [];

$(document).ready( function() {
    setupSelectChangeEvents();
});

function setupSelectChangeEvents()
{
    $('select.vehicle').change(function(e) {
        if ( $(this).hasClass("makeSelect") ) {
            make = $(this).val();
            modelLabel = $(this).parent().parent().parent().find("label[for='modelSelect']");
            modelSelect = $(this).parent().parent().parent().find("select.modelSelect");
            options = makeOptions(vehicles[make]);
            $.when(  modelLabel.removeClass("hidden"), modelSelect.removeClass("hidden") ).done( modelSelect.html(options) );

        }
        else if ( $(this).hasClass("modelSelect") ) {
            model = $(this).val();
                make = $(this).parent().parent().parent().find("select.makeSelect").val();
                yearLabel = $(this).parent().parent().parent().find("label[for='yearSelect']");
                yearSelect = $(this).parent().parent().parent().find("select.yearSelect");
                options = makeYearOptions(vehicles[make][model]['Year']);
                $.when( yearLabel.removeClass("hidden"), yearSelect.removeClass("hidden") ).done( yearSelect.html(options) ); 
            
        }
        else {
            readyCheck();
        }
    });
        
    $('select.vehicle').mousedown(function() {
        $(this).val("");
        
        if ( $(this).hasClass("makeSelect") ) {
            yearLabel = $(this).parent().parent().parent().find("label[for='yearSelect']");
            yearSelect = $(this).parent().parent().parent().find("select.yearSelect");
            modelLabel = $(this).parent().parent().parent().find("label[for='modelSelect']");
            modelSelect = $(this).parent().parent().parent().find("select.modelSelect");
            yearLabel.addClass("hidden");
            yearSelect.addClass("hidden");
            yearSelect.html("");
            modelLabel.addClass("hidden");
            modelSelect.addClass("hidden");
            modelSelect.html("");
        }
        else if ( $(this).hasClass("modelSelect") ) {
            yearLabel = $(this).parent().parent().parent().find("label[for='yearSelect']");
            yearSelect = $(this).parent().parent().parent().find("select.yearSelect");
            yearLabel.addClass("hidden");
            yearSelect.html("");
            yearSelect.addClass("hidden");
        }
        readyCheck();
    });
        
}

function readyCheck()
{
    vehicleData = [];
    selected_count = 0;
    $("select.vehicle").each( function() {
        selected = $(this).find('option:selected');
        selected.each( function() { 
            if ($(this).val() !== "" ) 
                selected_count++;
        });
        if ( selected_count === 3 ) {
            year = $('select.yearSelect option:selected').val();
            make = $('select.makeSelect option:selected').val();
            model = $('select.modelSelect option:selected').val();
            vehicleData.push({Year:year,Make:make,Model:model});
        }

    });
    $("#vehicle_data").val(JSON.stringify(vehicleData));
}

function makeOptions(obj)
{
    var output = [];
    for (var key in obj) {
        if ( Number.isInteger(key) )
            output.push('<option value="'+obj[key]+'">'+obj[key]+'</option>');
        else
            output.push('<option value="'+key+'">'+key+'</option>');
    }
    output.push('<option value=""></option>');
    return output.reverse().join("\n");
}

function makeYearOptions(obj)
{
    var output = [];
    for (var key in obj) {
        output.push('<option value="'+obj[key]+'">'+obj[key]+'</option>');
    }
    output.push('<option value=""></option>');
    return output.reverse().join("\n");
}