<?php

// Pull constants from the local database as well
// as from the central datastore.
//

if(!defined("_MB_DEVELOPMENT"))
   define("_MB_DEVELOPMENT", 0);
if(!defined("_MB_PRODUCTION"))
   define("_MB_PRODUCTION", 1);



$centralDatastore = new \Mumby\DB\GenericDB(_MB_CENTRAL_DATASTORE_HOST_, _MB_CENTRAL_DATASTORE_NAME_, _MB_CENTRAL_DATASTORE_USER_, _MB_CENTRAL_DATASTORE_PASS_);
$localDB          = new \Mumby\DB\GenericDB(_MB_DB_HOST_, _MB_DB_NAME_, _MB_DB_USER_, _MB_DB_PASS_);

try
{
   $appID  = $localDB->query("SELECT `ConfigValue` FROM `Configs` WHERE `ConfigName`='_MB_WEB_APPLICATION_ID_'");
   if(empty($appID))
   {
      echo "Critical System Error: Missing Application ID in local datastore!";
      die();
   }
   else
      define("_MB_WEB_APPLICATION_ID_", $appID[0]["ConfigValue"]);
   
   $consts = $localDB->query("SELECT `ConstantID` AS `Constant`, NULL AS `Value` FROM Constants");
}
catch(\Exception $e)
{
   define("_MB_WEB_APPLICATION_ID_", 1);
   $consts = array();
}

$centralizedConstantsSQL  = "SELECT PermissionConstant AS `Constant`, `PermissionID` AS `Value` FROM Permissions WHERE (ApplicationID=:ApplicationID ||ApplicationID IS NULL) ";
$centralizedConstantsSQL .= "UNION ";
$centralizedConstantsSQL .= "SELECT PropertyConstant AS `Constant`, PropertyValue AS `Value` FROM ApplicationProperties WHERE ApplicationID=:ApplicationID ";
$centralizedConstantsSQL .= "UNION ";
$centralizedConstantsSQL .= "SELECT '_MB_APPLICATION_NAME_' AS `Constant`, `ApplicationName` AS `Value` FROM Applications WHERE ApplicationID=:ApplicationID";

$permissions = $centralDatastore->query($centralizedConstantsSQL, array("ApplicationID"=>_MB_WEB_APPLICATION_ID_));

$allConstants = array_merge($consts, $permissions);

foreach($allConstants as $k=>$c)
{
   if(!defined($c["Constant"]))
   {
      if($c["Value"] === null)
         define($c["Constant"], $k);
      else
      {
         if(preg_match("/^boolean\:((?:true|false))$/", $c["Value"], $booleanValue))
         {
            switch($booleanValue[1])
            {
               case "false":
                  define($c["Constant"], false);
                  break;
               case "true":
                  define($c["Constant"], true);
                  break;
               default:
                  define($c["Constant"], $c["Value"]);
                  break;
            }
         }
         else
         {
            if(ctype_digit($c["Value"]))
               define($c["Constant"], (int) $c["Value"]);
            else
               define($c["Constant"], $c["Value"]);
         }
      }
   }
}

if(defined("_MB_ENVIRONMENT_") && !defined("_MB_DEBUG"))
   define("_MB_DEBUG", (_MB_ENVIRONMENT_ == _MB_DEVELOPMENT));