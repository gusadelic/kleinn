<?php
/*
 * If the http_response_code function doesn't
 * exist, then we need to create it. For older versions of PHP < 5.4
 */
if (!function_exists('http_response_code')) { function http_response_code($code = NULL)
{
   if ($code !== NULL) 
   {
       switch ($code) 
       {
           case 100: $text = 'Continue'; break;
           case 101: $text = 'Switching Protocols'; break;
           case 200: $text = 'OK'; break;
           case 201: $text = 'Created'; break;
           case 202: $text = 'Accepted'; break;
           case 203: $text = 'Non-Authoritative Information'; break;
           case 204: $text = 'No Content'; break;
           case 205: $text = 'Reset Content'; break;
           case 206: $text = 'Partial Content'; break;
           case 300: $text = 'Multiple Choices'; break;
           case 301: $text = 'Moved Permanently'; break;
           case 302: $text = 'Moved Temporarily'; break;
           case 303: $text = 'See Other'; break;
           case 304: $text = 'Not Modified'; break;
           case 305: $text = 'Use Proxy'; break;
           case 400: $text = 'Bad Request'; break;
           case 401: $text = 'Unauthorized'; break;
           case 402: $text = 'Payment Required'; break;
           case 403: $text = 'Forbidden'; break;
           case 404: $text = 'Not Found'; break;
           case 405: $text = 'Method Not Allowed'; break;
           case 406: $text = 'Not Acceptable'; break;
           case 407: $text = 'Proxy Authentication Required'; break;
           case 408: $text = 'Request Time-out'; break;
           case 409: $text = 'Conflict'; break;
           case 410: $text = 'Gone'; break;
           case 411: $text = 'Length Required'; break;
           case 412: $text = 'Precondition Failed'; break;
           case 413: $text = 'Request Entity Too Large'; break;
           case 414: $text = 'Request-URI Too Large'; break;
           case 415: $text = 'Unsupported Media Type'; break;
           case 500: $text = 'Internal Server Error'; break;
           case 501: $text = 'Not Implemented'; break;
           case 502: $text = 'Bad Gateway'; break;
           case 503: $text = 'Service Unavailable'; break;
           case 504: $text = 'Gateway Time-out'; break;
           case 505: $text = 'HTTP Version not supported'; break;
           default:
               exit('Unknown http status code "' . htmlentities($code) . '"');
           break;
       }

       $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
       header($protocol . ' ' . $code . ' ' . $text);
       $GLOBALS['http_response_code'] = $code;
   } 
   else
   {
      $code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200);
   }

   return $code;
}}


/**
 * Create function for dumping out data to the screen.
 * Useful for debugging.
 */
if (!function_exists('dump')) { function dump($data, $sanitizeOutput=false, $returnContents=false, $label="")
{
   ob_start();
   if(is_array($data))
      print_r($data);
   else
      var_dump($data);
   $output = ob_get_contents();
   ob_end_clean();

   if($sanitizeOutput)
      $output = clean($output);

   if(!empty($label))
      $output = "<strong>".$label."</strong><hr style='border-color:black;' />\n".$output;

   $output = "<pre>\n".$output."</pre>\n";
   
   if($returnContents)
      return $output;
   else
      echo $output;
}}

/**
 * Create function for outputing clean content to the screen
 * by using htmlentities and optionally stripping tags.
 */
if (!function_exists('clean')) { function clean($html, $stripTags=false, $allowableTags=null)
{
   if($stripTags)
      return htmlentities(strip_tags ($html, $allowableTags));
   else
      return htmlentities($html);
}}

/**
 * Create function for outputing content into an HTML attribute.
 */
if (!function_exists('cleanAttr')) { function cleanAttr($html, $quoteMark = "'")
{
   if($quoteMark == "'")
      return str_replace($quoteMark, "&apos;", htmlentities($html));
   else
      return str_replace($quoteMark, "'", htmlentities($html));
}}



/**
 * Create function for outputing clean content to the screen
 * by using htmlentities and optionally stripping tags.
 */
if (!function_exists('fetchRoutes')) { function fetchRoutes($path, $extension="", $exclude = array())
{
   $files = array();
   if(file_exists($path))
   {
      foreach(glob($path."*".$extension) as $route)
      {
         if(is_dir($route))
         {
            $files = array_merge($files, fetchRoutes($route."/", $extension, $exclude));
            continue;
         }

         if(in_array($route, $exclude))
         {
            continue;
         }
         $files[] = $route;
      }
   }

   return $files;
}}

if (!function_exists('getAtomLink')) { function getAtomLink($rel, $href, $moreAttr="", $fullURL=true)
{
   $atomLink  = "<atom:link rel='".$rel."' href='";

   if($fullURL)
   {
      $atomLink .= "http";
      if(isset($_SERVER["HTTPS"]) && !empty($_SERVER["HTTPS"]) && trim(strtolower($_SERVER["HTTPS"])) !== "off")
         $atomLink .= "s";
      $atomLink .= "://".$_SERVER["HTTP_HOST"];
      if(strpos($href, "/") !== 0)
         $atomLink .= "/";
      $atomLink .= $href;
   }
   else
      $atomLink .= $href;

   $atomLink .= "' ".$moreAttr." />";
   
   return $atomLink;
}}

if (!function_exists('getPagination')) { function getPagination($rootURL, $totalPages, $currentPage, $queryStr="", $range=10)
{
   if($totalPages <= 1)
      return "";

   $pagination  = "<nav class='text-center paginationLinks'>";
   $pagination .= "<ul class='pagination'>";
      
   $half = floor(($range)/2);
   $countDown = $half;
   $countUp   = $half;
   
   $start = $currentPage;
   $end = $currentPage;
   
   while($countDown > 0 && $start > 1)
   {
      $start--;
      $countDown--;
   }
   
   $countUp += $countDown;

   while($countUp > 0 && $end < $totalPages)
   {
      $end++;
      $countUp--;
   }
   
   if($countUp > 0)
   {
      while($countUp > 0 && $start > 1)
      {
         $start--;
         $countUp--;
      }
   }
   
   if($currentPage > 1)
   {
      $pagination .= "<li><a href='".$rootURL."1".$queryStr."' title='First' aria-label='First'>&laquo;</a></li>\n";
      $pagination .= "<li><a href='".$rootURL.($currentPage-1).$queryStr."' title='Previous' aria-label='Previous'>&lsaquo;</a></li>\n";
   }
   else
   {
      $pagination .= "<li class='disabled'><span>&laquo;</span></li>\n";
      $pagination .= "<li class='disabled'><span>&lsaquo;</span></li>\n";
   }
   
   for( ; $start <= $end; $start++)
   {
      if($currentPage == $start)
      {
         $pagination .= "<li class='active'><span>".($start)." <span class='sr-only'>(current)</span></span></li>";
      }
      else
      {
         $pagination .= "<li>";
         $pagination .= "<a href='".$rootURL.$start.$queryStr."'>".($start)."</a>";
         $pagination .= "</li>\n";

      }
   }
   
   if($currentPage < $totalPages)
   {
      $pagination .= "<li><a href='".$rootURL.($currentPage+1).$queryStr."' title='Next' aria-label='Next'>&rsaquo;</a></li>\n";
      $pagination .= "<li><a href='".$rootURL.$totalPages.$queryStr."' title='Last' aria-label='Last'>&raquo;</a></li>\n";
   }
   else
   {
      $pagination .= "<li class='disabled'><span>&rsaquo;</span></li>\n";
      $pagination .= "<li class='disabled'><span>&raquo;</span></li>\n";
   }
   
   $pagination .= "</ul>";
   $pagination .= "</nav>";
   
   return $pagination;
}}

if (!function_exists('checkUserPerms')) { function checkUserPerms($user, $perm)
{
   try
   {
      if(defined($perm) && !$user->hasPermission(constant($perm)))
         return false;
   }
   catch(Exception $e)
   {
      return false;
   }
   
   return true;
}}

/**
 * Take a number in any format and break it into pieces.
 * For example, this will take $5,432.10 and will return
 * an array with a prefix ("$"), the value (5432.10), and an empty suffix.
 * For 82%, it will return an array with an empty prefix,
 * the value (82), and the suffix ("%").
 * 
 * @param string $value The value to be parsed
 * @return mixed An array of parts or false if a non-number was passed.
 */
if (!function_exists('parseNumberValue')) { function parseNumberValue($value)
{
   // Remove commas
   $value = str_replace(",", "", $value);
  
   if(preg_match('/^(\$)?(\d*\.?\d+)(\%)?$/', $value, $pieces))
   {
      $result = array(
         "prefix" => $pieces[1],
         "value"  => $pieces[2]
      );
      if(isset($pieces[3]))
         $result["suffix"] = $pieces[3];
      else
         $result["suffix"] = "";

      $decimals = strpos($pieces[2], ".");
      if(empty($decimals))
         $result["decimalPlaces"] = 0;
      else
         $result["decimalPlaces"] = strlen($pieces[2]) - $decimals - 1;

      return $result;
   }
   else
         return false;

}}