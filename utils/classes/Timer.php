<?php
namespace Mumby;

use Mumby\DB\DBObject;

/**
 * This is a simple class used for timing scripts.
 * 
 * LICENSE: Apache License, Version 2.0
 * 
 * @category TimeUtility
 * @package Mumby
 * @copyright (c) 2015, Justin Spargur
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @version 1.0
 * @since Class available since Release 1.0
 * @author spargurj
 */
class Timer
{
   static $startTime;
   static $endTime;

   /**
    * This function simply starts the timer.
    */
   public static function startTimer()
   {
      list($usec, $sec) = explode(" ", microtime());
      self::$startTime = ((float)$usec + (float)$sec);
   }

   /**
    * This function stops the timer and returns the number of seconds
    * that the timer was running.
    * 
    * @return array Array containing the number of hours, minutes, seconds, as well as the total number of seconds that the timer was running.
    */
   public static function endTimer($justSeconds = false)
   {
      list($usec, $sec) = explode(" ", microtime());
      self::$endTime = ((float)$usec + (float)$sec);
      
      $seconds = self::$endTime - self::$startTime;
      
      if($justSeconds)
         return number_format($seconds, 15);

      $time = array(
          "hours"        => 0,
          "minutes"      => 0,
          "seconds"      => 0,
          "totalSeconds" => number_format($seconds, 15)
      );
      
      // Calculate hours
      if($seconds > (60*60))
      {
         $hours = floor(($seconds/(60*60)));
         $time["hours"] = $hours;
      }
      // Calculate minutes
      if($seconds > 60)
      {
         $minutes = floor(($seconds%(60*60)/60));
         $time["minutes"] = $minutes;
      }

      $time["seconds"] = number_format(fmod($seconds,60), 1);
      
      return $time;
   }
   
   public static function formattedResults($runtime)
   {
      $timeStr = "";
      if(!empty($runtime["hours"]))
         $timeStr .= $runtime["hours"]." ".DBObject::pluralize("hour", $runtime["hours"]).", ";
      if(!empty($runtime["minutes"]))
         $timeStr .= $runtime["minutes"]." ".DBObject::pluralize("minute", $runtime["minutes"]).", ";
      
      $timeStr .= $runtime["seconds"]." ".DBObject::pluralize("second", $runtime["seconds"]);
      
      return $timeStr;
   }
}