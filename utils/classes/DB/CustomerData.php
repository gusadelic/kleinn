<?php
namespace Mumby\DB;

use Exception;

class CustomerData extends DBObject
{
    protected $wireNames;
    
    public function __construct($id=null)
    {
       // The following variables SHOULD be non-null in the child class.
       $this->sourceTable      = "CustomerData";
       $this->idCol            = "CustomerID";

       // Don't let anyone change user IDs.
       $this->readOnlyFields   = array("ComponentID");

       $this->fieldInfo = array(
            "CustomerID"     => array("type"=>self::INTEGER,),
            "FirstName"      => array("type"=>self::STRING, "length" => 64),
            "LastName"       => array("type"=>self::STRING, "length" => 64),
            "Address1"       => array("type"=>self::STRING, "length" => 128),
            "Address2"       => array("type"=>self::STRING, "length" => 128),
            "City"           => array("type"=>self::STRING, "length" => 32),
            "State"          => array("type"=>self::STRING, "length" => 2),
            "Phone"          => array("type"=>self::STRING, "length" => 10),
            "EmailAddress"   => array("type"=>self::STRING, "length" => 64)
       );
       
       parent::__construct($id);

       if(!empty($id) && !$this->checkRowInstance())
       {
            throw new Exception("Unable to create new ".get_called_class().". It appears you passed an invalid id value.");
       }
    }
    
    function getAllCustomerData()
    {
        $sql  = "SELECT  O.OrderNumber as 'Order Number', D.FirstName as 'First Name', D.LastName as 'Last Name', D.Phone, D.EmailAddress as 'Email Address', O.SKU, V.Make as 'Vehicle Make', V.Model as 'Vehicle Model', V.Year as 'Vehicle Year', O.PurchaseLocation as 'Purchase Location' FROM CustomerData D ";
        $sql .= "RIGHT JOIN CustomerOrder O ON O.CustomerID = D.CustomerID ";
        $sql .= "INNER JOIN Vehicles V ON O.VehicleID = V.VehicleID ";
        
        $output = $this->query($sql);
        return $output;
    }

    function getCustomerData($CustomerID)
    {
        $sql  = "SELECT * FROM ".$this->sourceTable." WHERE CustomerID = :id";
        $params = array( "id" => $ComponentID );
        $result = $this->query($sql, $params);
        if ( $result === false ) return false;
        return $result[0];   
    }

    function updateCustomerData($data)
    {
        $result = $this->insertOrUpdate($data, "CustomerData", false);

        return $result;
    }
    
    function getCustomerOrder($CustomerID)
    {
        return $this->find(array("CustomerID"=>$CustomerID), "CustomerOrder");
    }
    
    function updateCustomerOrder($data)
    {   
        $result = $this->insertOrUpdate($data, "CustomerOrder", false);

        return $result;
    }

}
