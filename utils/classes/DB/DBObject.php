<?php
namespace Mumby\DB;

use Mumby\Inflect;
use PDO;
use Exception;

if(!defined("_MB_TIMEZONE_"))
   define("_MB_TIMEZONE_", "America/Phoenix");

/**
 * Generic database abstraction layer. Can be either be used to create
 * a connection to the database, targeting a specific table 
 * (using $this->sourceTable) or it can be used to create an
 * object representing a specific row in the database (when constructed
 * with a non-null id value).
 * 
 * LICENSE: Apache License, Version 2.0
 * 
 * @category DatabaseConnection
 * @package Mumby
 * @copyright (c) 2015, Justin Spargur
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @version 1.0
 * @since Class available since Release 1.0
 */
abstract class DBObject
{
   const NULL      = PDO::PARAM_NULL;
   const INTEGER   = PDO::PARAM_INT;  // 1
   const STRING    = PDO::PARAM_STR;  // 2
   const LOB       = PDO::PARAM_LOB;  // 3
   const STMT      = PDO::PARAM_STMT; // 4;
   const BOOLEAN   = PDO::PARAM_BOOL; // 5
   const TIMESTAMP = 6;
   const JSON      = 7;
   
   const MYSQL_DATETIME_FORMAT = "Y-m-d H:i:s";
   
   const FALSE     = 0;
   const TRUE      = 1;
   
   protected $db;
   protected $id;
   protected $data;
   protected $originalData;
   protected $idCol;
   protected $readOnlyFields;
   protected $fieldInfo;
   protected $requiredFields;
   protected $objectName;
   protected $sourceTable;

   protected $lastStatement;
   
   /**
    * Abstract database connection class. When extended, this function can
    * either be used to create a connection to the database, targeting
    * a specific table (using $this->table) or it can be used to create an
    * object representing a specific row in the database.
    * 
    * @param mixed $id Optional unique identifier(s) for a row in the table. This can be either a single value or an array of values if the table in question used a composite primary key.
    */
   public function __construct($id=null, $dbHost=null, $dbName=null, $dbUser=null, $dbPass=null)
   {
      $this->db = self::getDBConnection($dbHost, $dbName, $dbUser, $dbPass);
      
      $this->originalData = array();
      $this->data = array();
      $this->objectName = get_class();
      
      // The following variables SHOULD be non-null in the child class.
      if(empty($this->sourceTable))
         $this->sourceTable      = null;
      if(empty($this->idCol))
         $this->idCol            = array();
      else if(!is_array($this->idCol))
      {
         $this->idCol = array(
             array(
                 "name" => $this->idCol,
                 "type" => self::STRING
             )
         );
      }
      
      $this->id = null;
      
      // The following variables COULD be non-null in the child class.
      if(empty($this->readOnlyFields))
         $this->readOnlyFields   = null;
      if(empty($this->fieldInfo))
         $this->fieldInfo       = null;

      if($id !== null)
      {
         $this->load($id);
      }
      
      $this->db->query('SET SESSION SQL_BIG_SELECTS=1;');
   }

   /**
    * Insert a new row into the table.
    * 
    * This function checks to make sure that all required fields
    * are specified. If necessary, it is assumed that any
    * inheriting class will include code to ensure that all
    * data validation is done within that class.
    * 
    * @param array $data Associative array mapping column names to values.
    * @param string $tableName Optional table name for inserting into other tables.
    * @return mixed The unique ID for the new row or false on failure.
    */
   public function insert($data, $tableName=null, $throwExceptions=false)
   {
      if(empty($tableName))
      {
         if(!$this->checkTableInfo())
            return false;
         else
            $tableName = $this->sourceTable;
         
         $this->checkRequiredFields($data);
      }
      
      $sql = "INSERT INTO `".$tableName."` (";
      $vals = "";
      
      foreach($data as $col=>$val)
      {
         $sql  .= "`".$col."`, ";
         $vals .= ":".$col.", ";
      }
      $sql  = substr($sql,  0, -2) . ") VALUES (".substr($vals, 0, -2).")"; // Trim off the trailing comma.

      $stmt = $this->db->prepare($sql);
      
      foreach($data as $col=>$val)
      {
         $thisCol = ":".$col;
         $thisVal = $val;
         $fieldType = $this->checkFieldType($col, $thisVal);
         
         $stmt->bindValue($thisCol, $thisVal, $fieldType);
      }
      
      $this->lastStatement = $stmt;
      if(!$stmt->execute())
      {
         if($throwExceptions)
         {
            $msg = "Error: Unable to insert row. ";
            $msg .= implode("<br />\n", $stmt->errorInfo());
            throw new Exception($msg);
         }
         return false;
      }
      else
      {
         $insertedID = $this->db->lastInsertId();
         
         if($insertedID != 0)
            return $insertedID;
         else
         {
            /*
             * If there's no auto-increment column, then we can
             * assume that the user provided the necessary key information
             * in the data.
             */
            $thisRow = $this->getUniqueKey($data, $tableName);
            if($thisRow)
               return $thisRow;
            else
               return true;
         }
      }
   }
   
   /**
    * Insert a new row into the table or update on duplicate key.
    * 
    * This function checks to make sure that all required fields
    * are specified. If necessary, it is assumed that any
    * inheriting class will include code to ensure that all
    * data validation is done within that class.
    * 
    * @param array $data Associative array mapping column names to values.
    * @param bool $returnAffected Specify whether the number of affected rows should be returned.
    * @param string $tableName Optional table name for inserting into other tables.
    * @return mixed The unique ID for the new row or false on failure.
    */
   public function insertOrUpdate($data, $tableName=null, $throwExceptions=false)
   {
      if(empty($tableName))
      {
         if(!$this->checkTableInfo())
            return false;
         else
            $tableName = $this->sourceTable;

         $this->checkRequiredFields($data);
      }
      
      $insertSQL = "INSERT INTO `".$tableName."` (";
      $updateSQL = " ON DUPLICATE KEY UPDATE ";
      $vals = "";
      
      foreach($data as $col=>$val)
      {
         $insertSQL .= "`".$col."`, ";
         $vals .= ":".$col.", ";
         $updateSQL .= "`".$col."`=:update_".$col.", ";
      }
      
      $sql = substr($insertSQL,  0, -2) . ") VALUES (".substr($vals, 0, -2).")" . substr($updateSQL, 0, -2);
      $this->sql = $sql;
      $stmt = $this->db->prepare($sql);
      
      foreach($data as $col=>$val)
      {
         $insertCol = ":".$col;
         $updateCol = ":update_".$col;
         $thisVal = $val;
         $fieldType = $this->checkFieldType($col, $thisVal);

         $stmt->bindValue($insertCol,  $thisVal, $fieldType);
         $stmt->bindValue($updateCol,  $thisVal, $fieldType);
      }
      
      $this->lastStatement = $stmt;
      if(!$stmt->execute())
      {
        if($throwExceptions)
        {
          $msg = "Error: Unable to insert row. ";
          $msg .= $stmt->getSQL()."\n";
          $msg .= implode("<br />\n", $stmt->errorInfo());
          throw new Exception($msg);
        }
         
        return false;
      }
      else
      {
         $insertedID = $this->db->lastInsertId();

         if($insertedID != 0)
            return $insertedID;
         else
         {
            $thisRow = $this->getUniqueKey($data, $tableName);
            if($thisRow)
            {
               while(is_array($thisRow) && count($thisRow) == 1)
               {
                  $thisRow = current($thisRow);
               }
               
               return $thisRow;
            }
            else
               return true;
         }
      }
      
      return empty($data);
   }
   
   /**
    * Search a table based on the criteria given.
    * 
    * @param array $where Associative array matching columns to values.
    * @param string $tableName Optional table name to be queried.
    * @param int $limit Optional limit on the number of rows returned.
    * @param array $order Optional associative array of column/sortDirection pairs. Direction must be either SORT_ASC or SORT_DESC
    * @return mixed Either an associative array or false if nothing is found.
    */
   public function find($where=null, $tableName=null, $limit=null, $orderBy=null)
   {
      // If the user didn't specify a table name, we assume that we're
      // updating the source table for the class.
      if(empty($tableName))
      {
         if(!$this->checkTableInfo())
            return false;
         else
            $tableName = $this->sourceTable;
      }
      
      if ( $this->formattedColumns ) {
          $sql = "SELECT *, ";
          foreach ( $this->formattedColumns as $k=>$c ) {
              $sql .= $k . " '" . $c . "', ";
          }
          $sql = substr($sql, 0, -2);
          $sql .= " FROM `".$tableName."`";
      }
      else {
        $sql = "SELECT * FROM `".$tableName."`";
      }
      
      if(!empty($where))
      {
         $sql .= " WHERE ";
         foreach(array_keys($where) as $c)
         {
            $sql .= "`".$c."`=:".$c." AND ";
         }
         $sql = substr($sql, 0, -5); // Trim off the trailing AND.
      }

      if(is_array($orderBy) && !empty($orderBy))
      {
         $orderString = "";
         foreach($orderBy as $o)
         {
            if(is_array($o))
            {
               if(!isset($o["column"]) || !isset($o["direction"]))
                  continue;
               
               if($o["direction"] != SORT_DESC &&
                  $o["direction"] != SORT_ASC  &&
                  $o["direction"] != "ASC" &&
                  $o["direction"] != "DESC")
                  continue;

               if($o["direction"] != SORT_DESC || $o["direction"] != SORT_ASC)
                  $o["direction"] = (($o["direction"] == SORT_ASC) ? "ASC" : "DESC");
               
               $orderString .= $o["column"]." ".$o["direction"].", ";
            }
            else
               $orderString .= $o." ASC, ";
         }
         
         if(!empty($orderString))
            $sql .= " ORDER BY ".substr($orderString, 0, -2);
      }

      if(!empty($limit) && (is_int($limit) || ctype_digit($limit)))
         $sql .= " LIMIT ".$limit;

      return $this->query($sql, $where);
   }

   /**
    * Read a specific row from the table. It should be noted that this does
    * not populate an empty object! Rather, it returns an associative array
    * with the data for the given row. To create an object representation
    * of a row, use the __construct method with the appropriate id value(s)
    * or use the load function.
    * 
    * @param array $id Associative array mapping column names to values, typically the unique ID for the table.
    * @param bool $limit Whether the results should be limited to one.
    * @return mixed Associative array of all non-restricted fields or false on failure.
    */
   public function read($id)
   {
      if(!$this->checkTableInfo())
         return false;

      if(!$this->checkID($id))
         return false;
      
      $results = $this->find($id, null, 1);

      if(!$results)
         return false;
      else
         return $results[0];
   }
   
   /**
    * This function converts the object from a generic DB connection
    * to an object representing a specific row in the table.
    * 
    * @param array $id Associative array mapping column names to values, typically the unique ID for the table.
    * @return bool True on success, false on failure.
    */
   public function load($id)
   {
      // Remove any previous instance data.
      $this->originalData = array();
      $this->data = array();

      $pulledData = $this->read($id);

      if(!$pulledData)
         return false;
      
      $this->originalData = $pulledData;
      $this->data = $pulledData;

      if(is_array($id))
         $this->id = $id;
      else
         $this->id = $this->formatVal($id);

      return true;
   }
   
   /**
    * This function converts an instance representation back to a generic DB
    * connection.
    */
   public function unload()
   {
      $this->originalData = array();
      $this->data = array();
      $this->id = null;
   }
   
   /**
    * Run an update query using the information provided.
    * 
    * @param array $data An associative array of column/value pairs to be updated in the database.
    * @param array $where Either null or an associative array to be used as WHERE conditions.
    * @param string $tableName Optional table name to be used in the query.
    * @return mixed Returns the number of rows affected (which could be 0) on success or false on failure.
    */
   public function update($data, $where=null, $tableName=null, $limit=null)
   {
      // If the user didn't specify a table name, we assume that we're
      // updating the source table for the class.
      if(empty($tableName))
      {
         if(!$this->checkTableInfo())
            return false;
         else
            $tableName = $this->sourceTable;
      }
      
      $allData = array();

      // If the user didn't specify a table name, we assume that they're
      // trying to update an instance of a specific row.
      if(empty($where))
      {
         if(!$this->checkRowInstance())
            return false;

         $where = $this->id;
      }
      else
         $where = $this->formatVal($where);
      
      foreach(array_keys($data) as $col)
      {
         if($tableName == $this->sourceTable && in_array($col, $this->readOnlyFields))
         {
            throw new Exception("Unable to update. `".$col."` is a read-only field.");
            return false;
         }
      }
      
      $sql = "UPDATE `".$tableName."` SET ";
      
      foreach($data as $col=>$val)
      {
         $sql  .= "`".$col."`=:".$col.", ";
         $allData[$col] = $val;
      }
      $sql  = substr($sql,  0, -2). " WHERE ";
      
      foreach($where as $k=>$c)
      {
         $sql .= "`".$k."`=:_id_".$k." AND ";
         $allData["_id_".$k] = $c;
      }
      
      $sql = substr($sql, 0, -4); // Trim off the trailing AND.
      if(!empty($limit) && (is_int($limit) || ctype_digit($limit)))
         $sql .= " LIMIT ".$limit;

      return $this->query($sql, $allData);
   }

   /**
    * Run a delete query using the information provided.
    * 
    * @param array $id An associative array of id column/value pairs for the row(s) being deleted
    * @param string $tableName Optional table name to be used in the query.
    * @param string $limit Optional limit for the query.
    * @return mixed Returns the number of rows affected (which could be 0) on success or false on failure.
    */
   public function delete($id=null, $tableName=null, $limit=null)
   {
      // If the user didn't specify a table name, we assume that we're
      // updating the source table for the class.
      if(empty($tableName))
      {
         if(!$this->checkTableInfo())
            return false;
         else
            $tableName = $this->sourceTable;
      }
      
      // If the user didn't specify a table name, we assume that they're
      // trying to update an instance of a specific row.
      if(empty($id))
      {
         if(!$this->checkRowInstance())
            return false;

         $id = $this->id;
      }
      else
         $id = $this->formatVal($id);
      
      $sql = "DELETE FROM `".$tableName."` WHERE ";
      
      foreach(array_keys($id) as $k)
         $sql .= "`".$k."`=:".$k." AND ";
      
      $sql = substr($sql, 0, -4); // Trim off the trailing comma.
      
      if(!empty($limit) && (is_int($limit) || ctype_digit($limit)))
         $sql .= " LIMIT ".$limit;

      return $this->query($sql, $id);
   }
   
   /**
    * Saves any updated information (stored in the object) to the database.
    * Worth noting that this function returns false if the object isn't
    * a representation of a single row in the database.
    *
    * @return mixed Returns the number of rows affected (which could be 0) on success or false on failure.
    */
   public function save()
   {
      if(!$this->checkTableInfo())
         return false;

      if(!$this->checkRowInstance())
      {
         throw new Exception("You are trying to save an non-object to the database. Try using the insert/update methods instead.");
         return false;
      }
      
      // Only send data that has been changed!
      $changedColumns = array();
      foreach($this->data as $k=>$v)
      {
         if($this->originalData[$k] != $v)
            $changedColumns[$k] = $v;
      }

      return $this->update($changedColumns, null, null, 1);
   }

   /**
    * Query the database using the provided query.
    * 
    * @param string $sql An SQL query, possibly in PDO prepared statement format
    * @param array $queryData An optional array of data values, with keys representing PDO params and values representing the values to bind
    * @return mixed For SELECT statements, this returns either an associative array (if data is found) or false. For DELETE, INSERT, and UPDATE queries, this returns the number of rows affected. For all other queries, it returns true on success.
    */
   public function query($sql, $queryData=array(), $ignoreStatementHistory=false)
   {
      $stmt = $this->db->prepare($sql);

      if(!empty($queryData))
      {
         foreach($queryData as $col=>$value)
         {
            $thisCol = ":".$col;
            $thisVal = $value;
            $fieldType = $this->checkFieldType($col, $thisVal);
            $stmt->bindValue($thisCol, $thisVal, $fieldType);
         }
      }

      if(!$ignoreStatementHistory)
         $this->lastStatement = $stmt;
      
      if(!$stmt->execute())
      {
         $msg  = "Error: Unable to run query. ".$sql."\n";
         $msg .= implode("<br />\n", $stmt->errorInfo());

         if(_MB_DEBUG)
            $msg .= "<br />".$stmt->getSQL()."<br />\n";

         throw new Exception($msg);
         return false;
      }

      if(stripos($sql, "SELECT") === 0)
      {
         $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
         if(is_array($data) && empty($data))
            return false;
         else
            return $data;
      }
      else if(stripos($sql, "UPDATE") === 0 || stripos($sql, "INSERT") === 0 || stripos($sql, "DELETE") === 0)
      {
         return $stmt->rowCount();
      }
      else
         return true;
   }

   /**
    * Run multiple queries in a single transaction.
    * 
    * @param array $queries An array of SQL statements
    * @param array $queryData An associative array of PDO params to be bound to the corresponding SQL statement in $queries.
    * @param array $errorMessages Optional error messages to be used for each query (rather than the generic Exception message for this function).
    * @return bool Whether the queries were executed successfully.
    * @throws Exception If a query fails, an exception is thrown and the DB is rolled back.
    */
   public function multiQuery($queries, $queryData=array())
   {
      $totalQueries = count($queries);
      
      if(!empty($queryData) && count($queryData) != $totalQueries)
         throw new Exception("The number of queries passed does not match the number of parameter arrays passed.");

      try
      {
         // Begin the transaction.
         $this->db->beginTransaction();
         
         $responses = array();
         foreach($queries as $k=>$sql)
         {
            if(array_key_exists($k, $queryData))
               $responses[] = $this->query($sql, $queryData[$k]);
            else
               $responses[] = $this->query($sql);
         }
         
         $this->db->commit();
         return $responses;
      }
      catch(Exception $e)
      {
         $this->db->rollBack();

         $msg  = "Unable to run multiple queries. ";
         $msg .= $e->getMessage()."\n";
         $msg .= $e->getFile().", Line ";
         $msg .= $e->getLine()."\n";
         throw new Exception($msg);
         return false;
      }
   }

   /**
    * Reset a table by deleting every row and, if necessary,
    * reseting the auto-increment field to 1.
    * 
    * @param string $tableName Optional table name to be used in the query.
    * @return bool True on success, false on failure.
    */
   protected function resetTable($tableName=null)
   {
      if(empty($tableName))
      {
         if(!$this->checkTableInfo())
            return false;
         else
            $tableName = $this->sourceTable;
      }

      $empty = "DELETE FROM `".$tableName."`";
      $stmt = $this->db->prepare($empty);
      $this->lastStatement = $stmt;
      if(!$stmt->execute())
      {
         $msg = "Error: Unable to reset the table. ";
         $msg .= implode("<br />\n", $stmt->errorInfo());
         throw new Exception($msg);
         return false;
      }
      else
      {
         if($this->checkAutoIncrement($tableName))
         {
            $autoIncrementSQL = "ALTER TABLE `".$tableName."` AUTO_INCREMENT=1";
            $this->query($autoIncrementSQL);
         }
         
         return true;
      }
   }

   /**
    * Simply converts a string into an array (for consistency
    * within functions).
    * 
    * @param string $val The value to be stored within the array
    * @param string $key Optional key to be used within the array. If not provided, then it is assumed that we should use primary key (or the first column of a composite key)
    * @return array Returns an associative array for the key/value pair.
    */
   protected function formatVal($val, $key=null)
   {
      if(!is_array($val))
      {
         if(!empty($key))
            $val = array( $key=>$val);
         else
            $val = array( $this->idCol[0]["name"]=>$val);
      }
      
      return $val;
   }
   
   /**
    * Make sure that all required fields are present in the given array.
    * 
    * @param string $data Associative array to be checked for required fields. It should be noted that because the parameter is passed by reference, it can converted if necessary.
    * @param array $requiredFields Optional array of required fields. If not specified, then the class property $requiredFields is used instead.
    * @return bool Returns true for valid data and false for invalid data.
    * @throws Exception If there's a missing field, then an exception is thrown.
    */
   public function checkRequiredFields(&$data, $requiredFields=array())
   {
      // If the table uses a composite key, make sure that the user
      // specified all of the required fields for the key.
      if(count($this->idCol) > 1)
      {
         foreach($this->idCol as $c)
         {
            if(!isset($data[$c["name"]]))
            {
               throw new Exception("Missing one of the fields required for the primary key.");
               return false;
            }
         }
      }
      
      if(empty($requiredFields))
         $requiredFields = $this->requiredFields;

      if(!empty($requiredFields))
      {
         foreach($requiredFields as $f)
         {
            if(!isset($data[$f]))
            {
               throw new Exception("Missing required field: ".$f);
               return false;
            }
         }
      }

      return true;
   }

   /**
    * Check to see if the object is a representation of a single row
    * from the table.
    * 
    * @return bool True for a valid row instance and false otherwise.
    * @throws Exception If there's a missing field, then an exception is thrown.
    */
   protected function checkRowInstance()
   {
      return !empty($this->id);
   }

   /**
    * Check to see if source table and the primary key(s) have been
    * defined for the class.
    * 
    * @param bool $throwExceptions Whether the function should throw exceptions.
    * @return bool True for a valid row instance and false otherwise.
    * @throws Exception If the source table or primary key(s) haven't been defined, then an exception is thrown.
    */
   protected function checkTableInfo($throwExceptions = true)
   {
      if(empty($this->sourceTable))
      {
         if($throwExceptions)
            throw new Exception("The source table information for the class not valid.");
         
         return false;
      }

      if(empty($this->idCol))
      {
         if($throwExceptions)
            throw new Exception("The primary key information for the class not valid.");
         
         return false;
      }
      
      return true;
   }

   /**
    * See if a table has an auto increment column.
    * 
    * @param string $tableName Optional table name. If not specified, then the source table for the class is assumed.
    * @return bool True if an auto-increment column exists, false otherwise.
    */
   protected function checkAutoIncrement($tableName=null)
   {
      static $hasAutoInc = null;
      if(null === $hasAutoInc)
      {
         $data = $this->getAutoIncrementColumn($tableName);
         if(!empty($data))
            $result = true;
         else
            $result = false;
            
         if($tableName != $this->sourceTable)
            return $result;
         else
            $hasAutoInc = $result;
      }
      
      return $hasAutoInc;
   }
   
   /**
    * Checks the field value being submitted based on the 
    * field definition specified for the class.
    * 
    * Options for field info:
    *    type       - INTEGER, STRING, LOB, STMT, BOOLEAN, TIMESTAMP, JSON
    *    length     - Max string length. Only applies to STRING fields.
    *    enum       - An array of valid values. Only applies to STRING fields.
    *    expression - Regular expression that must be matched in order
    *                 for the value to be valid. Only applies to STRING fields.
    * 
    *    minVal     - Minimum integer value (must be greater than or equal too). Only applies to INTEGER fields.
    *    maxVal     - Maximum integer value (must be less than or equal too). Only applies to INTEGER fields.
    * 
    *    minDate    - Farthest date in the past that is valid. Only applies to TIMESTAMP fields.
    *    maxDate    - Farthest date in the future that is valid. Only applies to TIMESTAMP fields.
    * 
    * @param string $fieldName Name of the field to be checked.
    * @param mixed $fieldValue Value for the field.
    * @return bool True if an auto-increment column exists, false otherwise.
    */
   protected function checkFieldType($fieldName, &$fieldValue)
   {
      if($fieldValue === null)
         return self::NULL;
      
      $thisFieldInfo = (isset($this->fieldInfo[$fieldName]) ? $this->fieldInfo[$fieldName] : null);

      if(empty($thisFieldInfo))
         return self::STRING;

      if(!isset($thisFieldInfo["type"]))
         $thisFieldInfo["type"] = self::STRING;

      switch($thisFieldInfo["type"])
      {
         case self::STRING:
         case self::JSON:
            if(isset($thisFieldInfo["length"]) &&  strlen($fieldValue) > $thisFieldInfo["length"])
            {
               $msg = "Unable to insert row. ".$fieldName." must be ".$thisFieldInfo["length"]." characters or less.";
               throw new Exception($msg);
               return false;
            }
            if(isset($thisFieldInfo["enum"]) && !in_array($fieldValue, $thisFieldInfo["enum"]))
            {
               $msg = "Unable to insert row. ".$fieldName." must be one of the following values: ";
               foreach($thisFieldInfo["enum"] as $e)
               {
                  if($e === null)
                     $msg.= "null, ";
                  else
                     $msg.= $e.", ";
               }
               throw new Exception(substr($msg,0,-2));
               return false;
            }
            if(isset($thisFieldInfo["expression"]) && preg_match($thisFieldInfo["expression"], $fieldValue))
            {
               throw new Exception("Unable to insert row. ".$fieldName." must be in the form '".$thisFieldInfo["expression"]."'.");
               return false;
            }
            
            if($thisFieldInfo["type"] == self::JSON)
            {
               json_decode($fieldValue);
               $jsonErrors = json_last_error();
               if($jsonErrors !== JSON_ERROR_NONE)
               {
                  throw new Exception("Unable to insert row. ".$fieldName." must be valid JSON (Error Code: ".$jsonErrors.").");
                  return false;
               }
            }
            
            return self::STRING;
            break;
            
            
         case self::INTEGER:
            if(isset($thisFieldInfo["minVal"]) &&  strlen($fieldValue) > $thisFieldInfo["minVal"])
            {
               throw new Exception("Unable to insert row. ".$fieldName." must be ".$thisFieldInfo["minVal"]." or more.");
               return false;
            }
            if(isset($thisFieldInfo["maxVal"]) &&  strlen($fieldValue) > $thisFieldInfo["maxVal"])
            {
               throw new Exception("Unable to insert row. ".$fieldName." must be ".$thisFieldInfo["maxVal"]." or less.");
               return false;
            }
            
            return self::INTEGER;
            break;
            
            
         case self::TIMESTAMP:

            // Allow for empty values (i.e., null)
            $fieldValue = trim($fieldValue);
            if(empty($fieldValue))
               return self::STRING;

            $thisDate = new \DateTime($fieldValue, new \DateTimeZone(_MB_TIMEZONE_));
            
            if(isset($thisFieldInfo["minDate"]))
            {
               $minDate = new \DateTime($thisFieldInfo["minDate"], new \DateTimeZone(_MB_TIMEZONE_));
               if($thisDate < $minDate)
               {
                  throw new Exception("Unable to insert row. ".$fieldName." must be after ".$minDate->format(DBObject::MYSQL_DATETIME_FORMAT));
                  return false;
               }
            }
            if(isset($thisFieldInfo["maxDate"]))
            {
               $maxDate = new \DateTime($thisFieldInfo["maxDate"], new \DateTimeZone(_MB_TIMEZONE_));
               if($thisDate > $maxDate)
               {
                  throw new Exception("Unable to insert row. ".$fieldName." must be earlier than ".$maxDate->format(DBObject::MYSQL_DATETIME_FORMAT));
                  return false;
               }
            }
            
            $fieldValue = $thisDate->format(DBObject::MYSQL_DATETIME_FORMAT);

            return self::STRING;
            break;
            
            
         case self::BOOLEAN:
            if($fieldValue !== true && $fieldValue !== false && $fieldValue != 1 && $fieldValue != 0)
            {
               throw new Exception("Unable to insert row. ".$fieldName." must be a boolean value.");
               return false;
            }
            
            return self::BOOLEAN;
            break;
            
            
         default:
            return self::STRING;
            break;
      }
      
      return self::STRING;
   }

   /**
    * See if a table has an auto increment column.
    * 
    * @param array $id Associative array mapping column names to values, typically the unique ID for the table.
    * @param bool $throwExceptions Whether the function should throw exceptions.
    * @return bool True if an auto-increment column exists, false otherwise.
    */
   protected function checkID(&$id, $throwExceptions = true)
   {
      $id = $this->formatVal($id);
      $idsNeeded = count($this->idCol);
      $idsPassed = count($id);
      
      if($idsNeeded != $idsPassed)
      {
         if($throwExceptions)
            throw new Exception("Invalid id for ".$this->objectName.". Expected ".$idsNeeded." identifiers but was passed a single identifier.");

         return false;
      }
      
      return true;
   }

   /**
    * Get the source table for the class.
    * 
    * @return mixed Either the source table name or false if none is specified.
    */
   public function getSourceTable()
   {
      if(empty($this->sourceTable))
         return false;
      
      return $this->sourceTable;
   }
   
   /**
    * Get the primary key(s) for the source table.
    * 
    * @return mixed Either the source table primary key(s) or false if no key is specified.
    */
   public function getIDColumns()
   {
      if(empty($this->idCol))
         return false;
      
      return $this->idCol;
   }

   /**
    * Get the last inserted id for the source table. This function is primary
    * used when the built-in PDO functions don't work (e.g., composite
    * keys, etc.)
    * 
    * @param array $data Associative array mapping column names to values.
    * @param string $tableName Optional table name. If not specified, then the source table for the class is assumed.
    * @return bool True if an auto-increment column exists, false otherwise.
    */
   protected function getUniqueKey($data, $tableName=null)
   {
      if(empty($tableName))
      {
         if(!$this->checkTableInfo())
            return false;
         else
            $tableName = $this->sourceTable;
      }

      $idCols = array();
      if($tableName == $this->sourceTable)
      {
         foreach($this->idCol as $c)
            $idCols[] = $c["name"];
         
         $idSQL = "SELECT `".implode("`,`", $idCols)."` FROM `".$tableName."` WHERE ";
      }
      else
      {
         
         $idSQL = "SELECT * FROM `".$tableName."` WHERE ";
      }

      $queryData = array();
      foreach($data as $col=>$val)
      {
         if($val === null)
            continue;

         $idSQL .= "`".$col."`=:".$col." AND ";
         $queryData[$col]=$val;
      }
      $idSQL = substr($idSQL, 0, -4);

      return $this->query($idSQL, $queryData);
   }
   
   /**
    * Returns the name of the auto increment column for a table if
    * one exists.
    * 
    * @param string $tableName The name of the MySQL table being queried
    * @return mixed Returns either the name of the auto increment column or false if one doesn't exist.
    * @throws Exception Throws an exception if the database query fails.
    */
   protected function getAutoIncrementColumn($tableName=null)
   {
      if(empty($tableName))
      {
         if(!$this->checkTableInfo())
            return false;
         else
            $tableName = $this->sourceTable;
      }

      $sql = "SHOW COLUMNS FROM `".$tableName."`";

      $stmt = $this->db->prepare($sql);
      $this->lastStatement = $stmt;
      if(!$stmt->execute())
      {
         $msg = "Error: Unable to fetch auto increment information regarding the `".$tableName."` table.";
         $msg .= implode("<br />\n", $stmt->errorInfo());
         throw new Exception($msg);
         return false;
      }
      else
      {
         $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
         $autoIncField = false;
         foreach($data as $d)
         {
            if(array_key_exists("Extra", $d) && !empty($d["Extra"]))
            {
               $autoIncField = $d["Field"];
               break;
            }
         }

         return $autoIncField;
      }
   }
   
   /**
    * Reset all auto-increment IDs for the given table
    * so that they are sequential.
    * 
    * @param string $tableName The name of the MySQL table being queried
    * @return bool Returns true on success or false on failure.
    * @throws Exception Throws an exception if the database query fails.
    */
   public function setSequentialIDs($tableName=null)
   {
      if(empty($tableName))
      {
         if(!$this->checkTableInfo())
            return false;
         else
            $tableName = $this->sourceTable;
      }
      
      $autoIncColumn = $this->getAutoIncrementColumn($tableName);
      $totalRowsQuery = $this->query("SELECT COUNT(*) as total FROM `".$tableName."`");
      $totalRows = $totalRowsQuery[0]["total"];

      if(!$autoIncColumn)
         return false;
      else
      {
         $queries = array(
            "SET @count = 0",
            "UPDATE `".$tableName."` SET `".$autoIncColumn."` = @count:= @count + 1",
            "ALTER TABLE `".$tableName."` AUTO_INCREMENT=".($totalRows+1)
         );
         
         return $this->multiQuery($queries);
      }
   }
   
   public function getLastQuery()
   {
      return $this->lastStatement->getSQL();
   }
   

   /**
    * See if a URL fragment is already in use by another row for the given table.
    * 
    * @param type $keyword
    * @return boolean Returns true if the fragment is in use, false otherwise.
    */
   public function checkURLFragmentUsage($urlFragment, $tableName="")
   {
      if(empty($tableName))
      {
         if(empty($this->sourceTable))
            throw new Exception ("Unable to check URL fragment usage. Table name not specified.");
         
         $tableName = $this->sourceTable;
      }
      
      // Make sure the keyword isn't already in use.
      $alreadyExists = $this->query("SELECT * FROM URLFragments WHERE TableName=:TableName AND URLFragment=:URLFragment LIMIT 1", array("TableName"=>$tableName, "URLFragment"=>$urlFragment));
      if(!empty($alreadyExists))
         return $alreadyExists;
      else
         return false;
   }

   /**
    * Singleton function for creating a DB connections.
    * 
    * @return PDO PDO database connection.
    */
   public static function getDBConnection($dbHost=null, $dbName=null, $dbUser=null, $dbPass=null)
   {
      static $DBInstances = null;
      if(null === $DBInstances)
         $DBInstances = array();
      
      $thisKey = preg_replace("/[^A-Za-z0-9]/", "", $dbHost.$dbName);
      
      if(!isset($DBInstances[$thisKey]))
      {
         if(empty($dbHost))
            $dbHost = _MB_CENTRAL_DATASTORE_HOST_;
         if(empty($dbName))
            $dbName = _MB_CENTRAL_DATASTORE_NAME_;
         if(empty($dbUser))
            $dbUser = _MB_CENTRAL_DATASTORE_USER_;
         if(empty($dbPass))
            $dbPass = _MB_CENTRAL_DATASTORE_PASS_;
         
         $thisInstance = new PDODebug("mysql:host=".$dbHost.";dbname=".$dbName.";charset=utf8", $dbUser, $dbPass, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
         $DBInstances[$thisKey] = $thisInstance;
      }
      
      return $DBInstances[$thisKey];
   }

   /**
    * Helper function for returning pluralized strings.
    * 
    * @param string $string The singular version of the string to be pluralized.
    * @param int $count The number of items.
    * @param string $plural A specific plural version of the string.
    * @return string Either the singular or pluralized version of the string.
    */
   public static function pluralize($string, $count, $plural="")
   {
      if($count == 1)
         return $string;
      else if(!empty($plural))
         return $plural;
      else
         return Inflect::pluralize($string);
   }
   
   /**
    * Helper function for returning a list using the Oxford comma.
    * 
    * @param string $string The singular version of the string to be pluralized.
    * @param int $count The number of items.
    * @param string $plural A specific plural version of the string.
    * @return string Either the singular or pluralized version of the string.
    */
   public static function commaList($items)
   {
      $itemCount = count($items);
      if($itemCount == 1)
         return $items[0];
      else if ($itemCount == 2)
         return implode(" and ", $items);
      else
      {
         $last = array_pop($items);
         $list = implode(", ", $items);
         $list .= ", and ".$last;
         return $list;
      }
   }
   
   /**
    * This function takes a string and returned a captilized version
    * of the string, using underscores instead of spaces.
    * 
    * @param string $name Name to be used to define the constant
    * @param mixed $val Value to be used for the constant
    */
   public static function val2const($name, $val)
   {
      $constName = self::constName($name);
      if(defined($constName))
         return false;
      else
         define($constName, $val);
   }
   
   /**
    * Simple helper function for taking a string and converting it to valid
    * PHP constant format. Note: This function does NOT actually define
    * any constants.
    * 
    * @param string $val The string to be reformatted.
    * @return string The formatted string.
    */
   public static function constName($val)
   {
      return preg_replace("/\_+/", "_", strtoupper(str_replace(" ","_",preg_replace("/[^A-Za-z0-9\ ]/", "", $val))));
   }

   /**
    * Provide 'getters' for an objects's properties. If the user
    * calls an undefined 'get___' function, then the class tries to
    * determine if that property exists for the class. If it does,
    * then the value of that property is returned. Otherwise, it
    * returns false.
    * 
    * Note: When the object is not a representation of a row within the
    * database, this will always return false.
    * 
    * @param string $name The name of the 'get' function being called.
    * @param string $args Not used in this context.
    * @return mixed Either false or the value of the object property.
    */
   public function __call($name,$args)
   {
      $name = strtolower($name);
      if(strpos($name, "get") === 0)
      {
         $name = substr($name,3);
         $lowerVal = strtolower($name);

         if(isset($this->data[$lowerVal])) // Legacy
            return $this->data[$lowerVal];
         else if(isset($this->data[$name]))
            return $this->data[$name];
         else
            return false;
      }
      return false;
   }
   
   /**
    * Provide direct access to objects's properties. If the property
    * doesn't exist, then the function returns false. Otherwise, it
    * returns the current value of the object property.
    * 
    * Note: When the object is not a representation of a row within the
    * database, this will always return false.
    * 
    * @param string $val The name of the property being queried.
    * @return mixed Either false or the value of the object property.
    */
   public function __get($val)
   {
      $lowerVal = strtolower($val);

      if(isset($this->data[$lowerVal])) // Legacy
         return $this->data[$lowerVal];
      else if(isset($this->data[$val]))
         return $this->data[$val];
      else
         return false;
   }

  public function getCols()
  {
    return array_keys($this->fieldInfo);
  }

}