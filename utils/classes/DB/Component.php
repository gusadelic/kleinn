<?php
namespace Mumby\DB;

use Exception;

class Component extends DBObject
{
    protected $wireNames;
    
    public function __construct($id=null)
    {
       // The following variables SHOULD be non-null in the child class.
       $this->sourceTable      = "Components";
       $this->idCol            = "ComponentID";

       // Don't let anyone change user IDs.
       $this->readOnlyFields   = array("ComponentID");

       $this->fieldInfo = array(
            "ComponentID"     => array("type"=>self::INTEGER,),
            "ComponentName"   => array("type"=>self::STRING, "length" => 16),
            "ComponentType"   => array("type"=>self::STRING, "length" => 16),
            "RSType"          => array("type"=>self::STRING, "length" => 16),
            "ComponentBrand"  => array("type"=>self::STRING, "length" => 16)
           
       );
       
//       $this->wireNames = array(
//            "12 VOLT CONSTANT",
//            "ACCESSORY/HEATER BLOWER 1",
//            "ACCESSORY/HEATER BLOWER 2",
//            "ANTI-THEFT",
//            "BRAKE",
//            "DOMELIGHT SUPERVISION",
//            "DOOR TRIGGER",
//            "DOOR TRIGGER ( + )",
//            "DOOR TRIGGER ( - )",
//            "FACTORY ALARM ARM",
//            "FACTORY ALARM DISARM",
//            "GROUND",
//            "GROUND WHEN RUNNING",
//            "HOOD PIN",
//            "HORN",
//            "IGNITION 1",
//            "IGNITION 2",
//            "IGNITION 3",
//            "KEYSENSE",
//            "LOCK MOTOR WIRE",
//            "PARKING LIGHTS ( + )",
//            "PARKING LIGHTS ( - )",
//            "POWER LOCK",
//            "POWER UNLOCK",
//            "SELECTABLE IGNITION (IGN2/ACC2/START2)",
//            "SELECTABLE PARKING LIGHTS ( + / - )",
//            "SIREN",
//            "SLIDING POWER DOOR",
//            "STARTER",
//            "STARTER 2",
//            "TACH",
//            "TRUNK RELEASE",
//            "TRUNK SWITCH",
//            "WAIT TO START LIGHT");
       
       parent::__construct($id);

       if(!empty($id) && !$this->checkRowInstance())
       {
            throw new Exception("Unable to create new ".get_called_class().". It appears you passed an invalid id value.");
       }
    }
    
    function getAllComponents()
    {
        $sql  = "SELECT * FROM ".$this->sourceTable." ";
        $output = $this->query($sql);
        return $output;
    }

    function getComponent($ComponentID)
    {
        $sql  = "SELECT * FROM ".$this->sourceTable." WHERE ComponentID = :id";
        $params = array( "id" => $ComponentID );
        $result = $this->query($sql, $params);
        if ( $result === false ) return false;
        return $result[0];   
    }
    
    function getComponentTypes()
    {
        $sql = "SELECT DISTINCT ComponentType FROM Components";
        return $this->query($sql);
    }
    
    function getComponentsByType($type)
    {
        $sql  = "SELECT * FROM ".$this->sourceTable." WHERE ComponentType = :type";
        $params = array( "type" => $type );
        return $this->query($sql, $params);   
    }
    
    function updateComponent($data)
    {
        $params = array(
            "ComponentID" => $data['ComponentID'],
            "ComponentName"  => $data['ComponentName'],
            "ComponentType"  => $data['ComponentType'],
            "RSType" => $data['RSType'],
            "ComponentBrand" => $data['ComponentBrand']
        );
        
        $result = $this->insertOrUpdate($params, "Components", false);

        return $result;
    }
    
    function generateCompCode($RSID, $BypassID, $OtherID)
    {       
        //if ( $RSID )
            $rs = $this->find(array("ComponentID" => $RSID), "Components");
        //if ( $BypassID )
            $bypass = $this->find(array("ComponentID" => $BypassID), "Components");
        //if ( $OtherID )
            $other = $this->find(array("ComponentID" => $OtherID), "Components");
        
        return ($rs[0]['ComponentName'] ? $rs[0]['ComponentName'] : "")  . ($bypass[0]['ComponentName'] ? "-".$bypass[0]['ComponentName'] : "")  . ($other[0]['ComponentName'] ? "-".$other[0]['ComponentName'] : "");
    }
    
    function getComponentWiring($ComponentID)
    {
        $sql =  "SELECT RemoteStartWires.*, Wires.WireName AS Name FROM RemoteStartWires ";
        $sql .= "JOIN Wires ON Wires.WireID = RemoteStartWires.WireID ";
        $sql .= "WHERE ComponentID = :ID ";
        $params = array( "ID" => $ComponentID);
        return $this->query($sql, $params);
    }
    
    function updateComponentWiring($data)
    {
        $sql  = "INSERT INTO `RemoteStartWires` (`ComponentID`, `WireID`, `Color`, `Harness`, `Notes`) ";
        $sql .= "VALUES (:ComponentID, :WireID, :Color, :Harness, :Notes) ";
        $sql .= "ON DUPLICATE KEY UPDATE `Color`=:update_Color, `Harness`=:update_Harness, `Notes`=:update_Notes";

        foreach ( $data as $d ) {
            $params = array(
                "ComponentID"       => $d['ComponentID'],
                "WireID"              => $d['WireID'],
                "Color"             => $d['Color'],
                "Harness"           => $d['Harness'],
                "Notes"             => $d['Notes'],
                "update_Color"      => $d['Color'],
                "update_Harness"    => $d['Harness'],
                "update_Notes"      => $d['Notes']
            );
            $this->query($sql,$params);
        }
         
        return true;
    }
    
    function getDefaultWiring()
    {
        $wireDB = new Wire();
        $wires = $wireDB->getWires();
        $wirenames = array();
        foreach ( $wires as $w ) {
            $wirenames[$w['WireID']] = $w['WireName'];
        }
        return $wirenames;
    }
}