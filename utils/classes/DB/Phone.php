<?php
namespace Mumby\DB;

use Exception;

class Phone extends DBObject
{
   const NO_AREA_CODE_PATTERN    = "/^(\d{3})\-(\d{4})$/";
   const NO_COUNTRY_CODE_PATTERN = "/^(\d{3})\-(\d{3})\-(\d{4})$/";
   const FULL_NUMBER_PATTERN     = "/^(\d{1,3})\-(\d{3})\-(\d{3})\-(\d{4})$/";
   const NUMBERS_ONLY_PATTERN     = "/^(\d{1,3})?(\d{3})?(\d{3})(\d{4})$/";

   public function __construct($id=null)
   {
      // The following variables SHOULD be non-null in the child class.
      $this->sourceTable      = "PhoneNumbers";
      $this->idCol            = "PhoneNumberID";
      
      // Don't let anyone change user IDs.
      $this->readOnlyFields   = array("PhoneNumberID");
      
      $this->fieldInfo = array(
         "UserID"            => array("type"=>self::INTEGER),
         "RoomID"            => array("type"=>self::INTEGER),
         "CountryPrefix"     => array("type"=>self::STRING, "length"=>5),
         "AreaCode"          => array("type"=>self::STRING, "length"=>4),
         "Switch"            => array("type"=>self::STRING, "length"=>4),
         "Line"              => array("type"=>self::STRING, "length"=>4),
         "PhoneNumberTypeID" => array("type"=>self::INTEGER),
         "PhoneNotes"        => array("type"=>self::STRING, "length"=>255)
      );
      
      $this->requiredFields = array(
         "Switch",
         "Line"
      );

      parent::__construct($id);
      
      if(!empty($id) && !$this->checkRowInstance())
      {
         throw new Exception("Unable to create new ".get_called_class().". It appears you passed an invalid id value.");
      }
   }
   
   public function getPhoneNumbers($id=null)
   {
      $sql  = "SELECT p.*, pt.* FROM ".$this->sourceTable." p ";
      $sql .= "INNER JOIN PhoneNumberTypes pt ON pt.PhoneNumberTypeID=p.PhoneNumberTypeID ";
      
      $data = array();
      if(!empty($id))
      {
         if(is_int($id) || ctype_digit($id))
         {
            $sql .= "WHERE p.PhoneNumberID=:phoneNumberID ";
            $data["phoneNumberID"] = $id;
         }
         else if($this->validatePhoneNumber($id))
         {
            $sql .= "WHERE ";
            $sql .= "p.CountryPrefix=:CountryPrefix AND ";
            $sql .= "p.AreaCode=:AreaCode AND ";
            $sql .= "p.Switch=:Switch AND ";
            $sql .= "p.Line=:Line ";
            
            $data = $id;
         }
         else
         {
            throw new Exception("Invalid phone number id.");
            return false;
         }
      }

      $sql .= "ORDER BY p.CountryPrefix ASC, p.AreaCode ASC, p.Switch ASC, p.Line ASC";

      return $this->query($sql, $data);
   }
   
   /**
    * Return information about a specific phone number.
    * 
    * @param mixed $id If an integer is passed, then the query looks for a PhoneNumberID that matches the integer. If a string matching a valid phone number format is passed, then the query looks for a phone number that matches that format.
    * @return mixed Returns either an associative array, true if no data is returned from the query, or false if there's an error with the query.
    */
   public function getPhoneInfo($id)
   {
      return $this->getPhoneNumbers($id);
   }
   
   /**
    * Validates whether a string is a phone number and converts it to an array of phone parts.
    * 
    * @param String $phoneNumber Phone number to be validated. It should be noted that because the parameter is passed by reference, it is converted to an array of phone number parts.
    * @return bool Returns true for a valid phone number and false for an invalid phone number.
    */
   public static function validatePhoneNumber(&$phoneNumber)
   {
      $phoneParts = array(
          "CountryPrefix" => "",
          "AreaCode"      => "",
          "Switch"        => "",
          "Line"          => ""
      );

      if(preg_match(self::NO_AREA_CODE_PATTERN, $phoneNumber, $matches))
      {
         $phoneParts["Switch"]  = $matches[1];
         $phoneParts["Line"]    = $matches[2];
      }
      if(preg_match(self::NO_COUNTRY_CODE_PATTERN, $phoneNumber, $matches))
      {
         $phoneParts["AreaCode"]    = $matches[1];
         $phoneParts["Switch"]  = $matches[2];
         $phoneParts["Line"]    = $matches[3];
      }
      if(preg_match(self::FULL_NUMBER_PATTERN, $phoneNumber, $matches))
      {
         $phoneParts["CountryPrefix"] = $matches[1];
         $phoneParts["AreaCode"]    = $matches[2];
         $phoneParts["Switch"]  = $matches[3];
         $phoneParts["Line"]    = $matches[4];
      }
      if(preg_match(self::NUMBERS_ONLY_PATTERN, $phoneNumber, $matches))
      {
         $phoneParts["Switch"]  = $matches[3];
         $phoneParts["Line"]    = $matches[4];
         
         if(!empty($matches[1]) && !empty($matches[2]))
         {
            $phoneParts["CountryPrefix"] = $matches[1];
            $phoneParts["AreaCode"]    = $matches[2];
         }

         if(!empty($matches[1]) && empty($matches[2]))
         {
            $phoneParts["AreaCode"]    = $matches[1];
         }
      }

      if(!empty($phoneParts["Switch"]) && !empty($phoneParts["Line"]))
      {
         $phoneNumber = $phoneParts;
         return true;
      }
      else
         return false;
   }
   
   /**
    * Get all phone number types from the database.
    * 
    * @return mixed Returns either an associative array of phone number types or false if there's an error.
    */
   public function getPhoneNumberTypes()
   {
      $sql = "SELECT * FROM PhoneNumberTypes";
      return $this->query($sql);
   }
   
   public function addPhoneNumberTypes($data)
   {
      $requiredFields = array(
          "PhoneNumberTypeName",
          "IsPublic"
      );
      
      foreach($requiredFields as $r)
      {
         if(!array_key_exists($r, $data))
         {
            throw new Exception("Missing required field for adding a phone number type.".$r);
            return false;
         }
      }
      
      return $this->insertOrUpdate($data, "PhoneTypes");
   }
   
   /**
    * Make sure that all required phone fields are present in the given array.
    * If a string is given for the phone number, we convert it to an
    * array of phone number parts.
    * 
    * @param String $data Associative array to be checked for required fields. It should be noted that because the parameter is passed by reference, it can converted if necessary.
    * @return bool Returns true for valid data and false for invalid data.
    * @throws Exception If there's a missing field, then an exception is thrown.
    */
   public function checkRequiredFields(&$data, $requiredFields=null)
   {
      if(isset($data["phone"]) || isset($data["Phone"]))
      {
         $thisKey = (isset($data["phone"]) ? "phone" : "Phone");
         $originalPhoneString = $data[$thisKey];
         
         // Make sure there aren't any leading/trailing spaces.
         $data[$thisKey] = trim($data[$thisKey]);
         
         $thisPhone = $this->validatePhoneNumber($data[$thisKey]);
         if(!$thisPhone)
         {
            throw new Exception($originalPhoneString." is not a valid phone number.");
            return false;
         }

         foreach($data[$thisKey] as $key=>$phoneParts)
         {
            $data[$key] = $phoneParts;
         }
         unset($data[$thisKey]);
      }

      return parent::checkRequiredFields($data);
   }
   
   /**
    * Takes a phone number (without any non-digit characters)
    * and returns a formatted string.
    * 
    * @param string $number
    */
   public static function formatPhoneNumber($number)
   {
      $number = trim($number);
      if(empty($number))
         return false;

      if(!self::validatePhoneNumber($number))
         return false;
            
      $formatted = "";
      if(!empty($number["CountryPrefix"]))
         $formatted .= $number["CountryPrefix"]."-";
      if(!empty($number["AreaCode"]))
         $formatted .= $number["AreaCode"]."-";
      $formatted .= $number["Switch"]."-".$number["Line"];

      return $formatted;
   }
}