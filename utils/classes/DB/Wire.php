<?php

namespace Mumby\DB;
use Exception;

class Wire extends DBObject
{
    public function __construct($id=null)
    {
        // The following variables SHOULD be non-null in the child class.
        $this->sourceTable      = "Wires";
        $this->idCol            = "WireID";

        // Don't let anyone change user IDs.
        $this->readOnlyFields   = array("WireID");

        $this->fieldInfo = array(
            "WireID"     => array("type"=>self::INTEGER,),
            "WireName"   => array("type"=>self::STRING, "length" => 64),
            "IsDefault"  => array("type"=>self::INTEGER,),
            "WireOrder"  => array("type"=>self::INTEGER,)
        );
        
        parent::__construct($id);
        $this->errorMessages = array();
        if(!empty($id) && !$this->checkRowInstance())
        {
            throw new Exception("Unable to create new ".get_called_class().". It appears you passed an invalid id value.");
        }
    }
    
    function getWires()
    {
        return $this->query("SELECT * FROM Wires");
    }
    
    function getDefaultWires()
    {
        return $this->query("SELECT * FROM Wires WHERE IsDefault = 1 ORDER BY WireOrder");
    }
    
    function updateWires($data)
    {   
        if ( !$this->clearMissingWires($data) ) {
            return false;
        }
        $results = array();

        if ( !is_array($data) )
            return $results;
        foreach($data as $d) {
            $result = $this->insertOrUpdate($d);
            if ( $result === false ) return false;
            else $results[] = $result;
        }
        return $results;
    }
    
    function clearMissingWires($data)
    {
        if ( !is_array($data) ) {
            $data = array();
        }
        $wires = $this->getWires();

        if ( is_array($wires) ) {
            foreach ( $wires as $w ) {
                $delete = true;
                foreach ($data as $k=>$d) {
                    if ( $d['WireID'] === $w['WireID'] ) {
                        $delete = false;
                        break;
                    }
                }
                if ( $delete ) {
                    try {
                        $this->delete($w['WireID']);
                    } catch(Exception $e) {
                        $this->errorMessages[] = "Delete failed. The wire you attempted to delete a wire that's currently in use.";
                        return false;
                    }
                }
            }
        }
        return true;
    }
    
    function updateWireOrder($data)
    {
        $sql = array();
        $wireData = array();
        foreach ( $data as $k=>$d ) {
            $sql[] = "UPDATE Wires SET WireOrder = :WireOrder WHERE WireID = :WireID";
            $wireData[] = array(
                "WireID" => $d,
                "WireOrder" => $k
            ); 
        }
        
        return $this->multiQuery($sql, $wireData);

    }
    
    public function getErrors() {
        $errors = "";
        foreach( $this->errorMessages as $message ) {
            $errors .= $message . "<br />";
        }
        return $errors;
    }
}

