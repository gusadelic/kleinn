<?php
namespace Mumby\DB;

use Exception;

class EmailAddress extends DBObject
{
  public function __construct($id=null)
  {
    // The following variables SHOULD be non-null in the child class.
    $this->sourceTable      = "EmailAddresses";
    $this->idCol            = "EmailAddressID";
    
    // Don't let anyone change user IDs.
    $this->readOnlyFields   = array("EmailAddressID");
    
    $this->fieldInfo = array(
      "UserID"            => array("type"=>self::INTEGER),
      "EmailAddress"      => array("type"=>self::STRING, "length"=>255),
      "EmailNotes"        => array("type"=>self::STRING, "length"=>512),
      "IsPrimaryAddress"  => array("type"=>self::BOOLEAN)
    );
    
    $this->requiredFields = array(
      "UserID",
      "EmailAddress"
    );

    parent::__construct($id);
    
    if(!empty($id) && !$this->checkRowInstance())
    {
      throw new Exception("Unable to create new ".get_called_class().". It appears you passed an invalid id value.");
    }
  }

  public function getPrimaryEmailAddress($UserID)
  {
    $sql = "SELECT EmailAddress FROM EmailAddresses WHERE UserID = :UserID AND IsPrimaryAddress = 1";
    $result = $this->query($sql, array("UserID" => $UserID));
    return $result;
  }
   
  /**
  * Validate whether a string is a valid email address.
  * 
  * @param string $emailAddress The string being tested.
  * @return bool True if a valid email address, false otherwise.
  */
  public static function validateEmailAddress($emailAddress)
  {
    return filter_var($emailAddress, FILTER_VALIDATE_EMAIL);
  }
   
  public function checkForUniqueness($emailAddress) {
    if ( $this->find(array('EmailAddress' => $emailAddress)) )
      return true;
    else return false;
  }
   
}