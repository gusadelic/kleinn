<?php
namespace Mumby\DB;
use GuzzleHttp\Client;
use RequestException;


class ConnectedApps extends DBObject
{
   var $app;
   var $token;
   
   public function __construct($id=null, $dbHost=null, $dbName=null, $dbUser=null, $dbPass=null)
   {
      $this->sourceTable = "ConnectedApps";

      $this->fieldInfo = array(
         "AppID"            => array("type"=>self::INTEGER),
         "AppName"      => array("type"=>self::STRING, "length"=>256),
         "AppClientID" => array("type"=>self::STRING, "length"=>64),
         "AppSecret" => array("type"=>self::STRING, "length"=>64),
         "AppState" => array("type"=>self::STRING, "length"=>64),
         "AppCodeRequestURI" => array("type"=>self::STRING, "length"=>512),
         "AppTokenRequestURI" => array("type"=>self::STRING, "length"=>512)
      );
      
      $this->requiredFields = array(
         "AppID",
         "AppName",
         "AppClientID",
         "AppSecret",
         "AppState",
         "AppCodeRequestURI",
         "AppTokenRequestURI"
      );

      parent::__construct(null, $dbHost, $dbName, $dbUser, $dbPass);
      if ( !empty($id) ) $this->app = $this->getApp($id);
      $this->token = $this->getToken($id);
   }
   
   function getAllApps() {
       $sql = "SELECT *, p.PropertyConstant AS Constant FROM ConnectedApps a ";
       $sql .= "INNER JOIN ApplicationProperties p ON a.AppConfigID = p.ApplicationPropertyID ";
       $sql .= "AND p.ApplicationID = :ApplicationID ";
       return $this->query($sql, array( "ApplicationID" => _MB_WEB_APPLICATION_ID_)); 
   }
   
   function getApp($appID) {
       $sql = "SELECT *, p.PropertyConstant AS Constant FROM ConnectedApps a ";
       $sql .= "INNER JOIN ApplicationProperties p ON a.AppConfigID = p.ApplicationPropertyID ";
       $sql .= "WHERE a.AppID = :AppID";
       $params = array( 'AppID' => $appID );
       $result = $this->query($sql, $params);
       return $result[0];
   }
   
   function getConstant($appID) {
       if ( !empty($this->app['Constant']) ) return $this->app['Constant'];
       $this->app = $this->getApp($appID);
       return $this->app['Constant'];
   }
   
   function getToken($appID) {
       //error_log("Getting Token!");
       //if ( $this->checkToken() ) {
       //    return $this->token;
       //}
       $params = array("AppID" => $appID );
       $sql = "SELECT * FROM ConnectedApps a ";
       $sql .= "INNER JOIN ConnectedApps_Tokens b ON a.AppID = b.AppID ";
       $sql .= "WHERE a.AppID = :AppID";
       $token = $this->query($sql, $params);
       $this->token = $token[0];
       if ( $this->checkToken() ) {
		return $this->token;
       }
       else return false;
   }

   function checkToken() {
	//error_log("Checking Token!");
	if ( empty($this->token) ) return false;
        if ( time() > $this->token['AppTokenExpire'] && $this->token['AppGrantType'] === "authorization_code" ) {
        	return $this->refreshToken();
        }

	return true;

   }
   
    public static function getTokenFromDatastore($appID)
   {
      $db = new GenericDB(_MB_CENTRAL_DATASTORE_HOST_, _MB_CENTRAL_DATASTORE_NAME_, _MB_CENTRAL_DATASTORE_USER_, _MB_CENTRAL_DATASTORE_PASS_);
       $params = array("AppID" => $appID );
       $sql = "SELECT * FROM ConnectedApps a ";
       $sql .= "INNER JOIN ConnectedApps_Tokens b ON a.AppID = b.AppID ";
       $sql .= "WHERE a.AppID = :AppID";
       $token = $db->query($sql, $params);
       if ( !empty($token) ) {
           return $token[0];
       }
       else return false;
   }
   
   function updateToken($data) {
       //error_log("Updating Token!");
       $result = $this->insertOrUpdate($data, 'ConnectedApps_Tokens');
       //error_log( print_r($result, true) );
       return $result;
   }
   
   function addApp($data) {
        $configData = array(
            'ApplicationID' => _MB_WEB_APPLICATION_ID_,
            'PropertyName' => $data['AppName'],
            'PropertyDescription' => "OAuth 2.0 API for " . $data['AppName'],
            'PropertyConstant' => $data['AppPropertyConstant'],
            'PropertyValue' => 0,
            'IsRequiredBySystem' => 0
        );
       
        $configID = $this->insert($configData, 'ApplicationProperties', true);

        $addData = array( 
            'AppName' => $data['AppName'],
            'AppClientID' => $data['AppClientID'],
            'AppSecret' => $data['AppSecret'],
            "AppGrantType" => $data['AppGrantType'],
            'AppState' => $data['AppState'],
            'AppCodeRequestURI' => $data['AppCodeRequestURI'],
            'AppTokenRequestURI' => $data['AppTokenRequestURI'],
            'AppConfigID' => $configID
        );

        $appID = $this->insert($addData, 'ConnectedApps', true);
       
        return $this->update(array('PropertyValue' => $appID ), array('ApplicationPropertyID' => $configID) ,'ApplicationProperties');  
       
   }
    // Deletes an App - Due to Referential Integrity in the DB, 
    //                  we delete the ApplicationProperties entry for the App to delete the app.
    function deleteApp($id = null) {
        if ( !empty($id) && empty($this->app['AppConfigID']) ) 
            $this->app = $this->getApp($id);
            
        $target = $this->app['AppConfigID'];

        return $this->delete(array('ApplicationPropertyID' => $target), "ApplicationProperties");
        
    }
   
    function getAuthCell($app) {
	$this->app = $app;
        $token = $this->getToken($app['AppID']);
        if ( $token != false ) {
            if ( empty($token['AppRefreshToken']) ) {
                return '<div class="label label-success">Token: ' . $token['AppToken'] . '</div>';
            }
            else if ( time() > $token['AppTokenExpire'] ) {
                return '<div class="label label-danger">Token Expired!</div> <button style="btn-warning  btn-xs btn-block" onClick="refreshToken(\'' . $app['AppID'] . '\', $(this))">Refresh</button>';
            }
            else {
                return '<div class="label label-success">Token: ' . $token['AppToken'] . '</div> <div>Expires: ' . date("H:m:s m/d/Y", $token['AppTokenExpire']) . '</div>';
            }        
        }
        else {
            $url = "";
            if ( $app['AppGrantType'] === "authorization_code" ) {
                $url = $app['AppCodeRequestURI'] . '?response_type=code&client_id=' . $app['AppClientID'] . '&redirect_uri=http://' . _MB_APP_DOMAIN_ . '/app/apps/auth&state=' . $app['AppState'];
            }
            else if ( $app['AppGrantType'] === "client_credentials" ) {
                $url = 'http://' . _MB_APP_DOMAIN_ . '/app/apps/auth?client_id=' . $app['AppClientID'] . '&client_secret=' . $app['AppSecret'] . '&grant_type=' . $app['AppGrantType'] . '&state=' . $app['AppState'];
            }
            $app['URL'] = $url;            
            $data = json_encode($app);
            return '<script>var data = \'' . $data . '\';</script><button style="btn-primary btn-xs btn-block" onClick="authApp(data, $(this))">Request Authorization</button>';
        }
        
    }
    
    function requestToken($id, $data) {
        //$app = $this->getApp($id);
        $app = $this->app;
        $client = new Client(['base_uri' => $app['AppTokenRequestURI']]);
        if ( $app['AppGrantType'] === "authorization_code" ) {
        
            $postData = 
                [
                "code"  => $data['code'],
                "client_id" => $app['AppClientID'],
                "client_secret" => $app['AppSecret'],
                "grant_type"    => $app['AppGrantType'],
                "redirect_uri"  => 'http://' . _MB_APP_DOMAIN_ . '/app/apps/auth'
                ];
	try {
            $request = $client->request("POST", $app['AppTokenRequestURI'], $postData, ['http_errors' => false, 'exceptions' => false]);
	} catch (RequestException $e) {
	    echo 'Uh oh! ' . $e->getMessage();
	}            
            $request = $client->request("POST", $app['AppTokenRequestURI'], $postData, ['http_errors' => false, 'exceptions' => false]);
            $result = $request->getBody()->read(1024);
        }
        else if ( $app['AppGrantType'] === "client_credentials" ) {
        
            $postData = 
                [
                "client_id" => $app['AppClientID'],
                "client_secret" => $app['AppSecret'],
                "grant_type"    => $app['AppGrantType'],
                "redirect_uri"  => 'http://' . _MB_APP_DOMAIN_ . '/app/apps/auth'
                ];

            $request = $client->request("GET", $app['AppTokenRequestURI'] . "?" . http_build_query($postData), ['http_errors' => false, 'exceptions' => false]);
            $result = '{"' . strtok($request->getBody()->read(1024), '=' ) . '":"' . strtok('=') . '"}';
        }

        $data = json_decode($result, true);
        
        if ( empty($data['access_token']) ) {
            return $data;
        }

        if ( empty($data['token_type']) ) {
            $data['token_type'] = "";
        }
        
        $update = array(
                "AppID" => $app['AppID'],
                "AppToken" => $data['access_token'],
                "AppTokenType" => $data['token_type']
        );
          
        
        if ( !empty( $data['refresh_token'] ))
            $update["AppRefreshToken"] = $data['refresh_token'];
        
        
        if ( empty($data['expires_in']) || $data['expires_in'] === 0 || empty($data['expires']) ) {
            $update["AppTokenExpire"] = 0;
        }
        else $update["AppTokenExpire"] = $data['expires'];
        
        return $this->updateToken($update);    
        
    }
    
    function refreshToken() {
	error_log("Refeshing Token!");
        $postData = [
            'form_params' => [
                "refresh_token" => $this->token['AppRefreshToken'],
                "client_id"     => $this->token['AppClientID'],
                "client_secret" => $this->token['AppSecret'],
                "grant_type"    => "refresh_token"
            ]
        ];

        $client = new Client(['base_uri' => $this->token['AppTokenRequestURI']]);
        //$request = $client->request("POST", $app['AppTokenRequestURI'],$postData, ['exceptions' => false]);
	try {
        	$request = $client->request("POST", $this->token['AppTokenRequestURI'],$postData);
    	} catch (RequestException $e) {
		error_log('Uh oh! ' . $e->getMessage());
		dump('Uh oh! ' . $e->getMessage());
		return false;
	}
        $data = json_decode($request->getBody()->read(1024), true);

        $update = array(
            "AppID" => $this->app['AppID'],
            "AppToken" => $data['access_token'],
            "AppTokenExpire" => $data['expires'],
            "AppTokenType" => $data['token_type']
        );
        if ( !empty($data['refresh_token']) ) {
           $update['AppRefreshToken'] = $data['refresh_token']; 
        }

        if ( $this->updateToken($update) ) {
            //$this->token = $this->getToken($this->token['AppID']);
            return true;
        }
	else return false;
    }

}