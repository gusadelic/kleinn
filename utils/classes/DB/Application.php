<?php
namespace Mumby\DB;

use Exception;

class Application extends DBObject
{
   public function __construct($id=null)
   {
      // The following variables SHOULD be non-null in the child class.
      $this->sourceTable      = "Applications";
      $this->idCol            = "ApplicationID";
      
      $this->fieldInfo = array(
         "ApplicationName"  => array("type"=>self::STRING, "length"=>255),
         "ApplicationDesc"  => array("type"=>self::STRING, "length"=>1024)
      );

      parent::__construct($id);
      
      if(!empty($id) && !$this->checkRowInstance())
      {
         throw new Exception("Unable to create new ".get_called_class().". It appears you passed an invalid id value.");
      }
   }
   
   public function getApplicationProperties($appID=null)
   {
      if(empty($appID))
         $appID = _MB_WEB_APPLICATION_ID_;
      return $this->find(array("ApplicationID"=>$appID), "ApplicationProperties");
   }
   
   public function getAllApplications()
   {
      return $this->find();
   }
   
   public function getApplicationProperty($propID, $appID=null)
   {
      if(empty($appID))
         $appID = _MB_WEB_APPLICATION_ID_;
      
      return $this->find(array("ApplicationPropertyID"=>$propID, "ApplicationID"=>$appID), "ApplicationProperties");
   }
   
   public function addApplicationProperty($name, $desc, $const, $val, $appID=null)
   {
      if(empty($appID))
         $appID = _MB_WEB_APPLICATION_ID_;

      $data = array(
         "PropertyName" => $name,
         "PropertyDescription" => $desc,
         "PropertyConstant" => $const,
         "PropertyValue" => $val,
         "ApplicationID" => $appID,
         "IsRequiredBySystem" => 0
      );
      
      return $this->insert($data, "ApplicationProperties");
   }
   
   public function updateApplicationProperty($id, $name, $desc, $val, $appID=null)
   {
      if(empty($appID))
         $appID = _MB_WEB_APPLICATION_ID_;
      
      $data = array(
         "PropertyName" => $name,
         "PropertyDescription" => $desc,
         "PropertyValue" => $val,
         "ApplicationID" => $appID
      );
      
      return $this->update($data, array("ApplicationPropertyID"=>$id), "ApplicationProperties");
   }
   
   public function deleteProperty($id, $appID=null)
   {
      if(empty($appID))
         $appID = _MB_WEB_APPLICATION_ID_;
      
      return $this->delete(array("ApplicationPropertyID"=>$id, "ApplicationID"=>$appID), "ApplicationProperties");
   }
   
}