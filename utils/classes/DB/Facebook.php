<?php
namespace Mumby\DB;
use GuzzleHttp\Client;
use RequestException;
//use Facebook;

class Facebook extends ConnectedApps
{
    
   public function __construct($appID = null, $clientID = null, $clientSecret = null, $graphVersion = "v2.5")
   {
      
       
      $this->uri = "https://graph.facebook.com";

      parent::__construct($appID);
      
      if ( empty($clientID) ) $clientID = $this->app['AppClientID'];
      if ( empty($clientSecret) ) $clientSecret = $this->app['AppSecret'];

// This new long-lived access token will expire on June 10th, 2017:
// Refresh and Extend Token here
// https://developers.facebook.com/tools/accesstoken/
      //$this->UserAccessToken = "EAAIwZBDLDxIEBANX9JxI94SzjdNLXZCk7ZCtP4TFrZBjt047dYbJ9AIjxJNZBf5UM42Ip5J79tKIaGVzuLKgxVZCBh7W35DaucdVtZBEdy4qEZB9O5sEkG8dlgGVm8lipzbGXYGGriwzMq95C44XQWIZC";
      $this->UserAccessToken = "EAAIwZBDLDxIEBADUXayU34ZCvf6wOZB9v2OFin5SOXczQ5WEdz0Q2I3sLGvR2LEldzYSvd0Or9OFHLgDJl1hNDIN2bSZC1NR5Q4XCW3SsX2vX2oHKY9K2ZCW4ToZAZBDqcBn9K9fHrN8CpDQKMzBvnaR3bXvJrCnWEZD";

//    $fb = new \Facebook\Facebook([
//        'app_id' => $clientID,
//        'app_secret' => $clientSecret,
//        'default_graph_version' => $graphVersion
//    ]);
    
    
      
   }
    
    function getEvents($id) {
        $data = array(
            "fields" => "id,name,category,description,cover,start_time,end_time",
            "access_token" => $this->token['AppToken']
        );
        
        $client = new Client(['base_uri' => $this->uri]);

        //$request = $client->get("/v2.5/".$id."/events?" . http_build_query($data),['http_errors' => false, 'exceptions' => false]);
        $response = $client->request("GET","/v2.8/".$id."/event?" . http_build_query($data),['http_errors' => false, 'exceptions' => false]);

        $data = json_decode($response->getBody()->read(32768), true);

        return $data;
    }
    
    function getGroupFeed($id, $since="", $limit="" ) {
        $data = array(
            "since" => $since,
            "limit" => $limit,
            "fields" => "full_picture,message,description,link",
            "access_token" => $this->token['AppToken']
        );
        
        $client = new Client(['base_uri' => $this->uri]);

        //$request = $client->get("/v2.5/".$id."/feed?" . http_build_query($data),['http_errors' => false, 'exceptions' => false]);
        $response = $client->request("GET","/v2.5/".$id."/feed?" . http_build_query($data),['http_errors' => false, 'exceptions' => false]);
        //$response = $request->send();

        $data = json_decode($response->getBody()->read(16384), true);

        return $data;
    
    }
    
    function addGroupSlides($id, $slideshow, $since="", $limit="") {
        $data = $this->getGroupFeed($id, $since, $limit);
        if (!empty($data['data']) && is_array($data['data'])) {
            foreach ( $data['data'] as $d ) {
                if ( empty($d['full_picture']) ) { continue; }
		if ( empty($d['description'] ) ) $d['description'] = "";
                if ( empty($d['message'] ) ) $d['message'] = "";
                $slideshow->addTempSlide($d['message'], $d['description'], $d['link'],$d['full_picture'] );
            } 
        }
        
    }
//$date("Y-m-d", strtotime('-1 week')
}