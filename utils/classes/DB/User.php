<?php
namespace Mumby\DB;

use Exception;

class User extends DBObject
{
   public function __construct($id=null)
   {
      // The following variables SHOULD be non-null in the child class.
      $this->sourceTable      = "Users";
      $this->idCol            = "UserID";
      
      // Don't let anyone change user IDs.
      $this->readOnlyFields   = array("UserID");
      
      $this->fieldInfo = array(
         "UAID"          => array("type"=>self::STRING),
         "NetID"         => array("type"=>self::STRING),
         "FirstName"     => array("type"=>self::STRING),
         "MiddleName"    => array("type"=>self::STRING),
         "LastName"      => array("type"=>self::STRING),
         "PreferredName" => array("type"=>self::STRING),
         "IsActive"      => array("type"=>self::BOOLEAN)
      );
      
      parent::__construct($id);
      
      if(!empty($id) && !$this->checkRowInstance())
      {
         throw new Exception("Unable to create new ".get_called_class().". It appears you passed an invalid id value.");
      }
   }
   
   public function insert($data, $tableName=null, $throwExceptions=false)
   {
      // Make sure they've included the minimally required fields.
      if(empty($tableName))
      {
         if(!isset($data["FirstName"]))
         {
            throw new Exception("You must specify a first name when inserting a new user.");
            return false;
         }
         if(!isset($data["LastName"]))
         {
            throw new Exception("You must specify a last name when inserting a new user.");
            return false;
         }
      }
      
      return parent::insert($data, $tableName, $throwExceptions);
   }
   
   protected function getUserInfo($field, $val)
   {
      $sql  = "SELECT ";
      $sql .= "*, ";
      $sql .= "CASE WHEN U.PreferredName = '' THEN CONCAT(U.FirstName, ' ', U.LastName) ELSE CONCAT(U.PreferredName, ' ', U.LastName) END AS FullName, "; 
      $sql .= "CASE WHEN U.PreferredName = '' THEN CONCAT(U.LastName, ', ', U.FirstName) ELSE CONCAT(U.LastName, ', ', U.PreferredName) END AS ReverseFullName ";
      $sql .= "FROM Users U ";
      $sql .= "WHERE U.".$field."=:".$field." LIMIT 1";
      $data = array($field => $val);

      $person = $this->query($sql, $data);
      if(!$person)
         return false;
      
      $thisUser = $person[0];

      $phones = $this->getUserPhoneNumbers($thisUser["UserID"]);
      if($phones)
         $thisUser["phones"] = $phones;
      else
         $thisUser["phones"] = array();

      $emailAddresses = $this->getUserEmailAddresses($thisUser["UserID"]);
      if($emailAddresses)
         $thisUser["emailAddresses"] = $emailAddresses;
      else
         $thisUser["emailAddresses"] = array();
      
      return $thisUser;
   }
   
   public function getUserByID($UserID)
   {
      return $this->getUserInfo("UserID", $UserID);
   }
   
   public function getUserByNetID($netid)
   {
      return $this->getUserInfo("NetID", $netid);
   }

   public function getUserByName($lastName, $firstName="")
   {
      $sql = "SELECT UserID FROM Users WHERE LastName=:LastName";
      $data = array("LastName" => $lastName);
      
      if(!empty($firstName))
      {
         $sql .= " AND FirstName=:FirstName";
         $data["FirstName"] = $firstName;
      }

      $person = $this->query($sql, $data);
      if($person)
      {
         if(count($person) == 1)
         {
            return $this->getUserByID($person[0]["UserID"]);
         }
         else
            return false;
      }
      else
         return false;
   }
   
   public function getUsersByName($lastName, $firstName="")
   {
      $sql = "SELECT UserID FROM Users WHERE LastName=:LastName";
      $data = array("LastName" => $lastName);
      
      if(!empty($firstName))
      {
         $sql .= " AND FirstName=:FirstName";
         $data["FirstName"] = $firstName;
      }

      $people = $this->query($sql, $data);
      if(!empty($people))
      {
         $allPeople = array();
         foreach($people as $p)
         {
            $allPeople = $this->getUserByID($p["UserID"]);
         }
         return $allPeople;
      }
      else
         return false;
   }
   
   public function findUser($userInfo)
   {
      $thisUser = null;

      if(!is_array($thisUser) && array_key_exists("NetID", $userInfo) && !empty($userInfo["NetID"]))
         $thisUser = $this->getUserByNetID($userInfo["NetID"]);
      
      if(!is_array($thisUser) && array_key_exists("LastName", $userInfo) && !empty($userInfo["LastName"]))
      {
         if(array_key_exists("FirstName", $userInfo))
            $thisUser = $this->getSingleUseByName($userInfo["LastName"], $userInfo["FirstName"]);
         else
            $thisUser = $this->getSingleUseByName($userInfo["LastName"]);
      }
      
      if(!is_array($thisUser))
         return false;
      else
         return $thisUser;
   }
   
   public function lookupCountryID($countryName)
   {
      $sql = "SELECT * FROM Countries WHERE CountryName=':country' OR UACountryName=':country' LIMIT 1";
      $data = array("country" => $countryName);
      return $this->query($sql, $data);
   }
   
   public function lookupCountryName($countryID)
   {
      $sql = "SELECT * FROM Countries WHERE CountryID=':country' LIMIT 1";
      $data = array("country" => $countryID);
      return $this->query($sql, $data);
   }
   
   public function addEmailAddress($emailAddressData)
   {
      static $emailMgr = null;
      if(null === $emailMgr)
      {
         $emailMgr = new EmailAddress();
      }
      
      return $emailMgr->insertOrUpdate($emailAddressData);
   }
   
   public function getUserEmailAddresses($id=null)
   {
      if(empty($id))
      {
         if(!$this->checkRowInstance())
         {
            throw new Exception("You must specify a UserID in order to query user email addresses.");
            return false;
         }
         else
            $id = $this->id;
      }
      
      $sql  = "SELECT ";
         $sql .= "* ";
      $sql .= "FROM EmailAddresses ";
      $sql .= "WHERE UserID=:UserID";
      
      $data = array("UserID"=>$id);
      
      return $this->query($sql, $data);
   }

   public function addPhoneNumber($phoneNumberData)
   {
      static $phoneMgr = null;
      if(null === $phoneMgr)
      {
         $phoneMgr = new Phone();
      }

      return $phoneMgr->insertOrUpdate($phoneNumberData);
   }
   
   public function getUserPhoneNumbers($id=null)
   {
      if(empty($id))
      {
         if(!$this->checkRowInstance())
         {
            throw new Exception("You must specify a UserID in order to query phone numbers.");
            return false;
         }
         else
            $id = $this->id;
      }
      
      $sql  = "SELECT ";
         $sql .= "P.CountryPrefix, P.AreaCode, P.Switch, P.Line, ";
         $sql .= "CONCAT(P.CountryPrefix, P.AreaCode, P.Switch, P.Line) AS FullPhoneNumber, ";
         $sql .= "PT.PhoneNumberTypeName, PT.PhoneNumberTypeID, PT.IsPublic, ";
         $sql .= "P.PhoneNotes ";
      $sql .= "FROM PhoneNumbers P ";
      $sql .= "INNER JOIN PhoneNumberTypes PT ON P.PhoneNumberTypeID=PT.PhoneNumberTypeID ";
      $sql .= "INNER JOIN Users U ON P.UserID=U.UserID ";
      $sql .= "WHERE U.UserID=:UserID";
      
      $data = array("UserID"=>$id);
      
      return $this->query($sql, $data);
   }
   
}