<?php
namespace Mumby\DB;

class GenericDB extends DBObject
{
   public function __construct($dbHost = null, $dbName = null, $dbUser = null, $dbPass = null) {
      parent::__construct(null, $dbHost, $dbName, $dbUser, $dbPass);
   }
}