<?php
namespace Mumby\DB;

use Exception;

class Tipsheet extends DBObject
{
    public function __construct($id=null)
    {
       // The following variables SHOULD be non-null in the child class.
       $this->sourceTable      = "Tipsheets";
       $this->idCol            = "SKU";

       // Don't let anyone change user IDs.
       //$this->readOnlyFields   = array("SKU");

       $this->fieldInfo = array(
            "SKU"           => array("type"=>self::STRING, "length" => 32),
            "Name"          => array("type"=>self::STRING, "length" => 128),
            "Description"   => array("type"=>self::STRING, "length" => 1024),
            "Price"         => array("type"=>self::INTEGER,),
            "RSID"          => array("type"=>self::INTEGER,),
            "BypassID"      => array("type"=>self::INTEGER,),
            "OtherCompID"   => array("type"=>self::INTEGER,)
       );

       parent::__construct($id);

       $this->SKU = $id;

       if ( $id !== null ) 
           $this->tipsheet = $this->getTipsheet($id, true);
       
//       $this->wireOrder = array(
//           "12 VOLT CONSTANT", "STARTER", "STARTER 1", "STARTER 2",
//           "IGNITION 1", "IGNITION 2", "IGNITION 3", "ACCESSORY/HEATER BLOWER 1", "ACCESSORY/HEATER BLOWER 2",
//           "BRAKE", "PARKING LIGHTS ( + )", "PARKING LIGHTS ( - )", "TACH", "POWER LOCK", "POWER UNLOCK", 
//           "FACTORY ALARM DISARM", "TRUNK RELEASE",  "WAIT TO START LIGHT", "KEYSENSE", "DOOR TRIGGER",
//           "DOMELIGHT SUPERVISION", "ANTI-THEFT", "LOCK MOTOR WIRE");
        
       $this->errorMessages = array();
       if(!empty($id) && !$this->checkRowInstance())
       {
            throw new Exception("Unable to create new ".get_called_class().". It appears you passed an invalid id value.");
       }
    }

    function getAllTipsheets()
    {
        $sql  = "SELECT * FROM ".$this->sourceTable." ORDER BY Name";
        $result = $this->query($sql);
        
        if ( is_array($result) ) {
            foreach( $result as $i=>$r ) {
                $result[$i]['Price'] = number_format(($r["Price"] /100), 2, '.', '');
            }
        }
        return $result;
    }

    function getAllTipsheetSections($byCategory = false)
    {
        $sql  = "SELECT P.PageID, P.PageTitle ";
        if ( $byCategory ) {
            $sql .= ", SCP.SectionCategoryID, SC.Name AS CategoryName ";
        }
        
        $sql .= "FROM "._MB_DB_NAME_.".Pages P ";
        if ( $byCategory ) {
            $sql .= "LEFT JOIN "._MB_DB_NAME_.".SectionCategory_Pages SCP ON SCP.PageID = P.PageID ";
            $sql .= "LEFT JOIN "._MB_DB_NAME_.".SectionCategories SC ON SC.SectionCategoryID = SCP.SectionCategoryID ";
        }
        $sql .= "WHERE P.IsPublished = 1 AND P.IsDeleted = 0 AND P.PageTypeID = 2 ";
        $sql .= "ORDER BY P.PageTitle";
        $result = $this->query($sql);
        $output = array();
        if ( is_array($result) ) {
            if ( $byCategory ) {
                foreach ($result as $r) {
                    if( is_numeric($r['SectionCategoryID']) ) {
                        $output[$r['SectionCategoryID']][$r['PageID']]=$r;
                    }
                    else {
                        $r['CategoryName'] = "Uncategorized";
                        $output[0][$r['PageID']]=$r;
                    }
                }            
            }
            else {
                foreach ($result as $r) {
                    $output[$r['PageID']]=$r['PageTitle'];
                }
            }
        }
        else return false;
        
        
        
        return $output;
    }
    
    function getTipsheetVehicles($SKU)
    {
        $sql  = "SELECT V.* FROM Vehicles V ";
        $sql .= "INNER JOIN Tipsheet_Vehicles TV ON TV.VehicleID = V.VehicleID ";
        $sql .= "WHERE SKU = :SKU ";
        $sql .= "ORDER BY V.VehicleID ASC ";
        
        $params = array( "SKU" => $SKU );
        
        $vehicles = $this->query($sql, $params);
        
        if ( is_array($vehicles) ) {
            return $vehicles;
        }
        else return false;
    }
    
    function getTipsheetWiring($SKU)
    {
        $sql  = "SELECT W.WireName, TW.* ";
        $sql .= "FROM Tipsheet_Wiring TW ";
        $sql .= "INNER JOIN Wires W ON W.WireID = TW.WireID ";
        $sql .= "WHERE TW.SKU = :SKU ";
        $sql .= "ORDER BY TW.Order ASC ";
        
        $params = array ( "SKU" => $SKU );
        
        return $this->query($sql, $params);
    }
    
    function getTipsheet($SKU, $includeSections = false, $byGroups = false)
    {
        $sql  = "SELECT * FROM ".$this->sourceTable." WHERE SKU = :SKU";
        $data = array(
            "SKU" => $SKU
        );
        $result = $this->query($sql, $data);
        
        if ( $result === false ) return false; 
        
        if ( isset($result[0]['Price']) ) {
            $result = $result[0];
            $result['Price'] = number_format(($result["Price"] /100), 2, '.', '');
        }
        
        if ( $includeSections ) {
            $layout = new \Mumby\WebTools\SectionLayout();
            if ( $byGroups ) {
                $sections = $layout->getSectionGroups(true, $SKU);
                $result['Sections'] = $sections;
            }
            else {
                $sections = $layout->getSections($SKU);
                $result['Sections'] = $sections;
            }
        } 

        return $result;
    }
    
    function findSKU($year, $make, $model, $RSID)
    {
        $sql  = "SELECT T.SKU FROM ".$this->sourceTable." T ";
        $sql .= "INNER JOIN Tipsheet_Vehicles TV ";
        $sql .=     "ON TV.VehicleID = (SELECT VehicleID FROM Vehicles WHERE Year = :year AND Make = :make AND Model = :model) ";
        $sql .= "WHERE T.RSID = :rsid ";
        
        $params = array(
            "year"  => $year,
            "make"  => $make,
            "model" => $model,
            "rsid"  => $RSID
        );
        $result = $this->query($sql, $params);
        
        if ( $result != false ) return $result['SKU'];
        else return false;
    }
    
    function uniqueSKU($SKU = null)
    {
        if ( $SKU === null )
            $SKU = $this->SKU;
        
        $params = array( "SKU" => $SKU);    
        return !$this->find($params);
    }
    
    function isEvo($ComponentID = null)
    {
        if ( $ComponentID == null )
            $ComponentID = $this->tipsheet['BypassID'];
        
        $compDB = new Component();
        $comp = $compDB->getComponent($ComponentID);
        return (boolean)preg_match("/Evo/",$comp['ComponentName']);
    }
    
    function updateTipsheet($TipsheetData)
    {
        return $this->insertOrUpdate($TipsheetData);
    }
    
    function saveSections($SKU, $sections)
    {
        if ( !is_array($sections) )
            return false;
        
        $delCount = $this->delete($SKU, "Tipsheet_Sections");
        
        foreach ( $sections as $k=>$s ) {
            $thisTS = array(
                'SKU' => $SKU,
                'PageID' => $s,
                'SectionOrder' => $k
            );
            $result = $this->insert($thisTS,"Tipsheet_Sections");
            if ( !$result ) {
                return false;
            }
        }
        
        return true;    
    }
    
    function saveVehicles($SKU, $vehicles)
    {
        if ( !is_array($vehicles) )
            return false;
        
        $delCount = $this->delete($SKU, "Tipsheet_Vehicles");
        
        $vehicleDB = new Vehicle();
        
        foreach ( $vehicles as $k=>$v ) {
            if ( $v['Model'] === "*") {
                $models = $vehicleDB->getModels($v['Make']);
                foreach ( $models as $m ) {
                    $years = $vehicleDB->getYears($m);
                    foreach ( $years as $y ) {
                        $vID = $vehicleDB->findVehicle($y, $v['Make'], $m);
                        if ( !is_numeric($vID) ) {
                            throw new Exception("Failed to find a mathcing VehicleID for '" . $v['Year'] . " " . $v['Make'] . " " . $v['Model']."'!");
                        }
                        $thisV = array(
                            'SKU' => $SKU,
                            'VehicleID' => $vID
                        );
                        if ( !$this->insert($thisV,"Tipsheet_Vehicles") ) {
                            return false;
                        }
                    }
                }
            }
            else if ($v['Year'] === "*") {
                $years = $vehicleDB->getYears($v['Model']);
                foreach ( $years as $y ) {
                    $vID = $vehicleDB->findVehicle($y, $v['Make'], $v['Model']);
                    if ( !is_numeric($vID) ) {
                        throw new Exception("Failed to find a mathcing VehicleID for '" . $v['Year'] . " " . $v['Make'] . " " . $v['Model']."'!");
                    }
                    $thisV = array(
                        'SKU' => $SKU,
                        'VehicleID' => $vID
                    );
                    if ( !$this->insert($thisV,"Tipsheet_Vehicles") ) {
                        return false;
                    }
                }
            }
            else {
                $vID = $vehicleDB->findVehicle($v['Year'], $v['Make'], $v['Model']);
                if ( !is_numeric($vID) ) {
                    throw new Exception("Failed to find a mathcing VehicleID for '" . $v['Year'] . " " . $v['Make'] . " " . $v['Model']."'!");
                }
                $thisV = array(
                    'SKU' => $SKU,
                    'VehicleID' => $vID
                );
                if ( !$this->insert($thisV,"Tipsheet_Vehicles") ) {
                    return false;
                }        
            }

        }
        
        return true;    
    }
    
    function saveWiring($SKU, $wiring)
    {
        if ( empty($wiring) ) {
            $this->delete($SKU, "Tipsheet_Wiring");
            return true;
        }
        if (array_search("", $wiring) ) {
            $this->errorMessages[] = "Blank Wires are not allowed.";
            return false;    
        }
        $noDups = array_unique($wiring);
        if ( count($noDups) !== count($wiring) ) {
            $this->errorMessages[] = "Dupliacte Wires are not allowed.";
            return false;
        }
        $this->delete($SKU, "Tipsheet_Wiring");
        $sql = array();
        $params = array();
        foreach( $wiring as $i=>$w) {
            $sql[] = "INSERT INTO Tipsheet_Wiring (`WireID`, `SKU`, `Order`) VALUES (:WireID,:SKU,:Order)";
            $params[] = array(
                "WireID" => $w,
                "SKU"    => $SKU,
                "Order"  => $i
            );
        }
        return $this->multiQuery($sql, $params);
    }
    
    function getWireChartData($vehicle, $SKU=null, $includeTipsheet = false) 
    {
        if ( $SKU == null )
            $SKU = $this->SKU;
        
        if ( is_array($vehicle) ) {
            if ( isset($vehicle['VehicleID']) && is_numeric($vehicle['VehicleID'])) {
                $VehicleID = $vehicle['VehicleID'];
            }
            else if ( isset($vehicle['Year']) && isset($vehicle['Make']) && isset($vehicle['Model']) ) {
                $vehicleDB = new Vehicle();
                $VehicleID = $vehicleDB->findVehicle($vehicle['Year'], $vehicle['Make'], $vehicle['Model']);
            }
            else return false;
        }
        else {
            if ( is_numeric($vehicle) )
                $VehicleID = $vehicle;
            else return false;
        }
        
        $sql  = "SELECT DISTINCT ";
        $sql .= "RS.Color as 'Connection Color', W.WireName as 'Connection Name', RS.Harness, RS.Notes, ";
        $sql .= "VW.VehicleWireColor as 'Color', VW.VehicleWirePolarity as 'Polarity', VW.VehicleWireLocation as 'Location' ";
        $sql .= "FROM Tipsheets T ";
        // Ignore Tipsheet/Vehicle Associaton
        //$sql .= "INNER JOIN Tipsheet_Vehicles TV ON TV.SKU = :SKU AND TV.VehicleID = :VehicleID ";
        $sql .= "INNER JOIN RemoteStartWires RS ON RS.ComponentID = T.RSID ";
        $sql .= "INNER JOIN VehicleWires VW ON VW.VehicleID = :VehicleID AND RS.WireID = VW.WireID ";
        if ( $includeTipsheet )
            $sql .= "INNER JOIN Tipsheet_Wiring TW ON TW.SKU = :SKU AND TW.WireID = VW.WireID ";
        $sql .= "INNER JOIN Wires W ON W.WireID = RS.WireID ";
        $sql .= "WHERE T.SKU = :SKU ";
        
        if ( $includeTipsheet )
            $sql .= "ORDER BY TW.Order ASC ";
        else 
            $sql .= "ORDER BY W.WireOrder ASC ";
        
        $params = array(
            "SKU" => $SKU,
            "VehicleID" => $VehicleID
        );
        
        $results = $this->query($sql, $params);
        
        return $results;
    }
    
    function buildWirechart($data)
    {
        $output = "";
        
        $output .= "<div class='row'>";
        $output .= "<div class='col-xs-12'><center><span class='wirechartTitle'>".$data['Component']['ComponentName']." <span class='glyphicon glyphicon-arrow-right'></span> ".$data["Vehicle"]['Year'] . " " . $data["Vehicle"]['Make']. " " . $data["Vehicle"]['Model']."</span></center></div>";
        $output .= "</div>";
        
        // Sort output by default Wire Layout
        $vehicleDB = new Vehicle();
        //$wireOrder = $vehicleDB->getDefaultWiring();
        //foreach ( $wireOrder as $i=>$o ) {
            foreach ( $data['Wirechart'] as $w ) {
                //if ( $w['Connection Name'] !== $o ) continue;
                $output .= "<ul class='list-group'>";
                $output .= "        <li class='list-group-item'><span class='connectionName'>".$w['Connection Name']."</span>";
                $output .= "            <div class='row'>";
                $output .= "                <div class='well col-xs-5'>";
                $output .= "                    <span class='connectionColor'>".$w['Connection Color']."</span>";
                $output .= "                    <div class='row'>";
                $output .= "                        <div class='col-xs-6'>".$w['Harness']."</div>";
                $output .= "                        <div class='col-xs-6'>".$w['Notes']."</div>";
                $output .= "                     </div>";
                $output .= "                </div>";
                $output .= "                <div class='col-xs-1'>";
                $output .= "                            <span class='connectionArrow glyphicon glyphicon-arrow-right'></span>";
                $output .= "                </div>";
                $output .= "                <div class='well col-xs-6'>";
                $output .= "                    <span class='connectionColor'>".$w["Color"]."</span>";
                $output .= "                    <div class='row'>";
                $output .= "                        <div class='col-xs-6'>Polarity: ".$w['Polarity']."</div>";
                $output .= "                        <div class='col-xs-6'>".$w['Location']."</div>";
                $output .= "                    </div>";
                $output .= "                </div>";
                $output .= "            </div>";
                $output .= "        </li>";
                $output .= "</ul>";
            }
        //}
        
        return $output;
        

    }
    
    public function getErrors() {
        $errors = "";
        foreach( $this->errorMessages as $message ) {
            $errors .= $message . "<br />";
        }
        return $errors;
    }
}