<?php
namespace Mumby\DB;

use Exception;

class Injection extends DBObject
{
   public function __construct($id=null)
   {
      // The following variables SHOULD be non-null in the child class.
      $this->sourceTable      = "Injections";
      $this->idCol            = "InjectionID";
      
      $this->fieldInfo = array(
         "InjectionName"         => array("type"=>self::STRING, "length"=>255),
         "InjectionDesc"         => array("type"=>self::STRING, "length"=>1024),
         "InjectionVariableName" => array("type"=>self::STRING, "length"=>255),
         "InjectionValue"        => array("type"=>self::STRING, "length"=>1024),
         "InjectionCategoryID"   => array("type"=>self::INTEGER)
      );

      parent::__construct($id, _MB_CENTRAL_DATASTORE_HOST_, _MB_CENTRAL_DATASTORE_NAME_, _MB_CENTRAL_DATASTORE_USER_, _MB_CENTRAL_DATASTORE_PASS_);
      
      if(!empty($id) && !$this->checkRowInstance())
      {
         throw new Exception("Unable to create new ".get_called_class().". It appears you passed an invalid id value.");
      }
   }
   
   public function getInjection($varName)
   {
      $rows = $this->find(array("InjectionVariableName"=>$varName), null, 1);
      if(empty($rows))
         return false;

      return $rows[0];
   }
   
   public function getAllInjections()
   {
      $allSQL = "SELECT i.*, ic.InjectionCategoryName, ic.InjectionCategoryDesc FROM Injections i INNER JOIN InjectionCategories ic ON i.InjectionCategoryID=ic.InjectionCategoryID ORDER BY ic.InjectionCategoryName, i.InjectionName";
      return $this->query($allSQL);
   }

   public function getAllInjectionCategories()
   {
      $allSQL = "SELECT * FROM InjectionCategories ORDER BY `InjectionCategoryName`";
      return $this->query($allSQL);
   }

   public function addInjection($name, $desc, $variable, $value, $category)
   {
      $data = array
      (
         "InjectionName"         => $name,
         "InjectionDesc"         => $desc,
         "InjectionVariableName" => $variable,
         "InjectionValue"        => $value,
         "InjectionCategoryID"   => $category
      );

      $injectionID = $this->insert($data);
      if(!$injectionID)
         throw new Exception("Error 001: Unable to add injection.");

      return $injectionID;
   }
   
   public function updateInjection($id, $name, $desc, $variable, $value, $category)
   {
      $data = array
      (
         "InjectionName"         => $name,
         "InjectionDesc"         => $desc,
         "InjectionVariableName" => $variable,
         "InjectionValue"        => $value,
         "InjectionCategoryID"   => $category
      );
      
      $result = $this->update($data, array("InjectionID"=>$id));
      if(!$result)
         throw new Exception("Error 002: Unable to update injection.");
      
      return $result;
   }
   
   public function deleteInjection($id)
   {
      $results = $this->delete($id);
      if(!$results)
         throw new Exception("Error 003: Unable to delete injection.");

      return $results;
   }

   public function addInjectionCategory($name, $desc)
   {
      $data = array(
         "InjectionCategoryName" => $name,
         "InjectionCategoryDesc" => $desc
      );
      $result = $this->insert($data, "InjectionCategories");
      
      if(!$result)
         throw new Exception("Error 004: Unable to add injection category.");
      
      return $result;
   }

   public function updateInjectionCategory($id, $name, $desc)
   {
      $data = array(
         "InjectionCategoryName" => $name,
         "InjectionCategoryDesc" => $desc
      );
      $result = $this->update($data, array("InjectionCategoryID"=>$id), "InjectionCategories", 1);
      
      if(!$result)
         throw new Exception("Error 005: Unable to update injection category.");
      
      return $result;
   }
   
   public function deleteInjectionCategory($id)
   {
      $result = $this->delete(array("InjectionCategoryID"=>$id), "InjectionCategories", 1);
      
      if(!$result)
         throw new Exception("Error 006: Unable to delete injection category.");
      
      return $result;
   }
}