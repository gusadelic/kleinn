<?php
namespace Mumby\DB;

use Exception;

class Vehicle extends DBObject
{
    protected $defaultWires;
    
    public function __construct($id=null)
    {
       // The following variables SHOULD be non-null in the child class.
       $this->sourceTable      = "Vehicles";
       $this->idCol            = "VehicleID";

       // Don't let anyone change user IDs.
       $this->readOnlyFields   = array("VehicleID");

       $this->fieldInfo = array(
            "VehicleID"     => array("type"=>self::INTEGER,),
            "Year"          => array("type"=>self::INTEGER,),
            "Make"          => array("type"=>self::STRING, "length" => 64),
            "Model"         => array("type"=>self::STRING, "length" => 64)
       );
       
//       $this->defaultWires = array(
//           "12 VOLT CONSTANT",
//           "STARTER",
//           "STARTER 1",
//           "STARTER 2",
//           "IGNITION 1",
//           "IGNITION 2",
//           "IGNITION 3",
//           "ACCESSORY/HEATER BLOWER 1",
//           "ACCESSORY/HEATER BLOWER 2",
//           "BRAKE",
//           "PARKING LIGHTS ( + )",
//           "PARKING LIGHTS ( - )",
//           "POWER LOCK",
//           "POWER UNLOCK",
//           "FACTORY ALARM DISARM",
//           "TRUNK RELEASE",
//           "WAIT TO START LIGHT",
//           "KEYSENSE",
//           "DOOR TRIGGER",
//           "DOMELIGHT SUPERVISION",
//           "ANTI-THEFT".
//           "LOCK MOTOR WIRE",
//           "TACH"
//        );


       parent::__construct($id);

       if(!empty($id) && !$this->checkRowInstance())
       {
            throw new Exception("Unable to create new ".get_called_class().". It appears you passed an invalid id value.");
       }
    }

    function getAllVehicles($nested = true, $yearLast = false)
    {
        $sql  = "SELECT * FROM ".$this->sourceTable." ORDER BY Year DESC";
        $vehicles = $this->query($sql);
        
        if ( $nested ) {
            $output = array();
            if ( $yearLast ) {
                foreach ( $vehicles as $v ) {
                    $output[$v['Make']][$v['Model']]['Year'][] = $v["Year"];
                }
            } 
            else {
                foreach ( $vehicles as $v ) {
                    $output[$v['Year']][$v['Make']]['Model'][] = $v['Model'];
                }
            }
        }
        else {
            return $vehicles;
        }

        return $output;
    }

    function getVehicle($VehicleID)
    {
        $sql  = "SELECT * FROM ".$this->sourceTable." WHERE VehicleID = :id";
        $params = array( "id" => $VehicleID );
        $result = $this->query($sql, $params);
        if ( $result === false ) return false;
        return $result[0];   
    }
    
    function getVehiclesByYear($year)
    {
        $sql  = "SELECT * FROM ".$this->sourceTable." WHERE Year = :year";
        $params = array( "year" => $year );
        return $this->query($sql, $params);   
    }
    
    function findVehicle($year, $make, $model)
    {
        $sql  = "SELECT VehicleID FROM ".$this->sourceTable." ";
        $sql .= "WHERE Year = :year AND Make = :make AND Model = :model";
        $params = array(
            "year" => $year,
            "make" => $make,
            "model" => $model
        );
        $result = $this->query($sql, $params);
        if ( $result ) return $result[0]['VehicleID'];
        else return false;
    }
    
    function getYears($model = null)
    {
        $sql  = "SELECT DISTINCT Year FROM ".$this->sourceTable." ";
        
        $sql .= "WHERE 1=1 ";
        
        $params = array();
        
        if ( $model !== null ) {
            $sql .= "AND Model = :model ";
            $params["model"] = $model;
        }     
        
        $sql .= "ORDER BY Year DESC ";
        
        $result = $this->query($sql, $params); 
        $years = array();
        foreach ( $result as $r ) {
            $years[] = $r['Year'];
        }
        return $years;
    }
    
    function getMakes($year=null)
    {
        $sql  = "SELECT DISTINCT Make FROM ".$this->sourceTable." ";
        $params = array();
        if ( is_numeric($year) ) {
            $sql .= "WHERE Year = :year ORDER BY Make ";
            $params["year"] = $year;
        }

        $result = $this->query($sql, $params);
        if ( $result === false ) return false;
        
        $makes = array();
        foreach ( $result as $r ) {
            $makes[] = $r['Make'];
        }
        return $makes;
    }
    
    function getModels($make=null, $year=null)
    {
        $sql  = "SELECT DISTINCT Model FROM ".$this->sourceTable." ";
        $params = array();
        
        $sql .= "WHERE 1=1 ";
        
        if ( $make !== null ) {
            $sql .= "AND Make = :make ";
            $params["make"] = $make;
        }     
        if ( is_numeric($year) ) {
            $sql .= "AND Year = :year ";
            $params["year"] = $year;
        }
        
        $sql .= "ORDER BY Model ";

        $result = $this->query($sql, $params);

        if ( $result === false ) return false;
        
        $models = array();
        foreach ( $result as $r ) {
            $models[] = $r['Model'];
        }
        return $models;
    }
    
    function updateVehicle($year, $make, $model, $id)
    {
        $params = array(
            "VehicleID" => $id,
            "Year"  => $year,
            "Make"  => $make,
            "Model" => $model
        );
        
        return $this->insertOrUpdate($params);
    }
    
    function getDefaultWiring()
    {
        $wireDB = new Wire();
        $wires = $wireDB->getDefaultWires();
        $wirenames = array();
        foreach ( $wires as $w ) {
            $wirenames[$w['WireID']] = $w['WireName'];
        }
        return $wirenames;
    }
    
    function getVehicleWiring($VehicleID)
    {
        $sql =  "SELECT VehicleWires.*, Wires.WireName AS VehicleWireName FROM VehicleWires ";
        $sql .= "JOIN Wires ON Wires.WireID = VehicleWires.WireID ";
        $sql .= "WHERE VehicleID = :ID ";
        $params = array( "ID" => $VehicleID);
        return $this->query($sql, $params);
    }
    
    function updateVehicleWiring($data)
    {
        foreach ( $data as $d ) {
            if ( !$this->insertOrUpdate($d, "VehicleWires", true) )
                    return false;
        }
         
        return true;
    }
}