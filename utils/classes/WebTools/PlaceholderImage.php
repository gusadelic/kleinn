<?php
namespace Mumby\WebTools;

class PlaceholderImage
{
   var $img;
   var $backgroundColor;
   var $text;
   var $textColor;
   var $textFont;
   var $textSize;
   var $minHeight = 30;
   
   var $icon;
   var $iconFont;
   var $useFontAwesome;
   
   var $missingIcon = " ? ";
   var $errorIcon = '&#xF06a;';
   var $isErrorIcon = false;

   public function __construct($text)
   {
      $this->backgroundColor = "#fff";
      
      $this->text = $text;
      $this->textColor = "#f00";
      
      if(defined("_MB_IMG_FONT_") && file_exists(_MB_IMG_FONT_))
         $this->textFont = _MB_IMG_FONT_;
      else
         $this->textFont  = false;
      $this->textSize  = 12;
      
      if(defined("_MB_IMG_ICON_FONT_") && file_exists(_MB_IMG_ICON_FONT_)) {
         $this->iconFont = _MB_IMG_ICON_FONT_;
         $this->useFontAwesome = true;
      }
      else {
         $this->useFontAwesome = false;
      }
   }
   
   public function render($setHeader=true)
   {
      $dimensions = $this->getImageInfo();
      if($dimensions["h"] < $this->minHeight)
      {
         $diff = $this->minHeight - $dimensions["h"];
         $dimensions["h"] += $diff;
         $dimensions["y"] += $diff;
      }

      $this->img = imagecreate($dimensions["w"], $dimensions["h"]);
           
      if($setHeader)
         header("Content-Type: image/png");
      
      $bg   = $this->parseColor($this->backgroundColor);
      $txtC = $this->parseColor($this->textColor);
      $backgroundColor = imagecolorallocate($this->img, $bg["r"], $bg["g"], $bg["b"]);
      $textColor = imagecolorallocate($this->img, $txtC["r"], $txtC["g"], $txtC["b"]);
      
      if($this->textFont)
      {
         imagettftext($this->img, $this->textSize, 0, $dimensions["x"], $dimensions["y"], $textColor, $this->textFont, $this->text);
      }
      else
      {
         $startX = $dimensions["x"]-$this->textSize;
         if($startX < 5)
            $startX = 5;

         imagestring ($this->img, 4, $startX, 0, $this->text, $textColor);
      }
      
      if($this->icon)
      {
         if($this->useFontAwesome)
         {
            if(trim($this->icon) == $this->getUnicodePUAChar($this->errorIcon) && $this->isErrorIcon)
            {
               $errorTextColor = imagecolorallocate($this->img, 255, 0, 0);
               imagettftext($this->img, $this->textSize, 0, 0, $dimensions["y"], $errorTextColor, $this->iconFont, $this->icon);
               $textColor = imagecolorallocate($this->img, $txtC["r"], $txtC["g"], $txtC["b"]);
            }
            else
            {
               imagettftext($this->img, $this->textSize, 0, 0, $dimensions["y"], $textColor, $this->iconFont, $this->icon);
            }
         }
         else if($this->textFont)
            imagettftext($this->img, $this->textSize, 0, 0, $dimensions["y"], $textColor, $this->textFont, $this->missingIcon);
         else
         {
            imagestring ($this->img, 4, 0, 0, "-", $textColor);
         }
      }
         
      imagepng($this->img);
      imagedestroy($this->img);
      return;
   }

   public function setBackgroundColor($c)
   {
      $this->backgroundColor = $c;
   }
   
   public function setText($txt)
   {
      $this->text = $txt;
   }
   
   public function setTextColor($c)
   {
      $this->textColor = $c;
   }
   
   public function setTextFont($font)
   {
      if(file_exists($font))
         $this->textFont  = $font;
   }
   
   public function setTextSize($size)
   {
      $this->textSize  = (int) $size;
   }
   
   public function addIcon($icon)
   {
      $icon = $this->getUnicodePUAChar($icon);
      $this->icon = " ".$icon." ";
   }
   
   //public function addPersonIcon()   { $this->addIcon('&#xF007;'); }
   //public function addFAQIcon()      { $this->addIcon('&#xF059;'); }
   //public function addFormIcon()     { $this->addIcon('&#xF0ae;'); }
   //public function addCalendarIcon() { $this->addIcon('&#xF073;'); }
   //public function addGearIcon()     { $this->addIcon('&#xF013;'); }
   //public function addCourseIcon()   { $this->addIcon('&#xF19d;'); }
   
   public function addErrorIcon()
   {
      $this->addIcon($this->errorIcon);
      $this->isErrorIcon = true;
   }
   
   protected function renderText()
   {
      $this->img;
   }
   
   /* Let JSON handle the complexity of the Unicode character set. */
   protected function getUnicodePUAChar($chr)
   {
      return json_decode('"'.$chr.'"');
   }

   protected function getImageInfo()
   {
      if($this->textFont)
      {
         $bbox = imagettfbbox($this->textSize, 0, $this->textFont, $this->text);
         
         $w = abs($bbox[4]-$bbox[0]);
         $h = abs($bbox[5]-$bbox[1]);
         $x = 0;
         $y = $h;
         
         if(!empty($this->icon))
         {
            if($this->useFontAwesome)
            {
               $iconBox = imagettfbbox($this->textSize, 0, $this->iconFont, $this->icon);
            }
            else
            {
               $iconBox = imagettfbbox($this->textSize, 0, $this->textFont, $this->missingIcon);
            }
            
            $iconWidth = abs($iconBox[4]-$iconBox[0]);
            $w += $iconWidth;
            $x += $iconWidth;
         }
         
         // Add padding
         $w += 20;
         $h += ($this->textSize/2);
        
      }
      else
      {
         $w = (8*strlen($this->text));
         $h = 12;
         $x = 0;
         $y = $h;
         
         if(!empty($this->icon))
         {
            $iconWidth = (8*strlen($this->missingIcon));

            $w += $iconWidth;
            $x += $iconWidth;
         }
      }
      
      return array("w"=>$w, "h"=>$h, "x"=>$x, "y"=>$y);
   }
   
   public function parseColor($c)
   {
      // Array Form: array(r, g, b)
      if(is_array($c))
      {
         return array('r'=>$c[0], 'g'=>$c[1], 'b'=>$c[2]);
      }
      // Shorthand notation: #rgb
      else if(preg_match("/^\#([0-9a-f])([0-9a-f])([0-9a-f])$/", $c, $rgb))
      {
         return array('r'=>$this->hex2rgb($rgb[1]), 'g'=>$this->hex2rgb($rgb[2]), 'b'=>$this->hex2rgb($rgb[3]));
      }
      // Full Hex: #rrggbb
      else if(preg_match("/^\#([0-9a-f][0-9a-f])([0-9a-f][0-9a-f])([0-9a-f][0-9a-f])$/", $c, $rgb))
      {
         return array('r'=>$this->hex2rgb($rgb[1]), 'g'=>$this->hex2rgb($rgb[2]), 'b'=>$this->hex2rgb($rgb[3]));
      }
      else
      {
         return false;
      }      
   }
   
   public function hex2rgb($h)
   {
      $len = strlen($h);
      if($len == 1)
         return hexdec($h.$h);
      else if($len == 2)
         return hexdec($h);
      else
         return 0;
   }
}