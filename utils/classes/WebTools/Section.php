<?php

namespace Mumby\WebTools;
use \Exception;

class Section extends Content {

    
    public function __construct()
    {
        parent::__construct();

        $categoryData = $this->getCategories();
        $categories = array();
        $categories[0] = array( "SectonCategoryID" => "0", "Name" => "Uncategorized", "Description" => "");
        if ( is_array($categoryData) ) {
            foreach ( $categoryData as $i=>$c ) {
                $categories[$c['SectionCategoryID']] = $c;
            }
        }
        
        $this->categories = $categories;
    }
    
    function getCategories()
    {
        $sql  = "SELECT * FROM SectionCategories";
        $c = $this->query($sql);
        
        return $c;
    }
    
    function getCategoryID($PageID)
    {
        $sql  = "SELECT SectionCategoryID FROM SectionCategory_Pages WHERE PageID = :PageID";
        $params = array( "PageID" => $PageID );
        $result = $this->query($sql, $params);
        
        if ( is_array($result) ) {
            return $result[0]['SectionCategoryID'];
        }
        else return false;
    }
    
    function deleteCategory($id)
    {
        return $this->delete(array("SectionCategoryID"=>$id), "SectionCategories");
    }
    
    function clearCategory($PageID)
    {
       return $this->delete(array("PageID"=>$PageID), "SectionCategory_Pages");
    }
    
    function copyCategory($newPageID, $sourcePageID){
        $cat = $this->getCategoryID($sourcePageID);
        return $this->saveCategory($newPageID, $cat);
    }
    
    function getCategory($PageID)
    {
        $catID = $this->getCategoryID($PageID);
        
        if ( !is_numeric($catID) )
        {
            $catID = 0;
        }
            
        return $this->categories[$catID];
    }
    
    function saveCategory($PageID, $CategoryID)
    {
        $params = array( "SecCatID"=>$CategoryID, "PageID" => $PageID );

        $sql  = "INSERT INTO SectionCategory_Pages (SectionCategoryID, PageID) VALUES (:SecCatID, :PageID) ";
        $sql .= "ON DUPLICATE KEY UPDATE SectionCategoryID = :SecCatID ";
                
        $this->query($sql, $params);
    }
    
    public function updateSectionCategory($data)
    {
        $catID = $this->insertOrUpdate($data, "SectionCategories");
        return $catID;
    }
    
}
