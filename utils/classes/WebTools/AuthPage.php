<?php
namespace Mumby\WebTools;

class AuthPage extends Page
{
   private $isAuth;
   public function __construct($pageTitle, &$user, $permissionsLevel=_MB_USER_AUTHENTICATED_, $themeFile = "index.html")
   {
      $user = \Mumby\WebTools\ACL\User::create(_MB_AUTH_TYPE_);
      
      if(is_array($permissionsLevel))
      {
         foreach($permissionsLevel as $p)
         {
            if($user->isAuthenticated() && $user->hasPermission($p))
            {
               $this->isAuth = true;
               break;
            }
            else
            {
               $this->isAuth = false;
            }
         }
         
         if(!$this->isAuth)
            return false;
         else
         {
            parent::__construct($themeFile);
            $this->setLoggedInUser($user->firstName." ".$user->lastName);
         }
      }
      else
      {  
         if($user->isAuthenticated() && $user->hasPermission($permissionsLevel))
         {
            $this->isAuth = true;
            parent::__construct($themeFile);
            $this->setLoggedInUser($user->firstName." ".$user->lastName);
         }
         else
            $this->isAuth = false;
      }
   }
   
   public function isAuthorized()
   {
      return $this->isAuth;
   }

}
?>