<?php
namespace Mumby\WebTools;
use \Exception;

class Page extends \Mumby\DB\DBObject
{
   var $pageContent;
   var $db;
   var $msgs;
   var $navObj;
   var $contentClass;
   var $showLoggedInNav;
   var $loggedInUserAlerts;
   
   var $primaryMenu;
   
   // Used for indentation purposes.
   protected $i;
   
   // Injectables
//   static $injectables;
   static $injectables;

   public function __construct($themeFile = "index.html", $headerFile = "header.html", $footerFile = "footer.html")
   {
      if(!defined("_MB_THEME_"))
      {
         throw new Exception("Mumby theme not found! You must define a Mumby theme (_MB_THEME_).");
         return;
      }
      else
      {
         if ( !strpos($themeFile, "/") ) { 
            $themeHeader = "themes/"._MB_THEME_."/".$headerFile;
            $themeBody   = "themes/"._MB_THEME_."/".$themeFile;
            $themeFooter = "themes/"._MB_THEME_."/".$footerFile;
         }
         else {
            $themeHeader = $headerFile;
            $themeBody   = $themeFile;
            $themeFooter = $footerFile;
         }
         

         if(!file_exists($themeHeader) || !file_exists($themeBody) || !file_exists($themeFooter))
         {
            throw new Exception("Missing Mumby theme file! You must include a header.html file, a footer.html file, and a file named ".$themeFile." in order to dislpay this page.");
            return;
         }

         parent::__construct(null, _MB_DB_HOST_, _MB_DB_NAME_, _MB_DB_USER_, _MB_DB_PASS_);

         $this->msgs = array();
         $this->pageContent .= file_get_contents($themeHeader);
         $this->pageContent .= file_get_contents($themeBody);
         $this->pageContent .= file_get_contents($themeFooter);

         $this->contentClass = "";

         $this->showLoggedInNav = true;
         
         $this->loggedInUserAlerts = array();
         
         if(empty(self::$injectables))
            self::$injectables = self::initInjectables();
         
         $this->injectTheme();
         
         $this->i = 0;
      }
   }
   
   protected function indent($multiplier=0)
   {
      if(empty($multiplier))
         $multiplier = $this->i;
      
      if($multiplier > 0)
         return str_repeat("   ", $multiplier);
      else
         return "";
   }
   
   public static function getNavigationObj()
   {
      static $navObj = null;
      if(empty($navObj))
      {
         $navObj = new \Mumby\WebTools\Menu();
      }
      return $navObj;
   }
   
   public static function initInjectables()
   {
      static $inj = null;
      if(empty($inj))
      {
         $inj = array();
      }
      return $inj;
   }
   
   public function getEmptyContentObj()
   {
      $contentObj = array(
          "content"=>"<p>Sample page content.</p>",
          "title"=>"Sample Page Title",
          "contentWrapper" => "article",
          "contentClass" => "sampleClass",
          "contentID" => "sampleContent",
          "parentLink" => null,
          "parentLink" => null,
          "additionalMenus" => null
      );
      
      return $contentObj;
   }
   
   /**
    * Display content as a web page.
    * 
    * This function assumes that $contentObj is an array with the following keys:
    *    - content
    *    - title
    *    - contentWrapper (optional) (default 'article')
    *    - contentClass (optional)
    *    - contentID (optional)
    *    - parentLink (optional)
    *    - id (optional) (pageID)
    *    - additionalMenus (optional)
    * 
    * @param array $contentObj Associative array containing the information about the page (title, content, etc.)
    * @param int $httpStatus The HTTP response code to be used when displaying the content
    * @param bool $skipImageReplacements Whether image replacements (i.e., injections) should be applied.
    * @param boot $returnInsteadOfEcho Whether we should return the content as a string or echo it to the screen.
    * 
    */
   public function displayContent($contentObj=null, $httpStatus=200, $skipImageReplacements=false, $returnInsteadOfEcho = false, $absoluteLink = false)
   {
      if(!is_array($contentObj))
      {
         $contentObj = array();
         $contentObj["title"]   = "";
         $contentObj["content"] = "";
      }

      if(!isset($contentObj["content"]))
         $this->redirect("/500", 500);
      
      if(!isset($contentObj["title"]))
         $this->redirect("/500", 500);

      if(!isset($contentObj["contentWrapper"]))
         $contentObj["contentWrapper"] = "article";

      if(!isset($contentObj["contentClass"]))
         $contentObj["contentClass"] = $this->contentClass;

      if(!isset($contentObj["contentID"]))
         $contentObj["contentID"] = "";
      
      // Set HTTP Response Code
      http_response_code($httpStatus);

      // Figure out navigation options
      if(!isset($contentObj["parentLink"]))
         $contentObj["parentLink"] = 0;

      // TODO: Need to rethink how we do this...
      // Seems a little  too rigid for general use.
      if(!$this->navObj)
         $this->navObj = self::getNavigationObj();
         
      if(!$this->noMenu)
      {
         
         $thisNode = 0;
         if($this->menuNode)
            $thisNode = $this->menuNode;
         $this->navObj->injectMenus($thisNode);
         
         $this->addLoggedInNav();
      }
      
      // ADD breadcrumbs hack - Need to add restrictions
      // $this->addJS("/themes/"._MB_THEME_."/js/breadcrumbs.js"); 
      // $this->addInjectable('<div class="breadcrumbs"></div>', 'breadcrumbs');

      $content  = "";
      // Set content wrapper (if specified)
      if(!empty($contentObj["contentWrapper"]))
      {
         $content .= "<".$contentObj["contentWrapper"];
         if(!empty($contentObj["contentClass"]))
            $content .= " class='".trim($contentObj["contentClass"])."'";
         if(!empty($contentObj["contentId"]))
            $content .= " id='".trim($contentObj["contentId"])."'";
         $content .= ">";
      }

      // Set page title and content
      if(!empty($contentObj["title"]))
         $content .= "<h1>".htmlentities($contentObj["title"])."</h1>";
      $content .= $contentObj["content"];

      // Close content wrapper (if specified)
      if(!empty($contentObj["contentWrapper"]))
         $content .= "</".$contentObj["contentWrapper"].">";
      
      if(!$skipImageReplacements) {
         $content = $this->setImageReplacements($content);
         // Set mobile menu label
         $navPath = $this->navObj->getNavigationPath($contentObj["parentLink"]);
         if(isset($navPath[1]) && isset($navPath[1]["text"]))
         {
            $secondaryMenuLabel = str_repeat("&nbsp;", ceil((strlen($navPath[1]["text"])*1.6)));
            $this->injectContent($secondaryMenuLabel, "secondaryMenuLabel");
         }
         else
            $secondaryMenuLabel = "More Options";
      }
      
      $this->injectContent($content, "mainContent");
      if(!empty($contentObj["title"]))
         $this->injectContent($contentObj["title"]." - ", "pageTitle");

      //Add mainCSS
      if ( is_file("themes/"._MB_THEME_."/css/main.css.php"))
      {
          $mainCSS = get_include_contents("themes/"._MB_THEME_."/css/main.css.php");
          $this->injectContent("<style>".$mainCSS."</style>", 'mainCSS');

      }

      // Add additional CSS, JS, and body classes
      if(!empty(self::$injectables))
      {
         foreach(self::$injectables as $prefix=>$content)
         {
            $unique = array_unique($content);
            $mergedContent = implode(" ",$unique);
            $this->injectContent($mergedContent, $prefix);
         }
      }

      // Add injection elements from the DB
      $injections = $this->getAllInjections();
      if($injections)
      {
         foreach($injections as $i)
         {
            $this->injectContent($i["InjectionValue"], $i["InjectionVariableName"]);
         }
      }

      // Add application property injections
      $thisApp = new \Mumby\DB\Application(_MB_WEB_APPLICATION_ID_);
      $properties = $thisApp->getApplicationProperties();
      if($properties)
      {
         foreach($properties as $p)
         {
            $this->injectContent($p["PropertyValue"], $p["PropertyConstant"]);
         }
      }
              
      // Add some common dynamic elements
      $this->injectContent(date("j"), "currentDay");
      $this->injectContent(date("M"), "currentMonth");
      $this->injectContent(date("Y"), "currentYear");
      
      // Add system messages to main content
      $this->injectSystemMessages();

      // Replace all remaining MB insertion points
      $cleanupPattern = "/\<\!\-\- \_MB\_\:\s*[a-zA-Z0-9\-\_]*\s*\-\-\>/";
      $this->pageContent = preg_replace($cleanupPattern, "", $this->pageContent);

      if ( $absoluteLink !== false ) {
          $rel = array("href='/", 'href="/', "src='/", 'src="/');
          $abs = array("href='".$absoluteLink."/", 'href="'.$absoluteLink.'/', "src='".$absoluteLink."/", 'src="'.$absoluteLink.'/');
          $this->pageContent = str_replace($rel, $abs, $this->pageContent);
      }

      // Insert content into pageContent in it's
      // current state and echo it out to the screen.
      
      if ( $returnInsteadOfEcho ) {
          return $this->pageContent;
      }
      else {
          echo $this->pageContent;
      }
   }

   public function getAllInjections()
   {
      $inj = new \Mumby\DB\Injection();
      return $inj->getAllInjections();
   }
   
   public function addCSS($cssFiles)
   {
      $additionalContent = "";
      
      if(is_array($cssFiles))
      {
         foreach($cssFiles as $css)
         {
            $additionalContent .= "   <link rel='stylesheet' type='text/css' href='".$css."' />\n";
         }
      }
      else
      {
         $additionalContent .= "   <link rel='stylesheet' type='text/css' href='".$cssFiles."' />\n";
      }
      
      $this->addInjectable($additionalContent, "moreCSS");
   }

   public function addJS($jsFiles)
   {
      $additionalContent = "";

      if(is_array($jsFiles))
      {
         foreach($jsFiles as $js)
         {
            $additionalContent .= "   <script type='text/javascript' src='".$js."'></script>\n";
         }
      }
      else
      {
         $additionalContent .= "   <script type='text/javascript' src='".$jsFiles."'></script>\n";
      }

      $this->addInjectable($additionalContent, "moreJS");
   }
   
   public function insertJS($jsFiles, $injectable="moreJS")
   {
      $additionalContent = "";

      if(is_array($jsFiles))
      {
         foreach($jsFiles as $js)
         {
            $additionalContent .= "   <script type='text/javascript' src='".$js."'></script>\n";
         }
      }
      else
      {
         $additionalContent .= "   <script type='text/javascript' src='".$jsFiles."'></script>\n";
      }

      $this->addInjectable($additionalContent, $injectable);      
   }
   
   public function getJS()
   {
      static $js;
      if(empty($js))
      {
         $js = array();
      }
      return $js;
   }
   
   public function appendHead($additionalContent)
   {
      $this->injectContent($additionalContent, "appendHead");
   }
   
   public function prependHead($additionalContent)
   {
      $this->injectContent($additionalContent, "prependHead");
   }
   
   public function appendBody($additionalContent)
   {
      $this->injectContent($additionalContent, "appendBody");
   }
   
   public function prependBody($additionalContent)
   {
      $this->injectContent($additionalContent, "prependBody");
   }
   
   public function addBodyClass($cls)
   {
      $this->addInjectable($cls, "bodyClass");
   }

   public function addNavigation($nav, $type="")
   {
      if(empty($type))
         $injectionSite = "nav";
      else
         $injectionSite = $type."Nav";

      $this->injectContent($nav, $injectionSite);
   }
   
   private function addLoggedInNav()
   {
      $thisUser = \Mumby\WebTools\ACL\User::getAuthenticatedUser();

      $adminNav  = "<nav id='loggedInNav'>\n";
      
      if(!$thisUser) {
        $adminNav .= "<a class='btn btn-success btn-sm' href='/login";
        $adminNav .= "' id='signIn'>Sign In</a>\n";  
      }
      else {
        if($thisUser->hasPermission(_MB_USER_ADMIN_)) {
          $adminNav .= "<a href='/admin' id='adminDashHome'><i class='fa fa-dashboard'></i> Admin Dashboard </a><span class='moreMenu'><!-- _MB_: moreAdminMenu --></span>";
        }

        $adminNav .= "<span id='loggedInAs'>Logged in as <span class='loggedInUser'><a href='/admin'>".$thisUser->firstName." ".$thisUser->lastName."</a></span></span>";


        //Only show the switch users menu if they have access to do so.
        if($thisUser->hasPermission(_MB_ROLE_SWITCH_USERS_))
           $adminNav .= " <a href='/admin/users/switch' id='switchUsersLink'>[switch users]</a>\n";   

        $adminNav .= "<a href='/logout";
        if($thisUser->isFauxUser())
           $adminNav .= "?u=".htmlentities($_SERVER["REQUEST_URI"]);
        $adminNav .= "'class='btn btn-success btn-sm' id='signOut'>Sign Out</a>\n";
        }
      $adminNav .= "</nav>\n";
      $this->addBodyClass("loggedIn");
      $this->addCSS("/common/css/loggedIn.css");
      
      $this->injectContent($adminNav, 'loggedInNav');
   }
   
   public function setError($msg, $dismissible=false)
   {
      if(!isset($this->msgs["errors"]))
         $this->msgs["errors"] = array();
      $this->msgs["errors"][] = array("msg"=>$msg, "dismissible"=>$dismissible);
   }

   public function setWarning($msg, $dismissible=false)
   {
      if(!isset($this->msgs["warnings"]))
         $this->msgs["warnings"] = array();
      
      $this->msgs["warnings"][] = array("msg"=>$msg, "dismissible"=>$dismissible);
   }
   
   public function setSuccess($msg, $dismissible=false)
   {
      if(!isset($this->msgs["successes"]))
         $this->msgs["successes"] = array();
      
      $this->msgs["successes"][] = array("msg"=>$msg, "dismissible"=>$dismissible);
   }
   
   public function setInfo($msg, $dismissible=false)
   {
      if(!isset($this->msgs["info"]))
         $this->msgs["info"] = array();
      
      $this->msgs["info"][] = array("msg"=>$msg, "dismissible"=>$dismissible);
   }
   
   public function getErrors()
   {
      if(isset($this->msgs["errors"]) && !empty($this->msgs["errors"]))
         return $this->msgs["errors"];
      else
         return false;
   }
   
   public function getWarnings()
   {
      if(isset($this->msgs["warnings"]) && !empty($this->msgs["warnings"]))
         return $this->msgs["warnings"];
      else
         return false;
   }

   public function getSuccesses()
   {
      if(isset($this->msgs["successes"]) && !empty($this->msgs["successes"]))
         return $this->msgs["successes"];
      else
         return false;
   }
   
   public function getInfo()
   {
      if(isset($this->msgs["info"]) && !empty($this->msgs["info"]))
         return $this->msgs["info"];
      else
         return false;
   }
   
   // Should be called on every page load
   private function injectTheme()
   {
      $this->injectContent(_MB_THEME_, "themeName");
   }
   
   private function injectSystemMessages()
   {
      $systemMessages = "";
         
      if(isset($_SESSION['slim.flash']) && !empty($_SESSION['slim.flash']))
      {
         if(isset($_SESSION['slim.flash']["error"]) && !empty($_SESSION['slim.flash']["error"]))
            $this->setError($_SESSION['slim.flash']["error"]);
         if(isset($_SESSION['slim.flash']["warning"]) && !empty($_SESSION['slim.flash']["warning"]))
            $this->setWarning($_SESSION['slim.flash']["warning"]);
         if(isset($_SESSION['slim.flash']["success"]) && !empty($_SESSION['slim.flash']["success"]))
            $this->setSuccess($_SESSION['slim.flash']["success"]);
         if(isset($_SESSION['slim.flash']["info"]) && !empty($_SESSION['slim.flash']["info"]))
            $this->setInfo($_SESSION['slim.flash']["info"]);
      }
      
      if(!empty($this->msgs))
      {
         if($errors = $this->getErrors())
         {
            foreach($errors as $e)
            {
               if($e["dismissible"])
               {
                  $systemMessages .= "<div class='alert alert-danger alert-dismissible'>\n";
                  $systemMessages .= "   <button type='button' class='close' data-dismiss='alert' aria-label='Close'>\n";
                  $systemMessages .= "      <span aria-hidden='true'>&times;</span>\n";
                  $systemMessages .= "   </button>\n";
               }
               else
                  $systemMessages .= "<div class='alert alert-danger'>\n";
               
               $systemMessages .= "   ".$e["msg"]."\n";
               $systemMessages .= "</div>\n";
            }
         }
         
         if($warnings = $this->getWarnings())
         {
            foreach($warnings as $w)
            {
               if($w["dismissible"])
               {
                  $systemMessages .= "<div class='errorMessage alert alert-warning alert-dismissible'>\n";
                  $systemMessages .= "   <button type='button' class='close' data-dismiss='alert' aria-label='Close'>\n";
                  $systemMessages .= "      <span aria-hidden='true'>&times;</span>\n";
                  $systemMessages .= "   </button>\n";
               }
               else
                  $systemMessages .= "<div class='alert alert-warning'>\n";
               
               $systemMessages .= "   ".$w["msg"]."\n";
               $systemMessages .= "</div>\n";
            }
         }

         if($successes = $this->getSuccesses())
         {
            foreach($successes as $s)
            {
               if($s["dismissible"])
               {
                  $systemMessages .= "<div class='errorMessage alert alert-success alert-dismissible'>\n";
                  $systemMessages .= "   <button type='button' class='close' data-dismiss='alert' aria-label='Close'>\n";
                  $systemMessages .= "      <span aria-hidden='true'>&times;</span>\n";
                  $systemMessages .= "   </button>\n";
               }
               else
                  $systemMessages .= "<div class='alert alert-success'>\n";
               
               $systemMessages .= "   ".$s["msg"]."\n";
               $systemMessages .= "</div>\n";
            }
         }
         
         if($info = $this->getInfo())
         {
            foreach($info as $i)
            {
               if($i["dismissible"])
               {
                  $systemMessages .= "<div class='errorMessage alert alert-info alert-dismissible'>\n";
                  $systemMessages .= "   <button type='button' class='close' data-dismiss='alert' aria-label='Close'>\n";
                  $systemMessages .= "      <span aria-hidden='true'>&times;</span>\n";
                  $systemMessages .= "   </button>\n";
               }
               else
                  $systemMessages .= "<div class='alert alert-info'>\n";
               
               $systemMessages .= "   ".$i["msg"]."\n";
               $systemMessages .= "</div>\n";
            }
         }
      }
      
      $this->injectContent($systemMessages, "systemMessages");
   }

   public function addInjectable($content, $prefix)
   {
      if(!isset(self::$injectables[$prefix]))
         self::$injectables[$prefix] = array();
      
      self::$injectables[$prefix][] = $content;

   }
   
   private function injectContent($content, $prefix)
   {
      $injectPattern = "/\<\!\-\- \_MB\_\:\s*".$prefix."\s*\-\-\>/";
      $this->pageContent = preg_replace($injectPattern, str_replace("\$", '\$', $content), $this->pageContent);
   }
   
   protected function injectFAQ($faqID)
   {
      $thisFAQ = new \Mumby\WebTools\FAQ();
      return $thisFAQ->getFormattedFAQ($faqID);
   }

   protected function injectPerson($netid, $inline=false)
   {
        $peopleData = new \Mumby\DB\DeptMember();
        $person = $peopleData->getDeptMember($netid, true);
        //$person = \UAMath\DB\DeptMember::getPersonFromInfo($netid);
        $fromInfo = false;
        
        if ( $fromInfo ) {
            if(!empty($person["phone"]))
               $numsOnly = preg_replace('/[^0-9]/', '', $person["phone"]);
            else
               $numsOnly = preg_replace('/[^0-9]/', '', $person["roomPhone"]);

            if(strlen($numsOnly) == 7)
               $person["phone"] = "520-".substr($numsOnly,0,3)."-".substr($numsOnly,3);
        }

      
//      if($inline)
//      {
//           
//         $replacementContent  = "<a href='/people/".$person["netid"]."' ";
//         $replacementContent .= "class='inlinePerson' ";
//         $replacementContent .= "data-phone='".$numsOnly."' ";
//         $replacementContent .= "data-email='".$person["email"]."' ";
//         
//         if(!empty($person["preferred_name"]))
//            $thisPersonName = $person["preferred_name"];
//         else
//            $thisPersonName = $person["first_name"];
//         $thisPersonName .= " ".$person["last_name"];
//         
//         $replacementContent .= ">".$thisPersonName."</a>";
//      }
//      else
//      {
         if(function_exists('getProfile'))
         {
            $replacementContent = getProfile($person, "", $inline, $fromInfo);
         }
         else
         {
            $replacementContent  = "<div>";
         
            if(!empty($person["preferred_name"]))
               $thisPersonName = $person["preferred_name"];
            else
               $thisPersonName = $person["first_name"];
            $thisPersonName .= " ".$person["last_name"];
         
            $replacementContent .= "<h2><a href='/people/".$person["netid"]."'>".$thisPersonName."</a></h2>";
            $replacementContent .= "<span class='title'>".$person["printableCategory"]."</span>";
            $replacementContent .= "<span class='mail'><a href='".$person["email"]."'>".$person["email"]."</a></span>";
            
            if(!empty($person["phone"]))
               $personPhone = $person["phone"];
            else
               $personPhone = $person["roomPhone"];
            
            $replacementContent .= "<span class='phone'>".$personPhone."</span>";
            if(!empty($person["office"]))
               $replacementContent .= "<span class='office'>".$person["office"]."</span>";
            if(!empty($person["url"]))
               $replacementContent .= "<span class='homepage'><a href='".$person["url"]."' class='external'>Homepage</a></span>";
            $replacementContent  .= "</div>";
         }
//     }
      
      return $replacementContent;
   }

   protected function injectForm($formID)
   {
      $thisForm = new Form($formID);
      
      $replacementContent = "";

      if($thisForm->submitted())
      {
         $formOutput = $thisForm->processFormData();

         if($thisForm->processedData)
         {
            // If we're supposed to go to another page when we're
            // done, then we should.
            $redirect = trim($thisForm->redirectURL);
            if(!empty($redirect))
            {
               $this->redirect($redirect);
            }
            else
            {
               $return = trim($thisForm->returnURL);

               $successMessages = $thisForm->getSuccessMessages();
               if(!empty($successMessages))
               {
                  foreach($successMessages as $s)
                  {
                     $this->setSuccess($s);
                  }
               }
               else if(!$thisForm->hideSuccessMessage)
               {
                  $this->setSuccess("The form was submitted successfully.");
               }

               if($thisForm->showHeader)
                  $replacementContent .= "<h2>".$thisForm->getFormName()."</h2>";

               $revisitURL = $this->getResourceUri();

               if($thisForm->showSubmitted)
               {
                  $replacementContent .= "<div class='infoMessage'>";
                  $replacementContent .= "<p>This form has been submitted. ";
                  $replacementContent .= "The information that you submitted is displayed below.";
                  $replacementContent .= "</p><p>To enter another submission, please <a href='";
                  $replacementContent .= htmlentities($revisitURL);
                  $replacementContent .= "'>click here</a>.</p></div>";
               }
               else
               {
                  $replacementContent .= "<p class='submittedFormMessage'><em>The information provided was submitted successfully. To enter another submission, please <a href='";
                  $replacementContent .= htmlentities($revisitURL);
                  $replacementContent .= "'>click here</a>.</em></p>";

                  if ( !empty($return) ) {
                    $replacementContent .= "<p class='submittedFormMessage'><em>To return to the previous page, please <a href='";
                    $replacementContent .= htmlentities($return);
                    $replacementContent .= "'>click here</a>.</em></p>"; 
                  }                              
               }


               if($thisForm->showSubmitted)
                  $replacementContent .= $thisForm->showSubmittedData();

            }
         }
         else
         {
            $errorMessages = $thisForm->getErrors();
            if(!empty($errorMessages))
            {
               foreach($errorMessages as $e)
               {
                  $this->setError($e);
               }
            }
            else
            {
               $this->setError("<div class='errorMessage'>There was an error submitting the form.</div>");
            }

            $action = $this->getResourceUri();
            $replacementContent .= $thisForm->generateForm($action);
         }
      }
      else
      {
         $action = $this->getResourceUri();
         $replacementContent .= $thisForm->generateForm($action);
      }

      return $replacementContent;
   }
   
   protected function injectData($varName)
   {
      $thisInjection = new \Mumby\WebTools\InjectionData();
      $data = $thisInjection->getInjection($varName);

      if(strpos($data["InjectionValue"], "people-") === 0)
         $replacementContent = $this->injectPerson(substr($data["InjectionValue"],7), true);
      else
      {
         $replacementContent  = "<span";
         if(!empty($data["classValue"]))
            $replacementContent .= " class='".$data["classValue"]."'";
         $replacementContent .= ">";
         $replacementContent .= $data["InjectionValue"];
         $replacementContent .= "</span>";
      }
      
      return $replacementContent;
   }
   
   protected function injectCourse($courseID)
   {
      $this->addJS('/common/js/tooltips.js');
      
      $courseMgr = new \Mumby\DB\Course();
      $course = $courseMgr->getCourseByID($courseID);
      
      if(!empty($course))
      {
         $course = $course[0];
         if (empty($course["Description"])) $course["Description"] = "No Information";
         $replacementContent = "<span title='".cleanAttr($course["CourseName"])." - ".cleanAttr($course["Description"])."' class='injectedTooltip label label-primary'>".$course["SubjectPrefix"]." " .$course["CourseNumber"]."</span>";
      }
      else
         $replacementContent = "";
      
      return $replacementContent;
   }
   
   /*
    * Copied from Slim framework.
    */
   public function getResourceUri()
   {
      // Server params
      $scriptName = $_SERVER['SCRIPT_NAME']; // <-- "/foo/index.php"
      $requestUri = $_SERVER['REQUEST_URI']; // <-- "/foo/bar?test=abc" or "/foo/index.php/bar?test=abc"
      $queryString = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : ''; // <-- "test=abc" or ""

      // Physical path
      if (strpos($requestUri, $scriptName) !== false)
         $physicalPath = $scriptName; // <-- Without rewriting
      else
          $physicalPath = str_replace('\\', '', dirname($scriptName)); // <-- With rewriting

      // Virtual path
      $env['PATH_INFO'] = substr_replace($requestUri, '', 0, strlen($physicalPath)); // <-- Remove physical path
      $env['PATH_INFO'] = str_replace('?' . $queryString, '', $env['PATH_INFO']); // <-- Remove query string
      $env['PATH_INFO'] = '/' . ltrim($env['PATH_INFO'], '/'); // <-- Ensure leading slash
      
      return $env['PATH_INFO'];
   }

   public function redirect($url)
   {
      header("Location: ".$url);
      die();      
   }
   
   public function setImageReplacements($content)
   {
      // Check for image replacement options
      $imgReplacements = array();

      if(preg_match_all("/(\<p\>)?\<img ([^\>])*src\=[\'\"](\.\.\/)*(\/)?inject\/([^\'\"]*)[\'\"]([^\>]*)\/\>(\<\/p\>)?/im", $content, $imgReplacements))
      {
         foreach($imgReplacements[5] as $k=>$contentPiece)
         {
            $parts = explode("-",$contentPiece);
            $contentType = strtolower($parts[0]);
            $contentID = $parts[1];

            // Get any attributes attached to the image
            $attrPattern = "/(\S+)=[\"']?((?:.(?![\"']?\s+(?:\S+)=|[>\"']))+.)[\"']?/";
            $attributes = array();

            if(isset($imgReplacements[6]) && isset($imgReplacements[6][$k]))
               preg_match_all($attrPattern, trim($imgReplacements[6][$k]), $attributes);

            if(!empty($attributes))
            {
               $tmp = array();
               foreach($attributes[1] as $ak=>$v)
                  $tmp[$v] = $attributes[2][$ak];

               $attributes = $tmp;
            }

            switch($contentType)
            {
               case "faq":
                  $replacementContent = $this->injectFAQ($contentID);
                  break;
               case "people":
               {
                  if(!empty($attributes) && isset($attributes["data-inline"]) && $attributes["data-inline"])
                     $replacementContent = $this->injectPerson($contentID, true);
                  else
                     $replacementContent = $this->injectPerson($contentID);
                  break;
               }
               case "form":
                  $replacementContent = $this->injectForm($contentID);
                  break;
               case "data":
                  $replacementContent = $this->injectData($contentID);
                  break;
               case "course":
                  $replacementContent = $this->injectCourse($contentID);
                  break;
               default:
                  $replacementContent = "";
                  break;
            }

            $content = str_replace($imgReplacements[0][$k], $replacementContent, $content);
         }
      }
      return $content;
   }
   
   /**
    * Add a small notice that the user should view their messages.
    * 
    * @param int $msgCount
    */
   public function alertLoggedInUser($msgCount)
   {
      $this->injectContent("<a href='/messages' class='badge'>".$msgCount."</a>", "loggedInUserAlerts");
   }
   
   /**
    * Add a small notice that the user should view their messages.
    * 
    * @param int $msgCount
    */
   public function setLoggedInUser($userName)
   {
      $this->injectContent(htmlentities($userName), "loggedInName");
   }

   /**
    * Provides a 'clean' value for class/ID attributes.
    * 
    * @param string $str The string to be formatted 'cleaned'.
    * @return string The 'clean' value to be used for an attribute value.
    */
   public static function cleanAttr($str)
   {
      return preg_replace("/[^A-Za-z0-9]/", "", $str);
   }
   
   /**
    * Log an error for a given page.
    * 
    * @param object $app The Slim object used to run the application.
    * @param int $status HTTP status code to log for the request.
    * @return mixed 
    */
   public function logError($app, $status=404)
   {
      $url = $app->request->getResourceUri();
      $ref = $app->request->getReferrer();
      if(!$ref)
         $ref = null;
      $ip  = $app->request->getIp();
      if(!$ip)
         $ip = null;
      $ua  = $app->request->getUserAgent();
      if(!$ua)
         $ua = null;

      $data = array(
          "ErrorStatus"     => $status,
          "ErrorURL"        => $url,
          "ErrorReferrer"   => $ref,
          "ErrorIPAddress" => $ip,
          "ErrorUserAgent"   => $ua
      );
      
      if($this->insert($data, "ErrorLog"))
         return true;
      else
         return false;
   }
}