<?php
namespace Mumby\WebTools;

class JavascriptPage extends Page
{
   private $isAuth;
   public function __construct(&$app, $contentType = 'text/javascript')
   {
      parent::__construct("");
      $this->pageContent = "<!-- _MB_: mainContent -->";
      $this->noMenu = true;
      $app->contentType($contentType);
   }
   
   public function getEmptyContentObj()
   {
      $contentObj = array(
          "content"=>"",
          "title"=>"",
          "contentWrapper" => "",
          "contentClass" => "",
          "contentID" => "",
          "parentLink" => null,
          "parentLink" => null,
          "additionalMenus" => null
      );
      
      return $contentObj;
   }
}