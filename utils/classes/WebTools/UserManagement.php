<?php
namespace Mumby\WebTools;
use \Exception;

class UserManagement extends \Mumby\DB\DBObject
{
   protected $loggedIn;
   protected $perms;
   var $db;
   var $errors;
   
   var $username;
   var $firstName;
   var $lastName;
   var $email;
   var $userID;
   
   public function __construct()
   {
      // The following variables SHOULD be non-null in the child class.
      $this->sourceTable      = "Users";
      $this->idCol            = "UserID";
      
      // Don't let anyone change user IDs.
      $this->readOnlyFields   = array("UserID");
      
      $this->fieldInfo = array(
         "NetID"         => array("type"=>self::STRING, "length"=>255),
         "FirstName"     => array("type"=>self::STRING, "length"=>255),
         "PreferredName" => array("type"=>self::STRING, "length"=>255),
         "LastName"      => array("type"=>self::STRING, "length"=>255)
      );
      
//      parent::__construct(null, _MB_DB_HOST_, _MB_DB_NAME_, _MB_DB_USER_, _MB_DB_PASS_);
      parent::__construct(null, _MB_CENTRAL_DATASTORE_HOST_, _MB_CENTRAL_DATASTORE_NAME_, _MB_CENTRAL_DATASTORE_USER_, _MB_CENTRAL_DATASTORE_PASS_);
      $this->errorMessages = array();
   }
   
   public function getAllUsers()
   {
      $sql  = "SELECT u.FirstName as firstName, ";
      $sql .= "u.LastName as lastName, ";
      $sql .= "CASE WHEN u.PreferredName = '' THEN CONCAT(u.FirstName, ' ', u.LastName) ELSE CONCAT(u.PreferredName, ' ', u.LastName) END AS FullName, "; 
      $sql .= "CASE WHEN u.PreferredName = '' THEN CONCAT(u.LastName, ', ', u.FirstName) ELSE CONCAT(u.LastName, ', ', u.PreferredName) END AS ReverseFullName, ";
      $sql .= "u.UserID as userID, ";
      $sql .= "e.EmailAddress as email, ";
      $sql .= "u.NetID as username, ";
      $sql .= "u.IsActive as active, ";
      $sql .= "group_concat(p.PermissionConstant separator ',') as permissions ";
      $sql .= "FROM `Users` `u` ";
      $sql .= "INNER JOIN `User_Permissions` AS `up` ON `u`.`UserID`=`up`.`UserID` ";
      $sql .= "INNER JOIN `Permissions` AS `p` ON `up`.`PermissionID`=`p`.`PermissionID` ";
      $sql .= "LEFT JOIN `EmailAddresses` AS `e` ON `u`.`UserID`=`e`.`UserID` AND `e`.`IsPrimaryAddress`=1 ";
      //$sql .= "WHERE (`p`.`ApplicationID` IS NULL OR `p`.`ApplicationID`=:AppID) ";
      $sql .= "GROUP BY u.UserID ";
      $sql .= "ORDER BY LastName, FirstName";
      //$users = $this->query($sql, array("AppID"=>_MB_WEB_APPLICATION_ID_));
      $users = $this->query($sql, array());

      if(empty($users))
         return false;
      
      $finalUsers = array();
      foreach($users as $u)
      {
         $thisUser = $u;
         $perms = $this->parsePerms($thisUser["permissions"]);
         $thisUser["permissions"] = $perms;
         $finalUsers[] = $thisUser;
      }

      return $finalUsers;
   }
   
   public function getUserInfo($username)
   {
      $sql  = "SELECT u.UserID as id, u.NetID as username, u.FirstName, u.LastName, ";
      $sql .= "CASE WHEN u.PreferredName = '' THEN CONCAT(u.FirstName, ' ', u.LastName) ELSE CONCAT(u.PreferredName, ' ', u.LastName) END AS FullName, "; 
      $sql .= "CASE WHEN u.PreferredName = '' THEN CONCAT(u.LastName, ', ', u.FirstName) ELSE CONCAT(u.LastName, ', ', u.PreferredName) END AS ReverseFullName, ";
      $sql .= "group_concat(p.PermissionConstant separator ',') as permissions FROM `Users` `u` INNER JOIN `User_Permissions` AS `up` ON `u`.`UserID`=`up`.`UserID` LEFT JOIN `Permissions` AS `p` ON `up`.`PermissionID`=`p`.`PermissionID` ";
      $sql .= "WHERE u.NetID = :username ";
      $sql .= "AND u.IsActive=1";
      
      $user = $this->query($sql, array("username"=>$username));

      if(isset($user[0]))
         $finalUser = $user[0];
      else
         $finalUser = false;
      
      if(empty($finalUser) || !isset($finalUser["username"]) || empty($finalUser["username"]))
         return false;

      $perms = $this->parsePerms($finalUser["permissions"]);
      $finalUser["permissions"] = $perms;
      return $finalUser;
   }
   
   private function parsePerms($perms)
   {
      $permIDs = explode(",", $perms);
      $thisPerms = array();
      foreach($permIDs as $p)
      {
         if(defined($p))
            $thisPerms[] = constant($p);
      }
      return $thisPerms;
   }

   public function updateUserInfo($username, $firstName="", $lastName="", $email="")
   {
      if(empty($firstName) && empty($lastName) && (empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)))
      {
         $this->errorMessages[] = "Invalid data provided for updating user information.";
         return false;
      }
      
      $data = array();
      
      if(!empty($firstName))
         $data["firstName"] = $firstName;
      
      
      if(!empty($lastName))
         $data["lastName"]  = $lastName;
             
      if(!(empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)))
         $data["email"]  =  $email;
      
      return $this->update($data, array("username"=>$username), null, 1);
   }
   
   public function blockUser($username)
   {
      $data = array("active"=>0);
      return $this->update($data, array("username"=>$username), null, 1);
   }
   
   public function unblockUser($username)
   {
      $data = array("active"=>1);
      return $this->update($data, array("username"=>$username), null, 1);
   }
   
   public function addUser($userData, $permissions)
   {
        $userID = 0;
        $data = array( 'NetID' => $userData['netID'], 'FirstName' => $userData['firstName'], 'LastName' => $userData['lastName'], 'IsActive' => 1 );
        $userID = $this->insertOrUpdate($data, 'Users');
        if ( $userID < 1 ) {
          $this->errorMessages[] = "Failed to add User.";
          return false;
        }

        if ( !$this->addEmail($userID, $userData['emailAddress'], 1)) {
            $this->errorMessages[] = "Failed to add Email.";
        }

        if ( !$this->setPermissions($userID, $permissions) ) {
          $this->errorMessages[] = "Failed to add Permissions.";
        }
      
        if ( !empty( $userData['password'] ) ) {
           if ( !$this->setPassword($userID, $userData['password']) ) {
             $this->errorMessages[] = "Failed to set Password.";
           }
        }
     
      if ( !empty($this->errorMessages) ) {
          return false;
      }
      else {
        return $userID;
      }
   }
   
   public function getPermission($pID)
   {
      $perm = $this->find(array("PermissionID"=>$pID), "Permissions");
      if($perm !== false && is_array($perm) && !empty($perm))
         return $perm[0];
      else
         return false;
   }
   
   public function getPermissionCategories()
   {
      return $this->find(null, "PermissionCategories");
   }
   
   public function addPermission($pName, $pDesc, $pConstant, $pCategory=1, $appID=null)
   {
      if(empty($pName))
      {
         $this->errorMessages[] = "Missing permission name.";
         return false;
      }

      if(empty($pDesc))
      {
         $this->errorMessages[] = "Missing permission description.";
         return false;
      }

      if(empty($pConstant))
      {
         $this->errorMessages[] = "Missing permission constant.";
         return false;
      }

      if(defined($pConstant))
      {
         $this->errorMessages[] = "Permission constant already in use.";
         return false;
      }
      
      if(empty($appID))
         $appID = _MB_WEB_APPLICATION_ID_;

      $data = array(
         "PermissionName"       => $pName,
         "PermissionDesc"       => $pDesc,
         "PermissionConstant"   => $pConstant,
         "PermissionCategoryID" => $pCategory,
         "ApplicationID"        => $appID
      );
      
      return $this->insert($data, "Permissions");
   }
   
   public function updatePermission($pID, $pName, $pDesc, $pCategory=1)
   {
      if(empty($pID))
      {
         $this->errorMessages[] = "Missing permission id.";
         return false;
      }
      
      if(empty($pName))
      {
         $this->errorMessages[] = "Missing permission name.";
         return false;
      }

      if(empty($pDesc))
      {
         $this->errorMessages[] = "Missing permission description.";
         return false;
      }

      $data = array(
         "PermissionName"       => $pName,
         "PermissionDesc"       => $pDesc,
         "PermissionCategoryID" => $pCategory
      );
      
      return $this->update($data, array("PermissionID"=>$pID), "Permissions", 1);
   }
   
   public function revokeAllPermissions($userID)
   {
      return $this->setPermissions($userID, array());
   }
   
   public function setPassword($userID, $password, $isTemp = false) {
       $hash = password_hash($password, PASSWORD_DEFAULT);
       $data = array( 'userID' => $userID, 'Password' => $hash, 'isTempPassword' => $isTemp );
       if ( $this->insertOrUpdate($data, 'UserPasswords', true) ) {
           return true;
       } else {
           return false;
       }      
   }
   
   public function getPassword($UserID)
   {
       $sql = "SELECT Password FROM UserPasswords WHERE UserID = :UserID";
       return $this->query($sql, array("UserID"=>$UserID));
   }
   
   public function updatePassword($userID, $password, $oldPassword) {
       if(password_verify($oldPassword, $thisUser["Password"])) {
           $hash = password_hash($password, PASSWORD_DEFAULT);
           if ( $this->update(array('Password' => $hash), array('userID' => $userID), 'UserPasswords', 1) ) {
               return true;
           }
           else {
               return false;
           }
       }
       else {
           return false;
       }
   }
   
   public function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = "";
        for ($i = 0; $i < 8; $i++) {
            $n = mt_rand(0, strlen($alphabet)-1);
            $pass .= substr($alphabet, $n, 1);
        }
        return $pass;
    }
    
    public function hasTempPassword($userID) {
        $sql = "SELECT isTempPassword FROM UserPasswords WHERE UserID = :userID";
        $params = array( "userID" => $userID );
        if ( $this->query($sql, $params) === 1 ) {
            return true;
        }
        else return false;
    }
   
   public function addEmail($userID, $email, $isPrimary=0 ) {
       return $this->insertOrUpdate(array('UserID'=>$userID, 'EmailAddress'=>$email, 'IsPrimaryAddress'=>$isPrimary ), 'EmailAddresses', true);
   }
   
   public function setPrimaryEmail($userID, $email) {
       $this->update(array("isPrimaryAddress"=>0), array("userID"=>$userID), "EmailAddresses");
       return $this->update(array("isPrimaryAddress"=>1), array("userID"=>$userID, "EmailAddress"=>$email), "EmailAddresses" );
   }
   
   public function removeEmail($userID, $email) {
       $sql = "DELETE FROM EmailAddresses WHERE UserID = :userID AND EmailAddress = :email";
       $data = array( ':userID' => $userID, ':email' => '$email');
       return $this->query($sql, $data);
   }
   
   public function setPermissions($userID, $permissions)
   {
      if(!is_array($permissions))
         $permissions = array($permissions);
      
      $parsedPerms = array();
      foreach($permissions as $p)
      {
         if(!is_int($p))
         {
            if(ctype_digit($p))
               $p = (int) $p;
            else if(defined($p))
               $p = constant($p);
            else
               return false;
         }
         
         $parsedPerms[] = $p;
      }
      
      $queries = array();
      $queryData = array();
      
      $queries[]   = "DELETE FROM User_Permissions WHERE UserID=:userID";
      $queryData[] = array("userID"=>$userID);

      foreach($parsedPerms as $perm)
      {
         $queries[]   = "INSERT INTO User_Permissions (UserID, PermissionID) VALUES (:userID, :pid) ON DUPLICATE KEY UPDATE PermissionID=:pid";
         $queryData[] = array("userID"=>$userID, "pid"=>$perm);
      }

      return $this->multiQuery($queries, $queryData);
   }
   
   public function clearErrors()
   {
      $this->errorMessages = array();
   }
   
   public function getErrors() {
       $errors = "";
       foreach( $this->errorMessages as $message ) {
           $errors .= $message . "<br />";
       }
       return $errors;
   }
}