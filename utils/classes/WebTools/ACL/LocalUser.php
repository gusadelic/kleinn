<?php
namespace Mumby\WebTools\ACL;
use PDO;

// We set a session prefix to prevent the session
// info from being overwritten by any other libraries
// that might also use session and possibly the same
// generic keys.
if(!defined("_MB_SESSION_PREFIX_"))
   define("_MB_SESSION_PREFIX_", "_MB_PROTECTED_SESSION_");

if(!defined("_MB_USER_AUTHENTICATED_"))
   define("_MB_USER_AUTHENTICATED_", 1);
if(!defined("_MB_USER_ANONYMOUS_"))
   define("_MB_USER_ANONYMOUS_", 2);
if(!defined("_MB_USER_NOBODY_"))
   define("_MB_USER_NOBODY_", 3);

if(!defined("_MB_USER_ADMIN_"))
   define("_MB_USER_ADMIN_", -1);
if(!defined("_MB_ROLE_SWITCH_USERS_"))
   define("_MB_ROLE_SWITCH_USERS_", -1);

class LocalUser extends \Mumby\DB\DBObject
{
   protected $loggedIn;
   protected $perms;
   protected $hasTempPassword;
   var $db;
   var $username;
   var $firstName;
   var $lastName;
   var $email;
   var $userID;
   
   public function __construct($username, $password)
   {
      $this->db = new \Mumby\DB\GenericDB(_MB_CENTRAL_DATASTORE_HOST_, _MB_CENTRAL_DATASTORE_NAME_, _MB_CENTRAL_DATASTORE_USER_, _MB_CENTRAL_DATASTORE_PASS_);

      // See if we have the user in the session
      if(self::isAuthenticated())
      {

         $storedLogin = $this->getStoredLogin();
         //if($storedLogin == $username)
         //{
            $this->username = $storedLogin;
            $this->setUserMetadata();
            $this->loggedIn = true;
         //}
         //else
         //{
            /* If someone is logged in and the user is trying to log
             * in as someone else, they need to log the other
             * user out first.
             */
         //   return false;
         //}
      }
      // If not, we need to check against the DB
      else
      {

         if(!$this->authenticate($username, $password))
            return false;
      }

      return true;
   }
   
   public static function getAuthenticatedUser()
   {
      if(!self::isAuthenticated())
         return false;
      
      $loggedInUser = self::getStoredLogin();
      return new static($loggedInUser, "");
   }
   
   protected function setUserMetadata($row = "")
   {
      $thisUser = false;
      
      if(empty($row) || !isset($row["id"]) || !isset($row["firstName"]) || !isset($row["lastName"]) || !isset($row["email"]))
      {
         $userSQL = "SELECT U.UserID, U.FirstName, U.LastName, E.EmailAddress FROM Users U ".
                    "LEFT JOIN EmailAddresses E ON U.UserID=E.UserID AND E.IsPrimaryAddress=1 WHERE ";
         if(is_int($this->username) || ctype_digit($this->username))
            $userSQL .= "U.UserID=:UserID ";
         else
            $userSQL .= "U.NetID=:UserID ";

         $data = array("UserID"=>$this->username);

         $row = $this->db->query($userSQL, $data);

         if(empty($row))
         {
            $this->userID    = -1;
            $this->firstName = "Unknown";
            $this->lastName  = "User";
            $this->email     = "";
            return false;
         }

         $thisUser = $row[0];
      }
      else
         $thisUser = $row;
      
      if(!empty($thisUser))
      {
         $this->userID    = $thisUser["UserID"];
         $this->firstName = $thisUser["FirstName"];
         $this->lastName  = $thisUser["LastName"];
         $this->email     = $thisUser["EmailAddress"];
         return true;
      }
      
      return false;
   }
   
   protected function unsetUserMetadata()
   {
      unset($this->userID);
      unset($this->firstName);
      unset($this->lastName);
      unset($this->email);
   }
   
   protected function authenticate($u, $p)
   {
      $userSQL = "SELECT U.UserID, U.NetID, U.FirstName, U.LastName, P.Password, P.IsTempPassword FROM Users U ".
                 "INNER JOIN UserPasswords P ON U.UserID=P.UserID WHERE ";
      
      if(is_int($this->username) || ctype_digit($this->username)) {
         $userSQL .= "U.UserID=:UserID ";
      }
      else {
         $userSQL .= "U.NetID=:UserID";
      }

      $data = array("UserID"=>$u);
      $row = $this->db->query($userSQL, $data);

      if(empty($row))
      {
         $this->loggedIn = false;
      }
      else
      {
         $thisUser = $row[0];
         
         if(password_verify($p, $thisUser["Password"]))
         {
            $this->setLoggedIn($thisUser["NetID"]);
            $this->setUserMetadata($thisUser);
            $this->hasTempPassword = $thisUser["IsTempPassword"];
            $this->loggedIn = true;
         }
         else
            $this->loggedIn = false;
      }
      
      return $this->loggedIn;
   }
   
   public function getPermissions()
   {
      $perms = array(_MB_USER_ANONYMOUS_, _MB_USER_AUTHENTICATED_);

      $permsSQL = "SELECT `P`.`PermissionConstant` FROM `User_Permissions` `UP` ".
                  "INNER JOIN `Permissions` `P` ON `P`.`PermissionID`=`UP`.`PermissionID` ".
                  "INNER JOIN `Users` `U` ON `U`.`UserID`=`UP`.`UserID` ".
                  "INNER JOIN PermissionCategories PC ON P.PermissionCategoryID=PC.PermissionCategoryID ".
                  "WHERE ";
      
      if(is_int($this->username) || ctype_digit($this->username))
         $permsSQL .= "`U`.`UserID`=:username ";
      else
         $permsSQL .= "`U`.`NetID`=:username ";

      $permsSQL .= "ORDER BY `PC`.`PermissionCategoryName`, `P`.`PermissionName`";
      
      $permData = array("username"=>$this->username);
      $rows = $this->db->query($permsSQL, $permData);

      if(!empty($rows))
      {
         foreach($rows as $r)
         {
            $thisConst = constant($r["PermissionConstant"]);
            if(!in_array($thisConst, $perms))
               $perms[] = $thisConst;
         }
      }
      
      if(in_array(_MB_USER_SUPER_ADMIN_, $perms) || (defined("_MB_USER_ADMIN") && in_array(_MB_USER_ADMIN, $perms)))
      {
         $allPerms = $this->getAllPermissions(_MB_WEB_APPLICATION_ID_);

         foreach($allPerms as $p)
         {
            $thisConst = constant($p["PermissionConstant"]);
            if(!in_array($thisConst, $perms))
               $perms[] = $thisConst;
         }
      }
            
      $this->perms = $perms;
      return $this->perms;
   }

   public function hasPermission($role= _MB_USER_ANONYMOUS_)
   {
      if($role == _MB_USER_NOBODY_)
         return false;
      
      if(empty($this->perms))
         $this->getPermissions();

      if(in_array(_MB_USER_ADMIN_, $this->perms))
         return true;
      
      $hasPerms = false;
      switch($role)
      {
         case _MB_USER_ANONYMOUS_:
            $hasPerms = true;
            break;
         case _MB_USER_AUTHENTICATED_:
            $hasPerms = $this::isAuthenticated();
            break;
         default:
            $hasPerms = in_array($role, $this->perms);
            break;
      }
      

      return $hasPerms;
   }
   
   public static function getAllPermissions($appID=null)
   {
      $permsSQL = "SELECT `P`.*, `PC`.`PermissionCategoryName` FROM `Permissions` `P` ".
                  "INNER JOIN PermissionCategories PC ON P.PermissionCategoryID=PC.PermissionCategoryID ";
      
      $permData = array();
      
      if(!empty($appID))
      {
         $permsSQL .= "WHERE (P.ApplicationID=:ApplicationID || P.ApplicationID IS NULL) ";
         $permData["ApplicationID"] = $appID;
      }

      $permsSQL .= "ORDER BY `PC`.`PermissionCategoryName`, `P`.`PermissionName`";
      
      $db = new \Mumby\DB\GenericDB(_MB_CENTRAL_DATASTORE_HOST_, _MB_CENTRAL_DATASTORE_NAME_, _MB_CENTRAL_DATASTORE_USER_, _MB_CENTRAL_DATASTORE_PASS_);
      
      $rows = $db->query($permsSQL, $permData);
      
      $permissions = array();
      foreach($rows as $r)
      {
         if(defined($r["PermissionConstant"]))
         {
            $const = constant($r["PermissionConstant"]);
            $permissions[$const] = $r;
         }
      }
     
      return $permissions;
   }
   
   public static function isAuthenticated()
   {
      return isset($_SESSION[_MB_SESSION_PREFIX_."USERNAME"]);
   }
   
   public function isFauxUser()
   {
      if(!isset($_SESSION[_MB_SESSION_PREFIX_."USERNAME"]) || !isset($_SESSION[_MB_SESSION_PREFIX_."ACTIVE_USERNAME"]))
         return false;
      
      return ($_SESSION[_MB_SESSION_PREFIX_."USERNAME"] != $_SESSION[_MB_SESSION_PREFIX_."ACTIVE_USERNAME"]);
   }
   
   public static function getAllUsers()
   {
      $db = new \Mumby\DB\GenericDB(_MB_CENTRAL_DATASTORE_HOST_, _MB_CENTRAL_DATASTORE_NAME_, _MB_CENTRAL_DATASTORE_USER_, _MB_CENTRAL_DATASTORE_PASS_);
      $userSQL = "SELECT U.UserID, U.NetID, U.FirstName, U.LastName, P.Password, P.IsTempPassword, E.EmailAddress FROM Users U ".
                 "INNER JOIN UserPasswords P ON U.UserID=P.UserID ".
                 "LEFT JOIN EmailAddresses E ON U.UserID=E.UserID WHERE E.IsPrimaryAddress=1";
      
      return $db->query($userSQL);
   }

   public static function getUsersName($ID) {
      $db = new \Mumby\DB\GenericDB(_MB_CENTRAL_DATASTORE_HOST_, _MB_CENTRAL_DATASTORE_NAME_, _MB_CENTRAL_DATASTORE_USER_, _MB_CENTRAL_DATASTORE_PASS_);
      $userSQL = "SELECT U.PreferredName, U.FirstName, U.LastName FROM Users U ";
      if (is_numeric($ID)) {
          $userSQL .= "WHERE U.UserID = :UserID";
          $params = array( "UserID" => $ID );
      }
      else { 
          $userSQL .= "WHERE U.NetID = :NetID";
          $params = array( "NetID" => $ID );
      }
      $namedata = $db->query($userSQL, $params);

      if ( !empty($namedata[0]['PreferredName']) ) {
         return $namedata[0]['PreferredName'];
      } 
      else {
         return $namedata[0]['FirstName'] . " " . $namedata[0]['LastName'];
      }

   }

   
   // Set username to session so we can see if a user is logged in,
   // when they logged in, etc. We don't store permissions in the
   // session since those could change mid-session.
   protected function setLoggedIn($username)
   {
      // Set username to session so we don't have to log in everytime.
      $_SESSION[_MB_SESSION_PREFIX_."USERNAME"] = $username;

      if(!isset($_SESSION[_MB_SESSION_PREFIX_."ACTIVE_USERNAME"]) || empty($_SESSION[_MB_SESSION_PREFIX_."ACTIVE_USERNAME"]))
         $_SESSION[_MB_SESSION_PREFIX_."ACTIVE_USERNAME"] = $username;

      $_SESSION[_MB_SESSION_PREFIX_."LAST_LOGIN"] = date('r');
      $this->username  = $username;
   }
   
   protected static function getStoredLogin()
   {
      if(!isset($_SESSION[_MB_SESSION_PREFIX_."ACTIVE_USERNAME"]))
         return false;

      return $_SESSION[_MB_SESSION_PREFIX_."ACTIVE_USERNAME"];
   }
   
   public function switchUser($fauxUsername)
   {
      if($this->hasPermission(_MB_ROLE_SWITCH_USERS_))
      {
         $prevUsername = $this->username;
         $this->username = $fauxUsername;
         if($this->setUserMetadata())
         {
            $_SESSION[_MB_SESSION_PREFIX_."ACTIVE_USERNAME"] = $fauxUsername;
            $this->perms = array();
            return true;
         }
         else
         {
            $this->username = $prevUsername;
            return false;
         }
      }
      else
         return false;
   }
   
   public function logout($redirectURL="")
   {
      // If the user is logged in as someone else, we simply log the faux user out.
      if($this->isFauxUser())
      {
         $_SESSION[_MB_SESSION_PREFIX_."ACTIVE_USERNAME"] = $_SESSION[_MB_SESSION_PREFIX_."USERNAME"];
         $this->username = $_SESSION[_MB_SESSION_PREFIX_."USERNAME"];
         $this->setUserMetadata();
         $this->perms = array();
      }
      else
      {
         if(isset($_SESSION[_MB_SESSION_PREFIX_."ACTIVE_USERNAME"]))
            unset($_SESSION[_MB_SESSION_PREFIX_."ACTIVE_USERNAME"]);
         
         if(isset($_SESSION[_MB_SESSION_PREFIX_."USERNAME"]))
            unset($_SESSION[_MB_SESSION_PREFIX_."USERNAME"]);
         
         if(isset($_SESSION[_MB_SESSION_PREFIX_."LAST_LOGIN"]))
            unset($_SESSION[_MB_SESSION_PREFIX_."LAST_LOGIN"]);
         
         $this->unsetUserMetadata();
         session_destroy();
      }
   }
   
    public function hasTempPassword() {
        $sql = "SELECT * FROM UserPasswords WHERE UserID = :userID";
        $params = array( "userID" => $this->userID );
        $result = $this->db->query($sql, $params);
        if ( $result[0]['IsTempPassword'] === '1' ) {
            return true;
        }
        else return false;
    }
}