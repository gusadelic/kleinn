<?php
namespace Mumby\WebTools\ACL;
use PDO;

if(!defined("_MB_AUTH_TYPE_"))
   define("_MB_PREFERED_AUTH_TYPE_", "local");
else
   define("_MB_PREFERED_AUTH_TYPE_", _MB_AUTH_TYPE_);

/**
 * This is a generic class for creating user objects,
 * regardless of the type of authentication being used.
 */
class User
{
   // User factory
   public static function create($userType=_MB_PREFERED_AUTH_TYPE_, $username="", $password="")
   {
      switch($userType)
      {
         case "local":
            $user = new LocalUser($username, $password);
            break;
         case "debug":
            $user = new DebugUser($username, $password);
            break;
         case "cas":
            $user = new CASUser();
            break;
         case "anon":
            $user = new AnonUser();
            break;
         default:
            $user = false;
            break;
      }

      return $user;
   }
   
   public static function isAuthenticated()
   {
      return LocalUser::isAuthenticated();
   }

   public static function getAuthenticatedUser($userType=_MB_PREFERED_AUTH_TYPE_)
   {
      switch($userType)
      {
         case "local":
            return LocalUser::getAuthenticatedUser();
            break;
         case "debug":
            return DebugUser::getAuthenticatedUser();
            break;
         case "cas":
            return CASUser::getAuthenticatedUser();
            break;
         default:
            return false;
            break;
      }
   }
}