<?php
namespace Mumby\WebTools\ACL;

class AnonUser extends LocalUser
{
   public function __construct() { return true; }
   public static function getAuthenticatedUser() { return new static(); }
   protected function setUserMetadata($row="")
   {
      $this->userID    = "0";
      $this->firstName = "Anonymous";
      $this->lastName  = "User";
      $this->email     = "noone@example.com";
      return true;
   }
   
   public function authenticate($u, $p) { return false; }
   
   public function getPermissions()
   {
      $perms = array(_MB_USER_ANONYMOUS_);
      $this->perms = $perms;
      return $this->perms;
   }
   
   public static function getAllPermissions($appID=null) { return false; }
   public static function isAuthenticated()   { return false; }
   public function isFauxUser()                { return false; }
   public static function getAllUsers()        { return array(); }
   protected function setLoggedIn($username)    { return false; }
   protected static function getStoredLogin()    { return false; }
   public function switchUser($fauxUsername)     { return false; }
}