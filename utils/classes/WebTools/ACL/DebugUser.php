<?php
namespace Mumby\WebTools\ACL;
use PDO;

class DebugUser extends LocalUser
{
   
   /*
    * Basically the same as DBUser except there's no hash on the password.
    * THIS SHOULD NEVER BE USED IN PRODUCTION!
    */
   protected function authenticate($u, $p)
   {
      $userSQL = "SELECT * FROM `users` WHERE `username`=:username AND `password`=:password AND `active`=1 LIMIT 1";
      $stmt = $this->db->prepare($userSQL);
      $stmt->bindParam(":username", $u);
      $stmt->bindParam(":password", $p);
      $stmt->execute();
      $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
      
      if(empty($row))
      {
         $this->loggedIn = false;
      }
      else
      {
         $this->loggedIn = true;

         $thisUser = $row[0];
         $this->setLoggedIn($thisUser["username"]);
         $this->setUserMetadata($thisUser);
      }
      
      return $this->loggedIn;
   }
}