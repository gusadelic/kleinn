<?php
namespace Mumby\WebTools\ACL;
use jasig\phpCAS;
use \Exception;

$cas_errors = array();
if(!defined("_CAS_VERSION_"))
   $cas_errors[] = "You must specify '_CAS_VERSION_' in your application properties.";
if(!defined("_CAS_PROXY_"))
   $cas_errors[] = "You must specify '_CAS_PROXY_' in your application properties.";
if(!defined("_CAS_HOST_"))
   $cas_errors[] = "You must specify '_CAS_HOST_' in your application properties.";
if(!defined("_CAS_PORT_"))
   $cas_errors[] = "You must specify '_CAS_PORT_' in your application properties.";
if(!defined("_CAS_URI_"))
   $cas_errors[] = "You must specify '_CAS_URI_' in your application properties.";

if(!empty($cas_errors))
{
   $exceptionMsg = implode("<br />",$cas_errors);
   throw new Exception($exceptionMsg);
   die();
}

class CASUser extends LocalUser
{
   public function __construct()
   {
      if(isset($_SESSION["phpCAS"]["user"]))
      {
         $this->setLoggedIn($_SESSION["phpCAS"]["user"]);
      }
      else
      {
         // If the user hasn't logged in to CAS, then we need to force them to log in.
         $cas = @new \CAS_Client(_CAS_VERSION_, _CAS_PROXY_, _CAS_HOST_, _CAS_PORT_, _CAS_URI_);
         $cas->setNoCasServerValidation();
         $cas->forceAuthentication();
      }
      
      $thisUser = $this->getStoredLogin();
      parent::__construct($thisUser, "");
      parent::setUserMetadata();
   }
   
   public function logout($redirectURL="")
   {
      parent::logout();
      
      if(!self::isAuthenticated())
      {
         $cas = @new \CAS_Client(_CAS_VERSION_, _CAS_PROXY_, _CAS_HOST_, _CAS_PORT_, _CAS_URI_);
         if(empty($redirectURL))
            @$cas->logout(array());
         else
            @$cas->logout(array('service'=>$redirectURL, 'url'=>$redirectURL));
      }
   }
}