<?php
namespace Mumby\WebTools;
use PDO;

/**
 * Simple helper class that creates Bootstrap Panels. * 
 */
class Panel extends Page
{

  public function __construct()
  {
    parent::__construct();
    $this->depth = 0;
    $this->defaultID = "panel_".$this->depth;
    $this->panelCount = 0;
    $this->newTitle = "";
    $this->newBody = "";
  }
  
  
   /**
   * This function returns the HTML for a new row to me inserted into a panel.
   * 
   * @param string $title Title of the new row.
   * @param string $body  Body of the new row.
   * @param boolean/int $accordian  If false, will add accordian features to the new row. Otherwise, the index of the panelID.
   * @param boolean $interactive  If true, will add a remove button on the far right of the row
   *                           
   */
  function newRow($panelID, $title="New Panel", $body="Content", $accordian=false, $interactive=false) {
      
      $panelOutput = "";
      if ( $interactive ) {
        $panelOutput .= "<script type='text/javascript'>accordian='".$accordian."';</script>\n";
        $panelOutput .= "<script type='text/javascript'>panelcount=".$this->panelCount.";</script>\n";
        //$panelOutput .= "<script type='text/javascript'>newtitle='".$this->newTitle."';newbody='".str_replace("\n", "", str_replace('"', '\"', str_replace("'", "\'", $this->newBody)))."';</script>";
        $panelOutput .= "<script type='text/javascript'>newtitle='".$this->newTitle."';</script>";
        $panelOutput .= "<script type='text/javascript'>newrow=addRowFunction();</script>\n";
        $panelOutput .= "<div id='newbody' class='hidden'>".$this->newBody."</div>\n";
      }
     
      if ( !$accordian ) {
        if ( is_array($body)) {
            $panelOutput .= "   <li class='list-group-item'>\n";
            
          if ( $interactive )
              $panelOutput .= "<span class='panelDeleteLink'><a href='#' onclick='deleteRow($(this));return false;'>Delete</a></span>\n";


          $panelOutput .= $this->getPanel($body, $panelID."_".++$this->depth, array(), array("heading"=>$title, "showHeading"=>true));

        }
        else {
          $panelOutput .= "   <li class='list-group-item'>\n";

          if ( $interactive )
            $panelOutput .= "<span class='panelDeleteLink'><a href='#' onclick='deleteRow($(this));return false;'>Delete</a></span>";
                    
          $panelOutput .= $body . "\n";

        }
           
        $panelOutput .= "</li>\n";
               
      }
      else {
          
        $panelOutput .= "<div class='panel-heading'><h4 class='panel-title'>\n";
        $panelOutput .= "   <a data-toggle='collapse' data-parent='#accordion' href='#collapse_". $this->panelCount ."'>" . $title . "</a>\n";
        if ( $interactive )
          $panelOutput .= "   <span class='panelDeleteLink'><a href='#' onclick='deleteRow($(this));return false;'>Delete</a></span>";
        $panelOutput .= "</h4>\n</div>\n";
        $panelOutput .= "<div id='collapse_" . $this->panelCount . "' class='panel-collapse collapse'>\n";
        $panelOutput .= "   <div class='panel-body'>";
        if ( is_array($body) ) { 
          $panelOutput .= $this->getPanel($body, $panelID."_collapse_".++$this->depth, array(), array("heading"=>$title, "showHeading"=>false));
        }
        else {
          $panelOutput .= $body;
        }
        $panelOutput .= "\n</div>\n</div>\n</div>\n"; 
        
        $panelOutput .= "<script>\n";
        $panelOutput .= "$(function() {\n";
        $panelOutput .= "   $('#".$panelID."_".$this->panelCount."').accordion({\n";
        $panelOutput .= "      collapsible: true,\n";
        $panelOutput .= "      active: false\n";
        $panelOutput .= "  });\n";
        $panelOutput .= "});\n";
        $panelOutput .= "</script>";

        $this->panelCount++;
      }
      
      return $panelOutput;
      
  }
  
  /**
   * This function returns the HTML for a panel.
   * 
   * @param array/string $data Associative array of key/value pairs or String of body data in the format of a table or a list group.
   *                           
   */
  function getPanel($data, $panelID=null, $panelClassName=array(), $metadata=array("showHeading"=>true, "addText" => "Add", "newPanel" => "New Panel", "newPanelBody" => "Content") )
  {
      
    $this->addCSS("/common/css/panels.css");
    $this->panelCount = count($data);
     
    //declaring options 
    if ( !empty($metadata['accordian']) )
      $accordian = $metadata['accordian'];
    else
      $accordian = false;
    
    $interactive = false; 
    $jsOutput = "";
    
    if ( empty($metadata['addText']) )
      $metadata['addText'] = "Add";
    
    if ( empty($metadata['newPanel']) )
      $metadata['newPanel'] = "New Panel";

    if ( empty($metadata['newPanelBody']) )
      $metadata['newPanelBody'] = "Content";
    
    $this->newTitle = $metadata['newPanel'];
    $this->newBody = $metadata['newPanelBody'];
    
    if ( empty($panelID) )
      $panelID = $this->defaultID;
     
    if(empty($data))
      return false;
    
    $panelData = array();
    if ( !is_array($data) ) {
      $panelData[] = array(
        "title" => "Panel",
        "body"  => $data
      ); 
    }
    else {
        if ( empty($data['title']) && empty($data[0]['title']) ) {
          foreach( $data as $v ) {
            $panelData[] = array(
              "title" => "Panel",
              "body"  => $v
            ); 
          }
        }
        else {
          if ( empty($data['title']) ) {
            $panelData = $data;
          }
          else {
            $panelData[] = $data;
          }
        }
    }
    
    if(!empty($panelClassName))
    {
      if(!is_array($panelClassName))#collapse_". $accordian
      {
        $panelClassName = array($panelClassName);
      }

      $accordian = array_search("accordian", $panelClassName);
      $interactive = array_search("interactive", $panelClassName);
    }
    else
       $panelClassName = array();

    $panelOutput = "";
    
    if (is_numeric($interactive) ) {
      $panelOutput .= "<script type='text/javascript'>panelcount=".$this->panelCount.";</script>\n";
      $panelOutput .= "<script type='text/javascript'>accordian='".$accordian."';</script>\n";
      $panelOutput .= "<script type='text/javascript'>panelcount=".$this->panelCount.";</script>\n";
      $panelOutput .= "<script type='text/javascript'>newtitle='".$this->newTitle."';</script>\n";
      $panelOutput .= "<script type='text/javascript'>newrow=addRowFunction();</script>\n";
      $panelOutput .= "<div id='newbody' class='hidden'>".$this->newBody."</div>\n";
    }
    if ( $interactive ) {
      $jsOutput .= "panelid='".$panelID."';";
      $this->addJS("/common/js/panels.js");
      $panelOutput .= '<div style="float:right;">[<a href="#" onclick="addRow();return false;">'. $metadata['addText'] .'</a>]  </div><br />' . "\n";
    }
    
    $panelOutput .= "<div ";
    $panelClassName[] = "panel";
    
    if(!empty($panelClassName))
      $panelOutput .= " class='".  implode(" ", $panelClassName)."'";
    
    if(!empty($panelID)) {
      $thisPanelID = $panelID."_".$this->panelCount;
      $panelOutput .= " id='".$thisPanelID."'";
    }
          
    $panelOutput .= ">\n";      

    if ( empty($accordian) ) {               
        
      if ( !empty($metadata['showHeading']) ) {
        $panelOutput .= "<div class='panel-heading'><h3 class='panel-title'>" . $panelData[0]['title'];
        $panelOutput .= "</h3></div>\n";
      }
      
      $panelOutput .= "<ul class='list-group'>\n";
      foreach ( $panelData as $v ) {
          
          $panelOutput .= $this->newRow($panelID, $v['title'], $v['body'], $accordian, $interactive) . "\n";
          
      }
      $panelOutput .= "</ul>\n</div>\n";
    }
    else {
      foreach ( $panelData as $k=>$v ) {
        if ( empty($v['body']) ) continue;
        $newRow = $this->newRow($panelID, $v['title'], $v['body'], $accordian, $interactive) . "\n";
        $panelOutput .= $newRow;
      }
    }


    // Interactive JS code      
    
    if(!empty($jsOutput))
    {
       $jsOutput = "<script type='text/javascript'>".
                   "\$(document).ready( function() {".
                      $jsOutput.
                   "});\n".
                   "</script>\n\n";
    }
    
    return $panelOutput.$jsOutput;
  }
   
   
}