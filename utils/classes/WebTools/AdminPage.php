<?php
namespace Mumby\WebTools;

class AdminPage extends Page
{
   private $isAuth;
   public function __construct($pageTitle, &$user, $permissionsLevel=_MB_USER_AUTHENTICATED_, $themeFile = "common/admin/admin.html", $headerFile = "common/admin/header.html", $footerFile = "common/admin/footer.html")
   {
      $user = \Mumby\WebTools\ACL\User::create(_MB_AUTH_TYPE_);

      if(is_array($permissionsLevel))
      {
         foreach($permissionsLevel as $p)
         {
            if($user->isAuthenticated() && $user->hasPermission($p))
               $this->isAuth = true;
            else
            {
               $this->isAuth = false;
               break;
            }
         }
         
         if(!$this->isAuth)
            return false;
         else
         {
            
            parent::__construct($themeFile, $headerFile, $footerFile);
            $this->isAdminPage();
            $this->setLoggedInUser($user->firstName." ".$user->lastName);
         }
      }
      else
      {
         if($user->isAuthenticated() && $user->hasPermission($permissionsLevel))
         {
            $this->isAuth = true;
            parent::__construct($themeFile, $headerFile, $footerFile);
            $this->isAdminPage($pageTitle);
            $this->setLoggedInUser($user->firstName." ".$user->lastName);
         }
         else
            $this->isAuth = false;
      }
   }
   
   public function isAuthorized()
   {
      return $this->isAuth;
   }

   public function isAdminPage()
   {
      $this->addInjectable("adminPage", "bodyClass");
      $this->addCSS("/common/admin/css/core.css");
      $this->addCSS("/common/bower/font-awesome/css/font-awesome.min.css");
      $this->contentClass = "adminView";
   }
   
   public function hideAdminMenu()
   {
      $hideAdminJS  = "<script type='text/javascript'>\n";
      $hideAdminJS .= "      $('#page-wrapper').css('margin-left','auto');\n";
      $hideAdminJS .= "      $('#page-wrapper').css('border-left','0');\n";
      $hideAdminJS .= "      $('.navbar-default.sidebar').hide();\n";
      $hideAdminJS .= "   </script>\n";
      
      $this->appendBody($hideAdminJS);
   }
}