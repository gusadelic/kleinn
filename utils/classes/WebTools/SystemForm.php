<?php
namespace Mumby\WebTools;
use Mumby\WebTools;
   
class SystemForm extends Form
{
   var $fieldCount;
   var $formResponses;
   
   static $sysFormCounter;
   public function __construct($formID=0, $inlineForm=false, $method = 'post', $showLoading = true)
   {
      $this->fieldCount = 0;
      parent::__construct($formID, $inlineForm, $method, $showLoading);
      
      SystemForm::$sysFormCounter++;
      
      if(empty($formID))
         $this->id = $this->formatVal(SystemForm::$sysFormCounter);
      
      // Create a throw-away config object to make sure that
      // the autoloader includes the class.
      $config = new Config();
      
      $this->formResponses = array();
      
      $this->skipMultiFieldProcessing = false;
   }
   
   public function getResponses() {
      return $this->formResponses;
   }
  
   
   public function setMethod($method)
   {
      if($method == "get")
      {
         $this->method = "get";
         $this->setResponseSource($this->method);
      }
      else
      {
         $this->method = "post";
         $this->setResponseSource($this->method);
      }
   }
   
   public function addField($fieldType, $fieldData = "", $additionalData=array())
   {
      if(!isset($fieldData["FormFieldID"]))
         $fieldData["FormFieldID"] = $this->fieldCount;
      
      if(!isset($fieldData["FormFieldLabel"]))
         $fieldData["FormFieldLabel"] = "";
      
      if(!isset($fieldData["FormFieldTypeConstant"]))
         $fieldData["FormFieldTypeConstant"] = $fieldType;
      
      if(!isset($fieldData["FormFieldMetadata"]))
         $fieldData["FormFieldMetadata"] = array();
      
      foreach($additionalData as $k=>$v)
      {
         if(!isset($fieldData[$k]))
            $fieldData[strtolower($k)] = $v;
      }
      
      $fieldType = constant($fieldType);
      parent::addField($fieldType, $fieldData);
      $this->fieldCount++;
      
      return $this->getField($fieldData["FormFieldID"]);
   }
   
   public function addCaptchaField($metadata = array() )
   {
      return $this->addField("_MB_FORM_FIELD_CAPTCHA_", $metadata);
   }
   
   public function addCheckboxField($label, $options, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel" => $label,
         "options" => $options,
         "required"=>$required
      );
      
      return $this->addField("_MB_FORM_FIELD_CHECKBOX_", $fieldData, $metadata);
   }
   
   public function addColorOptionsField($label, $options, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel" => $label,
         "options" => $options,
         "required"=>$required
      );
      
      return $this->addField("_MB_FORM_FIELD_COLOR_OPTIONS_", $fieldData, $metadata);
   }
   
   public function addCurrencyField($label, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel" => $label,
         "required"=>$required
      );
      
      return $this->addField("_MB_FORM_FIELD_CURRENCY_", $fieldData, $metadata);
   }
   
   public function addDateTimeField($label, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel" => $label,
         "required"=>$required,
         "placeholder"=>"YYYY-MM-DD",
      );
      return $this->addField("_MB_FORM_FIELD_DATETIME_", $fieldData, $metadata);
   }
   
   public function addDateField($label, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel" => $label,
         "required"=>$required,
         "placeholder"=>"YYYY-MM-DD",
      );
      return $this->addField("_MB_FORM_FIELD_DATE_", $fieldData, $metadata);
   }
   
   
   public function addHTMLDateField($label, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel" => $label,
         "required"=>$required,
         "placeholder"=>"YYYY-MM-DD",
      );
      return $this->addField("_MB_FORM_FIELD_HTMLDATE_", $fieldData, $metadata);
   }
   

   public function addTextField($label, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel"     => $label,
         "required"  => $required
      );

      return $this->addField("_MB_FORM_FIELD_TEXT_", $fieldData, $metadata);
   }
   
   public function addMultiField($label, $rows, $cols, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel" => $label,
         "rows" => $rows,
         "cols" => $cols,
         "required"=>$required
      );
      return $this->addField("_MB_FORM_FIELD_MULTIBOX_", $fieldData, $metadata);      
   }
   
   public function addEmailField($label, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel"     => $label,
         "required"  => $required
      );

      return $this->addField("_MB_FORM_FIELD_EMAIL_", $fieldData, $metadata);
   }

   public function addFileField($label, $required=false, $metadata=array())
   {
      $this->data["enctype"] = "multipart/form-data";

      $fieldData = array(
         "FormFieldLabel"     => $label,
         "required"  => $required
      );

      return $this->addField("_MB_FORM_FIELD_FILE_", $fieldData, $metadata);
   }

   public function addHiddenField($label="", $metadata=array(), $required= false)
   {
      $fieldData = array(
         "FormFieldLabel"     => $label,
         "required"  => $required
      );

      return $this->addField("_MB_FORM_FIELD_HIDDEN_", $fieldData, $metadata);
   }

//   /* This doesn't work yet! */
//   public function addMultiFileField($label, $required=false, $metadata=array())
//   {
//      $fieldData = array(
//         "FormFieldLabel"     => $label,
//         "required"  => $required
//      );
//
//      return $this->addField("_MB_FORM_FIELD_MULTIFILE_", $fieldData, $metadata);
//   }

   public function addHTMLField($label, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel"     => $label,
         "required"  => $required
      );

      return $this->addField("_MB_FORM_FIELD_HTML_", $fieldData, $metadata);
   }

   public function addMarkup($markup, $leaveUnwrapped = false, $metadata=array())
   {
      $fieldData = array(
         "markup" => $markup,
         "dumpraw" => $leaveUnwrapped
      );

      return $this->addField("_MB_FORM_FIELD_MARKUP_", $fieldData, $metadata);
   }
   
   public function addNumberField($label, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel" => $label,
         "required"=>$required
      );
      
      return $this->addField("_MB_FORM_FIELD_NUMBER_", $fieldData, $metadata);
   }
   
   public function addPasswordField($label, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel"     => $label,
         "required"  => $required
      );

      return $this->addField("_MB_FORM_FIELD_PASS_", $fieldData, $metadata);
   }
   
   public function addPhoneField($label, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel"     => $label,
         "required"  => $required
      );

      return $this->addField("_MB_FORM_FIELD_PHONE_", $fieldData, $metadata);
   }
   
   public function addRadioField($label, $options, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel" => $label,
         "options" => $options,
         "required"=>$required
      );
      
      return $this->addField("_MB_FORM_FIELD_RADIO_", $fieldData, $metadata);
   }
   
   public function addSearchField($label, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel"     => $label,
         "required"  => $required
      );
      
      return $this->addField("_MB_FORM_FIELD_SEARCH_", $fieldData, $metadata);
   }
   
   public function addSelectField($label, $options, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel" => $label,
         "options" => $options,
         "required"=>$required
      );
      
      return $this->addField("_MB_FORM_FIELD_SELECT_", $fieldData, $metadata);
   }
   
   public function addTermField($label, $options, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel" => $label,
         "options" => $options,
         "required"=>$required
      );
      
      return $this->addField("_MB_FORM_FIELD_TERM_", $fieldData, $metadata);
   }

   public function addSchedulerFields($label, $required=false, $time = false, $skipProcessing=true, $metadata=array())
   {
            $fieldCount = 0;
            $schedulerFields = array();

            if ( $required == false ) $requiredText = "false";
            else $requiredText = "true";
            
            $metadata['enableTimeScheduling'] = $time;
            $metadata['skipMultifieldProcessing'] = $skipProcessing;
            
            // Each field needs the following:
            $fieldCount++;
            $thisFieldType = "_MB_FORM_FIELD_SELECT_";
            $thisFieldData = array(
                'FormFieldID'           =>  $label . "_" . $fieldCount,
                'FormID'                =>  $this->id,
                'FormFieldLabel'        =>  "Schedule",
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"multiFieldClass":"Scheduler","options":{"0":"One-Time","1":"Daily","2":"Every weekday (Monday to Friday)","3":"Every Monday, Wednesday, and Friday","4":"Every Tuesday and Thursday","5":"Weekly","6":"Monthly"},"required":'.$requiredText.',"fieldName":"day_schedule","onchange": "toggleFields($(this))"}',
                'form_metadata'         =>  $metadata
            );
            $schedulerFields[] = $this->addField($thisFieldType, $thisFieldData);
            //
            $fieldCount++;
            if ( $time ) {
                $thisFieldType = "_MB_FORM_FIELD_DATETIME_";
                $thisLabel = "Date/Time";
            }
            else {
                $thisFieldType = "_MB_FORM_FIELD_DATE_";
                $thisLabel = "Date";
            }
            $thisFieldData = array(
                'FormFieldID'           =>  $label . "_" . $fieldCount,
                'FormID'                =>  $this->id,
                'FormFieldLabel'        =>  $thisLabel,
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"fieldName":"Date_0","multiFieldClass":"Scheduler","fieldClass":"child_of_day_schedule parent_option_0","required":true,"allowBlank":"true","visibleByDefault": "false"}',
                'form_metadata'         =>  $metadata
            );
            $schedulerFields[] = $this->addField($thisFieldType, $thisFieldData);
            //
            $fieldCount++;
            $thisFieldType = "_MB_FORM_FIELD_SELECT_";
            $days = array();
            $days[] = '"0": "1 day"';
            for ($i=2;$i<=30;$i++) { $days[] = '"' . ($i-1) . '": "'. $i .' days"'; }
            $thisFieldData = array(
                'FormFieldID'           =>  $label . "_" . $fieldCount,
                'FormID'                =>  $this->id,
                'FormFieldLabel'        =>  "Repeat every",
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"fieldName":"Repeat_1","multiFieldClass":"Scheduler","options": {' . implode(",", $days) . '}, "fieldClass":"child_of_day_schedule parent_option_1","required":true,"allowBlank":"true","visibleByDefault": "false"}',
                'form_metadata'         =>  $metadata
            );
            $schedulerFields[] = $this->addField($thisFieldType, $thisFieldData);
            //
            $fieldCount++;
            $thisFieldType = "_MB_FORM_FIELD_SELECT_";
            $weeks = array();
            $weeks[] = '"0": "1 week"';
            for ($i=2;$i<=30;$i++) { $weeks[] = '"' . ($i-1) . '": "'. $i .' weeks"'; }
            $thisFieldData = array(
                'FormFieldID'           =>  $label . "_" . $fieldCount,
                'FormID'                =>  $this->id,
                'FormFieldLabel'        =>  "Repeat every",
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"fieldName":"Repeat_5","multiFieldClass":"Scheduler","options": {' . implode(",", $weeks) . '}, "fieldClass":"child_of_day_schedule parent_option_5","required":true,"allowBlank":"true","visibleByDefault": "false"}',
                'form_metadata'         =>  $metadata
            );
            $schedulerFields[] = $this->addField($thisFieldType, $thisFieldData);
            //
            $fieldCount++;
            $thisFieldType = "_MB_FORM_FIELD_SELECT_";
            $months = array();
            $months[] = '"0": "1 month"';
            for ($i=2;$i<=30;$i++) { $months[] = '"' . ($i-1) . '": "'. $i .' months"'; }
            $thisFieldData = array(
                'FormFieldID'           =>  $label . "_" . $fieldCount,
                'FormID'                =>  $this->id,
                'FormFieldLabel'        =>  "Repeat every",
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"fieldName":"Repeat_6","multiFieldClass":"Scheduler","options": {' . implode(",", $months) . '}, "fieldClass":"child_of_day_schedule parent_option_6","required":true,"allowBlank":"true","visibleByDefault": "false"}',
                'form_metadata'         =>  $metadata
            );
            $schedulerFields[] = $this->addField($thisFieldType, $thisFieldData);
            //
            $fieldCount++;
            $thisFieldType = "_MB_FORM_FIELD_CHECKBOX_";
            $thisFieldData = array(
                'FormFieldID'           =>  $label . "_" . $fieldCount,
                'FormID'                =>  $this->id,
                'FormFieldLabel'        =>  "Repeat On",
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"fieldName":"Days_5","multiFieldClass":"Scheduler","options": {"0": "Monday","1": "Tuesday","2": "Wednesday","3": "Thursday","4": "Friday","5": "Saturday","6": "Sunday"
},"fieldClass":"child_of_day_schedule parent_option_5","required":true,"allowBlank":"true","visibleByDefault": "false"}',
                'form_metadata'         =>  $metadata
            );
            $schedulerFields[] = $this->addField($thisFieldType, $thisFieldData);
            //
            $fieldCount++;
            $thisFieldType = "_MB_FORM_FIELD_RADIO_";
            $thisFieldData = array(
                'FormFieldID'           =>  $label . "_" . $fieldCount,
                'FormID'                =>  $this->id,
                'FormFieldLabel'        =>  "Repeat By",
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"fieldName":"Type_6","multiFieldClass":"Scheduler","options": {"0": "day of the month","1": "day of the week"},"fieldClass":"child_of_day_schedule parent_option_6","required":true,"allowBlank":"true","visibleByDefault": "false"}',
                'form_metadata'         =>  $metadata
            );
            $schedulerFields[] = $this->addField($thisFieldType, $thisFieldData);
            //
            $fieldCount++;
            if ( $time ) {
                $thisFieldType = "_MB_FORM_FIELD_DATETIME_";
                $thisLabel = "Start Date/Time";
            }
            else {
                $thisFieldType = "_MB_FORM_FIELD_DATE_";
                $thisLabel = "Start Date";
            }
            $thisFieldData = array(
                'FormFieldID'           =>  $label . $fieldCount,
                'FormID'                =>  $this->id,
                'FormFieldLabel'        =>  $thisLabel,
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"fieldName":"From","multiFieldClass":"Scheduler","fieldClass":"child_of_day_schedule parent_option_1 parent_option_2 parent_option_3 parent_option_4 parent_option_5 parent_option_6","required":true,"allowBlank":"true","visibleByDefault": "false"}',
                'form_metadata'         =>  $metadata
            );
            $schedulerFields[] = $this->addField($thisFieldType, $thisFieldData);
            //
            $fieldCount++;
            $thisFieldType = "_MB_FORM_FIELD_DATE_";
            $thisFieldData = array(
                'FormFieldID'           =>  $label . "_" . $fieldCount,
                'FormID'                =>  $this->id,
                'FormFieldLabel'        =>  "Stop Date",
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"fieldName":"Until","multiFieldClass":"Scheduler","fieldClass":"child_of_day_schedule parent_option_1 parent_option_2 parent_option_3 parent_option_4 parent_option_5 parent_option_6","visibleByDefault": "false","moreLabel":"Leave blank if never ending"}',
                'form_metadata'         =>  $metadata
            );
            $schedulerFields[] = $this->addField($thisFieldType, $thisFieldData);
            
            return $schedulerFields;
            
   }
   public function addBuildingsField($label, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel" => $label,
         "required"=>$required
      );
      
      return $this->addField("_MB_FORM_FIELD_BUILDINGS_", $fieldData, $metadata);
   }
   
   public function addStateField($label, $required=false, $useAbbreviations=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel" => $label,
         "required"=>$required,
         "useAbbr" => $useAbbreviations
      );
      
      return $this->addField("_MB_FORM_FIELD_STATE_", $fieldData, $metadata);
   }

   public function addTextAreaField($label, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel"     => $label,
         "required"  => $required
      );

      return $this->addField("_MB_FORM_FIELD_TEXTAREA_", $fieldData, $metadata);
   }
   
   public function addTimeField($label, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel"     => $label,
         "required"  => $required
      );

      return $this->addField("_MB_FORM_FIELD_TIME_", $fieldData, $metadata);
   }
   
   public function addURLField($label, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel"     => $label,
         "required"  => $required
      );

      return $this->addField("_MB_FORM_FIELD_URL_", $fieldData, $metadata);
   }
   
   public function addZipCodeField($label, $required=false, $metadata=array())
   {
      $fieldData = array(
         "FormFieldLabel"     => $label,
         "required"  => $required
      );

      return $this->addField("_MB_FORM_FIELD_ZIPCODE_", $fieldData, $metadata);
   }
   
   public function addFieldBreak($fieldbreakID=null, $fieldbreakClass=null)
   {
      return $this->addField("_MB_FORM_FIELD_BREAK_", array("FormFieldID"=>$fieldbreakID, "FormFieldClass"=>$fieldbreakClass));
   }
   
   private function setFormResponse($responseType, $responseMetadata)
   {
      $thisFRID = count($this->formResponses);
      $this->formResponses[$responseType] = array(
         "FormResponseID"           => count($this->formResponses), // Don't think this is actually used but just in case...
         "FormID"                   => $this->id,
         "FormResponseTypeConstant" => $responseType,
         "FormResponseMetadata"     => $responseMetadata
      );
   }

   /**
    * Set the form to store the submitted data in the CMS database
    */
   public function setDBStoreResponse()
   {
      $this->setFormResponse("_MB_FORM_SUBMISSION_DB_STORE_", "");
   }
   
   /**
    * Set the form to display the submitted data.
    */
   public function setDisplayResponse()
   {
      $this->setFormResponse("_MB_FORM_SUBMISSION_DISPLAY_", "");
   }
   
   /**
    * Set the form to email the submitted data.
    * 
    * @param mixed $name Either an individual email address or an array of email addresses
    */
   public function setEmailResponse($recipients)
   {
      if(!is_array($recipients))
         $metadata = array("recipients" => array($recipients));
      else
         $metadata = array("recipients" => $recipients);

      $this->setFormResponse("_MB_FORM_SUBMISSION_EMAIL_", json_encode($metadata));
   }
   
   /**
    * Set the form to email THE USER a copy of the submitted data.
    * This assumes that you have set an email field within the form
    * to be the user's email address (using $formField->sendUserEmail = true;).
    * 
    * You can also set fields to be the email subject line using
    * $formField->setAsSubject = true;
    * 
    */
   public function setUserEmailResponse()
   {
      if(!is_array($recipients))
         $metadata = array("recipients" => array($recipients));
      else
         $metadata = array("recipients" => $recipients);

      $this->setFormResponse("_MB_FORM_SUBMISSION_EMAIL_", json_encode($metadata));
   }
   
   /**
    * Set the form to display a specific message after the form is submitted.
    * 
    * @param mixed $message Message to be displayed.
    */
   public function setMessageResponse($message)
   {
      $metadata = array("message" => $message);
      $this->setFormResponse("_MB_FORM_SUBMISSION_EMAIL_", json_encode($metadata));
   }
   
   /**
    * Set the form to redirect to another page after the form is submitted.
    * 
    * @param string $url URL to be redirected to.
    */
   public function setRedirectResponse($url)
   {
      $metadata = array("url" => $url);
      $this->setFormResponse("_MB_FORM_SUBMISSION_REDIRECT_", json_encode($metadata));
   }
   
   /**
    * Set the form to store any files submitted via the form.
    * Note: File store path can be set by using the $formField->fileDestinationPath
    * property on the field itself.
    */
   public function setFileStoreResponse()
   {
      $this->setFormResponse("_MB_FORM_SUBMISSION_FILE_STORE_", "");
   }
}