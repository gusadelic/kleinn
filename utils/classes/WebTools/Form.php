<?php
namespace Mumby\WebTools;
use \PHPMailer;
use \HTMLPurifier;
use \HTMLPurifier_Config;
use \Captcha;
use \Mumby\Inflect;

use \Mumby\WebTools\Config;
use \Mumby\WebTools\WYSIWYG_Config;


/*
 * Lots of stuff going on in here. We have multiple classes defined
 * here, including the Form class, multiple FormField classes (for
 * each type of form field supported), and a FormFieldCrypt function
 * used for encrypting/decrypting data using AES.
 */

// This should be overwritten in your application properties!!
if(!defined("_MB_AES_ENC_SECRET_"))
   define("_MB_AES_ENC_SECRET_", "N0t A V3rY G0Od S3Cr3T!!");

class Form extends Page
{
   var $db;
   var $fields;
   var $data;
   var $id;
   var $dataSrc;

   protected $formTable;
   protected $formFieldTable;
   protected $formResponseTable;
   protected $formSubmissionTable;

   public $multiFieldResults;
   public $skipMultiFieldProcessing;

   public function __construct($formID=0, $inlineForm = false, $method = 'post', $showLoading = true)
   {
      $this->sourceTable      = "Forms";
      $this->idCol            = "FormID";

      $this->inlineForm = $inlineForm;
      $this->showLoading = $showLoading;

      $this->skipMultiFieldProcessing = true;

      // Don't let anyone change user IDs.
      $this->readOnlyFields   = array("FormID");

      $this->fieldInfo = array(
         "FormName"     => array("type"=>self::STRING, "length"=>255),
         "FormMetadata" => array("type"=>self::STRING, "length"=>65535)
      );

      if(empty($this->formTable))
         $this->formTable = "Forms";

      if(empty($this->formFieldTable))
         $this->formFieldTable = "FormFields";

      if(empty($this->formResponseTable))
         $this->formResponseTable = "FormResponses";

      if(empty($this->formSubmissionTable))
         $this->formSubmissionTable = "FormSubmissions";

      parent::__construct();
      $this->load($formID);

      $this->method = $method;
      $this->setResponseSource($this->method);

      //$this->data["enctype"] = "application/x-www-form-urlencoded";
      $this->data["valid"] = true;
      $this->data["errors"] = array();
      $this->data["uploadedfiles"] = array();
      $this->data["successMessages"] = array();

      if ( !empty($this->data['returnURL']) )
          $this->returnURL = $this->data['returnURL'];

      $this->addJS("/common/js/forms.js");
      $this->addCSS("/common/css/forms.css");

   }

   function setResponseSource($method="post")
   {
      if($method == "get")
         $this->responseSource = $_GET;
      else
         $this->responseSource = $_POST;
   }

   /**
    * Overwriting the parent function so that we can load form fields
    * upon instantiation.
    *
    * @param array $id Associative array mapping column names to values, typically the unique ID for the table.
    * @return bool True on success, false on failure.
    */
   public function load($id)
   {
      // Remove any previous instance data.
      $this->originalData = array();
      $this->data = array();

      $formSQL  = "SELECT ";
      $formSQL .= "fields.*, f.FormName as legend, f.FormMetadata as form_metadata ";
      $formSQL .= "FROM ".$this->formFieldTable." AS fields ";
      $formSQL .= "INNER JOIN ".$this->formTable." f ON f.FormID=fields.FormID ";
      $formSQL .= "WHERE f.FormID = :id ORDER BY fields.FormFieldOrder ASC";
      $formData = array("id"=>$id);

      $rows = $this->query($formSQL, $formData);

      if(empty($rows))
         return false;

      if(is_array($id))
         $this->id = $id;
      else
         $this->id['FormID'] = $id;

      foreach($rows as $r)
      {
         if(empty($this->formName))
            $this->formName = $r["legend"];

         if(empty($this->formMetadata) && !empty($r["form_metadata"]))
         {
            $this->formMetadata = $r["form_metadata"];
            if(($metadata = json_decode($r["form_metadata"], true)))
            {
               foreach($metadata as $k=>$m)
               {
                  $this->data[strtolower($k)] = $m;
               }
            }
         }

         $fieldType = constant($r["FormFieldTypeConstant"]);

         // Multi-Field Supertypes
         if ( $fieldType == _MB_FORM_FIELD_SCHEDULER_ ) {
            $fieldCount = 0;

            $time = !empty($this->data['enabletimescheduleing']);

            // Each field needs the following:
            $fieldCount++;
            $thisFieldType = "_MB_FORM_FIELD_SELECT_";
            $thisFieldData = array(
                'FormFieldID'           =>  $r['FormFieldID'] . "_" . $fieldCount,
                'FormID'                =>  $id,
                'FormFieldLabel'        =>  "Schedule",
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"multiFieldClass":"Scheduler","options":{"0":"One-Time","1":"Daily","2":"Every weekday (Monday to Friday)","3":"Every Monday, Wednesday, and Friday","4":"Every Tuesday and Thursday","5":"Weekly","6":"Monthly"},"required":true,"fieldName":"interval","onchange": "toggleFields($(this))"}',
                'FormFieldOrder'        =>  $r['FormFieldOrder'],
                'legend'                =>  $r["legend"],
                'form_metadata'         =>  $r["form_metadata"]
            );
            $this->addField($thisFieldType, $thisFieldData);
            //
            $fieldCount++;
            if ( $time ) {
                $thisFieldType = "_MB_FORM_FIELD_DATETIME_";
                $thisLabel = "Date/Time";
            }
            else {
                $thisFieldType = "_MB_FORM_FIELD_DATE_";
                $thisLabel = "Date";
            }
            $thisFieldData = array(
                'FormFieldID'           =>  $r['FormFieldID'] . "_" . $fieldCount,
                'FormID'                =>  $id,
                'FormFieldLabel'        =>  $thisLabel,
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"fieldName":"Date_0","multiFieldClass":"Scheduler","fieldClass":"child_of_interval parent_option_0","required":true,"allowBlank":"true","visibleByDefault": "false"}',
                'FormFieldOrder'        =>  $r['FormFieldOrder'],
                'legend'                =>  $r["legend"],
                'form_metadata'         =>  $r["form_metadata"]
            );
            $this->addField($thisFieldType, $thisFieldData);
            //
            $fieldCount++;
            $thisFieldType = "_MB_FORM_FIELD_SELECT_";
            $days = array();
            $days[] = '"0": "1 day"';
            for ($i=2;$i<=30;$i++) { $days[] = '"' . ($i-1) . '": "'. $i .' days"'; }
            $thisFieldData = array(
                'FormFieldID'           =>  $r['FormFieldID'] . "_" . $fieldCount,
                'FormID'                =>  $id,
                'FormFieldLabel'        =>  "Repeat every",
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"fieldName":"Repeat_1","multiFieldClass":"Scheduler","options": {' . implode(",", $days) . '}, "fieldClass":"child_of_interval parent_option_1","required":true,"allowBlank":"true","visibleByDefault": "false"}',
                'FormFieldOrder'        =>  $r['FormFieldOrder'],
                'legend'                =>  $r["legend"],
                'form_metadata'         =>  $r["form_metadata"]
            );
            $this->addField($thisFieldType, $thisFieldData);
            //
            $fieldCount++;
            $thisFieldType = "_MB_FORM_FIELD_SELECT_";
            $weeks = array();
            $weeks[] = '"0": "1 week"';
            for ($i=2;$i<=30;$i++) { $weeks[] = '"' . ($i-1) . '": "'. $i .' weeks"'; }
            $thisFieldData = array(
                'FormFieldID'           =>  $r['FormFieldID'] . "_" . $fieldCount,
                'FormID'                =>  $id,
                'FormFieldLabel'        =>  "Repeat every",
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"fieldName":"Repeat_5","multiFieldClass":"Scheduler","options": {' . implode(",", $weeks) . '}, "fieldClass":"child_of_interval parent_option_5","required":true,"allowBlank":"true","visibleByDefault": "false"}',
                'FormFieldOrder'        =>  $r['FormFieldOrder'],
                'legend'                =>  $r["legend"],
                'form_metadata'         =>  $r["form_metadata"]
            );
            $this->addField($thisFieldType, $thisFieldData);
            //
            $fieldCount++;
            $thisFieldType = "_MB_FORM_FIELD_SELECT_";
            $months = array();
            $months[] = '"0": "1 month"';
            for ($i=2;$i<=30;$i++) { $months[] = '"' . ($i-1) . '": "'. $i .' months"'; }
            $thisFieldData = array(
                'FormFieldID'           =>  $r['FormFieldID'] . "_" . $fieldCount,
                'FormID'                =>  $id,
                'FormFieldLabel'        =>  "Repeat every",
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"fieldName":"Repeat_6","multiFieldClass":"Scheduler","options": {' . implode(",", $months) . '}, "fieldClass":"child_of_interval parent_option_6","required":true,"allowBlank":"true","visibleByDefault": "false"}',
                'FormFieldOrder'        =>  $r['FormFieldOrder'],
                'legend'                =>  $r["legend"],
                'form_metadata'         =>  $r["form_metadata"]
            );
            $this->addField($thisFieldType, $thisFieldData);
            //
            $fieldCount++;
            $thisFieldType = "_MB_FORM_FIELD_CHECKBOX_";
            $thisFieldData = array(
                'FormFieldID'           =>  $r['FormFieldID'] . "_" . $fieldCount,
                'FormID'                =>  $id,
                'FormFieldLabel'        =>  "Repeat On",
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"fieldName":"Days_5","multiFieldClass":"Scheduler","options": {"0": "Monday","1": "Tuesday","2": "Wednesday","3": "Thursday","4": "Friday","5": "Saturday","6": "Sunday"
},"fieldClass":"child_of_interval parent_option_5","required":true,"allowBlank":"true","visibleByDefault": "false"}',
                'FormFieldOrder'        =>  $r['FormFieldOrder'],
                'legend'                =>  $r["legend"],
                'form_metadata'         =>  $r["form_metadata"]
            );
            $this->addField($thisFieldType, $thisFieldData);
            //
            $fieldCount++;
            $thisFieldType = "_MB_FORM_FIELD_RADIO_";
            $thisFieldData = array(
                'FormFieldID'           =>  $r['FormFieldID'] . "_" . $fieldCount,
                'FormID'                =>  $id,
                'FormFieldLabel'        =>  "Repeat By",
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"fieldName":"Type_6","multiFieldClass":"Scheduler","options": {"0": "day of the month","1": "day of the week"},"fieldClass":"child_of_interval parent_option_6","required":true,"allowBlank":"true","visibleByDefault": "false"}',
                'FormFieldOrder'        =>  $r['FormFieldOrder'],
                'legend'                =>  $r["legend"],
                'form_metadata'         =>  $r["form_metadata"]
            );
            $this->addField($thisFieldType, $thisFieldData);
            //
            $fieldCount++;
            if ( $time ) {
                $thisFieldType = "_MB_FORM_FIELD_DATETIME_";
                $thisLabel = "Start Date/Time";
            }
            else {
                $thisFieldType = "_MB_FORM_FIELD_DATE_";
                $thisLabel = "Start Date";
            }
            $thisFieldData = array(
                'FormFieldID'           =>  $r['FormFieldID'] . "_" . $fieldCount,
                'FormID'                =>  $id,
                'FormFieldLabel'        =>  $thisLabel,
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"fieldName":"From","multiFieldClass":"Scheduler","fieldClass":"child_of_interval parent_option_1 parent_option_2 parent_option_3 parent_option_4 parent_option_5 parent_option_6","required":true,"allowBlank":"true","visibleByDefault": "false"}',
                'FormFieldOrder'        =>  $r['FormFieldOrder'],
                'legend'                =>  $r["legend"],
                'form_metadata'         =>  $r["form_metadata"]
            );
            $this->addField($thisFieldType, $thisFieldData);
            //
            $fieldCount++;
            $thisFieldType = "_MB_FORM_FIELD_DATE_";
            $thisFieldData = array(
                'FormFieldID'           =>  $r['FormFieldID'] . "_" . $fieldCount,
                'FormID'                =>  $id,
                'FormFieldLabel'        =>  "Stop Date",
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"fieldName":"Until","multiFieldClass":"Scheduler","fieldClass":"child_of_interval parent_option_1 parent_option_2 parent_option_3 parent_option_4 parent_option_5 parent_option_6","visibleByDefault": "false","moreLabel":"Leave blank if never ending"}',
                'FormFieldOrder'        =>  $r['FormFieldOrder'],
                'legend'                =>  $r["legend"],
                'form_metadata'         =>  $r["form_metadata"]
            );
            $this->addField($thisFieldType, $thisFieldData);
            //

         }
         else if ( $fieldType == _MB_FORM_FIELD_RESERVABLE_ROOMS_ )
         {
            $roomDB = new \Mumby\DB\Room();
            $rooms = $roomDB->getReservableRooms(array(96,97,269,84,0));
            $options = array();
            if ( is_array($rooms) )
                foreach ( $rooms as $room ) {
                    $options[$room['RoomID']] = '"'.$room['RoomID'].'":"' . $room['BuildingAbbr'] . '-' . $room['RoomNumber'] . '"';
            }

            $thisFieldName =  "rooms_".$r['FormFieldID'];
            $thisFieldLabel = "Room";
            if ( $r['FormFieldLabel'] ) $thisFieldLabel = $r['FormFieldLabel'];
            if ( !$this->ignoreOther ) {
                $options[0] = '"0":"Other..."';
            }

            $thisFieldType = "_MB_FORM_FIELD_SELECT_";
            $thisFieldData = array(
                'FormFieldID'           =>  $r['FormFieldID'] . "_parent",
                'FormID'                =>  $id,
                'FormFieldLabel'        =>  $thisFieldLabel,
                'FormFieldTypeConstant' =>  $thisFieldType,
                'FormFieldMetadata'     =>  '{"multiFieldClass":"ReservableRooms","options": {' . implode(",", $options) . '},"fieldName":"'.$thisFieldName.'","onchange":"toggleFields($(this))"}',
                'FormFieldOrder'        =>  $r['FormFieldOrder'],
                'legend'                =>  $r["legend"],
                'form_metadata'         =>  $r["form_metadata"]
            );
            $this->addField($thisFieldType, $thisFieldData);
            //

            if ( !$this->ignoreOther ) {
                $thisFieldType = "_MB_FORM_FIELD_TEXT_";
                $thisFieldData = array(
                    'FormFieldID'           =>  $r['FormFieldID'] . "child",
                    'FormID'                =>  $id,
                    'FormFieldLabel'        =>  "Other Room",
                    'FormFieldTypeConstant' =>  $thisFieldType,
                    'FormFieldMetadata'     =>  '{"multiFieldClass":"ReservableRooms","fieldClass":"child_of_'.$thisFieldName.' parent_option_0","visibleByDefault": "false"}',
                    'FormFieldOrder'        =>  $r['FormFieldOrder'],
                    'legend'                =>  $r["legend"],
                    'form_metadata'         =>  $r["form_metadata"]
                );
                $this->addField($thisFieldType, $thisFieldData);
            }
         }
         else
            $this->addField($fieldType, $r);
      }

      return true;
   }

   /**
    * Pull all forms from the database.
    *
    * @return mixed Array of all forms or false on failure.
    */
   public function getAllForms()
   {
      $rows = $this->find(null, $this->formTable, null, array("FormName"));

      if(empty($rows))
         return false;

      return $rows;
   }

   /**
    * Check if the form has any fields.
    *
    * @return bool True if the form has fields, false otherwise.
    */
   public function hasFields()
   {
      return !empty($this->fields);
   }

   public function getFormName()
   {
      return $this->formName;
   }

   public function generateForm($action= "", $formContent = "")
   {
      $this->action = $action;

      if($this->hasFields())
      {
         $formName = $this->getFormName();

         $skipRendering = !empty($formContent);

         $fieldsetContent = "   <fieldset class='mainWrapper'>\n";
         foreach($this->fields as $f)
         {
            if ( $f->skipField == true ) continue;

            if($f->type == _MB_FORM_FIELD_BREAK_)
            {
               $fieldsetContent .= "\n   </fieldset>\n   <fieldset class='mainWrapper";
               if($f->FormFieldClass)
                  $fieldsetContent .= " ".$f->FormFieldClass;
               $fieldsetContent .= "'";
               if($f->FormFieldID)
                  $fieldsetContent .= " id='".$f->FormFieldID."'";

               $fieldsetContent .= ">";
            }
            else
            {
                if ( !$skipRendering )
                    if($this->inlineForm)
                       $formContent .= $f->render("form-inline");
                    else
                       $formContent .= $f->render();
            }
         }
         if($this->inlineForm)
            $formContent .= "\n";
         else
            $formContent .= "\n   </fieldset>\n";

         if(!empty($formContent))
         {
            $formHeader = $this->getFormHeader();
            $formFooter = $this->getFormFooter();
            $formContent = $formHeader.$fieldsetContent.$formContent.$formFooter;
         }
      }

      // Add additional JavaScript functionality if specified.
      if($this->onSubmit)
      {

         $formContent .= "\n<script type='text/javascript'>\n";
         $formContent .= "$('#mb_form_".$this->id["FormID"]."').submit( function(event) {\n";

         // Append the JavaScript
         $formContent .= $this->onSubmit;

         $formContent .= "\n});\n";
         $formContent .= "</script>\n";
      }

      if($this->onLoad)
      {
         $formContent .= "\n<script type='text/javascript'>\n";
         $formContent .= "$(window).on('load', function(event) {\n";

         // Append the JavaScript
         $formContent .= $this->onLoad;


         $formContent .= "\n});\n";
         $formContent .= "</script>\n";

      }

      if ($this->addJS)
      {
          $this->insertJS($this->addJS);
      }

      return $formContent;
   }

   protected function getFormHeader()
   {
      $formHeader  = "";
      if($this->prependForm)
         $formHeader .= $this->prependForm;
      $formHeader .= "\n<form id='mb_form_".$this->id["FormID"]."' ";

      // Set the form class
      if($this->inlineForm)
         $formHeader  .= "class='form-inline";
      else
         $formHeader  .= "class='form-horizontal";

      if($this->formClass)
         $formHeader  .= " ".$this->formClass;
      $formHeader  .= "' ";

      // Set the form method
      $formHeader .= "method='";
      if(!empty($this->method) && $this->method == "get")
         $formHeader .= $this->method;
      else
         $formHeader .= "post";
      $formHeader .= "' ";


      $formHeader .= "action='";

      $savedAction = $this->action;
      if(!empty($savedAction))
         $formHeader .= filter_var($savedAction, FILTER_SANITIZE_URL);
      else if(!empty($action))
         $formHeader .= filter_var($action, FILTER_SANITIZE_URL);
      else
         $formHeader .= filter_var($_SERVER["REQUEST_URI"], FILTER_SANITIZE_URL);

      if ( empty($this->enctype))
        $formHeader .= "' enctype='".$this->enctype;

      $formHeader .= "'>\n";

      // Provide a layer for hiding the form on submission.
      $formHeader .= "   <div id='formProcessingLayer'></div>\n";

      if($this->showHeader)
      {
         $formHeader .= "   <header>\n";
         $formHeader .= "      <h2>".filter_var($this->formName, FILTER_SANITIZE_STRING)."</h2>\n";
         $formHeader .= "   </header>\n";
      }

      return $formHeader;
   }

   protected function getFormFooter($overwriteSubmitValue="")
   {

      $formFooter = "";
      if(!$this->inlineForm)
      {
         $formFooter  = "<hr />";
         $formFooter .= "<div class='form-group'>\n";
         $formFooter .= "   <div class='col-sm-offset-3 col-sm-9'>\n";
      }

      if(!$this->noFormIDField)
         $formFooter .= "      <input type='hidden' name='_mb_form_id' value='".((int) $this->id["FormID"])."' />\n";

      if(!$this->hideSubmit)
      {
         $formFooter .= "      <button type='submit' class='btn ";

         // Allow users to set what type of submit button they want.
         if($this->submitClass)
            $formFooter .= $this->submitClass;
         else
            $formFooter .= "btn-default";

         // Disable the submit button so users only submit one entry.
         if($this->hideButtonOnSubmit)
            $formFooter .= " activateFormHider";

         $formFooter .= "'";
         
         if ( !$this->inlineForm )
             $formFooter .= " style='float:right' ";

         $formFooter .= ">";
         
         if(!empty($overwriteSubmitValue))
            $submitValue = $overwriteSubmitValue;
         else
            $submitValue = $this->submitValue;

         if(!empty($submitValue))
            $formFooter .= $submitValue;
         else
            $formFooter .= "Submit";
         $formFooter .= "</button>\n";

      if($this->inlineForm)
      {
         $formFooter .= "\n   </fieldset>\n";
         $formFooter .= "</form>\n\n";
      }
      else
      {
         $formFooter .= "   </div>\n";
         $formFooter .= "</div>\n";
         $formFooter .= "</form>\n\n";
      }


        if ( $this->showLoading ) {
            //$formFooter .=  '<div id="fade"></div>';
            //$formFooter .=  '<div id="loading"><img id="loader" src="/imgs/loading.gif" /></div>';
            $this->onLoad .= '$("body").prepend(\'<div id="fade"></div>\');';
            $this->onLoad .= '$("body").prepend(\'<div id="loading"><img id="loader" src="/common/imgs/loading.gif" /></div>\');';
            $this->onSubmit .= '$.when( $("#fade").show(), $("#loading").show() ).then( $(this).submit() );';
        }

      if($this->appendForm)
         $formFooter .= $this->appendForm;

     }

      return $formFooter;
   }

   /*
    * This function will generate a form that is
    * invisible to the user other than the submit button.
    * Useful for confirmation pages and/or preview pages.
    */
   public function generateHiddenForm($submitValue = "Confirm Submission")
   {
      $isInline = $this->inlineForm;
      $this->inlineForm = true;
      $formContent = "";
      foreach($this->fields as $f)
      {
         $formContent .= $f->getHiddenField();
      }

      if(!empty($formContent))
      {
         $formHeader = $this->getFormHeader();
         $formFooter = $this->getFormFooter($submitValue);
         $formContent = $formHeader.$formContent.$formFooter;
      }
      $this->inlineForm = $isInline;

      return $formContent;
   }

   public function submitted()
   {
      return (isset($this->responseSource["_mb_form_id"])) && ((int) $this->responseSource["_mb_form_id"] === (int) $this->id["FormID"]);
   }

   public function setInvalid()
   {
      $this->data["valid"] = false;
   }

   public function setError($e, $tmp=false)
   {
      $this->data["errors"][] = $e;
   }

   public function getErrors()
   {
      return $this->data["errors"];
   }

   public function setSuccessMessage($m)
   {
      $this->data["successMessages"][] = $m;
   }

   public function getSuccessMessages()
   {
      return $this->data["successMessages"];
   }

   public function setProcessed($val)
   {
      $this->data["processeddata"] = ( (bool) $val);
   }

   public function isValid()
   {
      return (isset($this->data["processeddata"]) && $this->data["processeddata"]);
   }

   public function processFormData()
   {
      $returnData = "";

      // Give ourselves the benefit of the doubt
      $this->setProcessed(true);

      if($this->submitted())
      {
         // Loop through each form field and see if it has data.
         // If empty and required, throw an error.
         // If not, then we need to throw an

         // Contains the data for the multiField SuperType, this may have additional
         // processing apart from the form.
         $multiFieldData = array();

         foreach($this->fields as $f)
         {
            $f->checkData();

            if(!$f->valid)
            {
               $this->setError($f->errorMessage);
               $this->setInvalid();
               $this->setProcessed(false);
            }
            else
            {
               // Collect Multiform Data
               if ( $f->multiFieldClass ) {
                    $fieldData = array(
                       "FieldLabel" => $f->getLabel(),
                       "FieldValue" => $f->value,
                       "FieldTextValue" => $f->getValue()
                    );

                   $multiFieldData[$f->multiFieldClass][$f->fieldName] = $fieldData;
               }

               if($f->type == _MB_FORM_FIELD_EMAIL_)
               {
                  if($f->setAsReplyTo)
                  {
                     $this->data['replyto'] = $f->value;
                  }

                  if($f->sendUserEmail)
                  {
                     $this->data['useremail'] = $f->value;
                  }
               }

               if($f->setAsSubject)
               {
                  $subj = $f->getSubjectLine();

                  if($f->appendToSubject)
                     $subj .= $f->appendToSubject;

                  if($f->prependToSubject)
                     $subj = $f->prependToSubject.$subj;

                  $this->data["mailsubject"] = $subj;
               }
            }
         }

       // Handle multiFieldData
       if ( !$this->skipMultiFieldProcessing )
        foreach ( $multiFieldData as $class => $fields )
        {
            $absClass="\Mumby\Webtools\MultiFormField_".$class;
            if ( class_exists($absClass) )
            {
                 $obj = new $absClass($fields, null);
                 $result = $obj->processMultiFieldData();
                 if ( $result != false ) {
                     $this->multiFieldResults[$class] = $result;
                     $this->setSuccessMessage("The " . $class . " was processed successfuly.");
                 }
                 else {
                     $this->setError("There was a problem processing the " . $class . ". Please contact system@math.arizona.edu.  Thanks!");
                 }
            }
            else {
                 $obj = new MultiFormField($fields, null);
                 $result = $obj->processMultiFieldData();
                 if ( $result != false ) {
                    $this->multiFieldResults[$class] = $result;
                 }
            }
        }

         if($this->valid)
         {
            $keepUploadedFiles = false;
            $submissionID      = false;

            $resp = $this->getResponses();

            if(!empty($resp))
            {
               foreach($resp as $r)
               {
                  $respType = constant($r["FormResponseTypeConstant"]);

                  switch($respType)
                  {
                     case _MB_FORM_SUBMISSION_DISPLAY_:
                        $this->showSubmitted = true;
                        break;
                     case _MB_FORM_SUBMISSION_DB_STORE_:
                            $keepUploadedFiles = true;

                            $submissionData = array();
                            $submissionData["FormID"] = $this->id["FormID"];
                            $submissionData["FormSubmissionData"] = serialize($this->fields);
                            $submissionData["FormSubmissionIPAddress"] = ((isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : "");

                            $submissionID = $this->insert($submissionData, $this->formSubmissionTable);
                            if(!$submissionID)
                            {
                               $this->setError("Unfortunately, we could not process your form results. Please try again later while we try to get things working again. (Error Code: F1)");
                               if(defined('_MB_DEBUG') && constant('_MB_DEBUG'))
                                  $this->setError(implode(" : ", $stmt->errorInfo()));
                            }
                         break;
                     case _MB_FORM_SUBMISSION_FILE_STORE_:
                        // We'll do the actual work of storing the files below...
                        $keepUploadedFiles = true;
                        break;
                     case _MB_FORM_SUBMISSION_EMAIL_:
                        if($this->sendEmail($r["FormResponseMetadata"]) == true)
                           $this->setSuccessMessage("Your information has been sent successfully.");
                        else
                        {
                           $this->setProcessed(false);
                           $this->setError("Unfortunately, we could not process your form results. Please try again later while we try to get things working again. (Error Code: F2)");
                        }
                        break;
                     case _MB_FORM_SUBMISSION_DYNAMIC_EMAIL_:
                        $target = 0;
                        $recipOptions = json_decode($r["FormResponseMetadata"], true);
                        foreach ($this->fields as $f) {
                            if ($f->recipientChooser && !empty($f->value) && isset($recipOptions[$f->value])) {
                                $target = $f->value;
                                break;
                            }
                        }
                        $recipients = json_encode($recipOptions[$target]);
                        if($this->sendEmail($recipients) == true)
                           $this->setSuccessMessage("Your information has been sent successfully.");
                        else
                        {
                           $this->setProcessed(false);
                           $this->setError("Unfortunately, we could not process your form results. Please try again later while we try to get things working again. (Error Code: F2)");
                        }
                        break;
                     case _MB_FORM_SUBMISSION_MESSAGE_:
                        $metadata = json_decode($r["FormResponseMetadata"]);
                        if(!empty($metadata))
                           $this->setSuccessMessage($metadata->message);
                        break;
                     case _MB_FORM_SUBMISSION_REDIRECT_:
                        $metadata = json_decode($r["FormResponseMetadata"]);
                        if(!empty($metadata)) {
                           $this->redirectURL = $metadata->url;
                        }
                        break;
                     case _MB_FORM_SUBMISSION_USER_EMAIL_:
                        if($this->userEmail)
                        {
                           $metadata = '{"recipients": ["'.$this->userEmail.'"]}';
                           if($this->sendEmail($metadata) == true)
                              $this->setSuccessMessage("An email has been sent to your email address with the information submitted.");
                           else
                           {
                              $this->setWarningMessage("<em>Note: We attempted to send you a copy of this form submission but were unable to do so with the email address supplied.</em>.");
                           }
                        }
                        break;
                     default:
                        break;
                  }
               }


               /*
                * We need to loop through all of the fields and make
                * sure we save/delete any files that were submitted.
                */
               foreach($this->fields as $k=>$f)
               {
                  if( constant($f->field_type) == _MB_FORM_FIELD_FILE_)
                  {
                     $fileData = $f->value;

                     if(!empty($fileData))
                     {
                        if($keepUploadedFiles)
                        {

                           if( $f->fileDestinationPath != "" )
                           {
                              $uploadDir = $f->fileDestinationPath;
                              $filenamePrefix = "";
                              if(!$f->overwriteFiles)
                              {
                                 // Check to see if the file already exists.
                                 if(file_exists($uploadDir.$filenamePrefix.$fileData["name"]))
                                 {
                                    $this->setError("We were unable to upload your files because a file by that name already exists. (Error Code: F3)");
                                    $this->setProcessed(false);
                                    $f->processFiles = false;
                                 }
                              }
                           }
                           else
                           {
                              $filenamePrefix = $this->id["FormID"]."_".$f->id["FormFieldID"]."_";

                              if($submissionID)
                                 $filenamePrefix .= $submissionID."_";

                              $uploadDir = getcwd()._MB_FORM_SUBMISSION_FILESTORE_;
                           }

                           if($f->processFiles)
                           {
                              $newFilename = $uploadDir.$filenamePrefix.$fileData["name"];
                              $this->fields[$k]->data['value']["mb_filestore"] = $newFilename;
                              $result = rename($fileData["mb_holding"], $newFilename);

                              if(!$result)
                              {
                                 $this->setError("We saved your form information but were unable to store the attached file(s). (Error Code: F4)");
                                 $this->setProcessed(false);
                              }
                              else
                              {
                                 $this->setProcessed(true);
                                 $this->data["uploadedfiles"][$f->id["FormFieldID"]] = $newFilename;
                              }

                           }

                        }
                        else
                        {
                           // Remove any old files.
                           if ( !empty($fileData['mb_holding']) )
                                   unlink($fileData["mb_holding"]);
                        }

                     }

                  }
               }


               if($this->processedData)
               {
                  return true;
               }
               else
               {
                  return false;
               }
            }
            else
            {
               $this->setError("System Error: No form response was specified.");
               return false;
            }
         }
         else
         {
            $this->setError("System Error: Data was invalid.");
            return false;
         }
      }

      return $returnData;
   }

   protected function sendEmail($data)
   {
      $metadata = json_decode($data);

      $mail = new PHPMailer;

      if(!empty($metadata))
      {
         $recipients = $metadata->recipients;
         if(!empty($recipients))
         {
            foreach($recipients as $r)
            {
               // Email each person with the output.
               $mail->addAddress($r);
            }
         }
      }

      if(defined('_MB_SMTP_HOST_'))
      {
         $mail->isSMTP();
         $mail->Host = _MB_SMTP_HOST_;

         if(defined('_MB_SMTP_USER_') && defined('_MB_SMTP_PASS_'))
         {
            $mail->Username = _MB_SMTP_USER_;
            $mail->Password = _MB_SMTP_PASS_;
            $mail->SMTPAuth = true;
         }

         if(defined("_MB_SMTP_PORT_"))
            $mail->Port = _MB_SMTP_PORT_;
      }

      $customFromAddress = $this->fromAddress;
      if(!empty($customFromAddress))
      {
         $fromAddress = $customFromAddress;
      }
      else if(defined("_MB_DEFAULT_EMAIL_FROM_"))
         $fromAddress = _MB_DEFAULT_EMAIL_FROM_;
      else
         $fromAddress = "webmaster@".gethostname();

      $customFromName = $this->fromName;
      if(!empty($customFromName))
      {
         $fromName = $customFromName;
      }
      else if(defined("_MB_DEFAULT_EMAIL_FROM_NAME_"))
         $fromName = _MB_DEFAULT_EMAIL_FROM_NAME_;
      else
         $fromName = "Website Form";

      $mail->setFrom($fromAddress, $fromName);

      if($this->replyTo)
      {
         $mail->addReplyTo($this->replyTo);
      }

      $mail->isHTML(true);

      if($this->mailSubject) {
        if (is_array($this->mailSubject))
            $mail->Subject = strip_tags(str_replace("<br />","\n",$this->mailSubject[0]));
        else
            $mail->Subject = strip_tags(str_replace("<br />","\n",$this->mailSubject));
      }
      else
         $mail->Subject = $this->getFormName();

      $mailBody  = "<p>The following information was submitted through "._MB_APPLICATION_NAME_.":</p>\n\n";

      $displayData = $this->showSubmittedData();

        $oldHTML = array();
        $newHTML = array();

        $oldHTML[] = "<div class='formFieldOutput'>";
        $newHTML[] = "<tr>";

        $oldHTML[] = "strong>";
        $newHTML[] = "th>";

        $oldHTML[] = "div>";
        $newHTML[] = "td>";

        $oldHTML[] = "</td></td>";
        $newHTML[] = "</td></tr>";

        $oldHTML[] = "<th>";
        $newHTML[] = "<th style='color: #036;border-top:1px solid #ccc;vertical-align:top;text-align:right;padding:.25em .5em; min-width: 130px;'>";

        $oldHTML[] = "<td>";
        $newHTML[] = "<td style='border-top:1px solid #ccc;vertical-align:top;text-align:left;padding:.25em .5em;'>";

        $oldHTML[] = "<span class='fauxHeader'>";
        $newHTML[] = "<span style='font-weight:bold;'>";

        $displayData = str_replace($oldHTML, $newHTML, $displayData);

        $displayData = "<table style='font-family:Helvetica,Arial,sans-serif;border-collapse:collapse; border-spacing:0;'>".$displayData."</table>";


      $mailBody .= $displayData;

      $mail->Body = $mailBody;
      $mail->AltBody = ( strip_tags(str_replace("<br />","\n",$mailBody) ) );

      foreach($this->fields as $f)
      {
         if($f->type == _MB_FORM_FIELD_FILE_)
         {
            $uploadedFile = $f->value;
            if(!empty($uploadedFile))
            {
               $mail->addAttachment($uploadedFile["mb_holding"], $uploadedFile["name"]);
            }
         }
      }

      if(!$mail->send())
      {
         return false;
      }
      else
         return true;
   }

   public function getResponses()
   {
      $respSQL = "SELECT fr.* FROM ".$this->formResponseTable." fr INNER JOIN ".$this->formTable." f ON f.FormID=fr.FormID WHERE f.FormID = :FormID";
      $data = array("FormID"=>$this->id["FormID"]);
      return $this->query($respSQL, $data);
   }

   public function showSubmittedData()
   {

      $output = "";
        foreach($this->fields as $f)
        {
          $output .= $f->printValue;
        }
      return $output;
   }

   /**
    * Add a field to the form. Field type must be one of the following:
    *
    * - _MB_FORM_FIELD_CAPTCHA_
    * - _MB_FORM_FIELD_CHECKBOX_
    * - _MB_FORM_FIELD_COLOR_OPTIONS_
    * - _MB_FORM_FIELD_COURSE_EQUIV_
    * - _MB_FORM_FIELD_CURRENCY_
    * - _MB_FORM_FIELD_DATE_
    * - _MB_FORM_FIELD_EVENTCATEGORIES_
    * - _MB_FORM_FIELD_TIME_
    * - _MB_FORM_FIELD_DATETIME_
    * - _MB_FORM_FIELD_EMAIL_
    * - _MB_FORM_FIELD_FILE_
    * - _MB_FORM_FIELD_HIDDEN_
    * - _MB_FORM_FIELD_HTML_
    * - _MB_FORM_FIELD_MARKUP_
    * - _MB_FORM_FIELD_MULTIFILE_
    * - _MB_FORM_FIELD_MULTIBOX_
    * - _MB_FORM_FIELD_NUMBER_
    * - _MB_FORM_FIELD_PASS_
    * - _MB_FORM_FIELD_PHONE_
    * - _MB_FORM_FIELD_RADIO_
    * - _MB_FORM_FIELD_SEARCH_
    * - _MB_FORM_FIELD_SELECT_
    * - _MB_FORM_FIELD_STATE_
    * - _MB_FORM_FIELD_TEXT_
    * - _MB_FORM_FIELD_TEXTAREA_
    * - _MB_FORM_FIELD_URL_
    * - _MB_FORM_FIELD_ZIPCODE_
    *
    * @param int $fieldType Type of form field
    * @param array $fieldData Optional data for the form field. Varies depending on type.
    */
   public function addField($fieldType, $fieldData="")
   {
      if(isset($fieldData["FormFieldID"]) && !isset($fieldData["id"]))
         $fieldData["id"] = $fieldData["FormFieldID"];

      if(isset($fieldData["FormID"]) && !isset($fieldData["form_id"]))
         $fieldData["form_id"] = $fieldData["FormID"];

      if(isset($fieldData["FormFieldLabel"]) && !isset($fieldData["label"]))
         $fieldData["label"] = $fieldData["FormFieldLabel"];

      if(isset($fieldData["FormFieldTypeConstant"]) && !isset($fieldData["field_type"]))
         $fieldData["field_type"] = $fieldData["FormFieldTypeConstant"];

      if(isset($fieldData["FormFieldMetadata"]) && !isset($fieldData["metadata"]))
         $fieldData["metadata"] = $fieldData["FormFieldMetadata"];

      if(isset($fieldData["FormFieldOrder"]) && !isset($fieldData["field_order"]))
         $fieldData["field_order"] = $fieldData["FormFieldOrder"];

      if(!empty($fieldData) && isset($fieldData["FormFieldID"]))
         $fieldKey = $fieldData["FormFieldID"];
      else
         $fieldKey = "";

      if($this->method && $this->method == "get")
         $responseType = "get";
      else
         $responseType = "post";

      if ( !is_integer($fieldType) ) $fieldType = constant($fieldType);
      switch($fieldType)
      {
         case _MB_FORM_FIELD_CAPTCHA_:
            $fieldObj = new FormField_Captcha($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_CHECKBOX_:
            $fieldObj = new FormField_Checkbox($fieldData, $responseType);
            $this->addJS('/common/js/childFields.js');
            break;
         case _MB_FORM_FIELD_COLOR_OPTIONS_:
            $fieldObj = new FormField_ColorChooser($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_COURSE_EQUIV_:
            $fieldObj = new FormField_CourseEquivalency($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_CURRENCY_:
            $fieldObj = new FormField_Currency($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_HTMLDATE_:
            $fieldObj = new FormField_HTMLDate($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_DATE_:
            $fieldObj = new FormField_Date($fieldData, $responseType);
            $this->addJS('/common/bower/datetimepicker/jquery.datetimepicker.js');
            $this->addCSS('/common/bower/datetimepicker/jquery.datetimepicker.css');
            break;
         case _MB_FORM_FIELD_TIME_:
            $fieldObj = new FormField_Time($fieldData, $responseType);
            $this->addJS('/common/bower/datetimepicker/jquery.datetimepicker.js');
            $this->addCSS('/common/bower/datetimepicker/jquery.datetimepicker.css');
            break;
         case _MB_FORM_FIELD_DATETIME_:
            $fieldObj = new FormField_DateTime($fieldData, $responseType);
            $this->addJS('/common/bower/datetimepicker/jquery.datetimepicker.js');
            $this->addCSS('/common/bower/datetimepicker/jquery.datetimepicker.css');
            break;
         case _MB_FORM_FIELD_EVENTCATEGORIES_:
            $fieldObj = new FormField_EventCategories($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_EMAIL_:
            $fieldObj = new FormField_Email($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_FILE_:
            $this->data["enctype"] = "multipart/form-data";
            $fieldObj = new FormField_File($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_HIDDEN_:
            $fieldObj = new FormField_Hidden($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_HTML_:
            $fieldObj = new FormField_HTML($fieldData, $responseType);
            $this->addJS('/common/bower/tinymce-dist/tinymce.min.js');
            $this->addJS('/js/injectionData.js');
            break;
         case _MB_FORM_FIELD_MARKUP_:
            $fieldObj = new FormField_Markup($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_MULTIBOX_:
            $fieldObj = new FormField_Multibox($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_MULTIFILE_:
            $fieldObj = new FormField_Multifile($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_NUMBER_:
            $fieldObj = new FormField_Number($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_PASS_:
            $fieldObj = new FormField_Password($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_PHONE_:
            $fieldObj = new FormField_Phone($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_RADIO_:
            $fieldObj = new FormField_Radio($fieldData, $responseType);
            $this->addJS('/common/js/childFields.js');
            break;
         case _MB_FORM_FIELD_SEARCH_:
            $fieldObj = new FormField_Search($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_SELECT_:
            $fieldObj = new FormField_Select($fieldData, $responseType);
            $this->addJS('/common/js/childFields.js');
            break;
         case _MB_FORM_FIELD_STATE_:
            $fieldObj = new FormField_State($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_TEXT_:
            $fieldObj = new FormField_Text($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_TEXTAREA_:
            $fieldObj = new FormField_TextArea($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_URL_:
            $fieldObj = new FormField_URL($fieldData, $responseType);
            break;
         case _MB_FORM_FIELD_ZIPCODE_:
            $fieldObj = new FormField_ZipCode($fieldData, $responseType);
            break;
         default:
            $fieldObj = new FormField($fieldData, $responseType);
            break;
         }

      if(!empty($fieldKey))
         $this->fields[$fieldKey] = $fieldObj;
      else
         $this->fields[] = $fieldObj;
   }

   // Remove a field from the form
   public function removeField($fieldID)
   {
      if(isset($this->fields[$fieldID]))
      {
         unset($this->fields[$fieldID]);
         return true;
      }
      else
         return false;
   }

   public function hideFields($fieldIDs)
   {
      if(is_array($fieldIDs))
      {
         $allSet = true;
         foreach($fieldIDs as $f)
         {
            if(!isset($this->fields[$f]))
            {
               $allSet = false;
               break;
            }

            $fieldData = $this->fields[$f]->getFieldData();
            $this->fields[$f] = new FormField_Hidden($fieldData);
         }

         return $allSet;
      }
      else if(isset($this->fields[$fieldIDs]))
      {

         $fieldData = $this->fields[$fieldIDs]->getFieldData();
         $this->fields[$fieldIDs] = new FormField_Hidden($fieldData);
         return true;
      }
      else
         return false;
   }

   // Update a given field's metadata/properties
   public function updateField($fieldID, $metadata)
   {
      if(isset($this->fields[$fieldID]))
      {
         return $this->fields[$fieldID]->updateField($metadata);
      }
      else
         return false;
   }

   /**
    * Returns an array of all field objects for the form.
    *
    * @return array
    */
   public function getFields()
   {
      return $this->fields;
   }

   /**
    * Returns files uploaded via the form.
    *
    * @param int $fieldID Optional field key for a given file input.
    * @return mixed An array of files or false if no files are found.
    */
   public function getUploadedFiles($fieldID=0)
   {
      if(empty($fieldID))
         return $this->uploadedFiles;
      else if(isset($this->uploadedFiles[$fieldID]))
         return $this->uploadedFiles[$fieldID];
      else
         return false;
   }

   // Return an individual field
   public function getField($fieldID)
   {
      if(isset($this->fields[$fieldID]))
         return @$this->fields[$fieldID];
      else
         return false;
   }

   // Return the submitted value of an individual field
   public function getFieldValue($fieldID, $asSubmitted=true)
   {
      if(isset($this->fields[$fieldID]))
      {
         if($asSubmitted)
            return $this->fields[$fieldID]->getSubmittedValue();
         else
            return $this->fields[$fieldID]->getValue();
      }
      else
         return false;
   }

   public function setFieldValue($fieldID, $val)
   {
      if(isset($this->fields[$fieldID]))
      {
         return $this->fields[$fieldID]->setValue($val);
      }
      else
         return false;
   }

   public function validateField($fieldID)
   {
      if(isset($this->fields[$fieldID]))
      {
         return $this->fields[$fieldID]->validate();
      }
      else
         return false;
   }

   /**
    *
    */
   public function getQuickSelectForm($options, $label="Currently Viewing", $submitBtnValue = "Go!", $showSubmitBtn = false)
   {
      $quickForm = new \Mumby\WebTools\SystemForm();
      $quickForm->inlineForm = true;
      $quickForm->method="get";
      $quickForm->submitValue=$submitBtnValue;
      $quickForm->submitClass="btn btn-success";

      // Make it look a little nicer
      $quickForm->prependForm = "<div class='right-inline-form'>";
      $quickForm->appendForm  = "</div>";

      $quickField = $quickForm->addSelectField($label, $options);
      $quickField->noEmptyOption = true;

      if(!$showSubmitBtn)
      {
         $quickField->onchange = "$(this).parents('form').submit();";
         $quickForm->hideSubmit = true;
      }

      $quickForm->field = $quickField;

      return $quickForm;
   }

   /**
    *
    */
   public static function getQuickSearchForm($label="Search")
   {
      $quickForm = new \Mumby\WebTools\SystemForm();
      $quickForm->inlineForm = true;
      $quickForm->method="get";
      $quickForm->submitValue="Go!";
      $quickForm->submitClass="btn btn-success";

      // Make it look a little nicer
      $quickForm->prependForm = "<div class='right-inline-form'>";
      $quickForm->appendForm  = "</div>";

      $quickField = $quickForm->addSearchField($label);

      $quickForm->field = $quickField;

      return $quickForm;
   }
}

class FormField extends Page
{
   protected $visibleByDefault;
   protected $allowBlank;
   protected $responseSource;

   /*
    * Takes in a data array (such as a row from MySQL)
    */
   public function __construct($fieldData, $formMethod="post")
   {
      $this->data = $fieldData;

       $this->processFiles = true;

      if($formMethod=="get")
         $this->responseSource = $_GET;
      else
         $this->responseSource = $_POST;

      if(isset($fieldData["metadata"]) && !empty($fieldData["metadata"]))
      {
         if($metadata = json_decode($fieldData["metadata"], true))
         {
            foreach($metadata as $k=>$m)
            {
               $this->data[strtolower($k)] = $m;
            }
         }
      }

      if ( !empty($this->data['visiblebydefault']) && $this->data['visiblebydefault'] === "false" )
          $this->visibleByDefault = false;
      else
          $this->visibleByDefault = true;


      if ( !empty($this->data['allowblank']) && $this->data['allowblank'] === "true" )
          $this->allowBlank = true;
      else
          $this->allowBlank = false;


      unset($this->data["metadata"]);

      if(!isset($this->data["fieldname"]) || empty($this->data["fieldname"]))
         $this->data["fieldname"] = "field_".$this->data["id"];
      $this->data["labelclass"] = strtolower(str_replace("_MB_FORM_FIELD_", "", $this->field_type))."field";

      $this->data["type"] = constant($this->field_type);

      // Assume that the form field is valid for now.
      $this->data["valid"] = true;
   }

   public function resetField() {
       $this->setProcessed(false);
       $this->responseSource = "";
   }

   public function updateField($metadata)
   {
      if($md = json_decode($metadata, true))
      {
         foreach($md as $k=>$m)
         {
            $this->data[strtolower($k)] = $m;
         }
         return true;
      }
      else
         return false;
   }

   public function getFieldData()
   {
      return $this->data;
   }

   public function getFieldID()
   {
      return $this->fieldname;
   }

   protected function getLabel()
   {
      if($this->hiddenLabel) {
        $this->placeholder = htmlentities($this->label);
        return "";
      }

      if($this->inlineForm)
         $labelContent = $this->indent()."<label class='".$this->labelClass;
      else
         $labelContent = $this->indent()."<label class='col-sm-3 control-label ".$this->labelClass;

      // Some fields may require hidden labels
      if($this->hiddenLabel)
         $labelContent .= " hidden";

      // Some fields are 'required'
      if($this->required)
         $labelContent .= " required";

      $labelContent .= "' for='".$this->fieldName."'>";
      $labelContent .= "<span class='labelText";
      if($this->noColon)
         $labelContent .= " noColon";
      $labelContent .= "'>".htmlentities($this->label)."</span>";

      // Occasionally, we might want to specify
      // additional info next to the label.
      if($this->moreLabel)
         $labelContent .= "<span class='moreLabel'>".$this->moreLabel."</span>";

      $labelContent .= "</label>\n";
      $this->i--;
      return $labelContent;
   }

   /*
    * Returns an <input> nested inside of a div.
    */
   protected function getField($fieldType = 'text')
   {
      $currentValue  = $this->getSubmittedValue();

      $fieldContent = "";
      if(!$this->inlineForm)
      {
         $fieldContent .= $this->indent()."<div";
         if($this->divStyle)
            $fieldContent .= " style='".trim($this->divStyle)."'";
         $fieldContent .= " class='col-sm-9";
         if($this->hiddenLabel)
            $fieldContent .= " col-sm-offset-3";

         if($this->inputPrefix || $this->inputSuffix)
            $fieldContent .= " input-group";

         if($this->divClass)
            $fieldContent .= " ".trim($this->divClass);

         $fieldContent .= "'>\n";
      }


      $this->i++;

      if($this->inputPrefix)
         $fieldContent .= "<span class='input-group-addon' id='pre-addon-".$this->fieldName."'>".$this->inputPrefix."</span>";

      if ($this->topLabel)
         $fieldContent .= '<div class="topLabel">' . $this->topLabel . '</div>';

      $fieldContent .= $this->indent()."<input type='".$fieldType."' name='".$this->fieldName."' id='".$this->fieldName."' class='form-control";
      if($this->fieldClass)
         $fieldContent .= " ".trim($this->fieldClass);
      $fieldContent .= "'";

      if ( $this->fieldStyle ) {
          $fieldContent .= " style='".trim($this->fieldStyle)."'";
      }

      // We can optionally specify 'placeholder' text.
      if($this->placeholder)
         $fieldContent .= " placeholder='".htmlspecialchars ($this->placeholder)."'";

      if($this->required)
         $fieldContent .= " required='required'";

      if($this->disabled)
         $fieldContent .= " disabled='disabled'";

      if($this->noAutocomplete)
         $fieldContent .= " autocomplete='off'";

      if(!empty($currentValue))
         $fieldContent .= " value='".$this->clean($currentValue)."'";

      if(is_array($this->moreAttributes) )
      {
          foreach ($this->moreAttributes as $k=>$a ) {
              $fieldContent .= " " . $k . "='". $a ."'";
          }
      }

      // Prevent non-numeric characters in all Browsers.
      if ( $fieldType === "number" )
	$fieldContent .= " onkeypress='return event.charCode === 0 || /\d/.test(String.fromCharCode(event.charCode));'";

      $fieldContent .= " />\n";

      if ($this->bottomLabel)
         $fieldContent .= '<div class="topLabel">' . $this->bottomLabel . '</div>';

      if($this->inputSuffix)
         $fieldContent .= "<span class='input-group-addon' id='post-addon-".$this->fieldName."'>".$this->inputSuffix."</span>";

      $this->i--;

      if(!$this->inlineForm)
         $fieldContent .= $this->indent()."</div>\n";

      // Add any custom JavaScript.
      $fieldContent .= $this->getAdditionalJavaScript();

      return $fieldContent;
   }

   public function getFieldOptions()
   {
      $fieldOptions = array(
         "FormFieldLabel" => "string Label for the form field.",
         "required"     => "bool Whether the field is required. This is overridden by allowBlank.",
         "moreLabel"    => "string Additional text, to be displayed under the label.",
         "topLabel"     => "string Additional text, to be displayed above the field.",
         "hiddenLabel"  => "bool Whether the label should be hidden",
         "divClass"     => "string CSS class name to be used in the 'class' attribute for the containing div.",
         "divStyle"     => "string CSS style string to be used in the 'style' attribute for the containing div.",
         "fieldClass"   => "string CSS class name to be used in the 'class' attribute for the form field itself.",
         "fieldStyle"   => "string CSS style string to be used in the 'style' attribute for the form field itself.",
         "wrapperClass" => "string CSS class name to be used in the div surrounding the 'label' and 'input' elements.",
         "wrapperStyle" => "string CSS style string to be used in the 'style' attribute in the div surrounding the 'label' and 'input' elements.",
         "setAsSubject"    => "bool Whether the field value should be used as the Subject line for an email response.",
         "appendToSubject" => "string Text to be appended to Subject line (only applies if setAsSubject is true).",
         "prependToSubject" => "string Text to be prepended to Subject line (only applies if setAsSubject is true).",
         "visibleByDefault" => "bool Whether the field is visible initially",
         "skipField"    => "bool Whether to skip rendering this field when generateForm is called.",
         "moreAttributes" => "Array of additional attributes to add to the input tag",
         "allowBlank" => "bool Disables server-side validation of the form field, overriding the required option.",
         "multiFieldClass" => "string Unique name of the group of fields this field belongs to in a MultiField SuperType."
      );

      $jsOptions = $this->supportedJavascriptEvents();
      $allFieldOptions = array_merge($fieldOptions, $jsOptions);

      ksort($allFieldOptions);
      return $allFieldOptions;
   }

   protected function clean($val)
   {
      return str_replace("'", "&#39;", htmlentities($val));
   }

   public function getHiddenField()
   {
      $hiddenFieldContent  = "<input type='hidden' name='".htmlentities($this->fieldName)."' value='".htmlentities($this->getSubmittedValue())."' /> ";
      $hiddenFieldContent .= "<!-- " . $this->label . " -->\n";
      return $hiddenFieldContent;
   }

   public function supportedJavascriptEvents()
   {
      return array(
         "onblur"     => "Event triggered when the user moves the focus (cursor) away from the field",
         "onchange"   => "Event triggered when the value of the field changes",
         "onclick"    => "Event triggered when the user clicks the field with the mouse",
         "onfocus"    => "Event triggered when the user moves the focus (cursor) to the field",
         "onhover"    => "Event triggered when the user hovers their mouse over the field",
         "onkeydown"  => "Event triggered when the user presses any key while the focus is on the field",
         "onkeypress" => "Event triggered when the user presses a printable key while the focus is on the field",
         "onkeyup"    => "Event triggered when the user releases any key while the focus is on the field",
         "onselect"   => "Event triggered when the user makes a text selection within a field (results may vary from browser to browser...)",
         "onmousewheel"   => "Event triggered when the user scrolls with the mousewheel"
      );
   }

   protected function getAdditionalJavaScript($fieldName="")
   {
      if(empty($fieldName))
         $fieldName = $this->fieldName;

      $javaScript  = "";

      $possibleEvents = $this->supportedJavascriptEvents();
      foreach($possibleEvents as $e=>$d)
      {
         if($this->$e)
            $javaScript .= $this->createJavascriptEvent($e, $this->$e, $fieldName);
      }

      $javaScriptOutput  = "";
      if(!empty($javaScript))
      {
         $javaScriptOutput .= "\n<script type='text/javascript'>\n";

         // Make sure we set up our field variable.
         $javaScriptOutput .= $fieldName." = $('#".$fieldName."');\n";

         // Append the JavaScript
         $javaScriptOutput .= $javaScript;
         $javaScriptOutput .= "\n</script>\n";
      }

      return $javaScriptOutput;
   }

   protected function createJavascriptEvent($evtName, $evtAction, $fieldName)
   {
      $evtName = substr($evtName, 2);
      $javaScript  = $fieldName.".".$evtName."( function(event) {\n";
      $javaScript .= $evtAction;
      $javaScript .= "\n});\n";

      return $javaScript;
   }

   protected function fetchSanitizedValue()
   {

      if(isset($this->responseSource[$this->fieldName]))
        $this->setValue(trim(filter_var($this->responseSource[$this->fieldName], FILTER_SANITIZE_STRING, array("flags"=>FILTER_FLAG_NO_ENCODE_QUOTES))));
   }

   public function getSubjectLine()
   {
      $val = $this->value;
      if(empty($val))
         $val = "";

      return $val;
   }

   protected function validate()
   {
      return true;
   }

   protected function setErrorMessage()
   {
      $this->errorMessage = "Your entry for '".htmlspecialchars($this->label)."' was not valid. Please try again.";
   }

   protected function setPrintValues()
   {
      $val = trim(filter_var($this->value, FILTER_SANITIZE_STRING));

      if(!empty($val))
      {
         $this->printValue  = "";
         if(!$this->hiddenLabel)
            $this->printValue .= "<div class='formFieldOutput'><strong>".$this->label."</strong>: <div> ";

         if($this->modifyOutput)
         {
            switch($this->modifyOutput)
            {
               case "capitalize":
                  $this->printValue .= ucwords($val);
                  break;
               case "uppercase":
                  $this->printValue .= strtoupper($val);
                  break;
               case "lowercase":
                  $this->printValue .= strtolower($val);
                  break;
               default:
                  $this->printValue .= $val;
                  break;
            }
         }
         else
            $this->printValue .= $val;

         $this->printValue .= "</div>";
         $this->printValue .= "</div>\n";
      }
   }

   public function getValue()
   {
      return $this->getSubmittedValue();
   }

   /*
    * The following functions shouldn't need to be overwritten
    * by the extending field classes.
    */
   public function render($formType="form-horizontal")
   {
      if($formType == "form-inline")
         $this->inlineForm = true;
      else
         $this->horizontalForm = true;

      $label = $this->getLabel();
      $this->i++;
      $fields = $this->getField();
      $this->i--;
      $fieldContent = $this->wrapFields($label . $fields);

      return $fieldContent;
   }

   public function getSubmittedValue()
   {
      $currentVal = $this->value;
      if(empty($currentVal))
         $this->setValue(false);

      // Pull values from $_POST and
      // sanitize accordingly.
      $this->fetchSanitizedValue();

      return $this->value;
   }

   public function setInvalid()
   {
      $this->data["valid"] = false;
   }

   public function setValid()
   {
      $this->data["valid"] = true;
   }

   public function setValue($val, $setSource=false)
   {
      $this->value = $val;
      $this->data["value"] = $val;
      if($setSource)
         $this->responseSource[$this->fieldName] = $val;
   }

   protected function wrapFields($fieldHTML)
   {
      $fieldContent  = $this->indent()."<div class='form-group";
      if(!$this->valid)
         $fieldContent .= " has-error";
      if($this->wrapperClass)
         $fieldContent .= " ".trim($this->wrapperClass);
      if(!$this->visibleByDefault)
         $fieldContent .= " hiddenFieldWrapper";
      $fieldContent .= "'";
      $fieldContent .= " id='fieldWrapper_".$this->fieldName."'";
      if(!$this->valid)
         $fieldContent .= " aria-invalid='true'";
      if ($this->wrapperStyle)
         $fieldContent .= " style='".trim($this->wrapperStyle)."'";
      $fieldContent .= ">\n";

      $fieldContent .= $fieldHTML;

      $fieldContent .= $this->indent()."</div>\n\n";

      return $fieldContent;
   }

   public function checkData()
   {
      $val = $this->getSubmittedValue();

      if(!ctype_digit($val) && empty($val) && $this->required && $this->allowBlank == false )
      {
         $this->setValue(false);
         $this->setInvalid();
         $this->setErrorMessage();
      }
      else
      {
         if(empty($val))
            //$this->data["valid"] = true;
            $this->setValid();
         else
         {

            // This is either an integer (representing an error code)
            // or a boolean true (meaning the data passed validation).
            $isValid = $this->validate();

            if($isValid !== true)
            {
               $this->setInvalid();
               $this->setErrorMessage($isValid);
            }
            else
               $this->setValid();
         }
      }

      if($this->valid) {
         $this->setPrintValues();
      }
   }

   // Used by date-related form field types.
   protected function getRelativeTime($timeStr)
   {
      $relTime = strtotime($timeStr);
      if(strpos($timeStr, "weekday") !== false)
      {
         $adjustment = time() - strtotime("today");
         $relTime += $adjustment;
      }
      return $relTime;
   }
}

class FormField_Captcha extends FormField
{
   var $captcha;

   public function __construct($fieldData, $responseType)
   {
      parent::__construct($fieldData, $responseType);
      $this->captcha = new Captcha\Captcha();
      $this->required = true;

      if(!defined("_MB_RECAPTCHA_PUBLIC_KEY_"))
         define("_MB_RECAPTCHA_PUBLIC_KEY_", "6LeRvu0SAAAAAPTmxV38DSMwkkU8wQy2DdcCHiFL");
      if(!defined("_MB_RECAPTCHA_PRIVATE_KEY_"))
         define("_MB_RECAPTCHA_PRIVATE_KEY_", "6LeRvu0SAAAAADB6POkcJHjX6eSZ4kpDyvFvvp3J");

      $this->captcha->setPublicKey(_MB_RECAPTCHA_PUBLIC_KEY_);
      $this->captcha->setPrivateKey(_MB_RECAPTCHA_PRIVATE_KEY_);
   }

   protected function getLabel() {
      return "";
   }

   public function getFieldOptions() { return array(); }

   protected function getField($fieldType = 'captcha')
   {
      if(!$this->inlineForm)
      {
         $fieldContent  = "<div class='col-sm-9 col-sm-offset-4'>";
         $fieldContent .= $this->captcha->html();
         $fieldContent .= "</div>";
         return $fieldContent;
      }
      else
         return $this->captcha->html();
   }

   public function getSubjectLine() { return ""; }

   public function supportedJavascriptEvents()
   {
      return array();
   }

   protected function setPrintValues() { $this->printValue = ""; }

   protected function setErrorMessage()
   {
      $this->errorMessage = "The <a href='#fieldWrapper_".$this->fieldName."'>validation code</a> you entered was not correct. Please try again.";
   }

   public function checkData()
   {
      $isValid = $this->validate();
      if($isValid !== true && !$this->allowBlank)
      {
         $this->setInvalid();
         $this->setErrorMessage($isValid);
      }
      else
         $this->setValid();
   }

   protected function validate()
   {
      $response = $this->captcha->check();
      if($response->isValid())
         return true;
      else
      {
         return 0;
      }
   }
}

class FormField_Checkbox extends FormField
{
   protected function getLabel()
   {

      $fieldContent = $this->indent()."<fieldset class='pickFrom";
      if($this->required)
         $fieldContent .= " required";

      if($this->hiddenLabel)
      {
         if($this->divClass)
            $fieldContent .= " ".$this->divClass;

         return $fieldContent."'>\n";
      }

      $fieldContent .= "'>\n";
      $this->i++;

      $fieldContent .= $this->indent()."<legend";

      if($this->inlineForm)
      {
         $legendClass = "";
      }
      else
      {
         $legendClass = "col-sm-3 control-label ";
      }

      if($this->required)
         $legendClass .= "required ";

      $legendClass = trim($legendClass);

      if(!empty($legendClass))
         $fieldContent .= " class='".trim($legendClass)."'";

      $fieldContent .= ">";
      $fieldContent .= "<span class='labelText";
      if($this->noColon)
         $fieldContent .= " noColon";
      $fieldContent .= "'>".htmlspecialchars($this->label)."</span>";
      if($this->moreLabel)
         $fieldContent .= "<span class='moreLabel'>".($this->moreLabel)."</span>";
      $fieldContent .= "</legend>\n";
      $this->i--;

      return $fieldContent;
   }

   public function getHiddenField()
   {
      $checkedOptions = $this->getSubmittedValue();
      $hiddenFieldContent = "<!-- " . $this->label . " -->\n";

      foreach($this->options as $k=>$option)
      {
         $thisValue = $k;

         if(in_array(($thisValue), $checkedOptions))
            $hiddenFieldContent .= "<input type='hidden' name='".htmlentities($this->fieldName)."_option_".$thisValue."' value='".($thisValue)."' /> ";
      }

      return $hiddenFieldContent;
   }

   protected function getField($fieldType = 'checkbox')
   {
      $fieldContent  = "";
      if(!$this->inlineForm)
      {
         $fieldContent .= $this->indent()."<div class='";
         $fieldContent .= "col-sm-9";

         if($this->hiddenLabel)
            $fieldContent .= " col-sm-offset-3";
         $fieldContent .= "'>\n";
         $this->i++;
      }
      else
      {
         // TODO: Figure out how checkboxes should look when inline...
      }

      $fieldContent .= $this->indent()."<div class='checkbox'>\n";
      $this->i++;

      $currentValue = $this->value;

      if($this->options)
      {
         $options = $this->options;
         foreach($options as $k=>$o)
         {
            $fieldContent .= $this->indent()."<div>\n";
            $this->i++;

            $thisFieldName = $this->fieldName."_option_".$k;

            $fieldContent .= $this->indent()."<label class='choice' for='".$thisFieldName."'>";
            $fieldContent .= "<input type='checkbox' name='".$thisFieldName."' id='".$thisFieldName."'";

            if($this->fieldClass)
               $fieldContent .= " class='".trim($this->fieldClass)."'";

            $thisValue = $k;
            $fieldContent .= " value='".$thisValue."'";
            if(!empty($currentValue) && in_array($thisValue, $currentValue))
               $fieldContent .= " checked='checked'";
            else if($this->checked !== false)
            {
               if(is_array($this->checked) && in_array($thisValue, $this->checked))
                  $fieldContent .= " checked='checked'";
               else if(!is_array($this->checked) && $this->checked == $thisValue)
                  $fieldContent .= " checked='checked'";
            }

            $fieldContent .= " />";
            $fieldContent .= $o;
            $fieldContent .= "</label>\n";
            $this->i--;

            $fieldContent .= $this->indent()."</div>\n";
         }
      }
      else
         $fieldContent .= $this->indent()."<!-- Missing field options -->\n";

      $this->i--;
      $fieldContent .= $this->indent()."</div>\n";

      if(!$this->inlineForm)
      {
         $this->i--;
         $fieldContent .= $this->indent()."</div>\n";
      }

      $this->i--;
      $fieldContent .= $this->indent()."</fieldset>\n";
      $this->i--;

      // Add any custom JavaScript.
      $fieldContent .= $this->getAdditionalJavaScript();

      return $fieldContent;
   }

   public function getSubjectLine()
   {
      $val = $this->value;
      if(empty($val))
         return "";

      $options = $this->options;
      $selectedOptions = array();
      foreach($val as $v)
      {
         $thisValue = $v;

         $selectedOptions[] = $options[$thisValue];
      }
      return $selectedOptions;
   }

   protected function getAdditionalJavaScript($fieldName="")
   {
      if(empty($fieldName))
         $fieldName = "fieldset".preg_replace("/-/","",$this->fieldName);
      else $fieldName = preg_replace("/-/","",$fieldName);

      $javaScript  = "";

      $possibleEvents = $this->supportedJavascriptEvents();
      foreach($possibleEvents as $e=>$d)
      {
         if($this->$e)
            $javaScript .= $this->createJavascriptEvent($e, $this->$e, $fieldName);
      }

      $javaScriptOutput  = "";
      if(!empty($javaScript))
      {
         $javaScriptOutput .= "\n<script type='text/javascript'>\n";

         // Make sure we set up our field variable.
         $javaScriptOutput .= $fieldName." = $('#fieldWrapper_".$this->fieldName." fieldset.pickFrom input[type=checkbox]');\n";

         // Append the JavaScript
         $javaScriptOutput .= $javaScript;
         $javaScriptOutput .= "\n</script>\n";
      }

      return $javaScriptOutput;
   }

   public function supportedJavascriptEvents()
   {
      $events = parent::supportedJavascriptEvents();
      // There's no option for selecting text from a dropdown menu.
      unset($events["onselect"]);

      return $events;
   }

   protected function fetchSanitizedValue()
   {
      // Need to check for each option and send back an array
      // with the results.
      $options = $this->options;
      $results = array();
      foreach($options as $k=>$o)
      {
         $thisValue = $k;

         $thisFieldName = $this->fieldName."_option_".$thisValue;
         if(isset($this->responseSource[$thisFieldName]) && $this->responseSource[$thisFieldName] == $thisValue)
            $results[] = $k;
      }

      // We always want to submit the array, event
      // if it's empty.
      $this->setValue($results);
   }

   protected function setErrorMessage($errorNo=0)
   {
      $errorMessages = array();
      $errorMessages[] = "Please check at least one option for '".htmlspecialchars($this->label)."'.";
      $errorMessages[] = "You submitted an invalid option for '".htmlspecialchars($this->label)."' Please select from the options displayed.";

      if(isset($errorMessages[$errorNo]))
         $this->errorMessage = $errorMessages[$errorNo];
      else
         $this->errorMessage = $errorMessages[0];
   }

   protected function setPrintValues()
   {
      $options = $this->options;
      $this->printValue  = "<div class='formFieldOutput'><strong>".$this->label."</strong>: <div>\n";
      $vals = $this->value;
      if(!empty($vals))
      {
         foreach($vals as $v)
         {
            $this->printValue .= "<span style='font-size:150%;color: #3c763d;'>&#x2713;</span> ";

            if($this->modifyOutput)
            {
               switch($this->modifyOutput)
               {
                  case "capitalize":
                     $this->printValue .= ucwords($options[$v]);
                     break;
                  case "uppercase":
                     $this->printValue .= strtoupper($options[$v]);
                     break;
                  case "lowercase":
                     $this->printValue .= strtolower($options[$v]);
                     break;
                  default:
                     $this->printValue .= $options[$v];
                     break;
               }
            }
            else
               $this->printValue .= $options[$v];

            $this->printValue .= "<br />\n";
         }
      }
      $this->printValue .= "</div></div>\n";
   }

   public function getValue()
   {
      $values = array();
      $options = $this->options;
      $vals = $this->value;
      if(!empty($vals))
      {
         foreach($vals as $v)
         {
            $values[] = $options[$v];
         }
      }

      return $values;
   }

   public function getValueKey()
   {
      return $this->value;
   }

   protected function validate()
   {
      // Make sure that if they did submit something,
      // that it exists in the options.
      $validEntries = true;
      $options = $this->options;
      $vals = $this->value;
      if(!empty($vals))
      {
         foreach($vals as $v)
         {
            if(!isset($options[($v)]))
               $validEntries = false;
         }
      }

      if(!$validEntries)
         return false;
      else
         return true;
   }
}

class FormField_DateTime extends FormField
{
   public function __construct($fieldData, $responseType)
   {
      parent::__construct($fieldData, $responseType);
      $this->placeholder = "";

      // This is similar to PHP but not an exact match... :/
      // a: am or pm
      // A: AM or PM
      // d: days with leading zero
      // D: Day of the week, abbreviated (Sun, Mon, Tue, Wed, Thu, Fri, Sat)
      // F: Full name of the month
      // g: 12-hour format without leading zero
      // G: 24-hour format without leading zero
      // h: 12-hour format with leading zero
      // H: 24-hour format with leading zero
      // i: minute with leading zero
      // j: days without leading zero
      // l: Full day name
      // L: Whether it's a leap year
      // m: month with leading zero
      // M: Abbreviated month name
      // n: month without leading zero
      // O: Difference to Greenwich time (GMT) in hours
      // s: seconds with leading zero
      // S: Ordinal suffix for the day of the month
      // t: Number of days in the month
      // T: Full date time string. E.g., Wed Apr 15 2015 15:02:55 GMT-0700 (MST)
      // U: Abbreviated month name
      // W: Week of the year
      // w: day of the week, Sunday being 0 and Saturday being 6
      // y: Year, two digits
      // y: Year, four digits
      // z: day of the year, starting from 0
      // Z: Timezone offset in seconds


      if ( !empty($this->data['dateformat']))
        $this->dateFormat = $this->data['dateformat'];

      if ( !empty($this->data['timeformat']))
        $this->timeFormat = $this->data['timeformat'];

      if($this->dateFormat === false)
         $this->dateFormat = "Y/m/d";

      if($this->timeFormat === false)
         $this->timeFormat = "g:ia";

      $this->combinedFormat = trim($this->dateFormat." ".$this->timeFormat);

      $this->datepicker = true;
      $this->timepicker = true;

      $dateVal = $this->startDate;

      if(!empty($dateVal))
      {
         $preset = $this->getRelativeTime($dateVal);
         $this->setValue(date($this->dateFormat, $preset));
      }
   }

   protected function getField($fieldType = 'text')
   {
      $field = parent::getField($fieldType);

      $fieldID = $this->fieldName;

      // Documentation for the DatePicker class can be found at
      // http://xdsoft.net/jqplugins/datetimepicker/
      $javaScript  = "\n<script type='text/javascript'>\n";
      $javaScript .= $fieldID." = $('#".$fieldID."');\n";

      if($this->value)
         $javaScript .= $fieldID.".val('".date($this->combinedFormat, strtotime($this->value))."');\n";
      else if($this->startDate)
         $javaScript .= $fieldID.".val('".date($this->combinedFormat, strtotime($this->startDate))."');\n";

      $javaScript .= $fieldID.".datetimepicker({\n";
         // There's a small bug when a date is selected, the calendar doesn't open up
         // to the selected date but to the original start date instead.
         $javaScript .= "   onChangeDateTime: function(dp,input){this.setOptions({startDate: false});},\n";


         if($this->timepicker)
         {
            $javaScript .= "   formatTime: '".$this->timeFormat."',\n";
            // Set min/max times
            if($this->minTime)
               $javaScript .= "   minTime: '".$this->minTime."',\n";
            if($this->maxTime)
               $javaScript .= "   maxTime: '".$this->maxTime."',\n";

            if($this->allowTimes)
            {
               if(!is_array($this->allowTimes))
                  $allowedTimes = array($this->allowTimes);
               else
                  $allowedTimes = $this->allowTimes;

               $allowed = "";
               foreach($allowedTimes as $a)
               {
                  $allowed .= "'".date($this->timeFormat, strtotime($a))."', ";
               }
               $javaScript .= "   allowTimes: [".substr($allowed,0,-2)."],\n";
            }

            if($this->timeInterval)
               $interval = $this->timeInterval;
            else
               $interval = 30;
            $javaScript .= "   step: ".$interval.",\n";

            $javaScript .= "   timepickerScrollbar: false,\n";

         }
         else
            $javaScript .= "   timepicker: false,\n";

         if($this->datepicker)
         {
            $javaScript .= "   formatDate: '".$this->dateFormat."',\n";

            // Set min/max dates
            if($this->minDate)
               $javaScript .= "   minDate: '".$this->minDate."',\n";
            if($this->maxDate)
               $javaScript .= "   maxDate: '".$this->maxDate."',\n";

            // Set the initial date
            if($this->value)
               $javaScript .= "   value: '".date("Y/m/d", strtotime($this->value))."',\n";
            else if($this->startDate)
               $javaScript .= "   value: '".date("Y/m/d", strtotime($this->startDate))."',\n";

            // Disable days (if specified)
            if($this->disabledDates)
            {
               if(!is_array($this->disabledDates))
                  $disabledDates = array($this->disabledDates);
               else
                  $disabledDates = $this->disabledDates;

               $disabled = "";
               foreach($disabledDates as $dd)
               {
                  $disabled .= "'".date($this->dateFormat, strtotime($dd))."', ";
               }
               $javaScript .= "   disabledDates: [".substr($disabled,0,-2)."],\n";
            }

            // Disable mousewheel
            $javaScript .= "   scrollMonth : false,\n";
            $javaScript .= "   scrollInput : false,\n";

         }
         else
            $javaScript .= "   datepicker: false,\n";

         if($this->moreDatePickerConfig)
            $javaScript .= $this->moreDatePickerConfig;

         $javaScript .= "   format: '".$this->combinedFormat."',\n";


      $javaScript .= "});\n";
      $javaScript .= $fieldID.".attr('autocomplete', 'off');\n";
      $javaScript .= "</script>\n";

      $output  = $field.$javaScript;
      $output .= $this->getAdditionalJavaScript();

      return $output;
   }

   public function getSubjectLine()
   {
      $val = $this->value;
      if(!empty($val))
         return date($this->combinedFormat, strtotime($val));
      else
         return "";
   }

   public function supportedJavascriptEvents()
   {
      return array(
          "onchange" => "Event triggered when a datetime selector's value changes. Parameters passed are 'dateTimeVal' and 'instance'.",
          "onclose"  => "Event triggered when a datetime selector is closed. Parameters passed are 'dateTimeVal' and 'instance'."
      );
   }

   protected function validate()
   {
      if($this->required && !$this->value)
         return 0;

      $checkDate = date($this->combinedFormat, strtotime($this->value));
      if($this->value != $checkDate)
         return 1;

      return true;
   }

   protected function setErrorMessage($errorNo = 0)
   {
      $errorMessages = array();
      $errorMessages[] = "You must enter a date/time for '".$this->label."'.";
      $errorMessages[] = "Please enter a date/time for '".$this->label."' in the specified format (".$this->combinedFormat.").";

      if(isset($errorMessages[$errorNo]))
         $this->errorMessage = $errorMessages[$errorNo];
      else
         $this->errorMessage = $errorMessages[0];
   }

   protected function setPrintValues()
   {
      $this->printValue  = "<div class='formFieldOutput'><strong>".$this->label."</strong>: <div>";
      $val = $this->value;
      if(!empty($val))
         $this->printValue .= date($this->combinedFormat, strtotime($val));
      $this->printValue .= "</div></div>\n";
   }

   public function getValue()
   {
      $val = $this->value;
      if(empty($val))
         return "";

      return date($this->combinedFormat, strtotime($val));
   }

   public function getFieldOptions()
   {
      $fieldData = parent::getFieldOptions();
      $fieldData["dateValue"] = "string Initial date to be displayed.";
      $fieldData["minDate"] = "string Minimum date that users can pick.";
      $fieldData["maxDate"] = "string Maximum date that users can pick.";
      $fieldData["startDate"] = "string Initial date to be displayed.";
      $fieldData["dateFormat"] = "string Format the date should be displayed in.";
      $fieldData["timeFormat"] = "string Format the time should be displayed in.";
      $fieldData["minTime"] = "string Minimum time that users can pick.";
      $fieldData["maxTime"] = "string Maximum time that users can pick.";
      $fieldData["allowTimes"] = "array List of times that are allowed.";
      $fieldData["timeInterval"] = "int Number of minutes for each time interval.";

      ksort($fieldData);

      return $fieldData;
   }
}

class FormField_Date extends FormField_DateTime
{
   public function __construct($fieldData, $responseType)
   {
      if(!($this->dateFormat))
         $this->dateFormat = "Y/m/d";

      $this->timeFormat = "";

      parent::__construct($fieldData, $responseType);

      $this->timepicker = false;
   }

   protected function setErrorMessage($errorNo = 0)
   {
      $errorMessages = array();
      $errorMessages[] = "You must enter a date for '".$this->label."'.";
      $errorMessages[] = "Please enter a date for '".$this->label."' in the specified format (".$this->dateFormat.").";

      if(isset($errorMessages[$errorNo]))
         $this->errorMessage = $errorMessages[$errorNo];
      else
         $this->errorMessage = $errorMessages[0];
   }

   public function getFieldOptions()
   {
      $fieldData = parent::getFieldOptions();
      unset($fieldData["allowTimes"]);
      unset($fieldData["maxTime"]);
      unset($fieldData["minTime"]);
      unset($fieldData["timeFormat"]);
      unset($fieldData["timeInterval"]);

      return $fieldData;
   }
}

class FormField_HTMLDate extends FormField
{
   public function __construct($fieldData, $responseType)
   {
      if(!isset($fieldData["placeholder"]))
         $fieldData["placeholder"] = "XX/XX/XXXX";

      parent::__construct($fieldData, $responseType);
   }

   protected function getField($fieldType = 'date')
   {
      return parent::getField("date");
   }

//   protected function fetchSanitizedValue()
//   {
//      if(isset($this->responseSource[$this->fieldName]))
//         $this->setValue(trim(filter_var($this->responseSource[$this->fieldName], FILTER_SANITIZE_EMAIL)));
//   }

//   protected function setErrorMessage()
//   {
//      $this->errorMessage = "Please enter a valid email address for '".htmlspecialchars($this->label)."'.";
//   }

//   protected function setPrintValues()
//   {
//      $email = trim(filter_var($this->value, FILTER_SANITIZE_EMAIL));
//      if(!empty($email))
//      {
//         $this->printValue  = "<div class='formFieldOutput'><strong>".$this->label."</strong> <div>";
//         $this->printValue .= "<a href='mailto:".htmlentities($email)."'>";
//         $this->printValue .= $email;
//         $this->printValue .= "</a>";
//         $this->printValue .= "</div></div>\n";
//      }
//      else
//         $this->printValue = "";
//   }

//   protected function validate()
//   {
//      if(!filter_var($this->value, FILTER_VALIDATE_EMAIL))
//         return 0;
//      else
//         return true;
//   }
//
//   public function getFieldOptions()
//   {
//      $fieldData = parent::getFieldOptions();
//      $fieldData["placeholder"] = "string Value to be used as a placeholder attribute.";
//      $fieldData["setAsReplyTo"] = "bool Whether the field value should be used as a Reply-To address (only applies for email responses).";
//      $fieldData["sendUserEmail"] = "bool Whether the field value should be used as a To address (only applies for email responses).";
//
//      ksort($fieldData);
//      return $fieldData;
//   }
}

class FormField_Email extends FormField
{
   public function __construct($fieldData, $responseType)
   {
      if(!isset($fieldData["placeholder"]))
         $fieldData["placeholder"] = "example@email.arizona.edu";

      parent::__construct($fieldData, $responseType);
   }

   protected function getField($fieldType = 'email')
   {
      return parent::getField("email");
   }

   protected function fetchSanitizedValue()
   {
      if(isset($this->responseSource[$this->fieldName]))
         $this->setValue(trim(filter_var($this->responseSource[$this->fieldName], FILTER_SANITIZE_EMAIL)));
   }

   protected function setErrorMessage()
   {
      $this->errorMessage = "Please enter a valid email address for '".htmlspecialchars($this->label)."'.";
   }

   protected function setPrintValues()
   {
      $email = trim(filter_var($this->value, FILTER_SANITIZE_EMAIL));
      if(!empty($email))
      {
         $this->printValue  = "<div class='formFieldOutput'><strong>".$this->label."</strong>: <div>";
         $this->printValue .= "<a href='mailto:".htmlentities($email)."'>";
         $this->printValue .= $email;
         $this->printValue .= "</a>";
         $this->printValue .= "</div></div>\n";
      }
      else
         $this->printValue = "";
   }

   protected function validate()
   {
      if(!filter_var($this->value, FILTER_VALIDATE_EMAIL))
         return 0;
      else
         return true;
   }

   public function getFieldOptions()
   {
      $fieldData = parent::getFieldOptions();
      $fieldData["placeholder"] = "string Value to be used as a placeholder attribute.";
      $fieldData["setAsReplyTo"] = "bool Whether the field value should be used as a Reply-To address (only applies for email responses).";
      $fieldData["sendUserEmail"] = "bool Whether the field value should be used as a To address (only applies for email responses).";

      ksort($fieldData);
      return $fieldData;
   }
}

class FormField_File extends FormField
{
   var $fileData;

   public function __construct($fieldData, $responseType)
   {
      parent::__construct($fieldData, $responseType);
      $this->data["enctype"] = "multipart/form-data";
   }


   protected function getLabel()
   {
      if($this->hiddenLabel)
         return "";

      $label = parent::getLabel();
      $allowedTypes = $this->allowedFileTypes;
      if(!empty($allowedTypes))
      {
         $label = str_replace("</label>","",$label);
         $label .= "<span  class='moreLabel allowedFiletypes'>Allowed file types: ".implode(", ",$allowedTypes)."</span>";
         $label .= "</label>\n";
      }

      return $label;
   }

   protected function getField($fieldType = 'file')
   {
      $fieldContent  = "";
      if(!$this->inlineForm)
      {
         $fieldContent .= $this->indent()."<div";
         if($this->divStyle)
            $fieldContent .= " style='".trim($this->divStyle)."'";

         $fieldContent .= " class='col-sm-9";
         if($this->hiddenLabel)
            $fieldContent .=" col-sm-offset-3";

         if($this->divClass)
            $fieldContent .= " ".trim($this->divClass);
         $fieldContent .= "'>\n";
         $this->i++;
      }

      // See if they already uploaded a file...
      // TODO: Need to figure out a way to let users remove an already
      //       uploaded file.
      $val = $this->value;
      if(!empty($val) && $this->valid)
      {
         $file_data = serialize($this->value);
         $file_data = FormFieldCrypt::aes_enc($file_data, _MB_AES_ENC_SECRET_);
         $fieldContent .= "<input type='hidden' name='_file_upload_".$this->fieldName."' value='".$file_data."' />\n";

         $fieldContent .= $this->value["name"];
      }
      else {
         $fieldContent .= $this->indent()."<input type='file' name='".$this->fieldName."' class='form-control' id='".$this->fieldName."' ";

        if ( $this->multiple )
             $fieldContent .= "multiple";

        $fieldContent .= " />\n";
      }

      if(!$this->inlineForm)
      {
         $this->i--;
         $fieldContent .= $this->indent()."</div>\n";
      }

      return $fieldContent;
   }

   /**
    * Returns a string based on the uploaded file to be used as a subject line in an email.
    *
    * @return string Either the original name of the file uploaded or an empty string if nothing was submitted.
    */
   public function getSubjectLine()
   {
      $val = $this->value;
      if(!empty($val))
         return $val["name"];
      else
         return "";
   }

   /**
    * Returns a list of JavaScript events that are available for this form field type.
    *
    * @return array List of JavaScript events.
    */
   public function supportedJavascriptEvents()
   {
      return array();
   }

   protected function fetchSanitizedValue()
   {
      // If the user has uploaded a file, we should
      // store it for safe-keeping. This is also
      // useful if they've forgotten to fill out
      // a required field.
      if(!$this->storeUploadedFile())
      {
         if(isset($this->responseSource["_file_upload_".$this->fieldName]))
         {
            $file_data = $this->responseSource["_file_upload_".$this->fieldName];
            $file_data = FormFieldCrypt::aes_dec($file_data, _MB_AES_ENC_SECRET_);
            $file_data = unserialize($file_data);
            $this->setValue($file_data);
         }
         else
            $this->setValue(false);
      }

   }

   /**
    * This function takes the uploaded file and puts it into a temporary
    * holding directory ('mb_holding') outside of the webroot (so
    * that the files aren't accessible).
    *
    * @return boolean Returns true on success or false on failure. See
    * $field->errorMessages for specific details on a failure.
    */
   protected function storeUploadedFile()
   {
      if(isset($_FILES[$this->fieldName]))
      {
         $uploadedFile = $_FILES[$this->fieldName];

         // If they haven't uploaded a file successfully, then we
         // return false.
         if($uploadedFile["error"] != UPLOAD_ERR_OK)
         {

            switch ($uploadedFile["error"])
            {
               case UPLOAD_ERR_INI_SIZE:
               case UPLOAD_ERR_FORM_SIZE:
                  $this->setErrorMessage(1);
                  return false;
                  break;
               case UPLOAD_ERR_PARTIAL:
                  $this->setErrorMessage(2);
                  return false;
                  break;
               case UPLOAD_ERR_NO_FILE:
                  $this->setErrorMessage(3);
                  return false;
                  break;
               case UPLOAD_ERR_NO_TMP_DIR:
               case UPLOAD_ERR_CANT_WRITE:
                  $this->setErrorMessage(4);
                  return false;
                  break;
               case UPLOAD_ERR_EXTENSION:
                  $this->setErrorMessage(5);
                  return false;
                  break;
               default:
                  return false;
            }
         }

         /*
          * TODO: We could also check MIME types
          * here
          */
         if($this->allowedFileTypes)
         {
            // Check file extensions
            $filename = $uploadedFile["name"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);

            if(!in_array($ext, $this->allowedFileTypes))
            {
               $this->setErrorMessage(5);
               return false;
            }
         }

         // Check file size (just in case it slipped passed)
         if($this->maxFileSize)
         {
            if($this->maxFileSize < $uploadedFile["size"])
            {
               $this->setErrorMessage(1);
               return false;
            }
         }

         // Move the file to a less permanent location
         $holdingDir = getcwd()._MB_FORM_SUBMISSION_HOLDING_;
         $uniqueFilename = basename(tempnam($holdingDir, "_mb_".$this->fieldName));
         $tmpFilename = $holdingDir.$uniqueFilename;

         if(!move_uploaded_file($uploadedFile["tmp_name"], $tmpFilename))
         {
            $this->setErrorMessage(4);
            return false;
         }
         else
         {
            $fileData = array();
            $fileData["originalInfo"] = $uploadedFile;
            $fileData["name"]         = $uploadedFile["name"];
            $fileData["type"]         = $uploadedFile["type"];
            $fileData["tmp_name"]     = $uploadedFile["tmp_name"];
            $fileData["error"]        = $uploadedFile["error"];
            $fileData["size"]         = $uploadedFile["size"];
            $fileData["mb_holding"]   = $tmpFilename;

            $this->setValue($fileData);
            return true;
         }
      }
      return false;
   }

   protected function setErrorMessage($errorNo=0)
   {
      $errorMessages = array();
      $errorMessages[] = "Please attach a valid file for '".$this->label."'.";
      $errorMessages[] = "The file you submitted is larger than the maximum file size allowed. Please try again with a smaller file.";
      $errorMessages[] = "The file you submitted was only partially uploaded. Please try again.";
      $errorMessages[] = "The file you submitted was not uploaded.";
      $errorMessages[] = "Unfortunately, there was a system error and your file was not uploaded successfully. Please try again.";

      $allowedTypes = $this->allowedFileTypes;
      if(!empty($allowedTypes) && is_array($allowedTypes))
         $errorMessages[] = "The file type you submitted is not allowed. Please upload one of the following file types: ".strtolower(implode($allowedTypes, ", "));
      else
         $errorMessages[] = "The file type you submitted is not allowed.";

      if(isset($errorMessages[$errorNo]))
         $this->errorMessage = $errorMessages[$errorNo];
      else
         $this->errorMessage = $errorMessages[0];
   }

   protected function setPrintValues()
   {
      $val = $this->value;
      if(!empty($val))
         $this->printValue = "<div class='formFieldOutput'><strong>".$this->label."</strong>: <div>".filter_var($this->value["name"], FILTER_SANITIZE_STRING)."</div></div>\n";
      else
         $this->printValue = "";
   }

   public function getValue()
   {
      return $this->value;
      return filter_var($this->value["name"], FILTER_SANITIZE_STRING);
   }

   // We've done all of the file validation before this point
   // (in storeUploadedFile) so it's just a matter of figuring
   // out whether the field was required or not and if the
   // file was uploaded correctly.
   protected function validate()
   {
      $val = $this->value;
      if(empty($val) && !$this->required)
         return true;
      else if(empty($val) && $this->required)
         return false;
      else if(!isset($val["originalInfo"]) || !isset($val["mb_holding"]))
         return false;
      else if(!file_exists($val["mb_holding"]))
         return false;
      else
         return true;
   }

   public function getFieldOptions()
   {
      $fieldData = parent::getFieldOptions();
      $fieldData["allowedFileTypes"] = "array White list of extensions that are allowed.";
      $fieldData["maxFileSize"] = "int Maximum file size allowed (in bytes).";
      $fieldData["overwriteFiles"] = "bool CAUTION!! This will allow files to be overwritten if the user uploads a file with the same name to the same directory.";
      //unset($fieldData["fieldClass"]);
      ksort($fieldData);
      return $fieldData;
   }
}

class FormField_Hidden extends FormField
{
   public function __construct($fieldData, $responseType)
   {
      parent::__construct($fieldData, $responseType);

      // Set visibleByDefault to false so
      // that the fieldWrapper is hidden on the page.
      $this->visibleByDefault = false;
   }

   protected function getLabel() { return ""; }

   protected function getField($fieldType = 'hidden') { return parent::getField('hidden'); }

   public function supportedJavascriptEvents()
   {
      return array();
   }

   protected function setErrorMessage()
   {
      $this->errorMessage = "It appears there was an internal system error. Please try again.";
   }

   protected function setPrintValues() { $this->printValue = ""; }

   /* TODO: Think about if/how we want to validate hidden fields.
    *       This could possibly be specified in the metadata for
    *       hidden fields (e.g. 'dataType' : 'int')
    */
   public function validate()
   {
      return true;
   }

   public function getFieldOptions() { return array(); }
}

class FormField_HTML extends FormField
{
   protected function getField($fieldType = 'html')
   {
      $currentValue = $this->value;
      $fieldContent  = "";
      if(!$this->inlineForm)
      {
         $fieldContent .= $this->indent()."<div";
         if($this->divStyle)
            $fieldContent .= " style='".trim($this->divStyle)."'";

         $fieldContent .= " class='col-sm-9";
         if($this->hiddenLabel)
            $fieldContent .=" col-sm-offset-3";

         if($this->divClass)
            $fieldContent .= " ".trim($this->divClass);
         $fieldContent .= "'>\n";
         $this->i++;
      }

      $fieldContent .= $this->indent()."<textarea name='".$this->fieldName."' id='".$this->fieldName."' ";
      $fieldContent .= "cols='".($this->cols ? $this->cols : 30)."' ";
      $fieldContent .= "rows='".($this->rows ? $this->rows : 10)."' ";

      /*
       * Because the WYSIWYG editor hides the actual form field, we
       * don't want to mark this as "required". It will be validated
       * on submission but not otherwise.
       */

      if($this->type == _MB_FORM_FIELD_HTML_)
         $fieldContent .= " class='wysiwyg'";

      $fieldContent .= ">";
      if(!empty($currentValue))
      {
         $dirtyDirtyHTML = $currentValue;
         $config = $this->getHTMLPurifierConfig();
         $purifier = new HTMLPurifier($config);

         $cleanHTML = $purifier->purify($dirtyDirtyHTML);
         $fieldContent .= $cleanHTML;
      }

      if(defined("_MB_APP_URL_"))
         $websiteURL = _MB_APP_URL_;
      else
         $websiteURL = false;

      $fieldContent .= "</textarea>\n";

      /* Set up TinyMCE config per instance */
      if(isset($this->data["config"]))
      {
         $wysiwygConfig = $this->data["config"];
      }
      else
      {
         $confObj = new WYSIWYG_Config();
         $wysiwygConfig = $confObj->getJSONConfig();
      }

      $fieldContent .= $this->indent()."<script type='text/javascript'>\n".
                 $this->indent()."if((typeof(tinymce) != 'undefined')) {\n".
                 $this->indent()."   tinymce.init({\n".
                 $this->indent()."      selector:'textarea#".$this->fieldName."',\n".
                 $this->indent().$wysiwygConfig."\n".
                 $this->indent()."   });\n".
                 $this->indent()."}\n".
                 $this->indent()."</script>\n";

      $this->addJS("/common/admin/js/tinymce/plugins/customHeadings/plugin.min.js");
      $this->addJS("/common/admin/js/tinymce/plugins/injectCourse/plugin.min.js");
      $this->addJS("/common/admin/js/tinymce/plugins/injectData/plugin.min.js");
      $this->addJS("/common/admin/js/tinymce/plugins/injectFAQ/plugin.min.js");
      $this->addJS("/common/admin/js/tinymce/plugins/injectForm/plugin.min.js");
      $this->addJS("/common/admin/js/tinymce/plugins/injectPerson/plugin.min.js");
      $this->addJS("/common/admin/js/tinymce/plugins/responsivefilemanager/plugin.min.js");

      if(!$this->inlineForm)
      {
         $this->i--;
         $fieldContent .= $this->indent()."</div>\n";
      }

      return $fieldContent;
   }

   public function getHiddenField()
   {
      $hiddenFieldContent  = "<!-- Start " . $this->label . " -->\n";
      $hiddenFieldContent .= "<textarea style='display:none' name='".htmlentities($this->fieldName)."'>".$this->getSubmittedValue()."</textarea>\n<!-- End ".htmlentities($this->label)." -->\n\n";
      return $hiddenFieldContent;
   }

   public function getSubjectLine()
   {
      return $this->label;
   }

   /* TODO: Need to figure out what, if anything, we support here. */
   public function supportedJavascriptEvents()
   {
      return array();
   }

   static public function getHTMLPurifierConfig()
   {
      $config = HTMLPurifier_Config::createDefault();
      $default = self::getHTMLPurifierDefaultConfig();

      foreach($default as $c)
      {
         if($c["type"] == "CONFIG_BOOLEAN")
            $val = ($c["defaultValue"] == "true" ? true : false);
         else
            $val = $c["defaultValue"];

         $config->set($c["config"], $val);
      }

      $def = $config->getHTMLDefinition(true);
      $def->addAttribute('img', 'data-inline', 'Enum#true,false');

      return $config;
   }

   protected function fetchSanitizedValue()
   {
      if(isset($this->responseSource[$this->fieldName]))
      {
         $dirtyDirtyHTML = $this->responseSource[$this->fieldName];
         $config = $this->getHTMLPurifierConfig();
         $purifier = new HTMLPurifier($config);

         $cleanHTML = $purifier->purify($dirtyDirtyHTML);
         $this->setValue($cleanHTML);
      }
   }

   public static function getWYSIWYGDefaultConfig()
   {
      $conf = new WYSIWYG_Config();
      return $conf->getJSONConfig();
   }

   public static function getHTMLPurifierDefaultConfig()
   {
      $configMgr = new Config();
      return $configMgr->getAllConfigs(3);
   }

   public function getFinalWYSIWYGConfig()
   {
      $conf = new Config();
      $defaultConfig = new WYSIWYG_Config();
      return $conf->getJSONConfig();
   }

   public function getFieldOptions()
   {
      $fieldData = parent::getFieldOptions();
      $fieldData["config"] = "string JSON-formatted string of TinyMCE configuration options. Use Config class to make your life easier!";
      unset($fieldData["fieldClass"]);
      unset($fieldData["setAsSubject"]);
      unset($fieldData["appendToSubject"]);
      unset($fieldData["prependToSubject"]);

      ksort($fieldData);
      return $fieldData;
   }
}

class FormField_Markup extends FormField
{
   public function render($formType="form-horizontal")
   {
      $markup = trim($this->markup);
      if(!empty($markup))
      {
         if($this->dumpRaw)
            return $this->indent().$this->markup;
         else
         {
            $fieldContent = $this->indent()."<div";
            if($this->divStyle)
               $fieldContent .= " style='".trim($this->divStyle)."'";
            if($this->divClass)
               $fieldContent .= " class='".trim($this->divClass)."'";
            $fieldContent .= ">\n";
            $this->i++;
            $fieldContent .= $this->indent().$this->markup."\n";
            $this->i--;
            $fieldContent .= $this->indent()."</div>\n\n";

            return $fieldContent;
         }
      }
      else
         return "";
   }

   public function getSubjectLine() { return ""; }
   protected function getLabel() { return ""; }
   public function getHiddenField() { return ""; }
   protected function getField($fieldType = 'markup') { return ""; }
   public function supportedJavascriptEvents() { return array(); }
   protected function fetchSanitizedValue() { return ""; }
   protected function validate() { return true; }
   protected function setPrintValues()
   {
      if($this->showInEmail)
         $this->printValue = $this->markup;
      else
         $this->printValue = "";
   }
   public function getValue() { return ""; }
   //public function getFieldOptions() { return array(); }
}

class FormField_Multibox extends FormField
{
   public function __construct($fieldData, $responseType)
   {
      parent::__construct($fieldData, $responseType);
      $rows = $this->rows;
      $cols = $this->cols;
      $requiredCols = $this->requiredCols;
      $errorMessages = $this->errorMessages;

      if(empty($rows))
         $this->rows = 1;

      if(empty($cols))
         $this->cols = 1;

      // If required columns isn't specified, then
      // we assume every field in a row is required.
      if(empty($requiredCols))
      {
         $requiredCols = array();
         for($a=0; $a< $this->cols; $a++)
            $requiredCols[] = $a;

         $this->requiredCols = $requiredCols;
      }

      if(empty($errorMessages))
      {
         $this->errorMessages = array();
         $this->errorMessages[] = "You must fill out at least one row.";
         if(count($requiredCols) == $this->cols)
            $this->errorMessages[] = "You must fill out all fields in a given row.";
         else
         {

            $fields = array();
            foreach($requiredCols as $c)
               $fields = Inflect::ordinal($c);
            $message = "You must fill out the ".\Mumby\DB\DBObject::commaList($fields)." fields in a row.";
            $this->errorMessages[] = $message;
         }
      }
   }

   protected function getLabel()
   {
      if($this->hiddenLabel)
         return $this->indent()."<fieldset>\n";

      $fieldContent = $this->indent()."<fieldset class='";

      if($this->required)
         $fieldContent .= " required";
      $fieldContent .= "'>\n";
      $this->i++;

      $fieldContent .= $this->indent()."<legend";

      if($this->inlineForm)
         $legendClass = "";
      else
         $legendClass = "col-sm-3 control-label ";
      if($this->required)
         $legendClass .= "required ";

      $fieldContent .= " class='".trim($legendClass)."'";

      $fieldContent .= ">";
      $fieldContent .= "<span class='labelText";
      if($this->noColon)
         $fieldContent .= " noColon";
      $fieldContent .= "'>".htmlspecialchars($this->label)."</span>";
      if($this->moreLabel)
         $fieldContent .= "<span class='moreLabel'>".($this->moreLabel)."</span>";
      $fieldContent .= "</legend>\n";
      $this->i--;

      return $fieldContent;
   }

   protected function getField($fieldType = 'multiBox')
   {
      $currentValue  = $this->getSubmittedValue();
      $fieldContent = "";

      // TODO: Figure out if we can support inline with this field type...

      $fieldContent .= $this->indent()."<div class='container col-sm-9";
      if($this->hiddenLabel)
         $fieldContent .=" col-sm-offset-3";
      $fieldContent .="'>\n";
      $this->i++;

      for($a=0; $a < $this->rows; $a++)
      {
         if($this->required && $a == 0)
            $requiredAttr = " required='required' ";
         else
            $requiredAttr = "";

         $fieldContent .= $this->indent()."<div";
         if($this->divStyle)
            $fieldContent .= " style='".trim($this->divStyle)."'";
         if($this->divClass)
            $fieldContent .= " class='row ".trim($this->divClass)."'";
         else
            $fieldContent .= " class='row'";
         $fieldContent .= ">\n";
            $this->i++;

         $smColSpan = floor(12/$this->cols);

         for($c=0; $c < $this->cols; $c++)
         {
            $fieldContent .= $this->indent()."<div class='col-sm-".$smColSpan." multiboxFieldCol'>";
            $fieldContent .= "<input type='text' ";
            if(isset($currentValue[$a][$c]) && !empty($currentValue[$a][$c]))
               $fieldContent .= "value='".htmlentities($currentValue[$a][$c])."' ";
            $fieldContent .= "class='form-control";
            if($this->fieldClass)
               $fieldContent .= " ".trim($this->fieldClass);
            $fieldContent .= "' name='".$this->fieldName."_".$a."_".$c."' id='".$this->fieldName."_".$a."_".$c."' ";
            if($this->placeholders && isset($this->placeholders[$c]))
               $fieldContent .= "placeholder='".$this->placeholders[$c]."' ";
            else if($this->colLabels && isset($this->colLabels[$c]))
               $fieldContent .= "placeholder='".$this->colLabels[$c]."' ";

            $fieldContent .= $requiredAttr."/>";
            $fieldContent .= "</div>\n";
         }

         $this->i--;
         $fieldContent .= $this->indent()."</div>\n";
      }

      $this->i--;
      $fieldContent .= $this->indent()."</div>\n";

      $content = $fieldContent;
      $this->i--;
      $content .= $this->indent()."</fieldset>\n";

      // Add any custom JavaScript.
      $content .= $this->getAdditionalJavaScript();

      return $content;
   }

   public function supportedJavascriptEvents()
   {
      return array();
   }

   protected function fetchSanitizedValue()
   {
      $results = array();
      for($a = 0; $a < $this->rows; $a++)
      {
         $results[$a] = array();
         for($b = 0; $b < $this->cols; $b++)
         {
            $thisFieldName = $this->fieldName."_".$a."_".$b;
            if(isset($this->responseSource[$thisFieldName]))
               $results[$a][$b] = trim(filter_var($this->responseSource[$thisFieldName], FILTER_SANITIZE_STRING));
            else
               $results[$a][$b] = "";
         }
      }

      // We always want to submit the array, even
      // if it's empty.
      $this->setValue($results);
   }

   // TODO: Rework this so that it returns a comma-separated list of courses
   public function getSubjectLine()
   {
      return "";
   }

   public function checkData()
   {
      $isValid = $this->validate();
      if($isValid !== true && !$this->AllowBlank)
      {
         $this->setInvalid();
         $this->setErrorMessage($isValid);
      }
      else
         $this->setValid();

      if($this->valid)
         $this->setPrintValues();
   }

   protected function validate()
   {
      $isValid = true;
      $rowsSubmitted = 0;

      $currentVal = $this->getSubmittedValue();

      $requiredColsCount = count($this->requiredCols);

      foreach($currentVal as $rowCount=>$row)
      {
         $filledCols = 0;
         $validationErrors = 0;

         for($c=0; $c < $this->cols; $c++)
         {
            if(is_array($this->requiredCols))
            {
               if ( in_array($c, $this->requiredCols) ) {
                if(empty($row[$c]))
                  $validationErrors++;
                else
                  $filledCols++;
               }
            }
         }

         if($filledCols == $requiredColsCount) // They filled out a row successfully.
            $rowsSubmitted++;
         else if($validationErrors <= $requiredColsCount) // They partially filled out a row.
            //continue;
            return 1;
         else if($validationErrors == $requiredColsCount) // They left a row blank.
            continue;
      }

      /*
       * If it's required, then they needed to have submitted
       * at least one row.
       */
      if($this->required && $rowsSubmitted == 0)
         return 0;

      return true;
   }

   protected function setErrorMessage($errorNo=0)
   {
      if(isset($this->errorMessages[$errorNo]))
         $this->errorMessage = $this->errorMessages[$errorNo];
      else
         $this->errorMessage = $this->errorMessages[0];
   }

   protected function setPrintValues()
   {
      $multiboxData = "";

      $vals = $this->value;

      if(!empty($vals))
      {
         foreach($vals as $row)
         {
            $emptyRow = true;
            for($c=0; $c < $this->cols; $c++)
            {
               if(!empty($row[$c]))
                  $emptyRow = false;
            }

            // If nothing is in the given row, then we skip it.
            if($emptyRow)
               continue;
            else
            {
               for($c = 0; $c < $this->cols; $c++)
               {
                  if(!empty($row[$c]))
                     $multiboxData .= $this->indent()."<span class='fauxHeader'>".$this->colLabels[$c].":</span> ".$row[$c]."<br />\n";
               }
               $multiboxData .= "<br />\n\n";
            }

         }
      }

      if(!empty($multiboxData))
      {
         $this->printValue  = "<div class='formFieldOutput'><strong>".$this->label."</strong> <div>\n";
         $this->printValue .= $multiboxData;
         $this->printValue .= "</div></div>\n";
      }
   }

   public function getValue()
   {
      $values = array();
      $vals = $this->value;
      if(!empty($vals))
      {
         $requiredColsCount = count($this->requiredCols);

         foreach($vals as $row)
         {
            $filledCols = 0;
            $validationErrors = 0;

            for($c=0; $c < $this->cols; $c++)
            {
               if(in_array($c, $this->requiredCols))
               {
                  if(empty($row[$c]))
                     $validationErrors++;
                  else
                     $filledCols++;
               }
            }

            if($filledCols == $requiredColsCount) // They filled out a row successfully.
               $values[] = $row;

         }
      }

      return $values;
   }

   public function getFieldOptions()
   {
      $fieldData = parent::getFieldOptions();
      $fieldData["rows"] = "int How many rows of fields should be displayed.";
      $fieldData["cols"] = "int How many columns should be displayed (i.e. how many fields per row).";
      $fieldData["colLabels"] = "array Labels to be used for each field (on print only).";
      $fieldData["placeholders"] = "array Placeholder text for each field.";

      unset($fieldData["setAsSubject"]);
      unset($fieldData["appendToSubject"]);
      unset($fieldData["prependToSubject"]);

      ksort($fieldData);
      return $fieldData;
   }
}

class FormField_CourseEquivalency extends FormField_Multibox
{
   public function __construct($fieldData, $responseType, $requiredCols = array())
   {
      if(!isset($fieldData["rows"]))
         $fieldData["rows"] = 5;

      if(!isset($fieldData["cols"]))
         $fieldData["cols"] = 4;

      if(!isset($fieldData["colLabels"]))
         $fieldData["colLabels"] = array("Course Prefix &amp; Name", "UA Course", "Course Date", "Course Link");

      parent::__construct($fieldData, $responseType);

      $this->requiredCols = $requiredCols;

   }

   public function supportedJavascriptEvents()
   {
      return array();
   }

   protected function setErrorMessage($errorNo=0)
   {
      $errorMessages = array();
      $errorMessages[] = "Please specify at least one course to be considered.";
      $errorMessages[] = "Please specify course number, course name, and what you believe to be the UA equivalent course for all courses you would like considered.";

      if(isset($errorMessages[$errorNo]))
         $this->errorMessage = $errorMessages[$errorNo];
      else
         $this->errorMessage = $errorMessages[0];
   }
}

class FormField_Number extends FormField
{
   protected function getField($fieldType = 'number')
   {
      return parent::getField("number");
   }

   protected function setErrorMessage($errorNo=0)
   {
      $errorMessages = array();
      $errorMessages[] = "Please enter a valid amount for '".htmlspecialchars($this->label)."'.";
      $errorMessages[] = "Please enter a valid amount for '".htmlspecialchars($this->label)."' (no decimals allowed).";
      $errorMessages[] = "Please enter a valid amount for '".htmlspecialchars($this->label)."' (only ".$this->maxDecimals." decimals allowed).";
      $errorMessages[] = "Please enter an amount for '".htmlspecialchars($this->label)."' less than or equal to ".$this->maxVal.".";
      $errorMessages[] = "Please enter an amount for '".htmlspecialchars($this->label)."' greater than or equal to ".$this->minVal.".";

      if(isset($errorMessages[$errorNo]))
         $this->errorMessage = $errorMessages[$errorNo];
      else
         $this->errorMessage = $errorMessages[0];
   }

   protected function setPrintValues()
   {
      $val = $this->getValue();
      if(!empty($val))
      {
         $this->printValue  = "<div class='formFieldOutput'><strong>".$this->label."</strong>: <div>";
         $this->printValue .= $val;
         $this->printValue .= "</div></div>\n";
      }
      else
         $this->printValue = "";
   }

   protected function fetchSanitizedValue()
   {
      parent::fetchSanitizedValue();
      $pieces = parseNumberValue($this->value);

      $this->value = $pieces["value"];

   }

   public function getValue()
   {
      $this->fetchSanitizedValue();
      return $this->value;
   }

   protected function validate()
   {
      if($this->maxVal && $this->maxVal < $this->value)
         return 3;
      if($this->minVal && $this->minVal > $this->value)
         return 4;

      if($this->maxDecimals === false)
      {
         $numberPattern = "/^(\d*(\.\d+)?)\$/";
         $errorVal = 0;
      }
      else if($this->maxDecimals === 0)
      {
         $numberPattern = "/^(\d*)\$/";
         $errorVal = 1;
      }
      else
      {
         $numberPattern = "/^(\d*(\.\d{".$this->maxDecimals."})?)\$/";
         $errorVal = 2;
      }
      if(!preg_match($numberPattern, $this->value))
          return $errorVal;
      else
         return true;
   }

}

class FormField_Currency extends FormField_Number
{
   public function __construct($fieldData, $responseType)
   {
      parent::__construct($fieldData, $responseType);

      if(!$this->currencySymbol)
         $this->currencySymbol = '$';

      if(!$this->inputPrefix)
         $this->inputPrefix = $this->currencySymbol;

      if($this->maxDecimals === 0)
         $this->inputSuffix = ".00";
      else
         $this->maxDecimals = 2;
   }
   protected function getField($fieldType = 'currency')
   {
      return parent::getField($fieldType);
   }

   protected function setPrintValues()
   {
      $val = $this->fetchSanitizedValue();
      if(!empty($val))
      {
         $this->printValue  = "<div class='formFieldOutput'><strong>".$this->label."</strong>: <div>";
         $this->printValue .= $this->inputPrefix.$val;
         if($this->inputSuffix)
            $this->printValue .= $this->inputSuffix;
         $this->printValue .= "</div></div>\n";
      }
      else
         $this->printValue = "";
   }
}

class FormField_Password extends FormField
{
   /* Overwrite the parent method so we never include a value attribute. */
   protected function getField($fieldType = 'password')
   {
      $fieldContent  = "";
      if(!$this->inlineForm)
      {
         $fieldContent .= $this->indent()."<div";
         if($this->divStyle)
            $fieldContent .= " style='".trim($this->divStyle)."'";

         $fieldContent .= " class='col-sm-9";
         if($this->hiddenLabel)
            $fieldContent .=" col-sm-offset-3";

         if($this->divClass)
            $fieldContent .= " ".trim($this->divClass);
         $fieldContent .= "'>\n";
         $this->i++;
      }

      if ($this->topLabel)
         $fieldContent .= '<div class="topLabel">' . $this->topLabel . '</div>';

      $fieldContent .= $this->indent()."<input type='password' class='form-control";
      if($this->fieldClass)
         $fieldContent .= " ".trim($this->fieldClass);
      $fieldContent .= "' name='".$this->fieldName."' id='".$this->fieldName."'";

      if ( $this->fieldStyle ) {
          $fieldContent .= " style='".trim($this->fieldStyle)."'";
      }

      if($this->required)
         $fieldContent .= " required='required'";

      $fieldContent .= " />\n";

      if ($this->bottomLabel)
         $fieldContent .= '<div class="topLabel">' . $this->bottomLabel . '</div>';

      if(!$this->inlineForm)
      {
         $this->i--;
         $fieldContent .= $this->indent()."</div>\n";
      }

      // Add any custom JavaScript.
      $fieldContent .= $this->getAdditionalJavaScript();

      return $fieldContent;
   }

   protected function setPrintValues()
   {
      $val = $this->value;
      $this->printValue = "<div class='formFieldOutput'><strong>".$this->label."</strong>: <div>".str_repeat("*", strlen($val))."</div></div>\n";
   }

   public function getValue()
   {
      return $this->getSubmittedValue();
   }

   public function getProtectedValue()
   {
      $val = $this->value;
      return str_repeat("*", strlen($val));
   }


   public function getSubjectLine() { return "********"; }
}

class FormField_Phone extends FormField
{
   public function __construct($fieldData, $responseType)
   {
      // Set default placeholder text for phone fields.
      $this->placeholder = "(XXX) XXX-XXXX";
      parent::__construct($fieldData, $responseType);
   }

   protected function getField($fieldType = 'tel') {
      return parent::getField("tel");
   }

   protected function setErrorMessage($errorNo=0)
   {
      $errorMessages = array();
      $errorMessages[] = "Please enter a valid phone number for '".htmlspecialchars($this->label)."'.";
      $errorMessages[] = "Phone numbers cannot contain any letters or special characters.";

      if(isset($errorMessages[$errorNo]))
         $this->errorMessage = $errorMessages[$errorNo];
      else
         $this->errorMessage = $errorMessages[0];

   }

   protected function setPrintValues()
   {
      $formatted = preg_replace("/[^0-9]/","", $this->value);
      if(!empty($formatted))
      {
         if(strlen($formatted) == 10)
            $phoneNum = "(".substr($formatted,0,3).") ".substr($formatted,3,3)."-".substr($formatted,6,4);
         else
            $phoneNum = substr($formatted,0,3)."-".substr($formatted,3,4);
         $this->printValue = "<div class='formFieldOutput'><strong>".$this->label."</strong>: <div>".$phoneNum."</div></div>\n";
      }
      else
         $this->printValue = "";
   }

   public function getValue()
   {
      $formatted = preg_replace("/[^0-9]/","", $this->value);
      if(!empty($formatted))
      {
         if(strlen($formatted) == 10)
            $phoneNum = "(".substr($formatted,0,3).") ".substr($formatted,3,3)."-".substr($formatted,6,4);
         else
            $phoneNum = substr($formatted,0,3)."-".substr($formatted,3,4);

         return $phoneNum;
      }
      else
         return "";
   }

   protected function validate()
   {
      /* Make sure that phone nubmers are either 7 or 10 digit numbers.
       * If they need to enter an extension or additional info, then we'd need to add
       * a second 'text' field for that data. Not an ideal setup
       * but it will work for 99% of our use cases.
       */
      if(preg_match("/[^0-9\(\)\-\ ]/",$this->value))
      {
         return 1;
      }
      else
      {
         $phoneNum = preg_replace("/[^0-9]/", "", $this->value);
         $entryLen = strlen($phoneNum);
         if($entryLen != 7 && $entryLen != 10)
         {
            return 0;
         }
      }

      return true;
   }
}

class FormField_Radio extends FormField
{
   public function __construct($fieldData, $responseType)
   {
      parent::__construct($fieldData, $responseType);
      $this->fetchSanitizedValue();
      $selectedOption = $this->checked;
      $userOption = $this->value;

      // If the form is being displayed for the first time
      // and the user
      if($selectedOption !== false && $userOption === false)
         $this->setValue ($selectedOption);
   }

   protected function getLabel()
   {
      if($this->hiddenLabel)
         return $this->indent()."<fieldset class='pickFrom'>\n";

      $fieldContent = $this->indent()."<fieldset class='pickFrom";

      if($this->required)
         $fieldContent .= " required";
      $fieldContent .= "'>\n";
      $this->i++;

      $fieldContent .= $this->indent()."<legend";

      if($this->inlineForm)
         //$legendClass = $this->indent()."<label class='".$this->labelClass;
         $legendClass = $this->labelClass." ";
      else
         $legendClass = "col-sm-3 control-label ";

      if($this->required)
         $legendClass .= "required ";

      $fieldContent .= " class='".trim($legendClass)."'";

      $fieldContent .= ">";
      $fieldContent .= "<span class='labelText";
      if($this->noColon)
         $fieldContent .= " noColon";
      $fieldContent .= "'>".htmlspecialchars($this->label)."</span>";
      if($this->moreLabel)
         $fieldContent .= "<span class='moreLabel'>".($this->moreLabel)."</span>";
      $fieldContent .= "</legend>\n";

      return $fieldContent;
   }

   protected function getField($fieldType = 'radio')
   {
      $currentValue = $this->value;
      $fieldContent  = "";
      if(!$this->inlineForm)
      {
         $fieldContent .= $this->indent()."<div class='";
         $fieldContent .= "col-sm-9";
         if($this->hiddenLabel)
            $fieldContent .=" col-sm-offset-3";

         $fieldContent .= "'>\n";
         $this->i++;
      }

      if($this->options)
      {
         $options = $this->options;
         foreach($options as $k=>$o)
         {
            if ( !$this->inlineForm ) {
                $fieldContent .= $this->indent()."<div class='radio'>\n";
                $this->i++;
            }
            $fieldContent .= $this->indent()."<label class='choice' for='".$this->fieldName."_option_".$k."'>";

            $fieldContent .= "<input type='radio' name='".$this->fieldName."' id='".$this->fieldName."_option_".$k."'";

            if($this->fieldClass)
               $fieldContent .= " class='".trim($this->fieldClass)."'";

            $thisValue = $k;

            $fieldContent .= " value='".$thisValue."'";
            if($currentValue !== false && (int) $currentValue === $thisValue)
               $fieldContent .= " checked='checked'";
            if($this->required)
               $fieldContent .= " required='required'";
            $fieldContent .= " />";
            $fieldContent .= $o;
            $fieldContent .= "</label>\n";

            if ( !$this->inlineForm ) {
                $this->i--;
                $fieldContent .= $this->indent()."</div>\n";
            }
            else {
                $fieldContent .= "&nbsp;";
            }
         }
      }
      else
         $fieldContent .= $this->indent()."<!-- Missing field options -->\n";

      if(!$this->inlineForm)
      {
         $this->i--;
         $fieldContent .= $this->indent()."</div>\n";
      }

      $fieldContent .= $this->indent()."<label for='".$this->fieldName."' class='error' style='display:none;'></label>\n";


      $this->i--;
      $fieldContent .= $this->indent()."</fieldset>\n";

      // Add any custom JavaScript.
      $fieldContent .= $this->getAdditionalJavaScript();

      return $fieldContent;
   }

   public function getFieldOptions()
   {
      $fieldOptions = parent::getFieldOptions();
      $fieldOptions["modifyOutput"] = "Either 'capitalize', 'uppercase', or 'lowercase'. Note that this only affects printed (and email) output";
      $fieldOptions["options"] = "array Array of values to be presented as checkboxes.";
      $fieldOptions["checked"] = "mixed Index of the array value that should initially be checked. Can be an integer or an associative index.";
      $fieldOptions["horizontal"] = "boolean Set the orientation of the choices to horizontal instead of vertical.";
      $fieldOptions["recipientChooser"] = "boolean Flags this field as a selector for the Dynamic Email Response type.";

      ksort($fieldOptions);
      return $fieldOptions;
   }

   protected function getAdditionalJavaScript($fieldName="")
   {
      if(empty($fieldName))
         $fieldName = $this->fieldName;
      else $fieldName = preg_replace("/-/","",$fieldName);

      $javaScript  = "";

      $possibleEvents = $this->supportedJavascriptEvents();
      foreach($possibleEvents as $e=>$d)
      {
         if($this->$e)
            $javaScript .= $this->createJavascriptEvent($e, $this->$e, $fieldName);
      }

      $javaScriptOutput  = "";
      if(!empty($javaScript))
      {
         $javaScriptOutput .= "\n<script type='text/javascript'>\n";

         // Make sure we set up our field variable.
         $javaScriptOutput .= $fieldName." = $(\"[name='".$fieldName."']\");\n";

         // Append the JavaScript
         $javaScriptOutput .= $javaScript;
         $javaScriptOutput .= "\n</script>\n";
      }

      return $javaScriptOutput;
   }

   public function supportedJavascriptEvents()
   {
      $events = parent::supportedJavascriptEvents();

      // There's no option for selecting text from a dropdown menu.
      unset($events["onselect"]);

      return $events;
   }

   protected function fetchSanitizedValue()
   {
      if(isset($this->responseSource[$this->fieldName]))
         $this->setValue(filter_var($this->responseSource[$this->fieldName], FILTER_SANITIZE_STRING));
   }

   protected function setErrorMessage($errorNo=0)
   {
      $errorMessages = array();
      $errorMessages[] = "Please select an option for '".htmlspecialchars($this->label)."'.";
      $errorMessages[] = "You submitted an invalid option for '".htmlspecialchars($this->label)."' Please select from the options displayed.";

      if(isset($errorMessages[$errorNo]))
         $this->errorMessage = $errorMessages[$errorNo];
      else
         $this->errorMessage = $errorMessages[0];
   }

   protected function setPrintValues()
   {
      $options = $this->options;
      $selectedOption = false;
      $val = $this->value;
      if(ctype_digit($val))
         $selectedOption = $options[($val)];
      else if(!empty($val))
         $selectedOption = $options[$val];

      if(!empty($selectedOption))
      {
         $this->printValue  = "<div class='formFieldOutput'><strong>".$this->label."</strong>: <div>";
         $selectedVal = filter_var($selectedOption, FILTER_SANITIZE_STRING);

         if($this->modifyOutput)
         {
            switch($this->modifyOutput)
            {
               case "capitalize":
                  $this->printValue .= ucwords($selectedVal);
                  break;
               case "uppercase":
                  $this->printValue .= strtoupper($selectedVal);
                  break;
               case "lowercase":
                  $this->printValue .= strtolower($selectedVal);
                  break;
               default:
                  $this->printValue .= $selectedVal;
                  break;
            }
         }
         else
            $this->printValue .= $selectedVal;

         $this->printValue .= "</div></div>\n";
      }
   }

   public function getValue()
   {
      $options = $this->options;
      $selectedOption = false;
      $val = $this->value;
      if(ctype_digit($val))
         $selectedOption = $options[($val)];
      else if(!empty($val))
         $selectedOption = $options[$val];

      if(!empty($selectedOption))
         return filter_var($selectedOption, FILTER_SANITIZE_STRING);
      else
         return "";
   }

   public function getValueKey()
   {
      return $this->value;
   }

   public function getSubjectLine()
   {
      $options = $this->options;
      $val = $this->value;
      if(!isset($options[$val]))
         return "";

      $selectedOption = $options[$val];
      return $selectedOption;
   }

   protected function validate()
   {
      // Make sure that if they did submit something,
      // that it exists in the options.
      $options = $this->options;
      $val = $this->value;
      if(ctype_digit($val))
         $selectedOption = $val;
      else
         $selectedOption = $val;
      if(!isset($options[$selectedOption]))
         return 1;
      else
         return true;
   }
}

class FormField_ColorChooser extends FormField_Radio
{
   protected function getField($fieldType = 'colorChooser')
   {
      $currentValue = $this->value;
      $fieldContent  = "";
      if(!$this->inlineForm)
      {
         $fieldContent  .= $this->indent()."<div class='";
         $fieldContent .= "col-sm-9";
         if($this->hiddenLabel)
            $fieldContent .=" col-sm-offset-3";
         $fieldContent .= "'>\n";
         $this->i++;
      }

      if($this->options)
      {
         $options = $this->options;
         foreach($options as $k=>$o)
         {
            $fieldContent .= $this->indent()."<div class='radio'>\n";
            $this->i++;

            $fieldContent .= $this->indent()."<label for='".$this->fieldName."_option_".$k."'>";

            $fieldContent .= "<input type='radio' name='".$this->fieldName."' id='".$this->fieldName."_option_".$k."'";
            $thisValue = $k+1;
            $fieldContent .= " value='".$thisValue."'";
            if(!empty($currentValue) && $currentValue == $thisValue)
               $fieldContent .= " checked='checked'";
            if($this->required)
               $fieldContent .= " required='required'";
            if(isset($o["disabled"]) && $o["disabled"])
               $fieldContent .= " disabled='disabled'";
            $fieldContent .= " />\n";

            $fieldContent .= $this->indent()."<span class='radioColorChoice' style='background-color:".$o["color"].";";
            if(isset($o["text"]))
               $fieldContent .= " color:".$o["text"].";";
            $fieldContent .= "'>";

            $fieldContent .= $o["label"];
            $fieldContent .= "</span>\n";
            $fieldContent .= $this->indent()."</label>\n";
            $this->i--;

            $fieldContent .= $this->indent()."</div>\n";
         }
      }
      else
         $fieldContent .= $this->indent()."<!-- Missing field options -->\n";

      if(!$this->inlineForm)
      {
         $this->i--;
         $fieldContent .= $this->indent()."</div>\n";
      }

      $this->i--;
      $fieldContent .= $this->indent()."</fieldset>\n";

      // Add any custom JavaScript.
      $fieldContent .= $this->getAdditionalJavaScript();

      return $fieldContent;
   }

   public function getFieldOptions()
   {
      $options = parent::getFieldOptions();
      unset($options["fieldClass"]);
      return $options;
   }

   protected function fetchSanitizedValue()
   {
      if(isset($this->responseSource[$this->fieldName]))
         $this->setValue(filter_var($this->responseSource[$this->fieldName], FILTER_VALIDATE_INT));
   }

   protected function setErrorMessage($errorNo=0)
   {
      $errorMessages = array();
      $errorMessages[] = "Please select an option for '".htmlspecialchars($this->label)."'.";
      $errorMessages[] = "You submitted an invalid option for '".htmlspecialchars($this->label)."' Please select from the options displayed.";

      if(isset($errorMessages[$errorNo]))
         $this->errorMessage = $errorMessages[$errorNo];
      else
         $this->errorMessage = $errorMessages[0];
   }

   protected function setPrintValues()
   {
      $options = $this->options;
      $val = $this->value - 1;
      if(isset($options[$val]))
         $selectedOption = $options[$val]["label"];
      else
         $selectedOption = "";
      $this->printValue  = "<div class='formFieldOutput'><strong>".$this->label."</strong>: <div>".filter_var($selectedOption, FILTER_SANITIZE_STRING)."</div></div>\n";
   }

   protected function validate()
   {
      // Make sure that if they did submit something,
      // that it exists in the options.
      $options = $this->options;
      $val = $this->value - 1;
      if(!isset($options[$val]))
         return 1;
      else
         return true;
   }

   public function getSubjectLine()
   {
      $val = parent::getSubjectLine();

      if(empty($val))
         return $val;
      else
         return $val["label"];
   }

   public function getValue()
   {
      $options = $this->options;
      $selectedOption = false;
      $val = $this->value-1;

      if(ctype_digit($val))
         $selectedOption = $options[($val)];
      else if(!empty($val))
         $selectedOption = $options[$val];

      if(!empty($selectedOption))
         return filter_var($selectedOption["label"], FILTER_SANITIZE_STRING);
      else
         return "";
   }
}

class FormField_Search extends FormField
{
   public function __construct($fieldData, $responseType)
   {
      // Set default placeholder text for phone fields.
      $this->placeholder = "Enter search terms";
      parent::__construct($fieldData, $responseType);
   }

   protected function getField($fieldType = 'search')
   {
      return parent::getField("search");
   }
}

class FormField_Select extends FormField_Radio
{
   protected function getLabel()
   {
      if($this->hiddenLabel)
         return "";

      $labelContent = $this->indent()."<label ";
      if($this->inlineForm)
         $labelContent .= "class='".$this->labelClass;
      else
         $labelContent .= "class='col-sm-3 control-label ".$this->labelClass;

      // Some fields are 'required'
      if($this->required)
         $labelContent .= " required";

      $labelContent .= "' for='".$this->fieldName."'>";
      $labelContent .= "<span class='labelText";
      if($this->noColon)
         $labelContent .= " noColon";
      $labelContent .= "'>".htmlentities($this->label)."</span>";

      // Occasionally, we might want to specify
      // additional info next to the label.
      if($this->moreLabel)
         $labelContent .= "<span class='moreLabel'>".($this->moreLabel)."</span>";

      $labelContent .= "</label>\n";

      return $labelContent;
   }

   protected function getField($fieldType = 'select')
   {
      $fieldContent  = "";
      if(!$this->inlineForm)
      {
         $fieldContent .= $this->indent()."<div";
         if($this->divStyle)
            $fieldContent .= " style='".trim($this->divStyle)."'";

         $fieldContent .= " class='col-sm-9";
         if($this->hiddenLabel)
            $fieldContent .=" col-sm-offset-3";

         if($this->divClass)
            $fieldContent .= " ".trim($this->divClass);
         $fieldContent .= "'>\n";
         $this->i++;
      }

      $currentValue = $this->value;

      // If the form was submitted, the field isn't required, and
      // the user didn't select anything, we want to make sure
      // and preserve that.
      if($currentValue === "")
         $currentValue = -1;

      if($this->options)
      {


         $options = $this->options;
         $fieldContent .= $this->indent()."<select name='".$this->fieldName;
         if($this->multiple)
            $fieldContent .= "[]";
         $fieldContent .= "' id='".$this->fieldName."' class='form-control";
         if($this->fieldClass)
            $fieldContent .= " ".trim($this->fieldClass);

         $fieldContent .= "'";

         if ( $this->fieldStyle )
            $fieldContent .= " style='".trim($this->fieldStyle)."'";

         if($this->required)
            $fieldContent .= " required='required'";
         if($this->multiple)
            $fieldContent .= " multiple='multiple'";
         if($this->size)
            $fieldContent .= " size='" . $this->size . "'";

         $fieldContent .= ">\n";
         $this->i++;

         if(!$this->noEmptyOption)
         {
            if($currentValue == -1)
               $fieldContent .= $this->indent()."<option selected='selected'> </option>";
            else
               $fieldContent .= $this->indent()."<option> </option>";
         }

         foreach($options as $k=>$o)
         {

            $fieldContent .= $this->indent()."<option";
            $thisValue = $k;

            $fieldContent .= " value='".$thisValue."'";
            if($currentValue !== false && $currentValue == $thisValue)
               $fieldContent .= " selected='selected'";
            $fieldContent .= ">";
            $fieldContent .= $o;
            $fieldContent .= "</option>\n";
         }
         $this->i--;

         $fieldContent .= $this->indent()."</select>\n";
      }
      else
         $fieldContent .= $this->indent()."<!-- Missing field options -->\n";

      if(!$this->inlineForm)
      {
         $this->i--;
         $fieldContent .= $this->indent()."</div>\n";
      }

      // Add any custom JavaScript.
      $fieldContent .= $this->getAdditionalJavaScript();

      return $fieldContent;
   }

   public function supportedJavascriptEvents()
   {
      $events = parent::supportedJavascriptEvents();

      // There's no option for selecting text from a dropdown menu.
      unset($events["onselect"]);

      return $events;
   }



   protected function fetchSanitizedValue()
   {
      if(isset($this->responseSource[$this->fieldName]))
      {
         if($this->multiple)
         {
            $responses = array();
            foreach($this->responseSource[$this->fieldName] as $r)
            {
               $options = $this->options;
               $responses[] = filter_var($r, FILTER_SANITIZE_STRING);
            }

            $this->setValue($responses);
         }
         else
         {
               $this->setValue(filter_var($this->responseSource[$this->fieldName], FILTER_SANITIZE_STRING));
         }
      }
   }

   protected function setErrorMessage($errorNo=0)
   {
      $errorMessages = array();
      $errorMessages[] = "Please select an option for '".htmlspecialchars($this->label)."'.";
      $errorMessages[] = "You submitted an invalid option for '".htmlspecialchars($this->label)."' Please select from the options displayed.";

      if(isset($errorMessages[$errorNo]))
         $this->errorMessage = $errorMessages[$errorNo];
      else
         $this->errorMessage = $errorMessages[0];
   }

   protected function setPrintValues()
   {
      $options = $this->options;
      $val = $this->value;
      $selectedVal = "";

      if($this->multiple && is_array($val))
      {
         $selectedValues = array();
         foreach($val as $v)
         {
            $selectedValues[] = filter_var($options[$v], FILTER_SANITIZE_STRING);
         }
         $selectedVal = $this->commaList($selectedValues);
      }
      else if($val != "")
      {
         $selectedVal = filter_var($options[$val], FILTER_SANITIZE_STRING);
      }

      if(!empty($selectedVal))
      {
         $this->printValue  = "<div class='formFieldOutput'><strong>".$this->label."</strong>: <div>";

         if($this->modifyOutput)
         {
            switch($this->modifyOutput)
            {
               case "capitalize":
                  $this->printValue .= ucwords($selectedVal);
                  break;
               case "uppercase":
                  $this->printValue .= strtoupper($selectedVal);
                  break;
               case "lowercase":
                  $this->printValue .= strtolower($selectedVal);
                  break;
               default:
                  $this->printValue .= $selectedVal;
                  break;
            }
         }
         else
            $this->printValue .= $selectedVal;

         $this->printValue .= "</div></div>\n";
      }
   }

   public function getValue()
   {
      $options = $this->options;
      $val = $this->value;

      if($this->multiple && is_array($val))
      {
         $selectedValues = array();
         foreach($val as $v)
         {
            if(isset($options[$v]))
               $selectedValues[$v] = $options[$v];
         }

         return $selectedValues;
      }
      else
      {
         if(isset($options[$val]))
            return filter_var($options[$val], FILTER_SANITIZE_STRING);
         else
            return "";
      }
   }

   public function getSubjectLine()
   {
      return $this->printValue;
   }

   protected function validate()
   {
      // Make sure that if they did submit something,
      // that it exists in the options.

      $options = $this->options;
      $val = $this->value;

      if($this->multiple)
      {
         if(!is_array($val))
            return 1;

         foreach($val as $v)
         {
            if(!isset($options[$v]))
               return 1;
         }
         return true;
      }
      else
      {
         if(!isset($options[$val]))
            return 1;
         else
            return true;
      }
   }
}

class FormField_Buildings extends FormField_Select
{
   public function __construct($fieldData, $responseType)
   {
      parent::__construct($fieldData, $responseType);

      $buildingsDB = new \Mumby\DB\Building();
      $bs = $buildingsDB->getAllBuildings();
      $this->options = array();
      foreach ( $bs as $b ) {
          if ( $b['BuildingName'] === "" ) continue;
          $this->options[$b['BuildingID']] = $b['BuildingName'];
      }

   }

}

class FormField_EventCategories extends FormField_Select
{
   public function __construct($fieldData, $responseType)
   {
        parent::__construct($fieldData, $responseType);
        $this->options = array();

        $eventDB = new \UAMath\DB\Events();
        $eventCategories = $eventDB->getEventCategories();
        foreach ( $eventCategories as $ec ) {            
            $this->options[$ec["EventCategoryID"]] = $ec['EventCategoryName'];
        }

   }
    
}

class FormField_State extends FormField_Select
{
   public function __construct($fieldData, $responseType)
   {
      parent::__construct($fieldData, $responseType);

      $this->options = array();

      $this->states = array(
         "AL"=>"Alabama",
         "AK"=>"Alaska",
         "AZ"=>"Arizona",
         "AR"=>"Arkansas",
         "CA"=>"California",
         "CO"=>"Colorado",
         "CT"=>"Connecticut",
         "DC"=>"District of Columbia",
         "DE"=>"Delaware",
         "FL"=>"Florida",
         "GA"=>"Georgia",
         "HI"=>"Hawaii",
         "ID"=>"Idaho",
         "IL"=>"Illinois",
         "IN"=>"Indiana",
         "IA"=>"Iowa",
         "KS"=>"Kansas",
         "KY"=>"Kentucky",
         "LA"=>"Louisiana",
         "ME"=>"Maine",
         "MD"=>"Maryland",
         "MA"=>"Massachusetts",
         "MI"=>"Michigan",
         "MN"=>"Minnesota",
         "MS"=>"Mississippi",
         "MO"=>"Missouri",
         "MT"=>"Montana",
         "NE"=>"Nebraska",
         "NV"=>"Nevada",
         "NH"=>"New Hampshire",
         "NJ"=>"New Jersey",
         "NM"=>"New Mexico",
         "NY"=>"New York",
         "NC"=>"North Carolina",
         "ND"=>"North Dakota",
         "OH"=>"Ohio",
         "OK"=>"Oklahoma",
         "OR"=>"Oregon",
         "PA"=>"Pennsylvania",
         "RI"=>"Rhode Island",
         "SC"=>"South Carolina",
         "SD"=>"South Dakota",
         "TN"=>"Tennessee",
         "TX"=>"Texas",
         "UT"=>"Utah",
         "VT"=>"Vermont",
         "VA"=>"Virginia",
         "WA"=>"Washington",
         "WV"=>"West Virginia",
         "WI"=>"Wisconsin",
         "WY"=>"Wyoming"
      );

      if(!$this->statesOnly)
         $this->states["Outside the US"] = "Outside the US";

        if( !empty($this->data["useAbbr"]) )
           $this->options = array_keys($this->states);
        else
          $this->options = array_values($this->states);

   }

   public function setValue($val, $setSource=false)
   {
        if (is_numeric($val) )  {
           $this->data["value"] = $val;
        }
        else {
            $index = 0;
            foreach ( $this->states as $k => $v ) {
                if ( strlen($val) == 2 ) {
                    if ( $k === strtoupper($val) ) {
                        $this->data["value"] = $index;
                        break;
                    }
                }
                else {
                    if ( strtoupper($v) === strtoupper($val) ) {
                        $this->data["value"] = $index;
                        break;
                    }
                }
                $index++;
            }
        }

        if($setSource)
           $this->responseSource[$this->fieldName] = $val;
   }

}

class FormField_Text extends FormField
{
   protected function getField($fieldType = 'text')
   {
      if($this->options)
         $this->noAutocomplete = false;

      $output = parent::getField();

      if($this->options)
      {
         $dataListID = $this->fieldName."_datalist";
         $datalist = "<datalist id='".$dataListID."'>\n";
         $options = $this->options;
         foreach($options as $k=>$o)
         {
            $datalist .= "<option value='".htmlspecialchars($o)."'>".htmlspecialchars($o)."</option>\n";
         }
         $datalist .= "</datalist>\n";
         $output = str_replace("<input", "<input list='".$dataListID."'", $output).$datalist;
      }
      return $output;
   }

   public function getFieldOptions()
   {
      $fieldData = parent::getFieldOptions();
      $fieldData["placeholder"] = "string Value to be used as a placeholder attribute.";
      $fieldData["options"] = "array List of options to be provided as autofill values (using 'datalist').";

      ksort($fieldData);
      return $fieldData;
   }
}

class FormField_TextArea extends FormField
{
   protected function getField($fieldType = 'textarea')
   {
      $currentValue = $this->value;
      $fieldContent  = "";
      if(!$this->inlineForm)
      {
         $fieldContent .= $this->indent()."<div";
         if($this->divStyle)
            $fieldContent .= " style='".trim($this->divStyle)."'";

         $fieldContent .= " class='col-sm-9";
         if($this->hiddenLabel)
            $fieldContent .=" col-sm-offset-3";

         if($this->divClass)
            $fieldContent .= " ".trim($this->divClass);
         $fieldContent .= "'>\n";
         $this->i++;
      }

      $fieldContent .= $this->indent()."<textarea class='form-control";
      if($this->fieldClass)
         $fieldContent .= " ".trim($this->fieldClass);

      $fieldContent .= "' name='".$this->fieldName."' id='".$this->fieldName."' ";
      $fieldContent .= "cols='".($this->cols ? $this->cols : 30)."' ";
      $fieldContent .= "rows='".($this->rows ? $this->rows : 10)."' ";
      if($this->required)
         $fieldContent .= " required='required'";
      $fieldContent .= ">";
      if(!empty($currentValue))
         $fieldContent .= htmlspecialchars($currentValue);
      $fieldContent .= "</textarea>\n";

      if(!$this->inlineForm)
      {
         $this->i--;
         $fieldContent .= $this->indent()."</div>\n";
      }

      // Add any custom JavaScript.
      $fieldContent .= $this->getAdditionalJavaScript();

      return $fieldContent;
   }

   public function getSubjectLine()
   {
      $val = $this->value();
      if(strlen($val) > 20)
         return substr($val,0,20)."...";
      else if(!empty($val))
         return $val;
      else
         return "";
   }

   public function getHiddenField()
   {
      $hiddenFieldContent  = "<!-- " . $this->label . " -->\n";
      $hiddenFieldContent .= "<textarea style='display:none' name='".htmlentities($this->fieldName)."'>".$this->getSubmittedValue()."</textarea>";
      return $hiddenFieldContent;
   }

   protected function setPrintValues()
   {
      $val = trim(filter_var($this->value, FILTER_SANITIZE_STRING));
      if(!empty($val))
      {
         $this->printValue  = "<div class='formFieldOutput'><strong>".$this->label."</strong>: <div>";
         $this->printValue .= str_replace("\n", "<br />\n", $val);
         $this->printValue .= "</div></div>\n";
      }
   }

   public function getFieldOptions()
   {
      $fieldData = parent::getFieldOptions();
      $fieldData["placeholder"] = "string Value to be used as a placeholder attribute.";
      unset($fieldData["setAsSubject"]);
      unset($fieldData["appendToSubject"]);
      unset($fieldData["prependToSubject"]);

      ksort($fieldData);
      return $fieldData;
   }
}

class FormField_Time extends FormField_DateTime
{
   public function __construct($fieldData, $responseType)
   {
      $this->dateFormat = "";

      if(!($this->timeFormat))
         $this->timeFormat = "g:ia";

      if($this->startTime)
         $this->startDate = $this->startTime;

        parent::__construct($fieldData, $responseType);

      $this->datepicker = false;
   }

   protected function setErrorMessage($errorNo = 0)
   {
      $errorMessages = array();
      $errorMessages[] = "You must enter a time for '".$this->label."'.";
      $errorMessages[] = "Please enter a time for '".$this->label."' in the specified format (".$this->dateFormat.").";

      if(isset($errorMessages[$errorNo]))
         $this->errorMessage = $errorMessages[$errorNo];
      else
         $this->errorMessage = $errorMessages[0];
   }

   public function getFieldOptions()
   {
      $fieldData = parent::getFieldOptions();
      unset($fieldData["dateFormat"]);
      unset($fieldData["dateValue"]);
      unset($fieldData["maxDate"]);
      unset($fieldData["minDate"]);
      unset($fieldData["startDate"]);

      return $fieldData;
   }
}

class FormField_URL extends FormField
{
   public function __construct($fieldData, $responseType)
   {
      // Set default placeholder text for phone fields.
      $this->placeholder = "http(s)://www.example.com";
      parent::__construct($fieldData, $responseType);
   }

   protected function getField($fieldType = 'url')
   {
      return parent::getField("url");
   }

   protected function fetchSanitizedValue()
   {
      if(isset($this->responseSource[$this->fieldName]))
         $this->setValue(trim(filter_var($this->responseSource[$this->fieldName], FILTER_SANITIZE_URL)));
   }

   protected function setErrorMessage()
   {
      $this->errorMessage = "Please enter a full URL for '".htmlspecialchars($this->label)."'.";
   }

   protected function setPrintValues()
   {
      $url = trim(filter_var($this->value, FILTER_SANITIZE_URL));
      if(!empty($url))
      {
         $this->printValue  = "<div class='formFieldOutput'><strong>".$this->label."</strong>: <div>";
         $this->printValue .= "<a href='".htmlentities($url)."'>";
         $this->printValue .= $url;
         $this->printValue .= "</a>";
         $this->printValue .= "</div></div>\n";
      }
      else
         $this->printValue = "";
   }

   public function getValue()
   {
      $this->fetchSanitizedValue();
      return $this->value;
   }

   protected function validate()
   {
      if(!filter_var($this->value, FILTER_VALIDATE_URL))
         return 0;
      else
         return true;
   }
}

class FormField_ZipCode extends FormField
{
   protected function getField($fieldType = 'zip')
   {
      $this->divClass = "zipCodeField";
      return parent::getField();
   }
}

class FormFieldCrypt
{
   public static function aes_enc($sValue, $sSecretKey)
   {
       return rtrim(
           base64_encode(
               mcrypt_encrypt(
                   MCRYPT_RIJNDAEL_256,
                   md5($sSecretKey), $sValue,
                   MCRYPT_MODE_ECB,
                   mcrypt_create_iv(
                       mcrypt_get_iv_size(
                           MCRYPT_RIJNDAEL_256,
                           MCRYPT_MODE_ECB
                       ),
                       MCRYPT_RAND)
                   )
               ), "\0"
           );
   }

   public static function aes_dec($sValue, $sSecretKey)
   {
       return rtrim(
           mcrypt_decrypt(
               MCRYPT_RIJNDAEL_256,
               md5($sSecretKey),
               base64_decode($sValue),
               MCRYPT_MODE_ECB,
               mcrypt_create_iv(
                   mcrypt_get_iv_size(
                       MCRYPT_RIJNDAEL_256,
                       MCRYPT_MODE_ECB
                   ),
                   MCRYPT_RAND
               )
           ), "\0"
       );
   }

   public static function generateRandomPassword($length=10)
   {
      $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
      $password = substr(str_shuffle( $chars ), 0, rand(10, count($chars)));
      $salt = substr(str_shuffle( $chars ), 0, rand(10, count($chars)));
      return substr(self::aes_enc($password, $salt), 0, $length);
   }
}

class MultiFormField extends Page
{
   protected $fieldData;


   public function __construct($fieldData)
   {
       $this->fieldData = $fieldData;
   }

   public function processMultiFieldData()
   {

       return true;

   }
}

class MultiFormField_Scheduler extends MultiFormField
{
    private $scheduleType;
    private $scheduleData;

    public function __construct($fieldData)
    {
        $this->scheduleType = $fieldData['day_schedule']['FieldValue'];
        $this->scheduleData = array(
            "ScheduleStartDate"        => "",
            "ScheduleEndDate"          => "",
            "ScheduleInterval"         => "0",
            "ScheduleCron"             => ""
        );
        parent::__construct($fieldData);
    }
    public function processMultiFieldData()
    {
        $schedule = new \Mumby\DB\Schedule();

        switch ($this->scheduleType) {
            // One Time
            case "0":
                $timestamp = strtotime($this->fieldData['Date_0']['FieldValue']);
                $hour = date("G",$timestamp);
                $min = date("i",$timestamp);
                $day = date("j",$timestamp);
                $month = date("m",$timestamp);
                $year = date("Y",$timestamp);
                $this->scheduleData['ScheduleStartDate'] = date($this::MYSQL_DATETIME_FORMAT, $timestamp);
                $this->scheduleData['ScheduleCron'] = $min." ".$hour." ".$day." ".$month." * ".$year;
                break;

            // Daily
            case "1":
                $timestampFrom = strtotime($this->fieldData["From"]['FieldValue']);
                $timestampUntil = strtotime($this->fieldData["Until"]['FieldValue']);
                $time = date("H:i", $timestampFrom);
                $this->scheduleData['ScheduleStartDate'] = date($this::MYSQL_DATETIME_FORMAT, $timestampFrom);
                $this->scheduleData['ScheduleEndDate'] = date($this::MYSQL_DATETIME_FORMAT, $timestampUntil);
                $this->scheduleData["ScheduleInterval"] = (int)$this->fieldData['Repeat_1']["FieldValue"];
                $this->scheduleData['ScheduleCron'] = $schedule->getCronDaily($time);
                break;

            // Every Weekday
            case "2":
                $timestampFrom = strtotime($this->fieldData["From"]['FieldValue']);
                $timestampUntil = strtotime($this->fieldData["Until"]['FieldValue']);
                $time = date("H:i", $timestampFrom);
                $this->scheduleData['ScheduleStartDate'] = date($this::MYSQL_DATETIME_FORMAT, $timestampFrom);
                $this->scheduleData['ScheduleEndDate'] = date($this::MYSQL_DATETIME_FORMAT, $timestampUntil);
                $this->scheduleData['ScheduleCron'] = $schedule->getCronWeekday($time);
                break;

            // Every M/W/F
            case "3":
                $timestampFrom = strtotime($this->fieldData["From"]['FieldValue']);
                $timestampUntil = strtotime($this->fieldData["Until"]['FieldValue']);
                $time = date("H:i", $timestampFrom);
                $this->scheduleData['ScheduleStartDate'] = date($this::MYSQL_DATETIME_FORMAT, $timestampFrom);
                $this->scheduleData['ScheduleEndDate'] = date($this::MYSQL_DATETIME_FORMAT, $timestampUntil);
                $this->scheduleData['ScheduleCron'] = $schedule->getCronMWF($time);
                break;

            // Every T/TH
            case "4":
                $timestampFrom = strtotime($this->fieldData["From"]['FieldValue']);
                $timestampUntil = strtotime($this->fieldData["Until"]['FieldValue']);
                $time = date("H:i", $timestampFrom);
                $this->scheduleData['ScheduleStartDate'] = date($this::MYSQL_DATETIME_FORMAT, $timestampFrom);
                $this->scheduleData['ScheduleEndDate'] = date($this::MYSQL_DATETIME_FORMAT, $timestampUntil);
                $this->scheduleData['ScheduleCron'] = $schedule->getCronTTH($time);
                break;

            // Weekly
            case "5":
                $timestampFrom = strtotime($this->fieldData["From"]['FieldValue']);
                $timestampUntil = strtotime($this->fieldData["Until"]['FieldValue']);
                $time = date("H:i", $timestampFrom);
                $days = $this->fieldData['Days_5']['FieldTextValue'];
                $numDays = count($days);
                $this->scheduleData['ScheduleStartDate'] = date($this::MYSQL_DATETIME_FORMAT, $timestampFrom);
                $this->scheduleData['ScheduleEndDate'] = date($this::MYSQL_DATETIME_FORMAT, $timestampUntil);
                $this->scheduleData['ScheduleInterval'] = ((int)$this->fieldData['Repeat_5']["FieldValue"] * $numDays ) - $numDays;
                $this->scheduleData['ScheduleCron'] = $schedule->getCronWeekly($days, $time);
                break;

            // Monthly
            case "6":
                $timestampFrom = strtotime($this->fieldData["From"]['FieldValue']);
                $timestampUntil = strtotime($this->fieldData["Until"]['FieldValue']);
                $type = $this->fieldData['Type_6']['FieldValue'];
                $time = date("H:i", $timestampFrom);
                $date = date("j", $timestampFrom);
                if ( $type === "0") {
                    $this->scheduleData['ScheduleCron'] = $schedule->getCronMonthly($date, $type, $time);
                }
                else {
                    $this->scheduleData['ScheduleCron'] = $schedule->getCronMonthly($date, $timestampFrom, $time);
                }

                $this->scheduleData['ScheduleStartDate'] = date($this::MYSQL_DATETIME_FORMAT, $timestampFrom);
                $this->scheduleData['ScheduleEndDate'] = date($this::MYSQL_DATETIME_FORMAT, $timestampUntil);
                $this->scheduleData["ScheduleInterval"] = (int)$this->fieldData['Repeat_6']["FieldValue"];
                break;

            default:
                return false;
        }

    // Use the following to extract the date/time of the next recurrance.
    //$cron = \Cron\CronExpression::factory($this->scheduleData['ScheduleCron']);
    //$cron->getNextRunDate(date($this::MYSQL_DATETIME_FORMAT), $this->scheduleData['ScheduleInterval'], true)->format($this::MYSQL_DATETIME_FORMAT);
        $this->scheduleData['ScheduleID'] = null;
        return $schedule->insertOrUpdate($this->scheduleData);

    }
}