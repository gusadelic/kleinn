<?php
namespace Mumby\WebTools;
use \Exception;

if(!defined("_MB_DEBUG"))
   define("_MB_DEBUG", false);

class Content extends Page
{
   public function __construct()
   {
      $this->sourceTable      = "PageRevisions";
      $this->idCol            = "PageRevisionID";
      
      $this->readOnlyFields   = array("PageRevisionID");
      
      $this->fieldInfo = array(
         "PageID"  => array("type"=>self::INTEGER),
         "RevisionContent" => array("type"=>self::STRING, "length"=>65535), // mediumtext
         "EditorNetID"  => array("type"=>self::STRING, "length"=>255),
         "RevisionNotes"   => array("type"=>self::STRING, "length"=>1024),
         "CreatedOn" => array("type"=>self::STRING),
         "IsDraft" => array("type"=>self::BOOLEAN),
      );

      parent::__construct();

      $pageTypes = $this->query("SELECT * FROM PageTypes");
      $this->PageTypes = array();
      foreach ( $pageTypes as $pt ) {
        $this->PageTypes[$pt['PageTypeID']] = $pt['PageTypeName'] . " - " . $pt['Description'];
      }
   }
   
   /**
    * Pull the content for a given page/revision.
    * 
    * @param mixed $contentID Either the URL for the page requested or the id
    * @param int $revisionID Specific revision to pull. -1 by default
    * @param bool $allowDrafts Either the URL for the page requested or the id
    * @param bool $allowDeleted Either the URL for the page requested or the id
    * @return mixed Associative array containing the content information for the given page or false if nothing is found.
    */
   public function getContent($contentID, $revisionID=-1, $allowDrafts=false, $allowDeleted=false)
   {
      $data = array();
      $contentSQL  = "SELECT p.PageTitle AS title, p.ParentMenuItemID AS parentLink, p.PageID as id, p.PageTypeID, p.HasRestrictions as hasRestrictions,
                      r.RevisionContent AS content, u.URL, (SELECT u2.URL FROM URLs u2 WHERE u2.PageID=p.PageID AND u2.IsCurrent=1) AS currentURL,
                      r.PageRevisionID AS revisionID, r.IsDraft as draft, r.CreatedOn as created, r.RevisionNotes as notes, p.IsSystemPage AS systemPage
                      FROM PageRevisions r
                      INNER JOIN Pages p ON r.PageID=p.PageID
                      LEFT JOIN URLs u ON p.PageID=u.PageID
                      WHERE p.IsPublished=1 AND ";

      if(!$allowDrafts && $revisionID==-1)
         $contentSQL .= "r.IsDraft=0 AND ";
      
      if(!$allowDeleted && $revisionID==-1)
         $contentSQL .= "p.IsDeleted=0 AND ";

      if(is_int($contentID) || ctype_digit($contentID))
         $contentSQL .= "p.PageID=:id AND (u.IsCurrent=1 OR u.IsCurrent IS NULL)";
      else
         $contentSQL .= "u.URL=:id ";
      
      if($revisionID == -1)
         $contentSQL .= "ORDER BY r.CreatedOn DESC, r.PageRevisionID DESC ";
      else
         $contentSQL .= "AND r.PageRevisionID=:rid ";
      $contentSQL .= "LIMIT 1";

      $data["id"] = $contentID;
      if($revisionID != -1)
         $data["rid"] = $revisionID;

      $content = $this->query($contentSQL, $data);
      if(!$content)
         return false;
      else
         return $content[0];
   }
   
   /**
    * Pull the content for a given page/revision.
    * 
    * @param string $title Title of page requested
    * @param int $revisionID Specific revision to pull. -1 by default
    * @param bool $allowDrafts Either the URL for the page requested or the id
    * @param bool $allowDeleted Either the URL for the page requested or the id
    * @return mixed Associative array containing the content information for the given page or false if nothing is found.
    */
   public function getContentByTitle($title, $revisionID=-1, $allowDrafts=false, $allowDeleted=false)
   {
        $data = array();
        $contentSQL  = "SELECT p.PageTitle AS title, p.ParentMenuItemID AS parentLink, p.PageID as id, p.PageTypeID, p.HasRestrictions as hasRestrictions,
                        r.RevisionContent AS content, u.URL, (SELECT u2.URL FROM URLs u2 WHERE u2.PageID=p.PageID AND u2.IsCurrent=1) AS currentURL,
                        r.PageRevisionID AS revisionID, r.IsDraft as draft, r.CreatedOn as created, r.RevisionNotes as notes, p.IsSystemPage AS systemPage
                        FROM PageRevisions r
                        INNER JOIN Pages p ON r.PageID=p.PageID
                        LEFT JOIN URLs u ON p.PageID=u.PageID
                        WHERE p.IsPublished=1 AND ";

        if(!$allowDrafts && $revisionID==-1)
           $contentSQL .= "r.IsDraft=0 AND ";

        if(!$allowDeleted && $revisionID==-1)
           $contentSQL .= "p.IsDeleted=0 AND ";

        $contentSQL .= "p.PageTitle = :title ";

        if($revisionID == -1)
           $contentSQL .= "ORDER BY r.CreatedOn DESC, r.PageRevisionID DESC ";
        else
           $contentSQL .= "AND r.PageRevisionID=:rid ";
        $contentSQL .= "LIMIT 1";

        $data["title"] = $title;
        
        if($revisionID != -1)
           $data["rid"] = $revisionID;

        $content = $this->query($contentSQL, $data);
        if(!$content)
           return false;
        else
           return $content[0];
   }
   
   /**
    * Search page content for a specific search string.
    * 
    * @param string $query Phrase being searched for.
    * @param bool $allowDrafts Whether drafts should be included in the search
    * @param bool $allowDeleted Whether deleted content should be included in the search
    * @return mixed Whether 
    */
   public function searchContent($query, $allowDrafts=false, $allowDeleted=false)
   {
      $contentSQL  = "SELECT p.PageTitle AS title, p.ParentMenuItemID AS parentLink, ".
               "p.PageID as id, p.HasRestrictions as hasRestrictions, ".
               "r.RevisionContent AS content, u.URL as url, ".
               "(SELECT u2.URL FROM URLs u2 WHERE u2.PageID=p.PageID AND u2.IsCurrent=1) AS currentURL, ".
               "r.PageRevisionID AS revisionID, r.IsDraft as draft, r.CreatedOn AS created, r.RevisionNotes as notes ".
               "FROM ".
               "(SELECT `PageID` as pageID, MAX(`PageRevisionID`) as `id`, RevisionContent as `content` FROM `PageRevisions` GROUP BY PageID) AS `q` ".
               "INNER JOIN `PageRevisions` `r` ON `q`.`PageRevisionID`=`r`.`PageRevisionID` ".
               "INNER JOIN Pages p ON r.PageID=p.PageID".
               "INNER JOIN URLs u ON p.PageID=u.PageID ".
               "WHERE p.IsPublished=1 AND ";

      if(!$allowDrafts)
         $contentSQL .= "r.IsDraft=0 AND ";
      
      if(!$allowDeleted)
         $contentSQL .= "p.IsDeleted=0 AND ";

      $contentSQL .= "r.RevisionContent LIKE :query ";

      $contentSQL .= "GROUP BY p.PageID ";      
      $contentSQL .= "ORDER BY p.PageTitle, r.CreatedOn DESC ";
      
      $data = array("query"=>"%".$query."%");
      return $this->query($contentSQL, $data);
   }
   
   /**
    * Select all page information from the database.
    * 
    * @param bool $includeDeletedPages
    * @return mixed Associative array of pages or false on failure.
    */
   public function getAllContent($pageType = 1, $includeDeletedPages = false)
   {
        $contentSQL  = "SELECT p.PageID as pageID, p.PageTitle AS title,  
                      r.EditorNetID as authorUsername,
                      p.PageTypeID,
                      p.IsPublished as published,
                      r.CreatedOn as lastUpdated, ";
        
        // Sections do not use URL's
        if ( (int)$pageType !== 2 ) {
            $contentSQL .= "u.URL as url, ";
        }
            
        $contentSQL .= "p.IsDeleted as deleted 
                      FROM Pages p
                      INNER JOIN PageRevisions r ON r.PageID=p.PageID
                      INNER JOIN (SELECT MAX(CreatedOn) as created from PageRevisions GROUP BY PageID) tmp ON r.CreatedOn=tmp.created ";
        
        // Sections do not use URL's
        if ( (int)$pageType !== 2 ) {
            $contentSQL .= "INNER JOIN URLs u ON u.PageID=p.PageID ";
        }
            
        $contentSQL .= "WHERE p.PageTypeID = :PageTypeID AND ";
                      
      if(!$includeDeletedPages)
         $contentSQL .= "p.IsDeleted=0 AND ";
      
      // Sections do not use URL's
      if ( (int)$pageType !== 2 )
          $contentSQL .= "u.IsCurrent=1 AND ";
          
      $contentSQL .= "p.IsSystemPage=0 ";
      

      $contentSQL .= "GROUP BY p.PageID ";
      $contentSQL .= "ORDER BY r.CreatedOn DESC, p.PageTitle";

      $params = array("PageTypeID" => $pageType);
      
      return $this->query($contentSQL, $params);
   }

   /**
    * Select all Tipsheet Sections from the database.
    * 
    * @param bool $includeDeletedPages
    * @return mixed Associative array of pages or false on failure.
    */
   public function getAllTipsheets($includeDeletedPages=false)
   {
      $contentSQL  = "SELECT p.PageID as pageID, p.PageTitle AS title,  
                      r.EditorNetID as authorUsername,
                      p.PageTypeID,
                      p.IsPublished as published,
                      r.CreatedOn as lastUpdated,
                      p.IsDeleted as deleted,
                      u.URL as url
                      FROM Pages p
                      INNER JOIN PageRevisions r ON r.PageID=p.PageID
                      INNER JOIN (SELECT MAX(CreatedOn) as created from PageRevisions GROUP BY PageID) tmp ON r.CreatedOn=tmp.created
                      INNER JOIN URLs u ON u.PageID=p.PageID
                      WHERE ";

      if(!$includeDeletedPages)
         $contentSQL .= "p.IsDeleted=0 AND ";
      
      $contentSQL .= "p.PageTypeID=2 AND ";
      $contentSQL .= "p.IsSystemPage=0 AND ";
      $contentSQL .= "u.IsCurrent=1 ";

      $contentSQL .= "GROUP BY p.PageID ";
      $contentSQL .= "ORDER BY r.CreatedOn DESC, p.PageTitle";

      return $this->query($contentSQL);
   }
   
   /**
    * Get all revisions for a given page.
    * 
    * @param int $pageID
    * @return mixed Associative array of revisions or false on failure.
    */
   public function getAllRevisions($pageID)
   {
      $contentSQL  = "SELECT p.PageID as pageID, p.PageTitle AS title,  
                      r.IsDraft as draft,
                      r.PageRevisionID as revID,
                      r.EditorNetID as authorUsername,
                      p.IsPublished,
                      r.CreatedOn as lastUpdated,
                      p.IsDeleted as deleted,
                      r.RevisionNotes as notes,
                      u.URL
                      FROM Pages p
                      INNER JOIN PageRevisions r ON r.PageID=p.PageID
                      LEFT JOIN URLs u ON u.PageID=p.PageID 
                      WHERE ";

      $contentSQL .= "p.IsDeleted=0 AND ";
      $contentSQL .= "p.IsSystemPage=0 AND ";
      $contentSQL .= "(u.IsCurrent=1 OR u.IsCurrent IS NULL) AND ";
      $contentSQL .= "p.PageID=:pageID ";
      $contentSQL .= "ORDER BY r.CreatedOn DESC, p.PageTitle";

      $data = array("pageID"=>$pageID);
      return $this->query($contentSQL, $data);
   }
   
   /**
    * Get the most current redirect for a given URL.
    * 
    * @param string  $url
    * @return boolean|string
    */
   public function getRedirect($url)
   {
      $parts = parse_url($url);
      $url = $parts["path"];
      
      $redirectURL = false;
      $data = array("OriginalURL"=>$url);
      $order = array(
         array(
            "column"    => "CreatedOn",
            "direction" => "DESC",
         )
      );

      $results = $this->find($data, "URLRedirects", 1, $order);
      
      // If we found something...
      if($results)
      {
         $redirectURL = $results[0]["RedirectURL"];
         if(isset($parts["query"]) && !empty($parts["query"]))
            $redirectURL .= "?".$parts["query"];
      }
      
      return $redirectURL;
   }

   /**
    * Find all URLs associated with a given page.
    * 
    * @param type $pageID
    * @return array Either an empty array (if nothing is found) or all the URLs associated with a given page.
    */
   public function getURLs($pageID)
   {
      $urls = $this->find(array("PageID"=>$pageID), "URLs");
      if(!$urls)
         return array();
      
      return $urls;
   }
   
   /**
    * Get all restrictions for a given page. Restrictions will be returned in
    * an associative array with keys for "password", "roles", and "users".
    * The "password" key will have information about what password should be
    * used for the page. The "roles" key will have a role value (integer value
    * stored as a constant) that the user must have to see the page content.
    * The "users" key will have a list of NetIDs. User's NetID must match one of
    * these values in order to view the page.
    * 
    * @param int $contentID PageID for the page in question.
    * @return mixed Associative array of restrictions or false if no restrictions exist.
    */
   public function getRestrictions($contentID)
   {
      $restrictions = $this->find(array("PageID"=>$contentID), "PageRestrictions");
      
      if(!$restrictions)
         return false;
      
      $allRestrictions = array("roles"=>array(), "users"=>array(), "password"=>"");
      foreach($restrictions as $r)
      {
         $restrictionType = constant($r["PageRestrictionTypeConstant"]);
         switch($restrictionType)
         {
            case _MB_RESTRICTED_PASS_:
               $allRestrictions["password"] = $r["PageRestrictionMetadata"];
               break;
            case _MB_RESTRICTED_ROLE_:
               $allRestrictions["roles"][] = constant($r["PageRestrictionMetadata"]);
               break;
            case _MB_RESTRICTED_USER_:
               $allRestrictions["users"] = explode(",", $r["PageRestrictionMetadata"]);
               break;
            default:
               break;
         }
      }
      
      return $allRestrictions;
   }
   
   public function getErrors() { return $this->errorMessages; }
   public function getFormattedErrors() { return implode("<br />\n", $this->errorMessages); }
   private function resetErrors() { $this->errorMessages = array(); }
   
   /**
    * Add a new page to the site.
    * 
    * The function expects an array with the following keys to be passed:
    *    - pageTitle
    *    - username
    *    - parentLink
    *    - url
    *    - content
    *    - PageTypeID
    *    - isDraft (optional, default is false)
    *    - notes (optional)
    *    - redirects (optional)
    * 
    * @param array $data Associative array of the new page information.
    * @returns mixed Returns either the new page ID or false on failure.
    */
   public function addContent($data)
   {
      // Make sure they specified all required fields.
      if(!isset($data["pageTitle"]) || empty($data["pageTitle"]))
      {
         throw new Exception("Missing Data: You did not specify a page title.");
         return false;
      }
      else if(!isset($data["username"]) || empty($data["username"]))
      {
         throw new Exception("Missing Data: We cannot create a new page without an associated username.");
         return false;
      }
      // Causes an exception when copying a page that has parentLink set to NULL.
      else if(!isset($data["parentLink"]) || empty($data["parentLink"]))
      {
        $data["parentLink"] = "/";
      //    throw new Exception("Missing Data: You must specify an associated navigation link.");
      //    return false;
      }
      else if(!isset($data["url"]) || empty($data["url"]))
      {
        $data["url"] = "";
        //throw new Exception("Missing Data: You did not specify a URL for the page.");
        //return false;
      }
      else if(!isset($data["content"]))
      {
         throw new Exception("Missing Data: You did not provide any content for the page.");
         return false;
      }
      
      
        // Make sure title is unique
        $existingTitle = $this->getContentByTitle($data["pageTitle"]);
        if($existingTitle)
        {
           throw new Exception("Error 000: The Title specified is already in use by another page.");
           return false;
        }
      
      if ( !empty($data["url"]) ) {
        if(strpos($data["url"], "/") !== 0)
           $data["url"] = "/".$data["url"];

        // Make sure the URL is available.
        $existingURL = $this->getContent($data["url"]);
        if($existingURL)
        {
           throw new Exception("Error 000: The URL specified is already in use by another page.");
           return false;
        }
      }
      
      if(isset($data["isDraft"]) && $data["isDraft"])
         $data["isDraft"] = 1;
      else
         $data["isDraft"] = 0;
      
      if(!isset($data["notes"]))
         $data["notes"] = "Initial page creation.";
      
      try
      {
         // Begin the transaction.
         $this->db->beginTransaction();
         
         // Add page to DB
         $pageData = array(
             "PageTitle"         => $data["pageTitle"],
             "IsPublished"       => 1,
             "HasRestrictions"   => 0,
             "IsDeleted"         => 0,
             "IsSystemPage"      => 0             
         );
         
         if(isset($data["parentLink"]))
            $pageData["ParentMenuItemID"] = $data["parentLink"];
         else
            $pageData["ParentMenuItemID"] = null;

         if(isset($data["PageTypeID"]))
            $pageData["PageTypeID"] = $data["PageTypeID"];
         else
            $pageData["PageTypeID"] = "1";

         $pageID = $this->insert($pageData, "Pages", true);
         if(!$pageID)
            throw new Exception("Error Code 001: Unable to add new page.");
         
         // Add revision to DB
         $revData = array(
            "PageID"  => $pageID,
            "RevisionContent" => $data["content"],
            "EditorNetID"  => $data["username"],
            "RevisionNotes"   => $data["notes"],
            "IsDraft" => ((int) $data["isDraft"])
         );
         if(!$this->insert($revData, "PageRevisions", true))
            throw new Exception("Error Code 002: Unable to add new revision.");

         // Add URL to DB
         if ( !empty($data["url"]) ) { 
            $urlData = array(
               "PageID"  => $pageID,
               "URL"     => $data["url"],
               "IsCurrent" => 1
            );
            if(!$this->insert($urlData, "URLs", true))
               throw new Exception("Error Code 003: Unable to add page URL.");
            
            // Add URL redirects (if present)
            if(isset($data["redirects"]) && !empty($data["redirects"]))
            {
               if(!is_array($data["redirects"]))
                  $pageRedirect = array($data["redirects"]);
               else
                  $pageRedirect = $data["redirects"];

               foreach($pageRedirect as $pr)
               {
                  $thisRedirectURL = trim($pr);
                  if(empty($thisRedirectURL))
                     continue;

                  $redirectData = array(
                     "PageID"  => $pageID,
                     "URL"     => $thisRedirectURL,
                     "IsCurrent" => 0
                  );
                  if(!$this->insert($redirectData, "URLs", true))
                     throw new Exception("Error Code 004: Unable to add URL redirects.");
               }
            }
         }

         
         $this->db->commit();
         return $pageID;
      }
      catch(Exception $e)
      {
         $this->db->rollBack();

         $msg  = "Unable to add page. \n";
         $msg .= $e->getMessage()."\n";
         $msg .= $e->getFile().", Line ";
         $msg .= $e->getLine()."\n";
         throw new Exception($msg);
         return false;
      }
   }
   
   public function reallyDeleteContent($pageID, $resetErrors=true)
   {
      try
      {
         // Begin the transaction.
         $this->db->beginTransaction();
         
         $pageData = array("pageID"=>$pageID);
         
         // Delete URLs
         $urlSQL  = "DELETE FROM URLs WHERE PageID=:pageID";
         if(!$this->query($urlSQL, $pageData))
            throw new Exception("Error Code 001: Unable to delete page URLs.");
         
         // Delete revisions
         $revSQL = "DELETE FROM PageRevisions WHERE PageID=:pageID";
         if(!$this->query($revSQL, $pageData))
            throw new Exception("Error Code 002: Unable to delete page revisions.");
         
         // Delete page
         $pageSQL = "DELETE FROM Pages WHERE id=:pageID LIMIT 1";
         if(!$this->query($pageSQL, $pageData))
            throw new Exception("Error Code 003: Unable to delete page from database.");
         
         $this->db->commit();
         
         return true;
      }
      catch(Exception $e)
      {
         $this->db->rollBack();

         $msg  = "Unable to really delete page. \n";
         $msg .= $e->getMessage()."\n";
         $msg .= $e->getFile().", Line ";
         $msg .= $e->getLine()."\n";
         throw new Exception($msg);
         return false;
      }
   }
   
   /**
    * Archive the specified page. We don't actually remove
    * any content with this function. We just mark it as
    * "deleted".
    * 
    * @param int $pageID The id of the page being archived.
    * @return bool True on success, false on failure.
    */
   public function deleteContent($pageID)
   {  
      $pageSQL = "UPDATE Pages SET IsDeleted=1 WHERE PageID=:pageID LIMIT 1";
      $pageData = array("pageID"=>$pageID);
      
      return $this->query($pageSQL, $pageData);
   }

   /**
    * Un-archive the specified page.
    * 
    * @param int $pageID The id of the page being archived.
    * @return bool True on success, false on failure.
    */
   public function undeleteContent($pageID)
   {  
      $pageSQL = "UPDATE Pages SET IsDeleted=0 WHERE PageID=:pageID LIMIT 1";
      $pageData = array("pageID"=>$pageID);
      
      return $this->query($pageSQL, $pageData);      
   }
   
   /**
    * Update the content for a given page (i.e., add a revision).
    * 
    * Expects a page ID and an array with the following keys:
    *    - username
    *    - content
    *    - notes (optional)
    *    - isDraft (optional boolean)
    *    - pageTitle (optional)
    *    - url  (optional)
    *    - parentLink (optional)
    *    - PageTypeID
    * 
    * @param int $id The id of the page being updated.
    * @param array $data An associative array of the data for the page update.
    * @return bool
    */
   public function updateContent($id, $data)
   {
      $thisPage = $this->getContent($id);
      if(!$thisPage) // Make sure the specified page exists.
         throw new Exception("Error Code 001: Unable to add a revision because the page does not exist in our database.");

      try
      {
         // Begin the transaction.
         $this->db->beginTransaction();
         
         // Either published a saved revision with the newly updated information
         // or add a new revision to the DB.
         $revSQL = "INSERT INTO `PageRevisions` (`PageID`, `RevisionContent`, `EditorNetID`, `RevisionNotes`, `IsDraft`) VALUES (:pageID, :content, :userID, :notes, :isDraft);";

         $isDraft = ((isset($data["isDraft"]) && $data["isDraft"]) ? 1 : 0);
         $revData = array(
            "pageID"  => $id,
            "userID"  => $data["username"],
            "content" => $data["content"],
            "notes"   => (isset($data["notes"]) ? $data["notes"] : ""),
            "isDraft" => $isDraft
         );

         $revID = $this->query($revSQL, $revData);
         if(!$revID)
            throw new Exception("Error Code 002: Unable to add page revision.");

         if(!$isDraft)
         {
            $additionalNotes = array();

            if(isset($data["pageTitle"]) && $data["pageTitle"] != $thisPage["title"])
            {
               $ptSQL = "UPDATE Pages SET `PageTitle`=:title WHERE PageID=:pageID";
               $ptData = array(
                  "title"  => $data["pageTitle"],
                  "pageID" => $id
               );
               
               if(!$this->query($ptSQL, $ptData))
                  throw new Exception("Error Code 003: Revision created. Unable to update page title.");

               $additionalNotes[] = "Changed page title from '".$thisPage["title"]."' to '".$data["pageTitle"]."'.";
            }

            if(isset($data["parentLink"]) && $data["parentLink"] != $thisPage["parentLink"])
            {
               $navSQL = "UPDATE Pages SET `ParentMenuItemID`=:parentLink WHERE PageID=:pageID";
               $navData = array(
                  "parentLink" => $data["parentLink"],
                  "pageID"     => $id
               );
               
               if(!$this->query($navSQL, $navData))
                  throw new Exception("Error Code 004: Revision created. Unable to update page navigation link.");
               
               $additionalNotes[] = "Changed page navigation link from '".$thisPage["parentLink"]."' to '".$data["parentLink"]."'.";
            }

            if(isset($data["url"]) && $data["url"] != $thisPage["currentURL"])
            {
               if(!$this->updatePageURL($id, $data["url"]))
                  throw new Exception("Error Code 005: Revision created. Unable to update page URL.");

               $additionalNotes[] = "Changed page URL from '".$thisPage["currentURL"]."' to '".$data["url"]."'.";
            }
            
            if(!empty($additionalNotes))
            {
               $newNotes = $data["notes"]."\n\n".implode("\n\n", $additionalNotes);
               $notesSQL = "UPDATE `PageRevisions` SET `RevisionNotes`=:notes WHERE `PageRevisionID`=:revID";
               $notesData = array(
                  "revID" => $revID,
                  "notes" => $newNotes
               );
               
               if(!$this->query($notesSQL, $notesData))
                  throw new Exception("Error Code 006: Revision created. Unable to update revision notes.");
            }

            if(isset($data["PageTypeID"]) && $data["PageTypeID"] != $thisPage["PageTypeID"])
            {
               $navSQL = "UPDATE Pages SET `PageTypeID`=:PageTypeID WHERE PageID=:pageID";
               $navData = array(
                  "PageTypeID" => $data["PageTypeID"],
                  "pageID"     => $id
               );
               
               if(!$this->query($navSQL, $navData))
                  throw new Exception("Error Code 006: Revision created. Unable to update Page Type.");
               
               $additionalNotes[] = "Changed Page Type from '".$thisPage["PageTypeID"]."' to '".$data["PageTypeID"]."'.";
            }
         }
         
         $this->db->commit();
         return $revID;
      }
      catch(Exception $e)
      {
         // Catch attempts to rollback with no active transaction
         // as recommended from http://php.net/manual/en/pdo.rollback.php
         try { $this->db->rollBack(); } catch (Exception $e2) {}

         $msg  = "Unable to add revision. \n";
         $msg .= $e->getMessage()."\n";
         $msg .= $e->getFile().", Line ";
         $msg .= $e->getLine()."\n";
         throw new Exception($msg);
         return false;
      }
   }
   
   public function revertToRevision($pageID, $revID, $userID)
   {
      $revision = $this->getContent($pageID, $revID);
      if(empty($revision))
         return false;
      
      $revData = array(
         "PageID"  => $pageID,
         "EditorNetID"  => $userID,
         "RevisionContent" => $revision["content"],
         "RevisionNotes"    => ("Reverting to revision from ".date("F j, Y (g:ia)", strtotime($revision["created"]))."."),
         "IsDraft" => 0
      );

      return $this->insert($revData, "PageRevisions");
   }
   
   /**
    * Makes a copy of an existing page (not including it's revision history).
    * Requires the user to provide a new URL and a userID to attach to the
    * creation.
    * 
    * @param int $pageID ID of the page being copied.
    * @param string $newURL URL for the new page.
    * @param string $userID NetID of the user making the copy.
    * @param string $title Optional new title for the page.
    * @return mixed Either the new page ID or false on failure.
    */
   public function copyContent($pageID, $newURL, $userID, $title="")
   {
      $existingPage = $this->getContent($pageID);
      if(empty($existingPage))
         return false;
      
      $data = array();
      if(empty($title))
         $data["pageTitle"]  = $existingPage["title"];
      else
         $data["pageTitle"]  = $title;
      $data["username"]   = $userID;
      $data["parentLink"] = $existingPage["parentLink"];
      $data["PageTypeID"] = $existingPage["PageTypeID"];
      $data["url"]        = $newURL;
      $data["content"]    = $existingPage["content"];
      $data["notes"]      = "Created a copy of <a href='".clean($existingPage["URL"])."'>".$existingPage["title"]."</a>.";
      
      return $this->addContent($data);
   }

   /**
    * Check to make sure the URL doesn't exist for another page.
    * If the URL exists for the current page, then simply adjust the currentURL value.
    * If it's a new URL, then add it and adjust as needed.
    * 
    * @param int $id ID of page being updated
    * @param string $url New URL for the page.
    * @returns 
    */
   public function updatePageURL($id, $url)
   {
      if(strpos($url, "/") !== 0)
         $url = "/".$url;

      $existingURL = $this->urlExists($url);
     
      // If the URL already exists in the DB...
      if($existingURL && $existingURL != $id)
         throw new Exception("Error 001: The URL specified is already in use by another page.");
      
      // If the URL already exists in the DB, then we simply proceed
      // as normal and update the database to set the 'current' values.
      try
      {
         // Begin the transaction.
         $this->db->beginTransaction();

         $urlData = array(
            "PageID"  => $id,
            "URL"     => $url,
            "IsCurrent" => 0
         );
         $urlID = $this->insertOrUpdate($urlData, "urls");
         if(!$urlID)
            throw new Exception("Error 002: Unable to add/update the URL in the database.");
         
         $activateSQL    = "UPDATE `URLs` SET `IsCurrent`=1 WHERE `URL`=:url";
         $activateData   = array("url"=>$url);
         
         $deactivateSQL  = "UPDATE `URLs` SET `IsCurrent`=0 WHERE `PageID`=:pageID AND `URL`<>:url";
         $deactivateData = array(
            "pageID" => $id,
            "url"    => $url
         );

         if(!$this->query($activateSQL, $activateData))
            throw new Exception("Error 003: Unable to activate the new URL in the database.");

         if(!$this->query($deactivateSQL, $deactivateData))
            throw new Exception("Error 004: Unable to de-activate old URLs in the database.");
         
         $this->db->commit();
         
         return true;
      }
      catch(Exception $e)
      {
         $this->db->rollBack();

         $msg  = "Unable to update page URL. \n";
         $msg .= $e->getMessage()."\n";
         $msg .= $e->getFile().", Line ";
         $msg .= $e->getLine()."\n";
         throw new Exception($msg);
         
         return false;
      }
   }
   
   /**
    * Search the database to see if a URL is already in use.
    * 
    * @param string $url URL to check for
    * @return mixed Either the page ID associated with the URL or false if nothing is found.
    */
   public function urlExists($url)
   {
      $data = array("URL"=>$url);
      $existingContent = $this->find($data, "URLs");
      if(!$existingContent)
         return false;
      else
         return $existingContent[0]["PageID"];
   }

   
   /**
    * Search the database to see if a Title is already in use.
    * 
    * @param string $title Title to check for
    * @return mixed Either the page ID associated with the Title or false if nothing is found.
    */
   public function titleExists($title)
   {
      $data = array("PageTitle"=>$title);
      $existingContent = $this->find($data, "Pages");
      if(!$existingContent)
         return false;
      else
         return $existingContent[0]["PageID"];
   }     
}