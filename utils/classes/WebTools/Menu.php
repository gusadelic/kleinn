<?php
namespace Mumby\WebTools;

//class Menu extends \Mumby\DB\DBObject
class Menu extends Page
{
   public $menuInfo;
   const RESPONSIVE_NAV = 1;
   const BOOTSTRAP      = 2;

   public function __construct($id=null)
   {
      // The following variables SHOULD be non-null in the child class.
      $this->sourceTable      = "Menus";
      $this->idCol            = "MenuID";

      // Don't let anyone change user IDs.
      $this->readOnlyFields   = array("MenuID");

      $this->fieldInfo = array(
         "MenuName"       => array("type"=>self::STRING, "length"=>255),
         "MenuDesc"       => array("type"=>self::STRING, "length"=>1024),
         "MenuMetadata"   => array("type"=>self::JSON, "length"=>4096),
         "IsSystemMenu"   => array("type"=>self::BOOLEAN)
      );

      parent::__construct();//$id, _MB_DB_HOST_, _MB_DB_NAME_, _MB_DB_USER_, _MB_DB_PASS_);
      if(!empty($id))
         $this->load($id);
      $this->menuInfo = array();

      // Used for indentation
      $this->i = 0;
   }

   /**
    * Returns a list of all menus in the system.
    *
    * @return type
    */
   public function getAllMenus()
   {
      return $this->find();
   }

   public function injectMenus($highlightNode=0)
   {
      $allMenus = $this->getAllMenus();
      foreach($allMenus as $m)
      {
         $this->injectFormattedMenu($m, $highlightNode);
      }
   }

   public function injectFormattedMenu($menu, $node=0)
   {
      $menuID = $menu["MenuID"];
      if(!empty($menu["MenuMetadata"]))
         $menuData = json_decode($menu["MenuMetadata"], true);
      else
         $menuData = array();

      $menuItems = $this->getNavigation($menuID);
      if(empty($menuItems))
         return false;

      $nodePath = array();
      if(!empty($node))
      {
         $allPathInfo = $this->getNavigationPath($node);
         if(!empty($allPathInfo))
         {
            foreach($allPathInfo as $p)
            {
               $nodePath[] = $p["MenuItemID"];
            }
         }
      }
      else
         $nodePath = array();


      // Options for menus
      $isInline         = (isset($menuData["isInline"]) && $menuData["isInline"]);
      $isSidebar        = (isset($menuData["isSidebar"]) && $menuData["isSidebar"]);
      $isMobileFriendly = (isset($menuData["isMobileFriendly"]) && $menuData["isMobileFriendly"]);
      $hideOnMobile     = (isset($menuData["hideOnMobile"]) && $menuData["hideOnMobile"]);
      $menuInjectionID  = (isset($menuData["menuInjectionID"])) ? $menuData["menuInjectionID"] : "menu".$menuID;
      $hideBrand        = (isset($menuData["hideBrand"]) && $menuData["hideBrand"]);
      $brandText        = (isset($menuData["brandText"])) ? $menuData["brandText"] : "Menu";
      $brandLink        = (isset($menuData["brandLink"])) ? $menuData["brandLink"] : "#";

      /**** Start HTML generation ****/

      $menuString  = "\n\n";

      if($hideOnMobile)
      {
         $menuString .= $this->indent()."<div class='hidden-xs'>\n";
         $this->i++;
      }

      if(!$isSidebar && !$isInline)
         $menuString .= $this->indent()."<div class='container-fluid'>\n";

      if($isMobileFriendly)
      {
            $this->i++;
            $menuString .= $this->indent()."<div class='navbar-header'>\n";
               $this->i++;
               $menuString .= $this->indent()."<button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#".$menuInjectionID."'>\n";
                  $this->i++;
                  $menuString .= $this->indent()."<span class='sr-only'>Toggle navigation</span>\n";
                  $menuString .= $this->indent()."<span class='icon-bar'></span>\n";
                  $menuString .= $this->indent()."<span class='icon-bar'></span>\n";
                  $menuString .= $this->indent()."<span class='icon-bar'></span>\n";
                  $this->i--;
               $menuString .= $this->indent()."</button>\n";
               if(!$hideBrand)
                  $menuString .= $this->indent()."<a class='navbar-brand' href='".$brandLink."'>".$brandText."</a>\n";

               $this->i--;
            $menuString .= $this->indent()."</div>\n";
      }

      if(!$isSidebar && !$isInline)
         $menuString .= $this->indent()."<div class='collapse navbar-collapse' id='".$menuInjectionID."'>\n";

      if(!$isSidebar && !$isInline)
         $menuString .= $this->indent()."<ul class='nav navbar-nav'>\n";
      else if($isSidebar)
         $menuString .= $this->indent()."<ul class='nav' id='side-menu'>\n";
      else if($isInline)
          $menuString .= $this->indent()."<ul class='list-unstyled list-inline'>\n";

      $this->i++;


      if(isset($menuData["hideRoot"]) && $menuData["hideRoot"])
         $menuItems = $menuItems[0]["children"];

      foreach($menuItems as $i)
      {
         $menuString .= $this->formatMenuItem($i, $isSidebar, $nodePath);
      }
      $this->i--;
      $menuString .= $this->indent()."</ul>\n";

      if(!$isSidebar && !$isInline)
      {
         $menuString .= $this->indent()."</div>\n";
         $menuString .= $this->indent()."</div>\n\n";
      }

      if($hideOnMobile)
      {
         $menuString .= $this->indent()."</div>\n";
         $this->i--;
      }

      /**** End HTML generation ****/

      $this->addInjectable($menuString, $menuInjectionID);
   }

   protected function formatMenuItem($item, $isSidebar=false, $highlightNodes=array())
   {
      $hasChildren = (!empty($item["children"]));
      $menuString  = $this->indent()."<li";

      $liClass = array();

      if(in_array($item["MenuItemID"], $highlightNodes))
         $liClass[] = "active";
      if($hasChildren && !$isSidebar)
         $liClass[] = "dropdown";

      if(!empty($liClass))
         $menuString .= " class='".implode(" ", $liClass)."'";

      if(!empty($item["MenuItemLinkAttr"]))
         $menuString .= " ";
      $menuString .= ">";
      $menuString .= "<a href='".$item["MenuItemLink"]."'";
      if($hasChildren)
         $menuString .= " role='button' aria-expanded='false'";
      $menuString .= " " . $item["MenuItemLinkAttr"] ."> ".$item["MenuItemText"];

      if($isSidebar && !empty($item["children"]))
         $menuString .= "<span class='fa arrow'></span>";

      $menuString .= "</a>";

      if(!empty($item["children"]))
      {
         $this->i++;
         if($isSidebar)
            $menuString .= "\n".$this->indent()."<ul class='nav nav-second-level'>\n";
         else
            $menuString .= "\n".$this->indent()."<ul class='dropdown-menu' role='menu'>\n";
         $this->i++;
         foreach($item["children"] as $subNav)
            $menuString .= $this->formatMenuItem($subNav, $isSidebar, $highlightNodes);
         $this->i--;
         $menuString .= $this->indent()."</ul>";

         $this->i--;
         $menuString .= "\n".$this->indent();
      }
      $menuString .= "</li>\n";

      return $menuString;
   }

   public function updateMenuInfo($menuID, $data=array())
   {
      $where = array("MenuID"=>$menuID);
      return $this->update($data, $where);
   }

   /**
    * Fetches the navigational tree.
    *
    * @param int $nodeID The root node at the top of the navigational tree
    * @param bool $ignoreSelf Indicates whether the specified node should be included in the returned array
    * @return mixed[] Array containing all of the individual links and their associated metadata
    */
   public function getNavigation($menuID=null, $nestedArray=true)
   {
      if(empty($menuID))
      {
         if(!$this->checkRowInstance())
            return false;

         $menuID = $this->id["MenuID"];
      }

      $navSQL  = "SELECT `link`.* FROM `MenuItems` AS `link`, `MenuItems` AS `parent`,
                  `MenuItems` AS `sub_parent`,
                  (
                     SELECT `link`.`MenuItemID`, (COUNT(`parent`.`MenuItemID`) - 1) AS `MenuItemDepth`
                     FROM `MenuItems` AS `link`,
                     `MenuItems` AS `parent`
                     WHERE `link`.`MenuItemLHS` BETWEEN `parent`.`MenuItemLHS` AND `parent`.`MenuItemRHS`
                     GROUP BY `link`.`MenuItemID`
                     ORDER BY `link`.`MenuItemLHS`
                  ) AS `sub_tree`, `Menus` AS `menuInfo`
                  WHERE `link`.`MenuItemLHS` BETWEEN `parent`.`MenuItemLHS` AND `parent`.`MenuItemRHS`
                  AND `link`.`MenuItemLHS` BETWEEN `sub_parent`.`MenuItemLHS` AND `sub_parent`.`MenuItemRHS`
                  AND `sub_parent`.`MenuItemID` = `sub_tree`.`MenuItemID`
                  AND `menuInfo`.`MenuID`=`link`.`MenuID`
                  AND `menuInfo`.`MenuID`=:MenuID ";

      $navSQL .= "GROUP BY `link`.`MenuItemID` ORDER BY `link`.`MenuItemLHS`";

      $data = array();
      $data["MenuID"] = ((int) $menuID);

      // Get each item in the menu, along with its metadata.
      $rows = $this->query($navSQL, $data);
      if(empty($rows))
         return array();

      $navNodes = $this->sortNodes($rows, "MenuItemLHS");
      if(empty($navNodes))
         return array();

      /*
       * Put each navigation item into a tree for structural purposes.
       */
      $navTree = new NavigationTree();
      foreach($navNodes as $n)
      {
         $n["parent"] = ((int) $n["MenuItemParentID"]);
         $navTree->insert($n);
      }

      $navArray = $navTree->getTreeAsArray();

      $startingPoint = 0;
      $startingDepth = 0;

      $depthLimit = PHP_INT_MAX;

      $thisSet = array();

      for($a=$startingPoint, $len=count($navArray) ; $a < $len  ; $a++)
      {
         $thisNode = $navArray[$a];
         $thisSet[$thisNode["MenuItemID"]] = $thisNode;
      }

      if(!$nestedArray)
         return $thisSet;

      $stack = array();
      $nested = array();

      foreach($thisSet as $arrValues)
      {
         $stackSize = count($stack); //how many opened tags?
         while(
                 $stackSize > 0 &&
                 $stack[$stackSize-1]['MenuItemRHS'] < $arrValues['MenuItemRHS']
              )
         {
            array_pop($stack); //close sibling and his childrens
            $stackSize--;
         }

         $link =& $nested;
         for($i=0;$i<$stackSize;$i++)
         {
            $link =& $link[$stack[$i]['index']]["children"]; //navigate to the proper children array
         }

         $next = array();
         foreach($arrValues as $k=>$a)
            $next[$k] = $a;
         $next["children"] = array();

         $tmp = array_push($link,  $next);
         array_push($stack, array('index' => $tmp-1, 'MenuItemRHS' => $arrValues['MenuItemRHS']));
      }

      return $nested;
   }

   public function getRootNode($menuID=2)
   {
      $nodes = $this->find(array("MenuItemLHS"=>1, "MenuID"=>$menuID), "MenuItems");
      if(empty($nodes))
         return false;
      else
         return $nodes[0];
   }

   public function getNavigationPath($nodeID)
   {
      $navSQL  = "SELECT `parent`.* FROM `MenuItems` AS `link`, `MenuItems` AS `parent` WHERE `link`.`MenuItemLHS` BETWEEN `parent`.`MenuItemLHS` AND `parent`.`MenuItemRHS` AND `link`.`MenuID`=`parent`.`MenuID` AND ";
      if(is_int($nodeID))
         $navSQL .= "`link`.`MenuItemID` = :MenuItemID ORDER BY `link`.`MenuItemLHS`";
      else
         $navSQL .= "`link`.`MenuItemText` = :MenuItemID ORDER BY `link`.`MenuItemLHS`";

      $data = array("MenuItemID"=>$nodeID);
      $dbRows = $this->query($navSQL, $data);
      if(!$dbRows)
         $dbRows = array();

      $rows = $this->sortNodes($dbRows, "MenuItemLHS");
      return $rows;
   }

   /**
    * This function allows us to sort the navigational nodes
    * as well as check permissions for restricted links.
    */
   private function sortNodes($nodes, $sortKey, $sortOrder=SORT_ASC)
   {
      $sortKeys = array();
      foreach($nodes as $k=>$n)
      {
         $nodePerms = constant($n["MenuItemRestricted"]);
         if($nodePerms != _MB_USER_ANONYMOUS_)
         {
            if(\Mumby\WebTools\ACL\User::isAuthenticated())
            {
               $thisUser = \Mumby\WebTools\ACL\User::getAuthenticatedUser();
               if(!$thisUser->hasPermission($nodePerms))
               {
                  unset($nodes[$k]);
                  continue;
               }
               else
                  $sortKeys[$k] = (isset($n[$sortKey]) ? $n[$sortKey] : 0);
            }
            else
            {
               unset($nodes[$k]);
               continue;
            }
         }
         else
            $sortKeys[$k] = (isset($n[$sortKey]) ? $n[$sortKey] : 0);
      }
      array_multisort($sortKeys, $sortOrder, $nodes);

      return $nodes;
   }

   public function updateNavigation($navData, $menuID, $nodeOrder)
   {
      $menuTree = new \Mumby\WebTools\NavigationTree();

      // Add the homepage first
      $menuTree->insert($navData[0]);
      foreach($nodeOrder as $n)
      {
         $menuTree->insert($navData[$n]);
      }

      $fullMenu = $menuTree->getTreeAsArray();

      try
      {
         // Begin the transaction.
         $this->db->beginTransaction();
         $responses = array();

         // Initialize the database by setting isUpdated to 0 for the given menu.
         $initQuery = "UPDATE MenuItems SET MenuItemIsUpdated=0 WHERE MenuID=:MenuID";
         $menuData  = array("MenuID" => $menuID);
         $responses[] = $this->query($initQuery, $menuData);

         // Insert each navigation item individually.
         for($a=0,$len=count($fullMenu); $a < $len; $a++)
         {
            $m = $fullMenu[$a];

            if(!isset($m["MenuItemParentID"]))
            {
               $fullMenu[$a]["MenuItemParentID"] = 0;
               $m["MenuItemParentID"] = 0;
            }

            $data = array();
               if(!empty($m["MenuItemID"]))
                  $data["MenuItemID"] = $m["MenuItemID"];

               // Insert params
               $data["MenuItemText"] = $m["MenuItemText"];
               $data["MenuItemLink"] = $m["MenuItemLink"];
               $data["MenuItemParentID"] = ((int) $m["MenuItemParentID"]);
               $data["MenuItemLHS"] = $m["MenuItemLHS"];
               $data["MenuItemRHS"] = $m["MenuItemRHS"];
               $data["MenuItemDepth"] = $m["MenuItemDepth"];
               $data["MenuID"] = $menuID;

               if(isset($m["MenuItemLinkAttr"]))
                  $data["MenuItemLinkAttr"] = $m["MenuItemLinkAttr"];
               else
                  $data["MenuItemLinkAttr"] = "";
               $data["MenuItemIsUpdated"] = 1;

            $navNodeID = $this->insertOrUpdate($data, "MenuItems");

            // Figure out what id was submitted
            if(empty($m["MenuItemID"]))
            {
               if(!$navNodeID)
                  throw Exception("Unable to insert or update menu item for '".$m["MenuItemText"]."'.");
               else
               {
                  $fullMenu = \Mumby\WebTools\NavigationTree::updateNodeID($fullMenu, $m["MenuItemText"], $navNodeID);
                  $responses[] = "Added menu item: '".$m["MenuItemText"]."'.";
               }
            }
            else
            {
               $responses[] = "Updated menu item: '".$m["MenuItemText"]."'.";
            }
         }

         // Delete navigation items that were removed.
         $unswitchSQL = "DELETE FROM MenuItems WHERE MenuItemIsUpdated=0 AND MenuID=:MenuID";
         $responses[] = $this->query($unswitchSQL, $menuData);

         $this->db->commit();
         return $responses;
      }
      catch(Exception $e)
      {
         $this->db->rollBack();
         return false;
      }
   }

   /**
    * Returns the requested navigation as a single array.
    *
    * @param type $menuID
    * @param type $nodeID
    * @param type $ignoreSelf
    * @param type $maxDepth
    * @return string
    */
   public function getNavigationAsArray($menuID=null)
   {
      return $this->getNavigation($menuID, false);
   }

   /**
    * Returns the requested navigation as a set of nested arrays.
    *
    * @param type $menuID
    * @param type $nodeID
    * @param type $ignoreSelf
    * @param type $maxDepth
    * @return string
    */
   public function getNavigationAsNestedArrays($menuID=null)
   {
      return $this->getNavigation($menuID);
   }
}

class NavigationTree
{
   // The root
   private $root;
   private $adjacencySet;
   private $asArray;

   public function __construct()
   {
      $this->root = NULL;
      $this->adjacencySet = false;
   }

   /**
    * The public wrapper.
    * This is needed because we must start the recursive functions
    * at the root, and we do not want to give public access to root.
    */
   public function insert($data)
   {
      $root =& $this->root;
      $this->insertNode($root, $data);
   }

   private function insertNode(&$root, $data)
   {

      // Create the root of the entire tree if no parent is passed in
      if ( !$root && (!isset($data["parent"]) || !$data["parent"]))
      {
         $root = new NavigationNode;
         $temp =& $root;
         $temp->data = $data;
         $temp->data["MenuItemDepth"] = 0;
         $temp->data["parent_id"] = null;

         // Create space for next insertion
         $temp->next[] = null;
      }
      else if(
                ((is_int($data["MenuItemParentID"]) || ctype_digit($data["MenuItemParentID"])) && $root->data["MenuItemID"] == $data["MenuItemParentID"]) ||
                ($root->data["MenuItemText"] == $data["MenuItemParentID"])
             )
      {
         // Add data as a child if we're at the parent, and we're done.
         // Find the empty node to insert at
         foreach($root->next as &$child)
         {
            // Insert at the first (and only) NULL
            if (!$child)
            {
               $child = new NavigationNode;
               $temp =& $child;
               $temp->data = $data;
               $temp->data["MenuItemDepth"] = $root->data["MenuItemDepth"]+1;

               // For storing in the DB, we replace the text of the parent
               // link with the parent id.
               $temp->data["parent_id"] = $root->data["MenuItemID"];

               // Create space for next insertion
               $temp->next[] = null;
            }
         }

         // Create space for the next insertion
         $root->next[] = null;
      }
      else
      {
         // Keep searching for the parent as defulat behavior.
         foreach($root->next as $child)
         {
            if ($child)
            {
               $this->insertNode($child, $data);
            }
         }
      }
   }

   public function getTreeAsArray()
   {
      if(empty($this->asArray))
      {
         $this->setAdjacency();
         $root =& $this->root;
         $this->createTree($root, 0);
      }

      return $this->asArray;
   }

   public function updateNode($nodeText, $nodeID)
   {
      if(empty($this->asArray))
         $this->getTreeAsArray();

      $this->asArray = NavigationTree::updateNodeID($this->asArray, $nodeText, $nodeID);
   }

   public static function updateNodeID($array, $nodeText, $nodeID)
   {
      foreach($array as $k=>$node)
      {
         if($node["MenuItemText"] == $nodeText && $node["MenuItemID"] == 0)
            $array[$k]["MenuItemID"] = $nodeID;

         if($node["MenuItemParentID"] == $nodeText && $node["parent_id"] == 0)
            $array[$k]["parent_id"] = $nodeID;
      }

      return $array;
   }

   private function createTree($root, $spaces)
   {
      if ($root)
      {
         $this->asArray[] = $root->data;

         foreach( $root->next as $child)
         {
            if ($child)
            {
               $this->createTree($child, $spaces);
            }
         }
      }
   }

   /**
    * Set the adjacency for the entire tree.
    */
   protected function setAdjacency()
   {
      if(!$this->adjacencySet)
      {
         $root =& $this->root;
         $this->setAdj($root, 0);
         $this->adjacencySet = true;
      }
   }

   /**
    * Recursive function that sets the adjacency (lhs, rhs) for
    * each node and then calls the function on
    * its children (if any exist).
    */
   private function setAdj($root, $spaces)
   {
      if ($root)
      {
         $left = ++$spaces;
         foreach( $root->next as $child)
         {
            if ($child)
            {
               $spaces = $this->setAdj($child, $spaces);
            }
         }
      }

      $right = ++$spaces;

      $root->data["MenuItemLHS"] = $left;
      $root->data["MenuItemRHS"] = $right;

      return $spaces;
   }
}

class NavigationNode
{
   public $data;
   public $next = array();
}