<?php
namespace Mumby\WebTools;
use \Exception;

class InjectionData extends \Mumby\DB\DBObject
{
   public function __construct()
   {
      $this->sourceTable      = "Injections";
      $this->idCol            = "InjectionID";
      
      // Don't let anyone change user IDs.
      $this->readOnlyFields   = array("InjectionID");
      
      $this->fieldInfo = array(
         "InjectionName"         => array("type"=>self::STRING, "length"=>255),
         "InjectionDesc"         => array("type"=>self::STRING, "length"=>1024),
         "InjectionVariableName" => array("type"=>self::STRING, "length"=>255),
         "InjectionValue"        => array("type"=>self::STRING, "length"=>1024),
         "InjectionCategoryID"   => array("type"=>self::INTEGER),
         "ApplicationID"         => array("type"=>self::INTEGER)
      );
      
      parent::__construct(null, _MB_CENTRAL_DATASTORE_HOST_, _MB_CENTRAL_DATASTORE_NAME_, _MB_CENTRAL_DATASTORE_USER_, _MB_CENTRAL_DATASTORE_PASS_);
   }
   
   /**
    * 
    * @param mixed $contentID Either the URL for the page requested or the id
    * @return type
    */
   public function getInjection($varName)
   {
      $rows = $this->find(array("InjectionVariableName"=>$varName), null, 1);
      if(empty($rows))
         return false;

      return $rows[0];
   }
   
   public function getAllInjections($appID = null)
   {
      if(!$appID)
         $appID = _MB_WEB_APPLICATION_ID_;
      
      $allSQL = "SELECT i.*, ic.InjectionCategoryName as category_name, ic.InjectionCategoryDesc as category_desc FROM Injections i INNER JOIN InjectionCategories ic ON i.InjectionCategoryID=ic.InjectionCategoryID ORDER BY ic.InjectionCategoryName, i.InjectionName";
      return $this->query($allSQL);
   }

   public function getAllInjectionCategories()
   {
      $allSQL = "SELECT * FROM InjectionCategories ORDER BY `InjectionCategoryName`";
      return $this->query($allSQL);
   }

   public function addInjection($name, $desc, $variable, $value, $category, $appID=null)
   {
      $data = array
      (
         "InjectionName"         => $name,
         "InjectionDesc"         => $desc,
         "InjectionVariableName" => $variable,
         "InjectionValue"        => $value,
         "InjectionCategoryID"   => $category,
         "ApplicationID"         => $appID
      );

      $injectionID = $this->insert($data);
      if(!$injectionID)
         throw new Exception("Error 001: Unable to add injection.");

      return $injectionID;
   }
   
   public function updateInjection($id, $name, $desc, $variable, $value, $category, $appID=null)
   {
      $data = array
      (
         "InjectionName"         => $name,
         "InjectionDesc"         => $desc,
         "InjectionVariableName" => $variable,
         "InjectionValue"        => $value,
         "InjectionCategoryID"   => $category,
         "ApplicationID"         => $appID
      );
      dump($data);
      $result = $this->update($data, array("InjectionID"=>$id));
      if($result === false)
         throw new Exception("Error 002: Unable to update injection.");
      else
         return true;
   }
   
   public function deleteInjection($id)
   {
      $results = $this->delete($id);
      if(!$results)
         throw new Exception("Error 003: Unable to delete injection.");

      return $results;
   }

   public function addInjectionCategory($name, $desc)
   {
      $data = array(
         "name"        => $name,
         "description" => $desc
      );
      $result = $this->insert($data, "injection_categories");
      
      if(!$result)
         throw new Exception("Error 004: Unable to add injection category.");
      
      return $result;
   }

   public function updateInjectionCategory($id, $name, $desc)
   {
      $data = array(
         "InjectionCategoryName" => $name,
         "InjectionCategoryDesc" => $desc
      );
      $result = $this->update($data, array("InjectionCategoryID"=>$id), "InjectionCategories", 1);
      
      if(!$result)
         throw new Exception("Error 005: Unable to update injection category.");
      
      return $result;
   }
   
   public function deleteInjectionCategory($id)
   {
      $result = $this->delete(array("InjectionCategoryID"=>$id), "InjectionCategories", 1);
      
      if(!$result)
         throw new Exception("Error 006: Unable to delete injection category.");
      
      return $result;
   }
}