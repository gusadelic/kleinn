<?php
namespace Mumby\WebTools;

if(!defined("_MB_DEBUG"))
   define("_MB_DEBUG", false);

class Config extends \Mumby\DB\DBObject
//class Config extends Page
{
   public function __construct($id = null, $dbHost = null, $dbName = null, $dbUser = null, $dbPass = null)
   {
      $this->sourceTable      = "Configs";
      $this->idCol            = "ConfigID";
      
      // Don't let anyone change user IDs.
      $this->readOnlyFields   = array("ConfigID");
      
      $this->fieldInfo = array(
         "ConfigLibraryID" => array("type"=>self::INTEGER),
         "ConfigName"      => array("type"=>self::STRING, "length"=>255),
         "ConfigValue"     => array("type"=>self::STRING, "length"=>255),
         "ConfigTypeName"  => array("type"=>self::STRING, "length"=>255)
      );
      
      parent::__construct($id, _MB_DB_HOST_, _MB_DB_NAME_, _MB_DB_USER_, _MB_DB_PASS_);
   }
   
   public function getAllConfigs($thirdParty = 0)
   {
      $data = array();
      
      $sql  = "SELECT `c`.`ConfigID` as `id`, `c`.`ConfigName` as `config`, `c`.`ConfigValue` AS `defaultValue`, `c`.`ConfigTypeName` AS `type`, `l`.`LibraryName` as `library`, `l`.`LibraryDesc` as `libraryDesc` FROM Configs c ";
      $sql .= "INNER JOIN ConfigLibraries l ON c.ConfigLibraryID=l.ConfigLibraryID ";
      if(!empty($thirdParty))
      {
         $sql .= "WHERE c.ConfigLibraryID=:ConfigLibraryID ";
         $data["ConfigLibraryID"] = ((int) $thirdParty);
      }
      $sql .= "ORDER BY ConfigTypeName ASC";

      $rows = $this->query($sql, $data);

      if(!$rows)
         return false;

      $configs = array();
      foreach($rows as $r)
      {
         $configs[$r["config"]] = $r;
      }
      
      return $configs;
   }
}

class WYSIWYG_Config extends Config
{
   var $configData;
   public function __construct($preloadConfigs=true)
   {
      parent::__construct(null, _MB_DB_HOST_, _MB_DB_NAME_, _MB_DB_USER_, _MB_DB_PASS_);

      $this->configData = array();
      $this->configData["allowAlignCenter"] = false;
      $this->configData["allowAlignJustify"] = false;
      $this->configData["allowAlignLeft"] = false;
      $this->configData["allowAlignRight"] = false;
      $this->configData["allowAnchors"] = false;
      $this->configData["allowBackgroundColor"] = false;
      $this->configData["allowBold"] = false;
      $this->configData["allowCharmap"] = false;
      $this->configData["allowCode"] = false;
      $this->configData["allowContextMenu"] = false;
      $this->configData["allowFiles"] = false;
      $this->configData["allowFullScreen"] = false;
      $this->configData["allowHRs"] = false;
      $this->configData["allowHeadings"] = false;
      $this->configData["allowIFrames"] = false;
      $this->configData["allowImages"] = false;
      $this->configData["allowIndenting"] = false;
      $this->configData["allowCourseInjections"] = false;
      $this->configData["allowDataInjections"] = false;
      $this->configData["allowFAQInjections"] = false;
      $this->configData["allowFormInjections"] = false;
      $this->configData["allowPeopleInjections"] = false;
      $this->configData["allowInsertDateTime"] = false;
      $this->configData["allowItalics"] = false;
      $this->configData["allowLinks"] = false;
      $this->configData["allowLinkTargets"] = false;
      $this->configData["allowNewFile"] = false;
      $this->configData["allowOrderedLists"] = false;
      $this->configData["allowPaste"] = false;
      $this->configData["allowPreview"] = false;
      $this->configData["allowPrint"] = false;
      $this->configData["allowRedo"] = false;
      $this->configData["allowRemoveFormat"] = false;
      $this->configData["allowSearchReplace"] = false;
      $this->configData["allowSpellCheck"] = false;
      $this->configData["allowStrikethrough"] = false;
      $this->configData["allowSubscript"] = false;
      $this->configData["allowSuperscript"] = false;
      $this->configData["allowTables"] = false;
      $this->configData["allowTextColor"] = false;
      $this->configData["allowUnderline"] = false;
      $this->configData["allowUndo"] = false;
      $this->configData["allowUnorderedLists"] = false;
      $this->configData["allowVisualBlocks"] = false;
      $this->configData["allowVisualChars"] = false;
      $this->configData["allowWordCount"] = false;
      
      // Preset options
      $this->configData["skin"] = "lightgray";
      $this->configData["extended_valid_elements"] = array("@[id|name]");
      $this->configData["cssFile"] = "/common/admin/css/wysiwyg.css";
      $this->configData["removeScriptHost"] = true;
      $this->configData["relativeURLs"] = false;
      $this->configData["baseURL"] = "/";
      
      $this->configData["filemanagerPluginName"] = "responsivefilemanager";
      $this->configData["filemanagerTitle"] = "File Manager";
      $this->configData["filemanagerFilePath"] = "/filemanager/";
      $this->configData["filemanagerPath"] = "/filemanager/plugin.min.js";
      
      $this->configData["linksList"] = false;
      $this->configData["linkClassList"] = array(
          array("title"=>"", "value"=>""),
          array("title"=>"New Window", "value"=>"newWindow")
      );
      
      $this->configData["menubar"]       = array();
      $this->configData["menubarFile"]   = false;
      $this->configData["menubarEdit"]   = false;
      $this->configData["menubarInsert"] = false;
      $this->configData["menubarView"]   = false;
      $this->configData["menubarFormat"] = false;
      $this->configData["menubarTable"]  = false;
      $this->configData["menubarTools"]  = false;

      if($preloadConfigs)
      {
         // TODO: Not good that the id is hard-coded here...
         $defaultConfigData = parent::getAllConfigs(2);
         
         foreach($defaultConfigData as $f)
         {
            $config = $f["config"];
            $value  = $f["defaultValue"];
            switch($config)
            {
               case "align":
                  $this->allowAlign($value);
                  break;
               case "aligncenter":
                  $this->allowAlignCenter($value);
                  break;
               case "alignjustify":
                  $this->allowAlignJustify($value);
                  break;
               case "alignleft":
                  $this->allowAlignLeft($value);
                  break;
               case "alignright":
                  $this->allowAlignRight($value);
                  break;
               case "anchors":
                  $this->allowAnchors($value);
                  break;
               case "backgroundColor":
                  $this->allowBackgroundColor($value);
                  break;
               case "bold":
                  $this->allowBold($value);
                  break;
               case "specialChars":
                  $this->allowCharmap($value);
                  break;
               case "colors":
                  $this->allowColors($value);
                  break;
               case "contextMenu":
                  $this->allowContextMenu($value);
                  break;
               case "datetime":
                  $this->allowInsertDateTime($value);
                  break;
               case "files":
                  $this->allowFiles($value);
                  break;
               case "formatting":
                  $this->allowFormat($value);
                  break;
               case "fullScreen":
                  $this->allowFullScreen($value);
                  break;
               case "headings":
                  $this->allowHeadings($value);
                  break;
               case "hr":
                  $this->allowHorizontalRule($value);
                  break;
               case "iframe":
                  $this->allowIFrames($value);
                  break;
               case "images":
                  $this->allowImages($value);
                  break;
               case "indenting":
                  $this->allowIndenting($value);
                  break;
               case "dataInjection":
                  $this->allowInjections($value);
                  break;
               case "faqInjection":
                  $this->allowInjections($value);
                  break;
               case "formInjection":
                  $this->allowInjections($value);
                  break;
               case "peopleInjection":
                  $this->allowInjections($value);
                  break;
               case "italics":
                  $this->allowItalics($value);
                  break;
               case "links":
                  $this->allowLinks($value);
                  break;
               case "linksAndAnchors":
                  $this->allowAnchors($value);
                  $this->allowLinks($value);
                  break;
               case "lists":
                  $this->allowLists($value);
                  break;
               case "newDocument":
                  $this->allowNewDocument($value);
                  break;
               case "orderedLists":
                  $this->allowOrderedLists($value);
                  break;
               case "paste":
                  $this->allowPaste($value);
                  break;
               case "preview":
                  $this->allowPreview($value);
                  break;
               case "print":
                  $this->allowPrint($value);
                  break;
               case "redo":
                  $this->allowRedo($value);
                  break;
               case "removeFormat":
                  $this->allowRemoveFormat($value);
                  break;
               case "searchAndReplace":
                  $this->allowSearchReplace($value);
                  break;
               case "spellcheck":
                  $this->allowSpellcheck($value);
                  break;
               case "specialChars":
                  $this->allowCharmap($value);
                  break;
               case "strikethrough":
                  $this->allowStrikethrough($value);
                  break;
               case "subscript":
                  $this->allowSubscript($value);
                  break;
               case "superscript":
                  $this->allowSuperscript($value);
                  break;
               case "tables":
                  $this->allowTables($value);
                  break;
               case "textColor":
                  $this->allowTextColor($value);
                  break;
               case "underline":
                  $this->allowUnderline($value);
                  break;
               case "undo":
                  $this->allowUndo($value);
                  break;
               case "unorderedLists":
                  $this->allowUnorderedLists($value);
                  break;
               case "viewSource":
                  $this->allowViewSource($value);
                  break;
               case "visualBlocks":
                  $this->allowVisualBlocks($value);
                  break;
               case "visualChars":
                  $this->allowVisualChars($value);
                  break;
               case "wordCount":
                  $this->allowWordCount($value);
                  break;

               case "baseURL":
                  $this->setDocumentBaseURL($value);
                  break;
               case "cssFile":
                  $this->setCSSFile($value);
                  break;
               case "menu":
                  $this->setMenu(explode(" ", $value));
                  break;
               case "skin":
                  $this->setMenu(explode(" ", $value));
                  break;
               default:
                  break;
            }
         }
      }
   }
   
   public function allowAlignCenter($bool)    { $this->configData["allowAlignCenter"] = (bool) $bool; }
   public function allowAlignJustify($bool)   { $this->configData["allowAlignJustify"] = (bool) $bool; }
   public function allowAlignLeft($bool)      { $this->configData["allowAlignLeft"] = (bool) $bool; }
   public function allowAlignRight($bool)     { $this->configData["allowAlignRight"] = (bool) $bool; }
   public function allowAnchors($bool)        { $this->configData["allowAnchors"] = (bool) $bool; }
   public function allowBold($bool)           { $this->configData["allowBold"] = (bool) $bool; }
   public function allowCharmap($bool)        { $this->configData["allowCharmap"] = (bool) $bool; }
   public function allowBackgroundColor($bool){ $this->configData["allowBackgroundColor"] = (bool) $bool; }
   public function allowContextMenu($bool)    { $this->configData["allowContextMenu"] = (bool) $bool; }
   public function allowFiles($bool)          { $this->configData["allowFiles"] = (bool) $bool; }
   public function allowFullScreen($bool)     { $this->configData["allowFullScreen"] = (bool) $bool; }
   public function allowHeadings($bool)       { $this->configData["allowHeadings"] = (bool) $bool; }
   public function allowHorizontalRule($bool) { $this->configData["allowHRs"] = (bool) $bool; }
   public function allowIFrames($bool)        { $this->configData["allowIFrames"] = (bool) $bool; }
   public function allowImages($bool)         { $this->configData["allowImages"] = (bool) $bool; }
   public function allowIndenting($bool)      { $this->configData["allowIndenting"] = (bool) $bool; }
   public function allowPeopleInjections($bool) { $this->configData["allowPeopleInjections"] = (bool) $bool; }
   public function allowFAQInjections($bool)    { $this->configData["allowFAQInjections"] = (bool) $bool; }
   public function allowFormInjections($bool)   { $this->configData["allowFormInjections"] = (bool) $bool; }
   public function allowCourseInjections($bool) { $this->configData["allowCourseInjections"] = (bool) $bool; }
   public function allowDataInjections($bool)   { $this->configData["allowDataInjections"] = (bool) $bool; }
   public function allowInsertDateTime($bool) { $this->configData["allowInsertDateTime"] = (bool) $bool; }
   public function allowItalics($bool)        { $this->configData["allowItalics"] = (bool) $bool; }
   public function allowLinks($bool)          { $this->configData["allowLinks"] = (bool) $bool; }
   public function allowOrderedLists($bool)   { $this->configData["allowOrderedLists"] = (bool) $bool; }
   public function allowNewDocument($bool)    { $this->configData["allowNewFile"] = (bool) $bool; }
   public function allowPaste($bool)          { $this->configData["allowPaste"] = (bool) $bool; }
   public function allowPreview($bool)        { $this->configData["allowPreview"] = (bool) $bool; }
   public function allowPrint($bool)          { $this->configData["allowPrint"] = (bool) $bool; }
   public function allowRedo($bool)           { $this->configData["allowRedo"] = (bool) $bool; }
   public function allowRemoveFormat($bool)   { $this->configData["allowRemoveFormat"] = (bool) $bool; }
   public function allowSearchReplace($bool)  { $this->configData["allowSearchReplace"] = (bool) $bool; }
   public function allowSpellcheck($bool)     { $this->configData["allowSpellCheck"] = (bool) $bool; }
   public function allowStrikethrough($bool)  { $this->configData["allowStrikethrough"] = (bool) $bool; }
   public function allowSubscript($bool)      { $this->configData["allowSubscript"] = (bool) $bool; }
   public function allowSuperscript($bool)    { $this->configData["allowSuperscript"] = (bool) $bool; }
   public function allowTables($bool)         { $this->configData["allowTables"] = (bool) $bool; }
   public function allowTextColor($bool)      { $this->configData["allowTextColor"] = (bool) $bool; }
   public function allowUnderline($bool)      { $this->configData["allowUnderline"] = (bool) $bool; }
   public function allowUndo($bool)           { $this->configData["allowUndo"] = (bool) $bool; }
   public function allowUnorderedLists($bool) { $this->configData["allowUnorderedLists"] = (bool) $bool; }
   public function allowViewSource($bool)     { $this->configData["allowCode"] = (bool) $bool; }
   public function allowVisualBlocks($bool)   { $this->configData["allowVisualBlocks"] = (bool) $bool; }
   public function allowVisualChars($bool)    { $this->configData["allowVisualChars"] = (bool) $bool; }
   public function allowWordCount($bool)      { $this->configData["allowWordCount"] = (bool) $bool; }
   
   public function allowColors($bool)
   {
      $this->allowTextColor($bool);
      $this->allowBackgroundColor($bool);
   }
   
   public function allowFormat($bool)
   {
      $this->allowBold($bool);
      $this->allowItalics($bool);
      $this->allowUnderline($bool);
      $this->allowStrikethrough($bool);
      $this->allowSuperscript($bool);
      $this->allowSubscript($bool);
      
      $this->allowUndo($bool);
      $this->allowRedo($bool);
      $this->allowRemoveFormat($bool);
   }
   
   public function allowAlign($bool)
   {
      $this->allowAlignCenter($bool);
      $this->allowTextColor($bool);
      $this->allowAlignLeft($bool);
      $this->allowAlignRight($bool);
   }
   
   public function allowLists($bool)
   {
      $this->allowOrderedLists($bool);
      $this->allowUnorderedLists($bool);
   }
   
   public function allowInjections($bool)
   {
      $this->allowPeopleInjections($bool);
      $this->allowFAQInjections($bool);
      $this->allowFormInjections($bool);
      $this->allowDataInjections($bool);
      $this->allowCourseInjections($bool);
   }
   
   public function setSkin($theme)
   {
      $this->configData["skin"] = $theme;
   }
   
   public function setCSSFile($filePath)
   {
      $this->configData["cssFile"] = $filePath;
   }
   
   public function setDocumentBaseURL($baseURL)
   {
      $this->configData["baseURL"] = $baseURL;
   }

   public function setRemoveScriptHost($bool)
   {
      $this->configData["removeScriptHost"] = (bool) $bool;
   }
   
   public function setRelativeURLs($bool)
   {
      $this->configData["relativeURLs"] = (bool) $bool;
   }

   /**
    * Set a list of classes that can be applied to &lt;a&gt; elements.
    * 
    * @param array $list Nested array of title/value pairs to be used as optional classes for links.
    */
   public function setLinkClassList($list)
   {
      $this->configData["linkClassList"] = $list;
   }
   
   public function setLinkTargetOptions($targetArray)
   {
      $this->configData["allowLinkTargets"] = $targetArray;
   }

   public function setMenu($menuArray)     { $this->configData["menubar"] = $menuArray; }
   public function setMenubarFile($bool)   { $this->configData["menubarFile"]   = (bool) $bool; }
   public function setMenubarEdit($bool)   { $this->configData["menubarEdit"]   = (bool) $bool; }
   public function setMenubarInsert($bool) { $this->configData["menubarInsert"] = (bool) $bool; }
   public function setMenubarView($bool)   { $this->configData["menubarView"]   = (bool) $bool; }
   public function setMenubarFormat($bool) { $this->configData["menubarFormat"] = (bool) $bool; }
   public function setMenubarTable($bool)  { $this->configData["menubarTable"]  = (bool) $bool; }
   public function setMenubarTools($bool)  { $this->configData["menubarTools"]  = (bool) $bool; }

   public function allowMenubar($bool)
   {
      $this->setMenubarEdit($bool);
      $this->setMenubarFile($bool);
      $this->setMenubarInsert($bool);
      $this->setMenubarView($bool);
      $this->setMenubarFormat($bool);
      $this->setMenubarTable($bool);
      $this->setMenubarTools($bool);
      
      if(!$bool)
         $this->setMenu(array());
   }
   
   public function setLinksList($listVar)
   {
      $this->configData["linksList"] = $listVar;
   }
   
   public function fullFeatured($bool)
   {
      $this->allowAlign($bool);
      $this->allowAnchors($bool);
      $this->allowCharmap($bool);
      $this->allowColors($bool);
      $this->allowContextMenu($bool);
      $this->allowFiles($bool);
      $this->allowFormat($bool);
      $this->allowFullScreen($bool);
      $this->allowHeadings($bool);
      $this->allowHorizontalRule($bool);
      $this->allowIFrames($bool);
      $this->allowImages($bool);
      $this->allowIndenting($bool);
      $this->allowInjections($bool);
      $this->allowInsertDateTime($bool);
      $this->allowLinks($bool);
      $this->allowNewDocument($bool);
      $this->allowPaste($bool);
      $this->allowPreview($bool);
      $this->allowPrint($bool);
      $this->allowSearchReplace($bool);
      $this->allowSpellcheck($bool);
      $this->allowTables($bool);
      $this->allowViewSource($bool);
      $this->allowVisualBlocks($bool);
      $this->allowVisualChars($bool);
      $this->allowWordCount($bool);
      
      $this->setMenu(array("file","edit", "insert", "view", "format", "table", "tools"));

      return $this->getJSONConfig();
   }
   
   public function getJSONConfig()
   {
      $indent = "         ";
      
      $mark = "'";
      
      $finalConfigString  = "";
      $finalConfigString .= "      remove_script_host:".($this->configData["removeScriptHost"] ? "true" :"false").",\n";
      $finalConfigString .= $indent."relative_urls:".($this->configData["relativeURLs"] ? "true" :"false").",\n";
      $finalConfigString .= $indent."skin:".$mark.$this->configData["skin"].$mark.",\n";
      $finalConfigString .= $indent."content_css:".$mark.$this->configData["cssFile"].$mark.",\n";
      $finalConfigString .= $indent."document_base_url:".$mark.$this->configData["baseURL"].$mark.",\n";

      if($this->configData["allowSpellCheck"])
         $finalConfigString .= $indent."browser_spellcheck:true,\n";
      
      if($this->configData["allowIFrames"])
         $this->configData["extended_valid_elements"][] = "iframe[*]";

      $finalConfigString .= $indent."extended_valid_elements:'".implode(", ", $this->configData["extended_valid_elements"])."',\n";

      $toolbar1 = "";
      $toolbar2 = "";
      $plugins = "";
      $contextMenu = "";
      
      $hasTables      = false;
      $hasFileManger  = false;
      $hasLists       = false;
      $hasContextMenu = $this->configData["allowContextMenu"];
      
      // Section 1
      $section1 = "";
      if($this->configData["allowUndo"])
      {
         $section1 .= "undo ";
      }
      if($this->configData["allowRedo"])
      {
         $section1 .= "redo ";
      }
      if($this->configData["allowRemoveFormat"])
         $section1 .= "removeformat ";
      if(!empty($section1))
         $toolbar1 .= $section1."| ";

      // Section 2
      $section2 = "";
      if($this->configData["allowBold"])
         $section2 .= "bold ";
      if($this->configData["allowItalics"])
         $section2 .= "italic ";
      if($this->configData["allowUnderline"])
         $section2 .= "underline ";
      if($this->configData["allowStrikethrough"])
         $section2 .= "strikethrough ";
      if(!empty($section2))
      {
         $toolbar1 .= $section2."| ";
      }

      // Section 3
      $section3 = "";
      if($this->configData["allowAlignLeft"])
         $section3 .= "alignleft ";
      if($this->configData["allowAlignCenter"])
         $section3 .= "aligncenter ";
      if($this->configData["allowAlignRight"])
         $section3 .= "alignright ";
      if($this->configData["allowAlignJustify"])
         $section3 .= "alignjustify ";
      if(!empty($section3))
      {
         $toolbar1 .= $section3."| ";
      }

      // Section 4
      $section4 = "";
      if($this->configData["allowOrderedLists"])
      {
         $hasLists = true;
         $section4 .= "bullist ";
      }
      if($this->configData["allowUnorderedLists"])
      {
         $hasLists = true;
         $section4 .= "numlist ";
      }
      if($this->configData["allowIndenting"])
      {
         $section4 .= "outdent indent ";
      }
      if(!empty($section4))
         $toolbar1 .= $section4."| ";
      
      if($hasLists)
         $plugins .= "lists ";
      
      // Section 5
      if($this->configData["allowTables"])
      {
         $hasContextMenu = true;
         $hasTables = true;
         $plugins .= "table ";
         $toolbar1 .= "inserttable ";
         
         $contextMenu .= "inserttable | cell row column deletetable";
      }

      // Section 6
      if($this->configData["allowHeadings"])
      {
         $plugins .= "customHeadings ";
         $toolbar2 .= "customHeadings | ";
      }

      // Section 7
      $section7 = "";
      if($this->configData["allowSubscript"])
         $section7 .= "subscript ";
      if($this->configData["allowSuperscript"])
         $section7 .= "superscript ";
      if($this->configData["allowCharmap"])
      {
         $plugins .= "charmap ";
         $section7 .= "charmap ";
      }
      if(!empty($section7))
         $toolbar2 .= $section7."| ";

      // Check for images
      if($this->configData["allowImages"])
      {
         $hasFileManger = true;
         $plugins .= "image ";
         $toolbar2 .= "image ";
         $contextMenu = "image ".$contextMenu;
      }
      
      // Section 8
      $section8 = "";
      if($this->configData["allowLinks"])
      {
         $section8 .= "link ";
         $plugins .= "link ";
         
         if($this->configData["linksList"])
            $finalConfigString .= $indent."link_list: ".$this->configData["linksList"].",\n";
         
         if(!$this->configData["allowLinkTargets"])
            $finalConfigString .= $indent."target_list:false,\n"; // target_list:!1,\n
         else
            $finalConfigString .= $indent."target_list:".json_encode($this->configData["allowLinkTargets"]).",\n";

         if(!empty($this->configData["linkClassList"]))
         {
            $finalConfigString .= $indent."link_class_list:[";
            foreach($this->configData["linkClassList"] as $class)
            {
               if(isset($class["title"]) && isset($class["value"]))
                  $finalConfigString .= "{title: '".$class["title"]."', value: '".$class["value"]."'},";
            }
            $finalConfigString = substr($finalConfigString, 0, -1);
            $finalConfigString .= "],\n";
         }

         
         if($hasContextMenu)
            $contextMenu = "link ".$contextMenu;
      }
      if($this->configData["allowAnchors"])
      {
         $plugins .= "anchor ";
         $section8 .= "anchor ";
      }
      if(!empty($section8))
         $toolbar2 .= $section8."| ";
      
      // Section 9
      if($this->configData["allowFiles"])
      {
         $hasFileManger = true;
         $toolbar2 .= "responsivefilemanager | ";
      }
      
      $section10 = array();
/*      if($this->configData["allowCourseInjections"])
         $section10[] = "injectCourse";
      if($this->configData["allowDataInjections"])
         $section10[] = "injectData";
      if($this->configData["allowFAQInjections"])
         $section10[] = "injectFAQ";
      if($this->configData["allowFormInjections"])
         $section10[] = "injectForm";
      if($this->configData["allowPeopleInjections"])
         $section10[] = "injectPerson";*/
      
      // Section 10
      if(!empty($section10))
      {
         $this->addJS("/js/injectionData.js");
         $plugins .= implode(" ", $section10)." ";
         $toolbar2 .= implode(" ", $section10)." | ";
      }

      // Section 11
      $section11 = "";
      if($this->configData["allowTextColor"])
      {
         $section11 .= "forecolor ";
      }
      if($this->configData["allowBackgroundColor"])
      {
         $section11 .= "backcolor ";
      }
      if(!empty($section11))
      {
         $plugins .= "colorpicker ";
         $toolbar2 .= $section11."| ";
      }
      
      // Section 12
      if($this->configData["allowFullScreen"])
      {
         $plugins .= "fullscreen ";
         $toolbar2 .= "fullscreen | ";
      }

      // Section 13
      if($this->configData["allowCode"])
      {
         $plugins .= "code ";
         $toolbar2 .= "code | ";
      }

      if($this->configData["allowPreview"])
      {
         $plugins .= "preview ";
         $toolbar2 .= "preview | ";         
      }
      
      if($this->configData["allowPrint"])
      {
         $plugins .= "print ";
         $toolbar2 .= "print | ";
      }
      
      /*** Set additional menubar settings ***/
      if($this->configData["allowPrint"] || $this->configData["allowNewFile"])
         $this->configData["menubarFile"] = true;
         
      /*** Add Toolbars ***/
      if(!empty($toolbar1))
         $finalConfigString .= $indent."toolbar1:'".$toolbar1."',\n";
      if(!empty($toolbar2))
         $finalConfigString .= $indent."toolbar2:'".$toolbar2."',\n";
      
      /*** Add Context Menu ***/
      if($hasContextMenu)
      {
         $plugins .= "contextmenu ";
         $finalConfigString .= $indent."contextmenu:'".$contextMenu."',\n";
      }

      /*** Add Horizontal Rules ***/
      if($this->configData["allowHRs"])
         $plugins .= "hr ";
      
      /*** Add Insert Date/Time ***/
      if($this->configData["allowInsertDateTime"])
         $plugins .= "insertdatetime ";
      
      /*** Add Search/Replace ***/
      if($this->configData["allowSearchReplace"])
         $plugins .= "searchreplace ";
      
      /*** Add Visual Blocks ***/
      if($this->configData["allowVisualBlocks"])
         $plugins .= "visualblocks ";
      
      /*** Add Visual Chars ***/
      if($this->configData["allowVisualChars"])
         $plugins .= "visualchars ";
      
      /*** Add Word Count ***/
      if($this->configData["allowWordCount"])
         $plugins .= "wordcount ";

      /*** Add File Manager ***/
      if($hasFileManger)
      {
         $plugins .= $this->configData["filemanagerPluginName"]." ";
         $finalConfigString .= $indent."external_filemanager_path:'".$this->configData["filemanagerFilePath"]."',\n";
         $finalConfigString .= $indent."filemanager_title:'".$this->configData["filemanagerTitle"]."',\n";
         $finalConfigString .= $indent."external_plugins:{ 'filemanager' : '".$this->configData["filemanagerPath"]."'},\n";
      }
      
      /*** Add Paste Options ***/
      if($this->configData["allowPaste"])
      {
         $plugins .= "paste ";
         $finalConfigString .= $indent."paste_auto_cleanup_on_paste:true,\n";
         $finalConfigString .= $indent."paste_remove_styles:true,\n";
         $finalConfigString .= $indent."paste_remove_styles_if_webkit:true,\n";
         $finalConfigString .= $indent."paste_strip_class_attributes:true,\n";
         
         $validElements = array();
         if($this->configData["allowBold"])
         {
            $validElements[] = "b";
            $validElements[] = "strong";
         }
         if($this->configData["allowItalics"])
         {
            $validElements[] = "i";
            $validElements[] = "em";
         }
         if($this->configData["allowUnorderedLists"])
            $validElements[] = "ul";
         if($this->configData["allowOrderedLists"])
            $validElements[] = "ol";
         if($hasLists)
            $validElements[] = "li";
         if($this->configData["allowUnderline"])
            $validElements[] = "u";
         if($this->configData["allowLinks"])
            $validElements[] = "a";
         if($this->configData["allowHeadings"])
         {
            for($h=1; $h <=6; $h++)
               $validElements[] = "h".$h;
         }
         
         if(!empty($validElements))
            $finalConfigString .= $indent."paste_word_valid_elements:'".implode(", ",$validElements)."',\n";
      }
      
      /*** Add Plugins ***/
      if(!empty($plugins))
         $finalConfigString .= $indent."plugins:['".trim($plugins)."'],\n";
      
      /*** Add Menu Options ***/
      if(empty($this->configData["menubar"]))
      {
         $thisMenu = array();
         if($this->configData["menubarFile"])
            $thisMenu[] = "file";
         if($this->configData["menubarEdit"])
            $thisMenu[] = "edit";
         if($this->configData["menubarInsert"])
            $thisMenu[] = "insert";
         if($this->configData["menubarView"])
            $thisMenu[] = "view";
         if($this->configData["menubarFormat"])
            $thisMenu[] = "format";
         if($this->configData["menubarTable"])
            $thisMenu[] = "table";
         if($this->configData["menubarTools"])
            $thisMenu[] = "tools";
         
         if(!empty($thisMenu))
            $finalConfigString .= $indent."menubar:".$mark.implode(", ",$thisMenu).$mark."\n";
         else
            $finalConfigString .= $indent."menubar:false\n";
      }
      else
         $finalConfigString .= $indent."menubar:'".implode(" ",$this->configData["menubar"])."'\n";

      return $finalConfigString;
   }
}

class WYSIWYG_Tipsheet_Config extends Config
{
   var $configData;
   public function __construct($preloadConfigs=true)
   {
      parent::__construct(null, _MB_DB_HOST_, _MB_DB_NAME_, _MB_DB_USER_, _MB_DB_PASS_);

      $this->configData = array();
      $this->configData["allowAlignCenter"] = false;
      $this->configData["allowAlignJustify"] = false;
      $this->configData["allowAlignLeft"] = false;
      $this->configData["allowAlignRight"] = false;
      $this->configData["allowAnchors"] = false;
      $this->configData["allowBackgroundColor"] = false;
      $this->configData["allowBold"] = false;
      $this->configData["allowCharmap"] = false;
      $this->configData["allowCode"] = false;
      $this->configData["allowContextMenu"] = false;
      $this->configData["allowFiles"] = false;
      $this->configData["allowFullScreen"] = false;
      $this->configData["allowHRs"] = false;
      $this->configData["allowHeadings"] = false;
      $this->configData["allowIFrames"] = false;
      $this->configData["allowImages"] = false;
      $this->configData["allowIndenting"] = false;
      $this->configData["allowCourseInjections"] = false;
      $this->configData["allowDataInjections"] = false;
      $this->configData["allowFAQInjections"] = false;
      $this->configData["allowFormInjections"] = false;
      $this->configData["allowPeopleInjections"] = false;
      $this->configData["allowInsertDateTime"] = false;
      $this->configData["allowItalics"] = false;
      $this->configData["allowLinks"] = false;
      $this->configData["allowLinkTargets"] = false;
      $this->configData["allowNewFile"] = false;
      $this->configData["allowOrderedLists"] = false;
      $this->configData["allowPaste"] = false;
      $this->configData["allowPreview"] = false;
      $this->configData["allowPrint"] = false;
      $this->configData["allowRedo"] = false;
      $this->configData["allowRemoveFormat"] = false;
      $this->configData["allowSearchReplace"] = false;
      $this->configData["allowSpellCheck"] = false;
      $this->configData["allowStrikethrough"] = false;
      $this->configData["allowSubscript"] = false;
      $this->configData["allowSuperscript"] = false;
      $this->configData["allowTables"] = false;
      $this->configData["allowTextColor"] = false;
      $this->configData["allowUnderline"] = false;
      $this->configData["allowUndo"] = false;
      $this->configData["allowUnorderedLists"] = false;
      $this->configData["allowVisualBlocks"] = false;
      $this->configData["allowVisualChars"] = false;
      $this->configData["allowWordCount"] = false;
      
      // Preset options
      $this->configData["skin"] = "lightgray";
      $this->configData["extended_valid_elements"] = array("@[id|name]");
      $this->configData["cssFile"] = "/common/admin/css/wysiwyg_tipsheet.css";
      $this->configData["removeScriptHost"] = true;
      $this->configData["relativeURLs"] = false;
      $this->configData["baseURL"] = "/";
      
      $this->configData["filemanagerPluginName"] = "responsivefilemanager";
      $this->configData["filemanagerTitle"] = "File Manager";
      $this->configData["filemanagerFilePath"] = "/filemanager/";
      $this->configData["filemanagerPath"] = "/filemanager/plugin.min.js";
      
      $this->configData["linksList"] = false;
      $this->configData["linkClassList"] = array(
          array("title"=>"", "value"=>""),
          array("title"=>"New Window", "value"=>"newWindow")
      );
      
      $this->configData["menubar"]       = array();
      $this->configData["menubarFile"]   = false;
      $this->configData["menubarEdit"]   = false;
      $this->configData["menubarInsert"] = false;
      $this->configData["menubarView"]   = false;
      $this->configData["menubarFormat"] = false;
      $this->configData["menubarTable"]  = false;
      $this->configData["menubarTools"]  = false;

      if($preloadConfigs)
      {
         // TODO: Not good that the id is hard-coded here...
         $defaultConfigData = parent::getAllConfigs(2);
         
         foreach($defaultConfigData as $f)
         {
            $config = $f["config"];
            $value  = $f["defaultValue"];
            switch($config)
            {
               case "align":
                  $this->allowAlign($value);
                  break;
               case "aligncenter":
                  $this->allowAlignCenter($value);
                  break;
               case "alignjustify":
                  $this->allowAlignJustify($value);
                  break;
               case "alignleft":
                  $this->allowAlignLeft($value);
                  break;
               case "alignright":
                  $this->allowAlignRight($value);
                  break;
               case "anchors":
                  $this->allowAnchors($value);
                  break;
               case "backgroundColor":
                  $this->allowBackgroundColor($value);
                  break;
               case "bold":
                  $this->allowBold($value);
                  break;
               case "specialChars":
                  $this->allowCharmap($value);
                  break;
               case "colors":
                  $this->allowColors($value);
                  break;
               case "contextMenu":
                  $this->allowContextMenu($value);
                  break;
               case "datetime":
                  $this->allowInsertDateTime($value);
                  break;
               case "files":
                  $this->allowFiles($value);
                  break;
               case "formatting":
                  $this->allowFormat($value);
                  break;
               case "fullScreen":
                  $this->allowFullScreen($value);
                  break;
               case "headings":
                  $this->allowHeadings($value);
                  break;
               case "hr":
                  $this->allowHorizontalRule($value);
                  break;
               case "iframe":
                  $this->allowIFrames($value);
                  break;
               case "images":
                  $this->allowImages($value);
                  break;
               case "indenting":
                  $this->allowIndenting($value);
                  break;
               case "dataInjection":
                  $this->allowInjections($value);
                  break;
               case "faqInjection":
                  $this->allowInjections($value);
                  break;
               case "formInjection":
                  $this->allowInjections($value);
                  break;
               case "peopleInjection":
                  $this->allowInjections($value);
                  break;
               case "italics":
                  $this->allowItalics($value);
                  break;
               case "links":
                  $this->allowLinks($value);
                  break;
               case "linksAndAnchors":
                  $this->allowAnchors($value);
                  $this->allowLinks($value);
                  break;
               case "lists":
                  $this->allowLists($value);
                  break;
               case "newDocument":
                  $this->allowNewDocument($value);
                  break;
               case "orderedLists":
                  $this->allowOrderedLists($value);
                  break;
               case "paste":
                  $this->allowPaste($value);
                  break;
               case "preview":
                  $this->allowPreview($value);
                  break;
               case "print":
                  $this->allowPrint($value);
                  break;
               case "redo":
                  $this->allowRedo($value);
                  break;
               case "removeFormat":
                  $this->allowRemoveFormat($value);
                  break;
               case "searchAndReplace":
                  $this->allowSearchReplace($value);
                  break;
               case "spellcheck":
                  $this->allowSpellcheck($value);
                  break;
               case "specialChars":
                  $this->allowCharmap($value);
                  break;
               case "strikethrough":
                  $this->allowStrikethrough($value);
                  break;
               case "subscript":
                  $this->allowSubscript($value);
                  break;
               case "superscript":
                  $this->allowSuperscript($value);
                  break;
               case "tables":
                  $this->allowTables($value);
                  break;
               case "textColor":
                  $this->allowTextColor($value);
                  break;
               case "underline":
                  $this->allowUnderline($value);
                  break;
               case "undo":
                  $this->allowUndo($value);
                  break;
               case "unorderedLists":
                  $this->allowUnorderedLists($value);
                  break;
               case "viewSource":
                  $this->allowViewSource($value);
                  break;
               case "visualBlocks":
                  $this->allowVisualBlocks($value);
                  break;
               case "visualChars":
                  $this->allowVisualChars($value);
                  break;
               case "wordCount":
                  $this->allowWordCount($value);
                  break;

               case "baseURL":
                  $this->setDocumentBaseURL($value);
                  break;
               case "cssFile":
                  $this->setCSSFile($value);
                  break;
               case "menu":
                  $this->setMenu(explode(" ", $value));
                  break;
               case "skin":
                  $this->setMenu(explode(" ", $value));
                  break;
               default:
                  break;
            }
         }
      }
   }
   
   public function allowAlignCenter($bool)    { $this->configData["allowAlignCenter"] = (bool) $bool; }
   public function allowAlignJustify($bool)   { $this->configData["allowAlignJustify"] = (bool) $bool; }
   public function allowAlignLeft($bool)      { $this->configData["allowAlignLeft"] = (bool) $bool; }
   public function allowAlignRight($bool)     { $this->configData["allowAlignRight"] = (bool) $bool; }
   public function allowAnchors($bool)        { $this->configData["allowAnchors"] = (bool) $bool; }
   public function allowBold($bool)           { $this->configData["allowBold"] = (bool) $bool; }
   public function allowCharmap($bool)        { $this->configData["allowCharmap"] = (bool) $bool; }
   public function allowBackgroundColor($bool){ $this->configData["allowBackgroundColor"] = (bool) $bool; }
   public function allowContextMenu($bool)    { $this->configData["allowContextMenu"] = (bool) $bool; }
   public function allowFiles($bool)          { $this->configData["allowFiles"] = (bool) $bool; }
   public function allowFullScreen($bool)     { $this->configData["allowFullScreen"] = (bool) $bool; }
   public function allowHeadings($bool)       { $this->configData["allowHeadings"] = (bool) $bool; }
   public function allowHorizontalRule($bool) { $this->configData["allowHRs"] = (bool) $bool; }
   public function allowIFrames($bool)        { $this->configData["allowIFrames"] = (bool) $bool; }
   public function allowImages($bool)         { $this->configData["allowImages"] = (bool) $bool; }
   public function allowIndenting($bool)      { $this->configData["allowIndenting"] = (bool) $bool; }
   public function allowPeopleInjections($bool) { $this->configData["allowPeopleInjections"] = (bool) $bool; }
   public function allowFAQInjections($bool)    { $this->configData["allowFAQInjections"] = (bool) $bool; }
   public function allowFormInjections($bool)   { $this->configData["allowFormInjections"] = (bool) $bool; }
   public function allowCourseInjections($bool) { $this->configData["allowCourseInjections"] = (bool) $bool; }
   public function allowDataInjections($bool)   { $this->configData["allowDataInjections"] = (bool) $bool; }
   public function allowInsertDateTime($bool) { $this->configData["allowInsertDateTime"] = (bool) $bool; }
   public function allowItalics($bool)        { $this->configData["allowItalics"] = (bool) $bool; }
   public function allowLinks($bool)          { $this->configData["allowLinks"] = (bool) $bool; }
   public function allowOrderedLists($bool)   { $this->configData["allowOrderedLists"] = (bool) $bool; }
   public function allowNewDocument($bool)    { $this->configData["allowNewFile"] = (bool) $bool; }
   public function allowPaste($bool)          { $this->configData["allowPaste"] = (bool) $bool; }
   public function allowPreview($bool)        { $this->configData["allowPreview"] = (bool) $bool; }
   public function allowPrint($bool)          { $this->configData["allowPrint"] = (bool) $bool; }
   public function allowRedo($bool)           { $this->configData["allowRedo"] = (bool) $bool; }
   public function allowRemoveFormat($bool)   { $this->configData["allowRemoveFormat"] = (bool) $bool; }
   public function allowSearchReplace($bool)  { $this->configData["allowSearchReplace"] = (bool) $bool; }
   public function allowSpellcheck($bool)     { $this->configData["allowSpellCheck"] = (bool) $bool; }
   public function allowStrikethrough($bool)  { $this->configData["allowStrikethrough"] = (bool) $bool; }
   public function allowSubscript($bool)      { $this->configData["allowSubscript"] = (bool) $bool; }
   public function allowSuperscript($bool)    { $this->configData["allowSuperscript"] = (bool) $bool; }
   public function allowTables($bool)         { $this->configData["allowTables"] = (bool) $bool; }
   public function allowTextColor($bool)      { $this->configData["allowTextColor"] = (bool) $bool; }
   public function allowUnderline($bool)      { $this->configData["allowUnderline"] = (bool) $bool; }
   public function allowUndo($bool)           { $this->configData["allowUndo"] = (bool) $bool; }
   public function allowUnorderedLists($bool) { $this->configData["allowUnorderedLists"] = (bool) $bool; }
   public function allowViewSource($bool)     { $this->configData["allowCode"] = (bool) $bool; }
   public function allowVisualBlocks($bool)   { $this->configData["allowVisualBlocks"] = (bool) $bool; }
   public function allowVisualChars($bool)    { $this->configData["allowVisualChars"] = (bool) $bool; }
   public function allowWordCount($bool)      { $this->configData["allowWordCount"] = (bool) $bool; }
   
   public function allowColors($bool)
   {
      $this->allowTextColor($bool);
      $this->allowBackgroundColor($bool);
   }
   
   public function allowFormat($bool)
   {
      $this->allowBold($bool);
      $this->allowItalics($bool);
      $this->allowUnderline($bool);
      $this->allowStrikethrough($bool);
      $this->allowSuperscript($bool);
      $this->allowSubscript($bool);
      
      $this->allowUndo($bool);
      $this->allowRedo($bool);
      $this->allowRemoveFormat($bool);
   }
   
   public function allowAlign($bool)
   {
      $this->allowAlignCenter($bool);
      $this->allowTextColor($bool);
      $this->allowAlignLeft($bool);
      $this->allowAlignRight($bool);
   }
   
   public function allowLists($bool)
   {
      $this->allowOrderedLists($bool);
      $this->allowUnorderedLists($bool);
   }
   
   public function allowInjections($bool)
   {
      $this->allowPeopleInjections($bool);
      $this->allowFAQInjections($bool);
      $this->allowFormInjections($bool);
      $this->allowDataInjections($bool);
      $this->allowCourseInjections($bool);
   }
   
   public function setSkin($theme)
   {
      $this->configData["skin"] = $theme;
   }
   
   public function setCSSFile($filePath)
   {
      $this->configData["cssFile"] = $filePath;
   }
   
   public function setDocumentBaseURL($baseURL)
   {
      $this->configData["baseURL"] = $baseURL;
   }

   public function setRemoveScriptHost($bool)
   {
      $this->configData["removeScriptHost"] = (bool) $bool;
   }
   
   public function setRelativeURLs($bool)
   {
      $this->configData["relativeURLs"] = (bool) $bool;
   }

   /**
    * Set a list of classes that can be applied to &lt;a&gt; elements.
    * 
    * @param array $list Nested array of title/value pairs to be used as optional classes for links.
    */
   public function setLinkClassList($list)
   {
      $this->configData["linkClassList"] = $list;
   }
   
   public function setLinkTargetOptions($targetArray)
   {
      $this->configData["allowLinkTargets"] = $targetArray;
   }

   public function setMenu($menuArray)     { $this->configData["menubar"] = $menuArray; }
   public function setMenubarFile($bool)   { $this->configData["menubarFile"]   = (bool) $bool; }
   public function setMenubarEdit($bool)   { $this->configData["menubarEdit"]   = (bool) $bool; }
   public function setMenubarInsert($bool) { $this->configData["menubarInsert"] = (bool) $bool; }
   public function setMenubarView($bool)   { $this->configData["menubarView"]   = (bool) $bool; }
   public function setMenubarFormat($bool) { $this->configData["menubarFormat"] = (bool) $bool; }
   public function setMenubarTable($bool)  { $this->configData["menubarTable"]  = (bool) $bool; }
   public function setMenubarTools($bool)  { $this->configData["menubarTools"]  = (bool) $bool; }

   public function allowMenubar($bool)
   {
      $this->setMenubarEdit($bool);
      $this->setMenubarFile($bool);
      $this->setMenubarInsert($bool);
      $this->setMenubarView($bool);
      $this->setMenubarFormat($bool);
      $this->setMenubarTable($bool);
      $this->setMenubarTools($bool);
      
      if(!$bool)
         $this->setMenu(array());
   }
   
   public function setLinksList($listVar)
   {
      $this->configData["linksList"] = $listVar;
   }
   
   public function fullFeatured($bool)
   {
      $this->allowAlign($bool);
      $this->allowAnchors($bool);
      $this->allowCharmap($bool);
      $this->allowColors($bool);
      $this->allowContextMenu($bool);
      $this->allowFiles($bool);
      $this->allowFormat($bool);
      $this->allowFullScreen($bool);
      $this->allowHeadings($bool);
      $this->allowHorizontalRule($bool);
      $this->allowIFrames($bool);
      $this->allowImages($bool);
      $this->allowIndenting($bool);
      $this->allowInjections($bool);
      $this->allowInsertDateTime($bool);
      $this->allowLinks($bool);
      $this->allowNewDocument($bool);
      $this->allowPaste($bool);
      $this->allowPreview($bool);
      $this->allowPrint($bool);
      $this->allowSearchReplace($bool);
      $this->allowSpellcheck($bool);
      $this->allowTables($bool);
      $this->allowViewSource($bool);
      $this->allowVisualBlocks($bool);
      $this->allowVisualChars($bool);
      $this->allowWordCount($bool);
      
      $this->setMenu(array("file","edit", "insert", "view", "format", "table", "tools"));

      return $this->getJSONConfig();
   }
   
   public function getJSONConfig()
   {
      $indent = "         ";
      
      $mark = "'";
      
      $finalConfigString  = "";
      $finalConfigString .= "      remove_script_host:".($this->configData["removeScriptHost"] ? "true" :"false").",\n";
      $finalConfigString .= $indent."relative_urls:".($this->configData["relativeURLs"] ? "true" :"false").",\n";
      $finalConfigString .= $indent."skin:".$mark.$this->configData["skin"].$mark.",\n";
      $finalConfigString .= $indent."content_css:".$mark.$this->configData["cssFile"].$mark.",\n";
      $finalConfigString .= $indent."document_base_url:".$mark.$this->configData["baseURL"].$mark.",\n";

      if($this->configData["allowSpellCheck"])
         $finalConfigString .= $indent."browser_spellcheck:true,\n";
      
      if($this->configData["allowIFrames"])
         $this->configData["extended_valid_elements"][] = "iframe[*]";

      $finalConfigString .= $indent."extended_valid_elements:'".implode(", ", $this->configData["extended_valid_elements"])."',\n";

      $toolbar1 = "";
      $toolbar2 = "";
      $plugins = "";
      $contextMenu = "";
      
      $hasTables      = false;
      $hasFileManger  = false;
      $hasLists       = false;
      $hasContextMenu = $this->configData["allowContextMenu"];
      
      // Section 1
      $section1 = "";
      if($this->configData["allowUndo"])
      {
         $section1 .= "undo ";
      }
      if($this->configData["allowRedo"])
      {
         $section1 .= "redo ";
      }
      if($this->configData["allowRemoveFormat"])
         $section1 .= "removeformat ";
      if(!empty($section1))
         $toolbar1 .= $section1."| ";

      // Section 2
      $section2 = "";
      if($this->configData["allowBold"])
         $section2 .= "bold ";
      if($this->configData["allowItalics"])
         $section2 .= "italic ";
      if($this->configData["allowUnderline"])
         $section2 .= "underline ";
      if($this->configData["allowStrikethrough"])
         $section2 .= "strikethrough ";
      if(!empty($section2))
      {
         $toolbar1 .= $section2."| ";
      }

      // Section 3
      $section3 = "";
      if($this->configData["allowAlignLeft"])
         $section3 .= "alignleft ";
      if($this->configData["allowAlignCenter"])
         $section3 .= "aligncenter ";
      if($this->configData["allowAlignRight"])
         $section3 .= "alignright ";
      if($this->configData["allowAlignJustify"])
         $section3 .= "alignjustify ";
      if(!empty($section3))
      {
         $toolbar1 .= $section3."| ";
      }

      // Section 4
      $section4 = "";
      if($this->configData["allowOrderedLists"])
      {
         $hasLists = true;
         $section4 .= "bullist ";
      }
      if($this->configData["allowUnorderedLists"])
      {
         $hasLists = true;
         $section4 .= "numlist ";
      }
      if($this->configData["allowIndenting"])
      {
         $section4 .= "outdent indent ";
      }
      if(!empty($section4))
         $toolbar1 .= $section4."| ";
      
      if($hasLists)
         $plugins .= "lists ";
      
      // Section 5
      if($this->configData["allowTables"])
      {
         $hasContextMenu = true;
         $hasTables = true;
         $plugins .= "table ";
         $toolbar1 .= "inserttable ";
         
         $contextMenu .= "inserttable | cell row column deletetable";
      }

      // Section 6
      if($this->configData["allowHeadings"])
      {
         $plugins .= "customHeadings ";
         $toolbar2 .= "customHeadings | ";
      }

      // Section 7
      $section7 = "";
      if($this->configData["allowSubscript"])
         $section7 .= "subscript ";
      if($this->configData["allowSuperscript"])
         $section7 .= "superscript ";
      if($this->configData["allowCharmap"])
      {
         $plugins .= "charmap ";
         $section7 .= "charmap ";
      }
      if(!empty($section7))
         $toolbar2 .= $section7."| ";

      // Check for images
      if($this->configData["allowImages"])
      {
         $hasFileManger = true;
         $plugins .= "image ";
         $toolbar2 .= "image ";
         $contextMenu = "image ".$contextMenu;
      }
      
      // Section 8
      $section8 = "";
      if($this->configData["allowLinks"])
      {
         $section8 .= "link ";
         $plugins .= "link ";
         
         if($this->configData["linksList"])
            $finalConfigString .= $indent."link_list: ".$this->configData["linksList"].",\n";
         
         if(!$this->configData["allowLinkTargets"])
            $finalConfigString .= $indent."target_list:false,\n"; // target_list:!1,\n
         else
            $finalConfigString .= $indent."target_list:".json_encode($this->configData["allowLinkTargets"]).",\n";

         if(!empty($this->configData["linkClassList"]))
         {
            $finalConfigString .= $indent."link_class_list:[";
            foreach($this->configData["linkClassList"] as $class)
            {
               if(isset($class["title"]) && isset($class["value"]))
                  $finalConfigString .= "{title: '".$class["title"]."', value: '".$class["value"]."'},";
            }
            $finalConfigString = substr($finalConfigString, 0, -1);
            $finalConfigString .= "],\n";
         }

         
         if($hasContextMenu)
            $contextMenu = "link ".$contextMenu;
      }
      if($this->configData["allowAnchors"])
      {
         $plugins .= "anchor ";
         $section8 .= "anchor ";
      }
      if(!empty($section8))
         $toolbar2 .= $section8."| ";
      
      // Section 9
      if($this->configData["allowFiles"])
      {
         $hasFileManger = true;
         $toolbar2 .= "responsivefilemanager | ";
      }
      
      $section10 = array();
/*      if($this->configData["allowCourseInjections"])
         $section10[] = "injectCourse";
      if($this->configData["allowDataInjections"])
         $section10[] = "injectData";
      if($this->configData["allowFAQInjections"])
         $section10[] = "injectFAQ";
      if($this->configData["allowFormInjections"])
         $section10[] = "injectForm";
      if($this->configData["allowPeopleInjections"])
         $section10[] = "injectPerson";*/
      
      // Section 10
      if(!empty($section10))
      {
         $this->addJS("/js/injectionData.js");
         $plugins .= implode(" ", $section10)." ";
         $toolbar2 .= implode(" ", $section10)." | ";
      }

      // Section 11
      $section11 = "";
      if($this->configData["allowTextColor"])
      {
         $section11 .= "forecolor ";
      }
      if($this->configData["allowBackgroundColor"])
      {
         $section11 .= "backcolor ";
      }
      if(!empty($section11))
      {
         $plugins .= "colorpicker ";
         $toolbar2 .= $section11."| ";
      }
      
      // Section 12
      if($this->configData["allowFullScreen"])
      {
         $plugins .= "fullscreen ";
         $toolbar2 .= "fullscreen | ";
      }

      // Section 13
      if($this->configData["allowCode"])
      {
         $plugins .= "code ";
         $toolbar2 .= "code | ";
      }

      if($this->configData["allowPreview"])
      {
         $plugins .= "preview ";
         $toolbar2 .= "preview | ";         
      }
      
      if($this->configData["allowPrint"])
      {
         $plugins .= "print ";
         $toolbar2 .= "print | ";
      }
      
      /*** Set additional menubar settings ***/
      if($this->configData["allowPrint"] || $this->configData["allowNewFile"])
         $this->configData["menubarFile"] = true;
         
      /*** Add Toolbars ***/
      if(!empty($toolbar1))
         $finalConfigString .= $indent."toolbar1:'".$toolbar1."',\n";
      if(!empty($toolbar2))
         $finalConfigString .= $indent."toolbar2:'".$toolbar2."',\n";
      
      /*** Add Context Menu ***/
      if($hasContextMenu)
      {
         $plugins .= "contextmenu ";
         $finalConfigString .= $indent."contextmenu:'".$contextMenu."',\n";
      }

      /*** Add Horizontal Rules ***/
      if($this->configData["allowHRs"])
         $plugins .= "hr ";
      
      /*** Add Insert Date/Time ***/
      if($this->configData["allowInsertDateTime"])
         $plugins .= "insertdatetime ";
      
      /*** Add Search/Replace ***/
      if($this->configData["allowSearchReplace"])
         $plugins .= "searchreplace ";
      
      /*** Add Visual Blocks ***/
      if($this->configData["allowVisualBlocks"])
         $plugins .= "visualblocks ";
      
      /*** Add Visual Chars ***/
      if($this->configData["allowVisualChars"])
         $plugins .= "visualchars ";
      
      /*** Add Word Count ***/
      if($this->configData["allowWordCount"])
         $plugins .= "wordcount ";

      /*** Add File Manager ***/
      if($hasFileManger)
      {
         $plugins .= $this->configData["filemanagerPluginName"]." ";
         $finalConfigString .= $indent."external_filemanager_path:'".$this->configData["filemanagerFilePath"]."',\n";
         $finalConfigString .= $indent."filemanager_title:'".$this->configData["filemanagerTitle"]."',\n";
         $finalConfigString .= $indent."external_plugins:{ 'filemanager' : '".$this->configData["filemanagerPath"]."'},\n";
      }
      
      /*** Add Paste Options ***/
      if($this->configData["allowPaste"])
      {
         $plugins .= "paste ";
         $finalConfigString .= $indent."paste_auto_cleanup_on_paste:true,\n";
         $finalConfigString .= $indent."paste_remove_styles:true,\n";
         $finalConfigString .= $indent."paste_remove_styles_if_webkit:true,\n";
         $finalConfigString .= $indent."paste_strip_class_attributes:true,\n";
         
         $validElements = array();
         if($this->configData["allowBold"])
         {
            $validElements[] = "b";
            $validElements[] = "strong";
         }
         if($this->configData["allowItalics"])
         {
            $validElements[] = "i";
            $validElements[] = "em";
         }
         if($this->configData["allowUnorderedLists"])
            $validElements[] = "ul";
         if($this->configData["allowOrderedLists"])
            $validElements[] = "ol";
         if($hasLists)
            $validElements[] = "li";
         if($this->configData["allowUnderline"])
            $validElements[] = "u";
         if($this->configData["allowLinks"])
            $validElements[] = "a";
         if($this->configData["allowHeadings"])
         {
            for($h=1; $h <=6; $h++)
               $validElements[] = "h".$h;
         }
         
         if(!empty($validElements))
            $finalConfigString .= $indent."paste_word_valid_elements:'".implode(", ",$validElements)."',\n";
      }
      
      /*** Add Plugins ***/
      if(!empty($plugins))
         $finalConfigString .= $indent."plugins:['".trim($plugins)."'],\n";
      
      /*** Add Menu Options ***/
      if(empty($this->configData["menubar"]))
      {
         $thisMenu = array();
         if($this->configData["menubarFile"])
            $thisMenu[] = "file";
         if($this->configData["menubarEdit"])
            $thisMenu[] = "edit";
         if($this->configData["menubarInsert"])
            $thisMenu[] = "insert";
         if($this->configData["menubarView"])
            $thisMenu[] = "view";
         if($this->configData["menubarFormat"])
            $thisMenu[] = "format";
         if($this->configData["menubarTable"])
            $thisMenu[] = "table";
         if($this->configData["menubarTools"])
            $thisMenu[] = "tools";
         
         if(!empty($thisMenu))
            $finalConfigString .= $indent."menubar:".$mark.implode(", ",$thisMenu).$mark."\n";
         else
            $finalConfigString .= $indent."menubar:false\n";
      }
      else
         $finalConfigString .= $indent."menubar:'".implode(" ",$this->configData["menubar"])."'\n";

      return $finalConfigString;
   }
}