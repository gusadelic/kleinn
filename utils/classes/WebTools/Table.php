<?php
namespace Mumby\WebTools;
use PDO;

/**
 * Simple helper class that creates HTML tables using proper semantics. * 
 */
class Table extends Page
{
   static $tableCounter;
   
   public function __construct()
   {
      parent::__construct();
      self::$tableCounter++;
   }
   
   /**
    * This function returns the HTML for a sortable/toggleable data table.
    * 
    * @param array $data Associative array of key/value pairs to be shown
    */
   function getTable($data, $tableID=null, $tableClassName=array(), $isSortable=false, $showFilter=false, $paginate=0, $showToggleFields=false, $visibleColumns=null, $includeTotalsRow=false)
   {
      if(empty($data))
         return false;
      
      if(!empty($tableClassName))
      {
         if(!is_array($tableClassName))
         {
            $tableClassName = array($tableClassName);
         }
      }
      else
         $tableClassName = array();

      $firstKey = array_keys($data);
      $allColumns = array_keys($data[$firstKey[0]]);
      
      foreach($allColumns as $k=>$v)
      {
         if($v == "_tr_className_")
            unset($allColumns[$k]);
      }
      
      if($includeTotalsRow)
         $totals = array();
      
      // Reformat columns so that the toggle checkboxes
      // display in order.
      $orderedColumns = array();
      $totalItems = count($allColumns);
      $totalCols = 4;
      $totalRows = ceil($totalItems/$totalCols);
      
      for($a=0; $a < $totalRows; $a++)
      {
         for($b=0; $b < $totalCols; $b++)
         {
            $thisCol = $a + ($totalRows * $b);

            if(isset($allColumns[$thisCol]))
               $orderedColumns[] = $allColumns[$thisCol];
            else
               $orderedColumns[] = "";
         }
      }
      
      if(empty($visibleColumns))
         $visibleColumns = $allColumns;

      $this->addCSS("/common/css/tables.css");
      $this->addJS("/common/js/tables.js");

      if($isSortable)
         $this->addJS("/common/bower/datatables/media/js/jquery.dataTables.min.js");
      if($paginate)
      {
         $this->addCSS("/common/bower/datatables-bootstrap3-plugin/media/css/datatables-bootstrap3.min.css");
         $this->addJS("/common/bower/datatables-bootstrap3-plugin/media/js/datatables-bootstrap3.min.js");
      }

      $toggleFields = "";
      if($showToggleFields)
      {
         $toggleFields .= "<div class='row toggleTableFields'>\n\n";

         foreach($orderedColumns as $k=>$c)
         {
            $toggleFields .= "<div class='col-md-3'>";
            if(!empty($c))
            {
               $thisColumn = $this->cleanAttr($c);

               $toggleFields .= "<label for='".$thisColumn."_toggle'>";
               $toggleFields .= "<input class='mb_toggleTableColumn";
               if(empty($c))
                  $toggleFields .= "' style='visibility:hidden";
               $toggleFields .= "' ";
               if(in_array($c, $visibleColumns))
                  $toggleFields .= "checked='checked' ";

               $toggleFields .= "id='".$thisColumn."_toggle' data-colName='".$thisColumn."_cell' type='checkbox' /> ";
               $toggleFields .= $c;
               $toggleFields .= "</label>";
            }
            else
            {
               $toggleFields .= "<label for='empty_col_".$k."' style='visibility:hidden'>";
               $toggleFields .= "<input style='visibility:hidden' id='empty_col_".$k."' type='checkbox' /> ";
               $toggleFields .= "</label>";
            }
            $toggleFields .= "</div>\n";
         }
         $toggleFields .= "</div>\n\n";
      }
      
      $classNames = array();
    
      $tableOutput = "<table";

      $tableClassName[] = "table";
      $tableClassName[] = "responsiveTable";

      
      if($isSortable)
      {
         $tableClassName[] = "sortableTable";
         $tableClassName[] = "dataTable";
         $tableClassName[] = "table-hover";
         $tableClassName[] = "table-condensed";
         $tableClassName[] = "table-bordered";
         $tableClassName[] = "table-striped";
      }
      
      if($showFilter)
         $tableClassName[] = " showFilter";

      if(!empty($tableClassName))
         $tableOutput .= " class='".  implode(" ", $tableClassName)."'";

      
      if(!empty($tableID))
         $thisTableID = $tableID;
      else
         $thisTableID = "mb_generated_table_".self::$tableCounter;
      
      $tableOutput .= " id='".$thisTableID."'";
      
      $tableOutput .= ">\n";
      
      $tableOutput .= "   <thead>\n";
      $tableOutput .= "      <tr>";
      foreach($allColumns as $h)
      {
         $classNames[$h] = self::cleanAttr($h);
         $tableOutput .= "         <th class='".$classNames[$h]."_cell";
         if(!in_array($h, $visibleColumns))
            $tableOutput .= " hidden";
         $tableOutput .= "'>".  $h."</th>\n";
      }
      $tableOutput .= "      </tr>\n";
      $tableOutput .= "   </thead>\n";
      $tableOutput .= "   <tbody>\n";
      
      foreach($data as $d)
      {
         $tableOutput .= "      <tr";
         if(isset($d["_tr_className_"]) && !empty($d["_tr_className_"]))
            $tableOutput .= " class='".$d["_tr_className_"]."'";
         $tableOutput .= ">\n";
         foreach($allColumns as $h)
         {
            $thisValue = trim($d[$h]);
            
            $tableOutput .= "         <td class='".$classNames[$h]."_cell";
            if(!in_array($h, $visibleColumns))
               $tableOutput .= " hidden";
            $tableOutput .= "' data-label='".  htmlentities($h)."'>".$thisValue."</td>\n";
            
            if($includeTotalsRow)
            {
               if(!isset($totals[$h]))
               {
                  $totals[$h] = array(
                     "prefix"   => "",
                     "value"    => 0,
                     "suffix"   => "",
                     "decimalPlaces" => 0,
                     "noTotal"  => false
                  );
               }

               $parsedVal = parseNumberValue($thisValue);
               if($parsedVal)
               {
                  if(!$totals[$h]["noTotal"])
                  {
                     $totals[$h]["prefix"] = $parsedVal["prefix"];
                     $totals[$h]["value"] += $parsedVal["value"];
                     $totals[$h]["suffix"] = $parsedVal["suffix"];
                     
                     if($parsedVal["decimalPlaces"] > $totals[$h]["decimalPlaces"])
                        $totals[$h]["decimalPlaces"] = $parsedVal["decimalPlaces"];
                  }
               }
               else
                  $totals[$h]["noTotal"] = true; // If there is a non-numeric
            }
         }
         $tableOutput .= "      </tr>\n";
      }
      $tableOutput .= "   </tbody>\n";
      
      if($includeTotalsRow)
      {
         $tableOutput .= "   <tfoot>\n";
         $tableOutput .= "      <tr>";

         foreach($allColumns as $h)
         {
            $classNames[$h] = self::cleanAttr($h);
            if($totals[$h]["noTotal"])
            {
               $tableOutput .= "         <th class='".$classNames[$h]."_cell totalsCell";
               if(!in_array($h, $visibleColumns))
                  $tableOutput .= " hidden";
               $tableOutput .= "'>&nbsp;</th>\n";
            }
            else
            {
               $tableOutput .= "         <th class='".$classNames[$h]."_cell totalsCell";
               if(!in_array($h, $visibleColumns))
                  $tableOutput .= " hidden";
               $tableOutput .= "'>".$totals[$h]["prefix"].number_format($totals[$h]["value"], $totals[$h]["decimalPlaces"]).$totals[$h]["suffix"]."</th>\n";
            }
         }
         $tableOutput .= "      </tr>\n";
         $tableOutput .= "   </tfoot>\n";
      }
      $tableOutput .= "</table>\n\n";
      
      $jsOutput  = "";      
      if($isSortable)
      {         
         $jsOutput .= "      \$('#".$thisTableID.".sortableTable').dataTable({\n";
         if($paginate)
         {
            $pageLengthOptions = array($paginate, ($paginate*2), ($paginate*4));
            $jsOutput .= "         'paging': true,\n";
            $jsOutput .= "         'pageLength': ".$paginate.",\n";
            $jsOutput .= "         'info': true,\n";
            $jsOutput .= "         'lengthMenu': [[".implode(",",$pageLengthOptions).",-1],[".implode(",",$pageLengthOptions).",'All']],\n";
            $jsOutput .= "         'lengthChange': true,\n";
         }
         else
         {
            $jsOutput .= "         'paging': false,\n";
            $jsOutput .= "         'info': false,\n";
            $jsOutput .= "         'lengthChange': false,\n";
         }
         if($showFilter)
            $jsOutput .= "         'bFilter': true,\n";
         else
            $jsOutput .= "         'bFilter': false,\n";
         $jsOutput .= "         'bSort': true,\n";
         $jsOutput .= "         'bAutoWidth': false,\n";
         $jsOutput .= "         'oLanguage': { 'sSearch': 'Search Table:' },\n";
         $jsOutput .= "         'fnDrawCallback': function( oSettings ) {\n";
         $jsOutput .= "                     $('div#'+oSettings.sTableId+'_length select, div#'+oSettings.sTableId+'_filter input').addClass('form-control input-sm');\n";
         $jsOutput .= "          }\n";
         $jsOutput .= "      });\n";
//         $jsOutput .= "   });\n";
      }
      
      if($showToggleFields)
      {
         $jsOutput .= "   \$('.mb_toggleTableColumn').each( function() {\n";
         $jsOutput .= "      \$(this).click( function(){\n";
         $jsOutput .= "         \$('.'+\$(this).attr('data-colName')).toggleClass('hidden');\n";
         $jsOutput .= "      });\n";
         $jsOutput .= "   });\n";
      }

      if(!empty($jsOutput))
      {
         $jsOutput = "<script type='text/javascript'>".
                     "\$(document).ready( function() {\n".
                        $jsOutput.
                     "});\n".
                     "</script>\n\n";
      }
      
      return $toggleFields.$tableOutput.$jsOutput;
   }
   
   
   /**
    * Display data in a tabular format. It is assumed that the user has
    * santized all data for displaying as valid HTML.
    * 
    * @param array $data Data to be displayed in tabular format.
    * @param array $headers Optional array of which fields should be displayed. If included, it is assumed that there is a corresponding array key in data for each value in headers.
    * @param array $tableClassName Optional class name(s) for the table.
    * @param array $tableID Optional ID attribute for the table.
    * @return mixed Returns either the HTML table string or false if no data was provided.
    */
}