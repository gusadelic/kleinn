<?php
namespace Mumby\WebTools;

class FAQ extends Page
{
   public function __construct($id = null, $dbHost = null, $dbName = null, $dbUser = null, $dbPass = null)
   {
      $this->sourceTable      = "FAQs";
      $this->idCol            = "FAQID";
      
      // Don't let anyone change user IDs.
      $this->readOnlyFields   = array("FAQID");
      
      $this->fieldInfo = array(
         "FAQTitle" => array("type"=>self::STRING, "length"=>255)
      );

      parent::__construct();
   }
   
   /**
    * 
    * @param mixed $contentID Either the URL for the page requested or the id
    * @return type
    */
   public function getContent($faqID)
   {
      $faqSQL = "SELECT q.FAQQuestionID as id, c.FAQCategoryName as category, q.Question as question, q.Answer as answer FROM FAQs f
                 INNER JOIN FAQCategories c on c.FAQID=f.FAQID
                 INNER JOIN FAQQuestions q on q.FAQCategoryID=c.FAQCategoryID
                 WHERE f.FAQID=:FAQID
                 ORDER BY FAQCategoryOrder ASC, QuestionOrder ASC";
      $faqData = array("FAQID"=>$faqID);
      $rows = $this->query($faqSQL, $faqData);
      
      if(empty($rows))
         return false;

      return $rows;
   }
   
   public function getFormattedFAQ($faqID)
   {
      $this->addCSS("/common/css/faq.css");
      $questions = $this->getContent($faqID);

      $currentCategory = "";
      $questionContent = "<ul class='faqQuestions' id='faqTop".$faqID."'>";
      $answerContent   = "";

      foreach($questions as $q)
      {
         if($currentCategory != $q["category"])
         {
            if( !empty($currentCategory) )
            {
               $questionContent .= "</ul>\n";
               $questionContent .= "</li>\n";

               $answerContent .= "</dl>\n";
            }
            $currentCategory = $q["category"];

            $questionContent .= "\n<li>".$currentCategory."\n";
            $questionContent .= "<ul>\n";

            $answerContent .= "\n<h2 class='faqCategoryHeading'>".$currentCategory."</h2>\n";
            $answerContent .= "<dl class='faqAnswerList'>\n";
         }

         $questionContent .= "<li><a href='#q".$q["id"]."'>".$q["question"]."</a></li>\n";
         $answerContent .= "<dt id='q".$q["id"]."'>".($q["question"])."</dt>\n";
         $answerContent .= "<dd>".($q["answer"]);
         $answerContent .= "<a href='#faqTop".$faqID."' class='faqToTopLink'>Return to the top</a>";
         $answerContent .= "</dd>\n";
      }
      $questionContent .= "</ul>\n\n";
      $questionContent .= "</li>\n\n";
      $questionContent .= "</ul>\n\n";
      $answerContent   .= "</dl>\n\n";
      
      return $questionContent.$answerContent;      
   }
   
   public function getFAQInfo($faqID)
   {
      return $this->find(array("FAQID"=>$faqID), "FAQs");
   }

   public function getAllFAQs()
   {
      return $this->find();
   }
   
   public function updateFAQ($faqID, $categories, $questions)
   {
      try
      {
         if(empty($categories))
            throw new Exception("You must have at least one category for your FAQ.");
         else if(empty($questions))
            throw new Exception("You must have at least one question for your FAQ.");         
            
         // Begin the transaction.
         $this->db->beginTransaction();
         $responses = array();
         
         // Wipe out all previous questions
         $wipeOutSQL1 = "DELETE FROM FAQQuestions WHERE FAQCategoryID IN (SELECT FAQCategoryID FROM FAQCategories WHERE FAQID=:FAQID)";
         $responses[] = $this->query($wipeOutSQL1, array("FAQID"=>$faqID));
         
         // Wipe out all previous categories
         $wipeOutSQL2 = "DELETE FROM FAQCategories WHERE FAQID=:FAQID";
         $responses[] = $this->query($wipeOutSQL2, array("FAQID"=>$faqID));         

         // Add new categories (making note of the category ID)
         $newCategoryIDs = array();
         foreach($categories as $order=>$c)
         {
            $thisCategory = array(
               "FAQID"            => $faqID,
               "FAQCategoryName"  => $c["categoryName"],
               "FAQCategoryOrder" => $order
            );
            $newCategoryIDs[$c["fauxID"]] = $this->insert($thisCategory, "FAQCategories");
            $responses[] = $newCategoryIDs[$c["fauxID"]];
         }

         // Add new questions
         foreach($questions as $order=>$q)
         {
            if(!isset($newCategoryIDs[$q["categoryID"]]))
               throw new \Exception("Unable to locate FAQ category ID.");
            
            $thisQuestion = array(
               "FAQCategoryID" => $newCategoryIDs[$q["categoryID"]],
               "Question"      => $q["question"],
               "Answer"        => $q["answer"],
               "QuestionOrder" => $order
            );
            $responses[] = $this->insert($thisQuestion, "FAQQuestions");
         }
         
         
         foreach($responses as $r)
         {
            if ($r === false)
            {
               throw new \Exception("Unable to update FAQ.");
            }
         }
         
         $this->db->commit();
         return $responses;
      }
      catch(Exception $e)
      {
         $this->db->rollBack();
         return false;
      }
   }
   
   public function addFAQ($faqTitle)
   {
      try
      {
         $this->db->beginTransaction();

         $thisFAQID = $this->insert(array("FAQTitle"=>$faqTitle));
         
         if(empty($thisFAQID))
            throw new Exception("Unable to add FAQ to the database.");
         
         $thisFAQCategoryID = $this->insert(array(
            "FAQID"            => $thisFAQID,
            "FAQCategoryName"  => "General Category",
            "FAQCategoryOrder" => 1,
         ), "FAQCategories");
         
         $thisQuestionID = $this->insert(array(
            "FAQCategoryID" => $thisFAQCategoryID,
            "Question"      => "Placeholder Question",
            "Answer"        => "",
            "QuestionOrder" => 1
         ), "FAQQuestions");
         
         $this->db->commit();
         return true;
      }
      catch(Exception $e)
      {
         $this->db->rollBack();
         return false;
      }
   }
   
   public function updateFAQTitle($faqID, $newTitle)
   {
      return $this->update(array("FAQTitle"=>$newTitle), array("FAQID"=>$faqID));
   }
}