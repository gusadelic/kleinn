<?php
namespace Mumby\WebTools;
use \Exception;

class Slideshow extends Page
{
   var $id;
   var $title;
   var $slides;
   
   static $slideshowCounter;
   
   /**
    * @param int $slideshowID id of the desired slideshow
    */   
   public function __construct($slideshowID=null)
   {
      $this->slides = array();

      $this->sourceTable      = "Slideshows";
      $this->idCol            = "SlideshowID";
      
      // Don't let anyone change user IDs.
      $this->readOnlyFields   = array("SlideshowID");
      
      $this->fieldInfo = array(
         "SlideshowTitle"    => array("type"=>self::STRING, "length"=>255),
         "SlideshowMetadata" => array("type"=>self::STRING, "length"=>1024),
      );

      parent::__construct();
      $this->load($slideshowID);
      
      Slideshow::$slideshowCounter++;
   }

   /**
    * Get the slides for the slideshow.
    * 
    * @return mixed An associative array of slide objects or false on failure.
    */
   public function getSlides()
   {
      if(!$this->checkRowInstance())
      {
         throw new Exception("Error 001: Unable to fetch slides for uninstantiated slide show.");
         return false;
      }

      if(empty($this->slides))
      {
         $this->slides = array();
         $slidesSQL  = "SELECT s1.* ";
         $slidesSQL .= "FROM SlideshowSlides s1 ";
         $slidesSQL .= "INNER JOIN Slideshows s ON s1.SlideshowID=s.SlideshowID ";
         $slidesSQL .= "WHERE s.SlideshowID=:SlideshowID ORDER BY ImgOrder";
         $slidesData = array("SlideshowID"=>$this->id["SlideshowID"]);
         
         $rows = $this->query($slidesSQL, $slidesData);
         if(empty($rows))
            return array();
         
         foreach($rows as $r)
            $this->slides[] = new Slide($r);
	   }

      
      return $this->slides;
   }
   
   public function generateSlideShow()
   {
      $allSlides = $this->getSlides();
      
      $slidesContent    = "";
      $navigatorContent = "";
      
      $thisMetadata = json_decode($this->SlideshowMetadata, true);
      $includeNav = (isset($thisMetadata["includeNav"]) && $thisMetadata["includeNav"]);
      $includeArrows = (isset($thisMetadata["includeArrows"]) && $thisMetadata["includeArrows"]);

      $thisSlideshowID = "slideshow".Slideshow::$slideshowCounter;

      if(!empty($allSlides))
      {
         $navigatorContent .= "<ol class='carousel-indicators'>\n";
         $slidesContent .= "<div class='carousel-inner'>\n";
         
         foreach($allSlides as $index => $s)
         {
            $slidesContent    .= "   <div class='item";
            $navigatorContent .= "   <li data-target='#".$thisSlideshowID."' data-slide-to='".$index."'";
            if($index == 0)
            {
               $slidesContent .= " active";
               $navigatorContent .= " class='active'";
            }
            $slideClass = $s->SlideClass;
            if(!empty($slideClass))
               $slidesContent .= " ".$slideClass;
            
            $slidesContent .= "'>\n";
            $navigatorContent .= "></li>\n";
            
            $urlRegEx = "/(?:http|https)(?:\:\/\/)?(?:www.)?(([A-Za-z0-9-]+\.)*[A-Za-z0-9-]+\.[A-Za-z]+)(?:\/.*)?/";
            if ( preg_match($urlRegEx, $s->ImgFilename) ) {
                $imageURL = $s->ImgFilename;
            }
            else {
                $imageURL = "/imgs/front/".$s->ImgFilename;
            }
            
            $slidesContent .= "       <img src='".$imageURL."' alt='".$s->SlideHeading."' class='".$thisSlideshowID."_slide'>\n";
            $slidesContent .= "       <div class='container'>\n";
            $slidesContent .= "         <div class='carousel-caption'>\n";
            $slidesContent .= "           <h1>".$s->SlideHeading."</h1>\n";
            $slidesContent .= "           <p>".$s->SlideCaption."</p>\n";
            
            $thisLink = $s->SlideLink;
            if(!empty($thisLink))
               $slidesContent .= "           <a class='moreLink' href='".$s->SlideLink."'>More details...</a>\n";
            
            $slidesContent .= "         </div>\n";
            $slidesContent .= "       </div>\n";
            $slidesContent .= "     </div>\n";
         }
         $slidesContent .= "     </div>\n";
         $navigatorContent .= "</ol>\n";
         
         if($includeArrows)
         {
            $slidesContent .= "<a class='left carousel-control' href='#".$thisSlideshowID."' data-slide='prev'>\n";
            $slidesContent .= "<span class='glyphicon glyphicon-chevron-left'></span>\n";
            $slidesContent .= "</a>\n";
            $slidesContent .= "<a class='right carousel-control' href='#".$thisSlideshowID."' data-slide='next'>\n";
            $slidesContent .= "<span class='glyphicon glyphicon-chevron-right'></span>\n";
            $slidesContent .= "</a>\n";
         }
      }
      else
      {
         $slidesContent .= "<div class='carousel-inner'>\n";
         $slidesContent .= "   <div class='item active' style='background-color: #ccc;'>\n";
         $slidesContent .= "       <img alt='No slides available for this slideshow.' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAeMAAADlCAMAAABXh2+jAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAZQTFRFzMzMAAAA0zMzZAAAAIVJREFUeNrswTEBAAAAwqD1T20ND6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHg1AQYAsQMAATsEEx0AAAAASUVORK5CYII=' class='".$thisSlideshowID."_slide' />\n";
         $slidesContent .= "       <div class='container'>\n";
         $slidesContent .= "         <div class='carousel-caption'>\n";
         $slidesContent .= "           <h1> Whoops! </h1>\n";
         $slidesContent .= "           <p style='color:#000;text-shadow:none;'>There aren't any slides associated with this slideshow.</p>\n";
         $slidesContent .= "         </div>\n";
         $slidesContent .= "       </div>\n";
         $slidesContent .= "     </div>\n";
      }
      
      $slidesContent .= "</div>\n";

      if($includeNav)
         
      $slidesContent = $navigatorContent . $slidesContent;
      $slidesContent = "<div id='".$thisSlideshowID."' class='carousel slide' data-ride='carousel'>\n" . $slidesContent;
      
      return $slidesContent;
   }
   
   public function getAllSlideshows()
   {
      return $this->find(null, "Slideshows", null, array("SlideshowTitle"));
   }
   
   public function updateSlideshow($showID, $showName, $showMetadata)
   {
      $data = array(
         "SlideshowTitle"=>$showName,
         "SlideshowMetadata"=>$showMetadata
      );
      
      return $this->update($data, array("SlideshowID"=>$showID));
   }
   
   public function updateSlides($showID, $slides)
   {
      try
      {
         // Begin the transaction.
         $this->db->beginTransaction();
         $responses = array();
         
         // mark all previous slides for deletion
         $wipeOutSQL1 = "UPDATE SlideshowSlides SET ImgOrder = '-1' WHERE SlideshowID=:SlideshowID";
         $responses[] = $this->query($wipeOutSQL1, array("SlideshowID"=>$showID));

         // Add new slides
         foreach($slides as $order=>$s)
         {
            $thisSlide = array(
               "SlideHeading" => $s["heading"],
               "SlideCaption" => $s["caption"],
               "SlideLink"    => $s["link"],
               "ImgOrder"     => $order
            );
            
            $responses[] = $this->update($thisSlide, array("SlideshowSlideID"=>$s["slideID"]), "SlideshowSlides");
         }

         // Wipe out all previous questions
         $wipeOutSQL2 = "DELETE FROM SlideshowSlides WHERE ImgOrder = '-1' AND SlideshowID=:SlideshowID";
         $responses[] = $this->query($wipeOutSQL2, array("SlideshowID"=>$showID));
         
         foreach($responses as $r)
         {
            if ($r === false)
            {
               throw new \Exception("Unable to update Slideshow.");
            }
         }
         
         $this->db->commit();
         return $responses;
      }
      catch(Exception $e)
      {
         $this->db->rollBack();
         return false;
      }
   }
   
   public function addSlideshow($showName, $showMetadata)
   {
      $data = array(
         "SlideshowTitle"=>$showName,
         "SlideshowMetadata"=>$showMetadata
      );
      
      return $this->insert($data);
   }
   
   public function addSlide($heading, $caption, $link, $img)
   {
      $existingSlides = $this->getSlides();

      if($existingSlides === false)
         return false;
         
      $thisSlide = array(
         "SlideshowID"  => $this->id["SlideshowID"],
         "ImgFilename"  => $img,
         "SlideHeading" => $heading,
         "SlideCaption" => $caption,
         "SlideLink"    => $link,
         "ImgOrder"     => count($existingSlides)
      );
      
      return $this->insert($thisSlide, "SlideshowSlides");
   }

    public function addTempSlide($heading, $caption, $link, $img)
    {   
        $thisSlide = array(
            "SlideshowSlideID" => 0,
            "SlideshowID"  => $this->id["SlideshowID"],
            "ImgFilename"  => $img,
            "SlideHeading" => $heading,
            "SlideCaption" => $caption,
            "SlideLink"    => $link,
            "ImgOrder"     => 0
        );
        
        $this->slides[] = new Slide($thisSlide);
    }
}

/**
 * We have a separate Slide class because it makes editing individual
 * slides a little more clean.
 */
class Slide extends \Mumby\DB\DBObject
{
   /**
    * Create a slide object.
    * 
    * @param mixed $slide Either the id of a specific slide or a full row from the DB (in associative array form)
    * @return type
    */
   public function __construct($slide)
   {
      $this->sourceTable      = "SlideshowSlides";
      $this->idCol            = "SlideshowSlideID";
      
      // Don't let anyone change user IDs.
      $this->readOnlyFields   = array("SlideshowSlideID");
      
      $this->fieldInfo = array(
         "SlideshowID"  => array("type"=>self::INTEGER),
         "ImgFilename"  => array("type"=>self::STRING, "length"=>255),
         "SlideHeading" => array("type"=>self::STRING, "length"=>255),
         "SlideCaption" => array("type"=>self::STRING, "length"=>2048),
         "SlideLink"    => array("type"=>self::STRING, "length"=>255),
         "ImgOrder"     => array("type"=>self::INTEGER),
         "X1"           => array("type"=>self::INTEGER),
         "X2"           => array("type"=>self::INTEGER),
         "Y1"           => array("type"=>self::INTEGER),
         "Y2"           => array("type"=>self::INTEGER)
      );

      if(is_int($slide))
      {
         parent::__construct($slide, _MB_DB_HOST_, _MB_DB_NAME_, _MB_DB_USER_, _MB_DB_PASS_);
      }
      else if(is_array($slide))
      {
         parent::__construct(null, _MB_DB_HOST_, _MB_DB_NAME_, _MB_DB_USER_, _MB_DB_PASS_);

         $this->id = array("SlideshowSlideID"=>$slide["SlideshowSlideID"]);
         $this->data = $slide;
      }
      else
         return false;
   }
}