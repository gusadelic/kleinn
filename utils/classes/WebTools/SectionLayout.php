<?php
namespace Mumby\WebTools;

class SectionLayout extends Page
{
    public function __construct($id = null, $dbHost = null, $dbName = null, $dbUser = null, $dbPass = null)
    {
        $this->sourceTable      = "SectionGroups";
        $this->idCol            = "SectionGroupID";

        // Don't let anyone change user IDs.
        $this->readOnlyFields   = array("SectionGroupID");

        $this->fieldInfo = array(
            "SectionGroupID" => array("type"=>self::INTEGER),
            "SKU"           => array("type"=>self::STRING, "length" => 32),
            "Name"              => array("type"=>self::STRING, "length" => 32),
            "Description"       => array("type"=>self::STRING, "length" => 1024),
            "Order"             => array("type"=>self::INTEGER)
        );

        parent::__construct();
        
        $this->errorMessages = array();
        
        if(!empty($id) && !$this->checkRowInstance())
        {
             throw new Exception("Unable to create new ".get_called_class().". It appears you passed an invalid id value.");
        }
    }
    
    public function getErrors() {
        $errors = "";
        foreach( $this->errorMessages as $message ) {
            $errors .= $message . "<br />";
        }
        return $errors;
    }
    
    function getSections($SKU)
    {
        $sql  = "SELECT SGP.PageID, SGP.ShowTitle FROM SectionGroup_Pages SGP ";
        $sql .= "INNER JOIN SectionGroups SG ON SG.SKU = :SKU AND SG.SectionGroupID = SGP.SectionGroupID ";
        $sql .= "WHERE SG.SKU = :SKU ORDER BY SG.Order ASC, SGP.Order ASC";
        
        return $this->query($sql, array("SKU"=>$SKU));
    }
    
    function getLayoutSections($LayoutID)
    {
        $sql  = "SELECT SGP.PageID, SGP.ShowTitle FROM SectionGroup_Pages SGP ";
        $sql .= "INNER JOIN SectionGroups SG ON SG.SKU = :SKU AND SG.SectionGroupID = SGP.SectionGroupID ";
        $sql .= "WHERE SG.SKU = :SKU ORDER BY SG.Order ASC, SGP.Order ASC";
        
        return $this->query($sql, array("SKU"=>$SKU));
    }
    
    public function getAllLayouts()
    {
        $sql  = "SELECT L.* FROM `SectionLayouts` L ";
        $layouts = $this->query($sql);
        
        if ( !empty($layouts) ) {
            foreach($layouts as $j=>$l ) {
                
                $sql = "SELECT * FROM SectionLayout_Groups G ";
                $sql .= "INNER JOIN SectionGroups SG ON G.SectionGroupID = SG.SectionGroupID ";
                $sql .= "WHERE G.LayoutID = :LayoutID";
                
                $params = array("LayoutID"=>$l['LayoutID']);
                $groups = $this->query($sql, $params);
               
                if ( !empty($groups) ) {
                    $queries = array();
                    $params = array();
                    foreach($groups as $k=>$g) {
                        $query = "SELECT PageID, ShowTitle FROM `SectionGroup_Pages` ";
                        $query .= "WHERE `SectionGroupID` = :SGID ORDER BY `Order` ASC ";

                        $queries[] = $query;
                        $params[] = array("SGID" => $g['SectionGroupID'] );
                    }
                    $sections = $this->multiQuery($queries, $params);          
                    if ( !empty($sections) ) {
                        foreach($groups as $k=>$g) {
                            $groups[$k]['Sections'] = $sections;  
                        }
                    } 
                }
                $layouts[$j]['Groups'] = $groups;
            }
            
            return $layouts;       
            
        } else { return false; }
    }
    
    public function getLayout($LayoutID, $includeSections = true)
    {
        $sql = "SELECT * FROM SectionLayouts L ";
//        $sql  = "SELECT L.*, SG.SectionGroupID, SG.Name AS GroupName, SG.Description AS GroupDescription FROM `SectionLayouts` L ";
//        $sql .= "INNER JOIN SectionLayout_Groups G ON L.LayoutID = G.LayoutID ";
//        $sql .= "INNER JOIN SectionGroups SG ON G.SectionGroupID = SG.SectionGroupID ";
        $sql .= "WHERE L.LayoutID = :LayoutID ";
        $params = array("LayoutID" => $LayoutID);

        $layout = $this->query($sql, $params);
        
        if ( !empty($layout) ) {
            $layout = $layout[0];
            if ( $includeSections ) {
//                $queries = array();
//                $params = array();
//                foreach ( $layout as $i=>$l ) {
//                    $query = "SELECT PageID, ShowTitle FROM `SectionGroup_Pages` ";
//                    $query .= "WHERE `SectionGroupID` = :SGID ORDER BY `Order` ASC ";
//                    
//                    $queries[] = $query;
//                    $params[] = array("SGID" => $l['SectionGroupID'] );
//                }
//                $sections = $this->multiQuery($queries, $params);          
//                if ( !empty($sections) ) {
//                    foreach ( $sections as $i=>$s ) {
//                        $layout[$i]['Sections'] = $s;
//                    }
//                } 
               
                $sections = $this->getLayoutGroups($LayoutID);
                $layout['Sections'] = $sections;

            }
            
            return $layout;       
        } else { return false; }
    }
    
    public function getLayoutGroups($LayoutID, $includeSections = true)
    {
        $sql  = "SELECT * FROM `SectionLayout_Groups` SL ";
        $sql .= "INNER JOIN SectionGroups SG ON SL.SectionGroupID = SG.SectionGroupID ";
        $sql .= "WHERE LayoutID = :LayoutID ";
        $sql .= "ORDER BY SL.Order ASC ";
        $params = array('LayoutID' => $LayoutID);

        $categories = $this->query($sql, $params);
        if ( !empty($categories) ) {
            if ( $includeSections ) {
                $queries = array();
                $params = array();
                foreach ( $categories as $i=>$sc ) {
                    
                    $query =  "SELECT PageID, ShowTitle FROM `SectionGroup_Pages` ";
                    $query .= "WHERE `SectionGroupID` = :SGID ORDER BY `Order` ASC ";
                    
                    $queries[] = $query;
                    $params[] = array("SGID" => $sc['SectionGroupID'] );
                }
                $sections = $this->multiQuery($queries, $params);          
                if ( !empty($sections) ) {
                    foreach ( $sections as $i=>$s ) {
                        $categories[$i]['Sections'] = $s;
                    }
                } 
            }
            return $categories;       
        } else { return false; }
    }
   
    public function getSectionGroups($includeSections = true, $SKU = null)
    {
        $sql  = "SELECT * FROM `SectionGroups` ";
        $params = array();
        
        if ( $SKU == null ) {
            $sql .= "WHERE SKU IS NULL ";
        }
        else {
            $sql .= "WHERE SKU = :SKU ";
            $params["SKU"] = $SKU;
        }
        
        $sql .= "ORDER BY `Order` ASC ";
        
        $categories = $this->query($sql, $params);
        if ( !empty($categories) ) {
            if ( $includeSections ) {
                $queries = array();
                $params = array();
                foreach ( $categories as $i=>$sc ) {
                    $queries[] = "SELECT PageID, ShowTitle FROM `SectionGroup_Pages` WHERE `SectionGroupID` = :SGID ORDER BY `Order` ASC ";
                    $params[] = array("SGID" => $sc['SectionGroupID'] );
                }
                $sections = $this->multiQuery($queries, $params);          
                if ( !empty($sections) ) {
                    foreach ( $sections as $i=>$s ) {
                        $categories[$i]['Sections'] = $s;
                    }
                } 
            }
            return $categories;       
        } else { return false; }
    }
    
    public function getSectionGroup($SectionGroupID, $includeSections = true)
    {
        $sql  = "SELECT * FORM SectionGroups ";
        $sql .= "WHERE SectionGroupID = :SGID ";
        $params = array( "SCID" => $SectionGroupID );
        $category = $this->query($sql, $params);
        if ( !empty($category) ) {
            if ( $includeSections ) {
                $queries = array();
                $params = array();
                foreach ( $category as $i=>$sc ) {
                    $queries[] = "SELECT PageID, ShowTitle FROM SectionGroup_Pages WHERE SectionGroupID = :SGID ORDER BY Order ASC ";
                    $params[] = array("SGID" => $sc['SectionGroupID'] );
                }
                $sections = $this->multiQuery($queries, $params);          
                if ( !empty($sections) ) {
                    foreach ( $sections as $i=>$s ) {
                        $categories[$i]['Sections'] = $s;
                    }
                } 
            }
            return $category;       
        } else { return false; }
    }
    
    public function updateSectionGroups($data, $SKU = null)
    {
        if ( !is_array($data) ) return false;
        $results = array();

        $params = array();
        if ( $SKU == null ) {
            $sql = "DELETE FROM SectionGroups WHERE SKU IS NULL";
        }
        else {
            $sql = "DELETE FROM SectionGroups WHERE SKU = :SKU";
            $params = array("SKU"=>$SKU);
        }
        $this->query($sql, $params);
        foreach( $data as $d ) {
            $d["SKU"] = $SKU;
            if ( !isset($d['Name']) || empty($d['Name']) ) {
                continue;
            }
            else {
                $results[] = $this->updateSectionGroup($d);
            }
        }
        return $results;
    }
    
    public function updateSectionGroup($data)
    {
        if ( isset($data['Sections']) ) {
            $sections = $data['Sections'];
            unset($data['Sections']);
            unset($data['LayoutID']);
            $groupID = $this->insertOrUpdate($data);
            $result = $this->updateGroupPages($groupID, $sections);
            if ( $result === false ) 
                    return false;
        }
        else {
            $groupID = $this->insertOrUpdate($data);
        }
        return $groupID;
    }
    
    public function updateGroupPages($groupID, $data)
    {
        if ( !is_array($data) ) return false;

        $sql = array();
        $params = array();
        
        $sql[] = "DELETE FROM `SectionGroup_Pages` WHERE SectionGroupID = :id ";
        $params[] = array( "id" => $groupID);
        
        foreach( $data as $k=>$d ) {
            $sql[] = "INSERT INTO SectionGroup_Pages (`SectionGroupID`, `PageID`, `Order`, `ShowTitle`) VALUES (:id, :page, :order, :show ) ";
            $params[] = array(
                "id" => $groupID,
                "page" => $d[0],
                "order" => $k, 
                "show"  => $d[1]
            );
        }
        
        return $this->multiQuery($sql, $params);
    }
    
    public function saveLayout($layoutData, $groupData)
    {
        $layout = $this->insertOrUpdate($layoutData, "SectionLayouts");
        if ( is_array($layout) )
            $layoutID = $layout['LayoutID'];
        else
            $layoutID = $layout;
        $groupIDs = $this->updateLayoutGroups($groupData, $layoutID);
        $sqlQueries = array();
        $queryParams = array();
        
        //$sqlQueries[] = "DELETE FROM SectionLayout_Groups WHERE LayoutID = :ID ";
        //$queryParams[] = array("ID"=>$layoutID);
        
        foreach ( $groupIDs as $g ) {
            $sqlQueries[] = "INSERT INTO SectionLayout_Groups (LayoutID, SectionGroupID) VALUES (:LayoutID, :GroupID)";
            $queryParams[] = array( "LayoutID" => $layoutID, "GroupID" => $g );
        }

        return $this->multiQuery($sqlQueries, $queryParams);
        
    }
    
    
    public function updateLayoutGroups($data, $LayoutID = null)
    {
        if ( !is_array($data) ) return false;
        $results = array();
        
        if ( is_numeric($LayoutID) ) {
            $sql = "DELETE FROM SectionLayout_Groups WHERE LayoutID = :ID ";
            $params = array("ID"=>$LayoutID);
            $this->query($sql, $params);
        }
        
        foreach( $data as $d ) {
            $d["LayoutID"] = $LayoutID;
            if ( !isset($d['Name']) || empty($d['Name']) ) {
                continue;
            }
            else {
                $results[] = $this->updateSectionGroup($d);
            }
        }
        return $results;
    }
    
    function deleteLayout($LayoutID)
    {
        $sql = "DELETE FROM SectionLayouts WHERE LayoutID = :id ";
        $params = array( "id" => $LayoutID );
        return $this->query($sql, $params);
    }
    
}