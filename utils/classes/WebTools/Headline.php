<?php
namespace Mumby\WebTools;
use \Exception;

class Headline extends Page
{
   var $id;
   var $title;
  
   /**
    * @param int $headlineID id of the desired headline
    */   
   public function __construct($headlineID=null)
   {
      $this->sourceTable      = "Headlines";
      $this->idCol            = "HeadlineID";
      
      // Don't let anyone change user IDs.
      $this->readOnlyFields   = array("HeadlineID");
      
      $this->fieldInfo = array(
         "CreatedOn"    => array("type"=>self::TIMESTAMP),
         "HeadlineTitle"    => array("type"=>self::STRING, "length"=>512),
         "HeadlineDesc"    => array("type"=>self::STRING, "length"=>4096),
         "HeadlineLink"    => array("type"=>self::STRING, "length"=>512),
         "StartDate"    => array("type"=>self::TIMESTAMP),
         "EndDate"    => array("type"=>self::TIMESTAMP),
         "IsApproved" => array("type"=>self::BOOLEAN),
         "IsImportant" => array("type"=>self::BOOLEAN)
      );

      parent::__construct();
      $this->load($headlineID);
   }
   
   public function getAllHeadlines()
   {
      return $this->find();
   }

   public function getHeadline($headlineID)
   {
      return $this->find(array("HeadlineID"=>$headlineID));
   }

   public function addHeadline($userID, $name, $desc, $link, $start, $end, $approved, $important )
   {

      $data = array(
         "CreatorUserID" => $userID,
         "HeadlineTitle" => $name,
         "HeadlineDesc" => $desc,
         "HeadlineLink" => $link,
         "StartDate" => $start,
         "EndDate" => $end,
         "IsApproved" => $approved,
         "IsImportant" => $important
      );
      
      return $this->insert($data, "Headlines");
   }
   
   
   public function updateHeadline($headlineID, $name, $desc, $link, $start, $end, $approved, $important )
   {
      $data = array(
         "HeadlineID" => $headlineID,
         "HeadlineTitle" => $name,
         "HeadlineDesc" => $desc,
         "HeadlineLink" => $link,
         "StartDate" => $start,
         "EndDate" => $end,
         "IsApproved" => $approved,
         "IsImportant" => $important
      );
      
      return $this->insertOrUpdate($data, "Headlines");
   }
   
   public function deleteHeadline($HeadlineID)
   {
       
       return $this->delete($HeadlineID, "Headlines");
       
   }
   
   public function getCurrentHeadlines($approvedOnly=true)
   {
      $sql = "SELECT * FROM Headlines WHERE (EndDate > NOW() OR IsImportant=1) ";
      if($approvedOnly)
         $sql .= "AND IsApproved=1";
      
      //$data = array("today"=>date(\Mumby\DB\DBObject::MYSQL_DATETIME_FORMAT));
      
      return $this->query($sql);
   }
   
   // Add Headline CRUD functions here.
}