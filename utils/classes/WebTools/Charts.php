<?php
namespace Mumby\WebTools;
use PDO;

/**
 * Simple helper class that creates HTML tables using proper semantics. * 
 */
class Charts extends Page
{
   static $chartCounter;
   
   public function __construct()
   {
      parent::__construct();
      self::$chartCounter++;
   }

   /**
    * 
    * @param type $data
    * @param string $chartID
    * @param type $containerClass
    * @param type $showLegend
    * @param type $legendPosition
    * @param type $legendContainerID
    * @return string
    */
   function getPieChart($data, $chartID=null, $containerClass="", $showLegend=true, $legendPosition=false, $legendContainerID=false)
   {
      if(!is_array($data) || empty($data))
         return "";

      $this->addCSS("/common/bower/morrisjs/morris.css");
      
      $this->addJS("/common/bower/flot/excanvas.min.js");
      $this->addJS("/common/bower/flot/jquery.flot.js");
      $this->addJS("/common/bower/flot/jquery.flot.pie.js");
      $this->addJS("/common/bower/flot/jquery.flot.resize.js");
      $this->addJS("/common/bower/flot/jquery.flot.time.js");
      $this->addJS("/common/bower/flot.tooltip/js/jquery.flot.tooltip.min.js");
      $this->addCSS("/common/css/charts.css");

      if(empty($chartID))
         $chartID = "mb_generated_chart_".self::$chartCounter;
      
      $container  = "\n\n";
      $container .= "<div class='flot-chart'>";
      $container .= "<div id='".$chartID."' class='flot-chart-content";
      if(!empty($containerClass))
         $container .= " ".$containerClass."'";
     
      $container .= "'></div>";
      if($showLegend && $legendContainerID)
         $container .= "<div class='legend' id='".$legendContainerID."'></div>";
      
      $jsContent  = "";
      $jsContent .= "<script type='text/javascript'>\n";
      $jsContent .= "\$(function() {\n";
      $jsContent .= "   var data = ".  json_encode($data).";\n\n";
      $jsContent .= "   var plotObj".self::$chartCounter." = \$.plot(\$('#".$chartID."'), data, {\n";
      $jsContent .= "      series: {\n";
      $jsContent .= "         pie: {\n";
      $jsContent .= "            show: true\n";
      $jsContent .= "         }\n";
      $jsContent .= "      },\n";
      $jsContent .= "      grid: {\n";
      $jsContent .= "         hoverable: true\n";
      $jsContent .= "      },\n";
      
      if($showLegend)
      {
         $jsContent .= "      legend: {\n";
         $jsContent .= "         show: true,\n";
         if($legendContainerID)
            $jsContent .= "         container: \$('#".$legendContainerID."'),\n";
         if($legendPosition)
            $jsContent .= "         position: '".$legendPosition."',\n";
         else
            $jsContent .= "         position: 'sw',\n";
         $jsContent .= "      },\n";
      }
      else
      {
         $jsContent .= "      legend: {\n";
         $jsContent .= "         show: false\n";
         $jsContent .= "      },\n";
      }
      
      $jsContent .= "      tooltip: true,\n";
      $jsContent .= "      tooltipOpts: {\n";
      $jsContent .= "         content: '%p.0%, %s',\n";
      $jsContent .= "         shifts: {\n";
      $jsContent .= "            x: 20,\n";
      $jsContent .= "            y: 0\n";
      $jsContent .= "         },\n";
      $jsContent .= "         defaultTheme: false\n";
      $jsContent .= "      }\n";
      $jsContent .= "   });\n";
      $jsContent .= "});\n";
      $jsContent .= "</script>\n\n";
      
      return $container.$jsContent;
   }
}