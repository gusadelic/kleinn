<?php
namespace Mumby;

/**
 * This class is inspired by an amalgamation of code from all over. 
 * 
 * Inspiration from Sho Kuwamoto.
 * http://kuwamoto.org/2007/12/17/improved-pluralizing-in-php-actionscript-and-ror/
 * LICENSE: MIT License, http://opensource.org/licenses/MIT
 * 
 * Inspiration from ICanBoogie/Inflector
 * LICENSE: New BSD License, http://opensource.org/licenses/MIT
 * https://github.com/ICanBoogie/Inflector
 * 
 * Inspiration from the Doctrin Inflector class.
 * https://github.com/doctrine/inflector/blob/master/lib/Doctrine/Common/Inflector/Inflector.php
 * LICENSE: MIT License, http://opensource.org/licenses/MIT
 * 
 * LICENSE: Apache License, Version 2.0
 * 
 * @category StringUtility
 * @package Mumby
 * @copyright (c) 2015, Justin Spargur
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @version 1.0
 * @since Class available since Release 1.0
 * @author spargurj
 */
class Inflect
{
   const UPPER_CAMEL = 1;
   const LOWER_CAMEL = 2;
   const HYPHENATED  = 3;
   const UNDERSCORED = 4;
   
   static $plural = array(
      '/(oxen|chassis|pus)$/i'   => '$1',
      '/^(m|l)ice$$/i'           => '$1ice',
      '/(quiz)$/i'               => "$1zes",
      '/^(ox)$/i'                => "$1en",
      '/([m|l])ouse$/i'          => "$1ice",
      '/(matr|vert|ind)ix|ex$/i' => "$1ices",
      '/(x|ch|ss|sh)$/i'         => "$1es",
      '/([^aeiouy]|qu)y$/i'      => "$1ies",
      '/(hive|serf|wharf)$/i'    => "$1s",
      '/(?:([^f])fe|([lr])f)$/i' => "$1$2ves",
      '/(shea|lea|loa|thie)f$/i' => "$1ves",
      '/sis$/i'                  => "ses",
      '/([ti])a$/i'              => '$1a',
      '/([ti])um$/i'             => "$1a",
      '/(tomat|buffal|potat|ech|her|vet)o$/i' => "$1oes",
      '/(bu)s$/i'                => "$1ses",
      '/(alias)$/i'              => "$1es",
      '/(octop|alumn|emerit|loc|modul)us$/i' => "$1i",
      '/(ax|test)is$/i'          => "$1es",
      '/(us)$/i'                 => "$1es",
      '/s$/i'                    => "s",
      '/$/'                      => "s"
   );
    
   static $singular = array(
      '/(oxen|chassis|pus)$/i'    => '$1',
      '/(quiz)zes$/i'             => "$1",
      '/(matr)ices$/i'            => "$1ix",
      '/(vert|ind)ices$/i'        => "$1ex",
      '/^(ox)en$/i'               => "$1",
      '/(alias)es$/i'             => "$1",
      '/(octop|vir|alumn|emerit|loc|modul)i$/i' => "$1us",
      '/(cris|ax|test)es$/i'      => "$1is",
      '/(shoe)s$/i'               => "$1",
      '/(o)es$/i'                 => "$1",
      '/(bus)es$/i'               => "$1",
      '/([m|l])ice$/i'            => "$1ouse",
      '/(x|ch|ss|sh)es$/i'        => "$1",
      '/(m)ovies$/i'              => "$1ovie",
      '/(s)eries$/i'              => "$1eries",
      '/([^aeiouy]|qu)ies$/i'     => "$1y",
      '/([lr])ves$/i'             => "$1f",
      '/(tive)s$/i'               => "$1",
      '/(hive|serf|wharf)s$/i'    => "$1",
      '/(li|wi|kni)ves$/i'        => "$1fe",
      '/(shea|loa|lea|thie)ves$/i'=> "$1f",
      '/(^analy)ses$/i'           => "$1sis",
      '/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i'  => "$1$2sis",        
      '/([ti])a$/i'               => "$1um",
      '/(n)ews$/i'                => "$1ews",
      '/(h|bl)ouses$/i'           => "$1ouse",
      '/(corpse)s$/i'             => "$1",
      '/(us)es$/i'                => "$1",
      '/s$/i'                     => ""
   );
    
   static $irregular = array(
      'atlas' => 'atlases',
      'beef' => 'beefs',
      'brother' => 'brothers',
      'cafe' => 'cafes',
      'child' => 'children',
      'cookie' => 'cookies',
      'corpus' => 'corpuses',
      'cow' => 'cows',
      'criterion' => 'criteria',
      'foot'   => 'feet',
      'ganglion' => 'ganglions',
      'genie' => 'genies',
      'genus' => 'genera',
      'goose'  => 'geese',
      'graffito' => 'graffiti',
      'hoof' => 'hoofs',
      'human' => 'humans',
      'loaf' => 'loaves',
      'man' => 'men',
      'money' => 'monies',
      'mongoose' => 'mongooses',
      'move' => 'moves',
      'mythos' => 'mythoi',
      'niche' => 'niches',
      'numen' => 'numina',
      'occiput' => 'occiputs',
      'octopus' => 'octopuses',
      'opus' => 'opuses',
      'ox' => 'oxen',
      'penis' => 'penises',
      'person' => 'people',
      'sex' => 'sexes',
      'soliloquy' => 'soliloquies',
      'testis' => 'testes',
      'tooth'  => 'teeth',
      'trilby' => 'trilbys',
      'turf' => 'turfs'
   );
    
   static $uncountable = array( 
       ' ',
       'advice',
       'art',
       'coal',
       'baggage',
       'butter',
       'clothing',
       'cotton',
       'currency',
       'deer',
       'equipment',
       'experience',
       'fish',
       'flour',
       'food',
       'furniture',
       'gas',
       'homework',
       'impatience',
       'information',
       'jeans',
       'knowledge',
       'leather',
       'love',
       'luggage',
       'money',
       'oil',
       'patience',
       'police',
       'polish',
       'progress',
       'research',
       'rice',
       'series',
       'sheep',
       'silk',
       'soap',
       'species',
       'sugar',
       'talent',
       'toothpaste',
       'travel',
       'vinegar',
       'weather',
       'wood',
       'wool',
       'work'
   );
   
   static $articles = array( 
      'a',
      'an',
      'and',
      'as',
      'at',
      'but',
      'by',
      'for',
      'in',
      'it',
      'nor',
      'of',
      'on',
      'or',
      'the',
      'to',
      'up',
   );

   public static function pluralize($string ) 
   {
      // save some time in the case that singular and plural are the same
      if ( in_array( strtolower( $string ), self::$uncountable ) )
         return $string;
            
    
      // check for irregular singular forms
      foreach ( self::$irregular as $pattern => $result )
      {
         $pattern = '/' . $pattern . '$/i';
            
         if ( preg_match( $pattern, $string ) )
            return preg_replace( $pattern, $result, $string);
      }
        
      // check for matches using regular expressions
      foreach ( self::$plural as $pattern => $result )
      {
         if ( preg_match( $pattern, $string ) )
            return preg_replace( $pattern, $result, $string );
      }
        
      return $string;
   }
    
   public static function singularize( $string )
   {
      // save some time in the case that singular and plural are the same
      if ( in_array( strtolower( $string ), self::$uncountable ) )
         return $string;

      // check for irregular plural forms
      foreach ( self::$irregular as $result => $pattern )
      {
         $pattern = '/' . $pattern . '$/i';
         
         if ( preg_match( $pattern, $string ) )
            return preg_replace( $pattern, $result, $string);
      }
        
      // check for matches using regular expressions
      foreach ( self::$singular as $pattern => $result )
      {
         if ( preg_match( $pattern, $string ) )
            return preg_replace( $pattern, $result, $string );
      }
        
      return $string;
   }

   public static function camelize($string, $ucFirst=false)
   {
      $string = self::lowerCamel($string);
      
      if($ucFirst)
         return ucfirst($string);
      else
         return $string;
   }
   
   public static function lowerCamel($string)
   {
      return str_replace(" ", "", ucwords( $this->humanize($string)));
   }
   
   public static function hyphenate($string)
   {
      return strtr($string, "_ ", "--");
   }
   
   public static function humanize($string, $mode = self::UNDERSCORED)
   {
      if($mode == self::UNDERSCORED || $mode == self::HYPHENATED)
         return strtr($string, "_-", "  ");
      else
      {
         return preg_replace( '/([A-Z0-9][^A-Z0-9]*)/', "$1 $2", $string);
      }
   }
   
   public static function underscore($string)
   {
      return strtr($string, "- ", "__");
   }
   
   /*
    * Capitalizes all of appropriate words for a title.
    * Tries to use APA format, though not guaranteed!
    * 
    * 
    */
   public static function titlize($string, $alreadyHumanized=false)
   {
      if(!$alreadyHumanized)
         $string = self::humanize($string);
      
      $parts = explode(" ", $string);
      $compiled = "";
      $lastWord = count($parts)-1;
      foreach($parts as $k=>$p)
      {
         if($k != 0 && $k != $lastWord && in_array($p, self::$articles))
            $compiled .= $p." ";
         else
            $compiled .= ucwords($p)." ";
      }
      
      return trim($compiled);
   }
   
   public static function ordinal($number, $justSuffix=false)
   {
      $int = ((int) $number);
      switch($int%10)
      {
         case 1:
            $suffix = "st";
            break;
         case 2:
            $suffix = "nd";
            break;
         case 3:
            $suffix = "rd";
            break;
         default:
            $suffix = "th";
            break;
      }
      if($justSuffix)
         return $suffix;
      else
         return $int.$suffix;
   }
}