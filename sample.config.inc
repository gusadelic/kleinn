<?php

// Specify credentials for connecting to the CMS database
define("_MB_DB_HOST_", "localhost");
define("_MB_DB_NAME_", 'dbname');
define("_MB_DB_USER_", "dbuser");
define("_MB_DB_PASS_", 'dbpass');

// Specify credentials for connecting to the central datastore
define("_MB_CENTRAL_DATASTORE_HOST_", "localhost");
define("_MB_CENTRAL_DATASTORE_NAME_", 'dbname');
define("_MB_CENTRAL_DATASTORE_USER_", "dbuser");
define("_MB_CENTRAL_DATASTORE_PASS_", 'dbpass');

?>